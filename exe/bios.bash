#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

sudo dmidecode -s bios-version
sudo dmidecode -s bios-release-date
sudo dmidecode --type bios

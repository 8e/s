#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/z-snaprune.bash "${@}" || true
sleep 1
"${MYREALDIR}"/z-recoursivesnap.bash "${@}"
"${MYREALDIR}"/z-status.bash "${@}"

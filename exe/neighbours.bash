#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

arp -n
#nmap -sP 192.168.1.0/24
ip addr show | awk '! /127.0.0..*/ && /inet/ {print $2}' |  xargs -l nmap -sP    

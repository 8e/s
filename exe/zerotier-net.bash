#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

sudo /usr/sbin/zerotier-cli listnetworks | awk "/${ZT_IFACE}/ {print \$3}"

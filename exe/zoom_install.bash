#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))

pushd "${MYREALDIR}"
	./apt-urls.bash latest zoom "https://zoom.us/support/download?os=linux" || ./apt-urls.bash zoom https://zoom.us/client/latest/zoom_amd64.deb || ./apt-urls.bash zoom https://cdn.zoom.us/prod/5.14.2.2046/zoom_amd64.deb
	./apt-lists.bash install ../lib/zoom.list
popd

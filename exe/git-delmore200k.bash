#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

git filter-branch --tree-filter 'find . -type f -size +200k -print0 | xargs -0 echo git rm  --ignore-unmatch' HEAD

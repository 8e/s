#!/usr/bin/env bash

set -e

readonly MYREALDIR=$(dirname $(realpath ${0}))
readonly FHOSTS=/etc/dotdee/etc/hosts.d/75-zerotier
readonly ZT_NETWORK_ID=$("${MYREALDIR}"/zerotier-net.bash)

if [[ $ZT_API_KEY == '' ]]; then
	read -e -p "Enter API key: " ZT_API_KEY
fi

"${MYREALDIR}"/apt-ensure-cmd.bash jq jq
"${MYREALDIR}"/apt-ensure-cmd.bash curl curl
curl --silent -H "Authorization: Bearer $ZT_API_KEY" "https://my.zerotier.com/api/network/${ZT_NETWORK_ID}/member" | jq -r '.[] | "\(.config.ipAssignments[]) \(.name)"' 

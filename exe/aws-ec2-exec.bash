#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly INID=$1
shift
source "${MYREALDIR}"/../defaults.rc.bash

readonly SH_COMMAND_ID=$(aws ssm send-command \
    --instance-ids "${INID}" \
    --document-name "AWS-RunShellScript" \
    --comment "Demo run shell script on Linux Instances" \
    --parameters commands="${*}" \
    --output text --query "Command.CommandId")

sh -c "aws ssm list-command-invocations --command-id '$SH_COMMAND_ID' --details \
	--query 'CommandInvocations[].CommandPlugins[].{Status:Status,Output:Output}'"

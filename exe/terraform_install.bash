#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly BASH_SCRIPT="${*}"

[ -e "${BASH_SCRIPT}" ] || cp -v "${ME}" "${BASH_SCRIPT}"

[ -x "${BASH_SCRIPT}" ] || chmod -v +x "${BASH_SCRIPT}" &

timeout '0.3s' cat >> "${BASH_SCRIPT}" || true
"${EDITOR}" "${BASH_SCRIPT}"

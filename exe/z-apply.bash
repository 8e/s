#!/usr/bin/env bash

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

pushd "${MYREALDIR}"/../
	./apply.bash zfs
	apt update
	aptitude install -y $(cat lib/zfs.list)
	./exe/zswap-enable.bash
	./exe/sysctl-params_set.bash
	./exe/zfs-restart.bash
	./exe/zinit-pool.bash rpool
popd

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

zfs list -Ht snapshot -o name | "${MYREALDIR}"/xargs.bash -l zfs holds -Hr

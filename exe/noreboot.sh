#!/bin/sh

set -xe

[ -e /usr/sbin/reboot ] && mv /usr/sbin/reboot /var/backups
[ -e /usr/sbin/poweroff ] && mv /usr/sbin/poweroff /var/backups

exit 0

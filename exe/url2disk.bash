#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure-cmd.bash wget wget

read -e -p "Enter URL: " URL

wget -O $(realpath "${*}") "${URL}"

#/usr/bin/bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

function sortusplint() {
	awk -vOFS='\n' 'NF' | tr " " "\n" | sort -u
}

if (( ${#} == 0 )); then
	sortusplint
else for f in ${*}; do
	tmpfile=$(mktemp /tmp/$(basename $0).XXXXXX)
	chmod g+rw "${tmpfile}"
	sortusplint < "${f}" > "${tmpfile}"
	declare -a HardLinks=$("${MYREALDIR}"/hardlinks-to.bash "${f}")
	mv ${tmpfile} ${f}
	chmod g+rw "${f}"
	if (( "${#HardLinks[@]}" > 1 )); then
		for hl in "${HardLinks[@]}"; do
			cp -lvf "${f}" "${hl}"
		done
	fi
	pushd $(dirname ${f})
		git add $(basename ${f}) "${HardLinks[@]}"
	popd
done; fi

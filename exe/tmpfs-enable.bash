#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/focopy.bash /usr/share/systemd/tmp.mount /etc/systemd/system/
systemctl enable tmp.mount

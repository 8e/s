#!/usr/bin/env bash

set -ex
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
export SCREENN=$(basename "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/ensure-path.bash /var/cache/apt/archives/partial
#"${MYREALDIR}"/grperms.bash /var/cache/apt/archives/partial

echo htop tsp screen cpulimit | "${MYREALDIR}"/apt-ensure.bash htop task-spooler screen cpulimit

"${MYREALDIR}"/tsp.bash cpus

pushd /etc/systemd/system
	SCREENN=htop WINDOWN=htop sudo -E "${MYREALDIR}"/screen.bash /usr/bin/htop
popd
#pushd "${MYREALDIR}"/../..
#	SCREENN="${USER}" WINDOWN="${USER}" "${MYREALDIR}"/screen.bash /usr/bin/beebeep
#popd
pushd "${MYREALDIR}"
	tsp "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/disable.bash
	tsp systemctl --user start dbus.service
	tsp "${MYREALDIR}"/ensure-path.bash "/var/run/sshd"
	tsp "${MYREALDIR}"/ensure-path.bash "/var/cache/apt/archives/partial"
	tsp sudo chmod o+rwxt /run /var/run
	#tsp "${MYREALDIR}"/caches.bash
	tsp sudo zfs mount -av
	tsp sudo mount -av
	tsp sudo systemctl daemon-reload
	#tsp sudo systemctl restart bluetooth.service syncthing.service
	#tsp "${MYREALDIR}"/kfg_display.bash
	#tsp "${MYREALDIR}"/keyboard.bash
	#"${MYREALDIR}"/tsp.bash "${MYREALDIR}"/audio-restart.bash
	"${MYREALDIR}"/tsp.bash sudo ifup enp1s0

	tsp "${MYREALDIR}"/dns_set.bash
	tsp "${MYREALDIR}"/connect.bash
	tsp "${MYREALDIR}"/dns_set.bash

	#tsp "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/joplin_install.sh
	#tsp sudo systemctl restart syncthing.service

	#tsp "${MYREALDIR}"/proxy.bash
	#tsp sudo /etc/init.d/logmein-hamachi start
	#tsp sudo /etc/init.d/zerotier-one start
	#tsp "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/qbt.bash
	
	tsp ./unison efis
	#tsp ./joplin
	#./skype.bash
	#./brave.bash

	#./apt-safe-upgrade.bash
	./zbm_update.bash
popd

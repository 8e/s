#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure-cmd.bash python3 python3

if (( $# > 0 )); then
	python3 -c 'import sys, yaml, json; print(yaml.dump(json.loads(sys.stdin.read())))' < "${*}"
else
	python3 -c 'import sys, yaml, json; print(yaml.dump(json.loads(sys.stdin.read())))'
fi

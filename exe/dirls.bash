#!/usr/bin/env bash

set -e

[ ${DEBUG} ] && set -x
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR="$(dirname ${ME})"

source "${MYREALDIR}"/../defaults.rc.bash

function filt() {
	ifne grep -v '/\.$\|/\.\.$\|^$\|\*' | ifne sed 's|/\./|/|g; s|^//|/|g'
}

function list_deeper() {
	while read f; do
		printf '%q\n' "${f}"/* || true
		printf '%q\n' "${f}"/.* || true
	done | filt
}

(which ifne | "${MYREALDIR}"/apt-ensure.bash moreutils) 2>&1 >/dev/null

if [[ "${*}" == '-' ]]; then
	list_deeper
elif [[ "${*}" == '' ]]; then
	pwd | list_deeper
else
	echo "${*}" | list_deeper
fi

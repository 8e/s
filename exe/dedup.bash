#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd >/dev/null

readonly MAXJOBS='75%'
readonly LOGDIR=$("${MYREALDIR}"/ensure-path.bash /tmp/log/$(basename ${ME}).${USER})

if (( ${#} == 0 )); then
	echo arguemnts needed
	exit 1
fi

"${MYREALDIR}"/apt-ensure-cmd.bash jdupes jdupes

declare -a OtherParamsAndDirs

while [ ${#} -gt 0 ]; do

        case ${1} in

                '-')
                        readonly NOPIPEIN=false
                ;;

                '-d'|'--delete'|'-B'|'--dedupe'|'-s'|'--symlinks')
			OtherParamsAndDirs+=("--hardlinks")
                        readonly ACTION="${1}"
                ;;

		# jdupes -1fprLX size+:1G /root /home/ /var/tmp/
		## https://awesomeopensource.com/project/jbruchon/jdupes

                '-L'|'--linkhard')
                        readonly ACTION="${1}"
                ;;

                *)
                        OtherParamsAndDirs+=("${1}")
                ;;

        esac

	shift # past param

done

[[ "${ACTION}" == '' ]] && readonly ACTION='-L'

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	if $NOPIPEIN; then
		"${MYREALDIR}"/ltd.bash jdupes -fpr $ACTION "${OtherParamsAndDirs[@]}"
	else
				which ifne parallel | "${MYREALDIR}"/apt-ensure.bash moreutils parallel
				ifne "${MYREALDIR}"/ltd.bash parallel --no-run-if-empty --quote --jobs "${MAXJOBS}" --joblog "${LOGDIR}/parallel.log" --tmux "${MYREALDIR}"/ltd.bash jdupes -fpr $ACTION "${OtherParamsAndDirs[@]}"
	fi
else
	jdupes -fpr $ACTION "${OtherParamsAndDirs[@]}"
fi

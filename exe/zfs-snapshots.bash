#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly AMOUNT=${1:-1}

function get_datasets_with_good_snapshots() {
	zfs list -H -o name,canmount,mountpoint,type | awk '$4=="filesystem" && ($2=="on" || $2=="noauto") && $3!="none" {print $1" "$3}' | sort -k2 | awk '{print $1}'
}

get_datasets_with_good_snapshots | xargs -I{} bash -c "zfs list -H -t snapshot -s creation -o creation,name {} | tail -n ${AMOUNT}"

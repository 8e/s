#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

# https://github.com/mikenizo808/How-To-Crypto-Mine-for-Monero-XMR-on-Ubuntu-20.04-with-XMRig
readonly GIDI=$("${MYREALDIR}"/git-installrepo.bash https://github.com/xmrig/xmrig.git)
readonly BUILDI=$("${MYREALDIR}"/ensure-path.bash "${GIDI}/build")
readonly INDI=$("${MYREALDIR}"/ensure-path.bash "/opt/xmrig")
readonly LBL=$(hostname)
readonly POOL="gulf.moneroocean.stream:10128"
[ -z "${W}" ] || promptvar "Enter wallet:" W
readonly CMD=("${INDI}"/xmrig -o "${POOL}" -u "${W}" -p "${LBL}")


function install() {
	"${MYREALDIR}"/apt-ensure.bash build-essential cmake libuv1-dev libssl-dev libhwloc-dev
	pushd "${BUILDI}"

		#cmake (don't forget the .. which says the bits are up one directory)
		cmake ..

		#make (choose how many procs to compile on)
		make -j4

		"${MYREALDIR}"/focopy.bash ./xmrig "${INDI}"/

	popd
}

function start() {
	"${CMD[@]}"
}

$1

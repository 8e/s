#!/usr/bin/env bash

set -xe

readonly MYDIR="$(dirname $(realpath ${0}))"

"${MYDIR}"/apt-ensure.bash sysfsutils

systool -vm zfs

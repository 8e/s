#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash
readonly SOCPATH="${*}"

"${MYREALDIR}"/apt-ensure-cmd.bash socat socat
sudo socat UNIX-LISTEN:"${SOCPATH}",fork,reuseaddr - &

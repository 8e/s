#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

#[[ $1 == 'up' ]] && [[ $3 != '' ]] && sudo ip addr add dev $1 $3
sudo ip link set $1 $2
[[ $2 == 'down' ]] && sudo ip link delete $1
[[ $2 == 'up' ]] && [[ $3 != '' ]] && sudo ip addr add dev $1 $3

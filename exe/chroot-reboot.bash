#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

for mp in $(mount | awk '/mnt/ {print $3}' | sort -u); do
	umount -lfv $mp &
	"${MYREALDIR}"/zfs-unmount.bash
	zfs umount -a
	zpool export -a
	[[ $(echo -e 'yes\nno' | "${MYREALDIR}"/dialog.bash "Reboot?") == 'yes' ]] && reboot
done

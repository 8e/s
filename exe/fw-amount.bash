#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/fw-status.bash | awk -F '[][]' '/[0-9]+/ {print $2}' | wc -l

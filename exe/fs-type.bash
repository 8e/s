#!/usr/bin/env bash
[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
	source ../defaults.rc.bash
popd >/dev/null

readonly P="${@:-/}"

if [ -b "${P}" ]; then
	blkid "${P}" | awk -vFS='"' '{print $6}'
else
	df -H --output=fstype "${P}" | tail -n +2
fi

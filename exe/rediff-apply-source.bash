#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR="$(dirname ${ME})"
readonly SRC=${1}
readonly TARGET=${2}
declare -a Cmds=()
readonly P_SEMAPHORENAME=$(basename "${ME}").${USER}
readonly CORES=$("${MYREALDIR}"/cpus.sh)
readonly JOBS=$(( ( ( ${CORES} + 1 ) ** 3 ) / ( 81 / 13 ) + 1 ))

while read what2apply; do
	Cmds+=("${what2apply}")
done    

"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" create "${JOBS}" || true

diff -r "${SRC}" "${TARGET}" 2>&1 | ifne grep "^Only in ${SRC}" | ifne sed "s|^Only in ${SRC}||g; s|: |/|g" | while read relpath; do
	relparent=$(dirname "${relpath}")
	for cmd in "${Cmds[@]}"; do
		echo "${cmd}" "${SRC}${relpath}" "${TARGET}${relparent}" | "${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}"
	done
done

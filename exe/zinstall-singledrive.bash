#!/usr/bin/env bash

[ ${IGNORE} ] || set -e
if $DEBUG && [ ${DEBUG} ]; then
	set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
        source ../z-ensure-poolosid.rc.bash

	# https://github.com/zbm-dev/zfsbootmenu/wiki/debian-Buster-installation-with-ESP-on-the-zpool-disk

	if [[ $TARGET_DISK == '' ]]; then
		readonly TARGET_DISK="/dev/disk/by-id/$(ls -1 /dev/disk/by-id |grep -v part | ${MYREALDIR}/dialog.bash 'Select disk for single disk setup:')"
	fi
	if [[ "${TARGET_DISK}" == '' ]]; then
		exit 1
	fi
	readonly EFI_PART=${TARGET_DISK}-part1
	readonly TMPDR=/tmp/$(basename "${0}").$$

	readonly MNT=/mnt/z
	"${MYREALDIR}"/apt-ensure-cmd.bash bc bc
	readonly SWAPSIZE=$(echo "$(${MYREALDIR}/ramsizekb.bash)  * 1.25" | bc)K
	readonly EFI_BOOT_SIZE="512" #M

	if [[ "${OSID}" == '' ]]; then
		exit 2
	fi

	function target_update() {
		# https://www.2daygeek.com/how-to-reload-partition-table-in-linux-without-system-reboot/
		partprobe $TARGET_DISK || true
		(udevadm info --query=property --path=$device | grep -q "^ID_BUS=usb") && removable_devs_expression+="|$(basename $device)" || true
		kpartx $TARGET_DISK || true
		udevadm trigger || true
		blockdev --rereadpt -v $TARGET_DISK || true
		udevadm settle || true
		hdparm -z $TARGET_DISK || true
		sleep 3.33
	}

	./apt-update.bash

	./apt-lists.bash -y install ../lib/installer.list

	# Deleting everything from target:
	zpool labelclear -f $TARGET_DISK &
	sgdisk --zap-all $TARGET_DISK
	zpool labelclear -f $TARGET_DISK &

	##gdisk hex codes:
	##EF02 BIOS boot partitions
	##EF00 EFI system
	##BE00 Solaris boot
	##BF00 Solaris root
	##BF01 Solaris /usr & Mac Z
	##8200 Linux swap
	##8300 Linux file system

	# For both legacy and EFI booting:
	##create bootloader partition
	sgdisk -n1:1M:+"$EFI_BOOT_SIZE"M -t1:EF00 $TARGET_DISK

	##Create root pool partition
	sgdisk     -n2:0:0        -t2:BF00 $TARGET_DISK

	target_update

	fdisk -l $TARGET_DISK

	while ! ls -1 ${TARGET_DISK}-part2; do
		target_update
	done

	mkfs.fat -F32 ${TARGET_DISK}-part1 #unneeded ?
        mkdosfs -F 32 -s 1 -n EFI "${EFI_PART}"
        sleep 2.33

	./z-rootcreate.bash ${TARGET_DISK}-part2

	# Export, then re-import with a temporary mountpoint
	zfs unmount -a
	zpool export $POOL
	zpool import -md /dev/disk/by-id -N -R $MNT $POOL
	zfs load-key $POOL
	zfs mount $POOL/ROOT/$OSID
	zfs mount -a
	#./zfs-mount.bash

	#Mount EFI partition
	pushd $MNT
		"${MYREALDIR}"/ensure-path.bash boot/efi
		mount $EFI_PART boot/efi
	popd


	./z-debootstrap.bash

popd

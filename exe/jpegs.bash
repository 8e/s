#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

curl "${*}" | sed 's/<a/\n<a/g' | awk -F'"' '/>.*\.jpg.*<\/a>/ {print $2}' | "${MYREALDIR}"/xargs.bash -l wget -c

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

tsp -K
killall $* aptitude &
killall $* apt-get &
killall $* apt &
killall $* dpkg &
"${MYREALDIR}"/dpkg-ps.bash | awk "!/$(basename ${ME})/ {print \$2}" | "${MYREALDIR}"/xargs.bash -l kill $*

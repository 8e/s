#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

#dpkg-reconfigure tzdata

#read -e -p "Enter timezone: " tz

which pip || "${MYDIR}"/apt.bash -y install python3-pip

pip install -U tzupdate || sudo tzselect

sudo tzupdate

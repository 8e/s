#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

readonly INSTALLDIR=/opt/site24x7
readonly INSTALLERN="site24x7_installer.bash"

if [[ $SITE24X7_KEY == '' ]]; then
        read -e -p "Enter Site24x7 key: " SITE24X7_KEY
fi

if [[ "${SITE24X7_KEY}" == '' ]]; then
	echo "NO SITE24X7_KEY provided"
	echo "skipping site24x7 agent setup"
	exit 0
fi

"${MYREALDIR}"/ensure-path.bash "${INSTALLDIR}"

pushd "${INSTALLDIR}"
	wget -c "https://staticdownloads.site24x7.com/server/Site24x7InstallScript.sh" -O "${INSTALLERN}"
	chmod +x "${INSTALLERN}"
	bash -xc "./${INSTALLERN} -i -key=${SITE24X7_KEY}"
popd

/etc/init.d/site24x7monagent restart

/etc/init.d/site24x7monagent status

#!/usr/bin/env bash

set -xe 

declare -a Opts=(
	zfs_arc_max=8053063680
	zfs_arc_min=2147483648
	zfs_kern.maxvnodes=250000
	zfs_prefetch_disable=0
	zfs_resilver_delay=0
	zfs_resilver_min_time_ms=6000
	zfs_top_maxinflight=128
	zfs_txg.timeout=5
	zfs_vdev_scheduler=none
	zfs_write_limit_override=2G
)


insmod /lib/modules/*/kernel/zfs/zfs.ko ${Opts[@]}


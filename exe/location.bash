#!/usr/bin/env bash

set -e

DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

(which geoiplookup || sudo "${MYREALDIR}"/apt-ensure.bash geoip-bin) >/dev/null

geoiplookup "${*}"

#!/usr/bin/env bash

set -e

DEBUG=${DEBUG:-true}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

shopt -s extglob

recoursive=false

declare -a Cmd

i=0

for f in "$@"; do

	case "$f" in

		-*([fiIv])r*([fiIv]) | -*([fiIv])R*([fiIv]))
			tmp="${f//[rR]/}"
			if [ -n "$tmp" ]; then
				#echo "\$tmp == $tmp"
				Cmd[$i]="$tmp"
				i=$((i+1))
			fi
			rocoursive=true
		;;

		--recursive)
			recoursive=true
		;;

		*)
			if (! $recursive) && [ -d "$f" ]; then
				echo "skipping directory: $f"
				continue
			else
				Cmd[$i]="$f"
				i=$((i+1))
			fi
		;;

	esac

done

"${MYREALDIR}"/apt-ensure-cmd.bash trash-cli trash
if ! trash "${Cmd[@]}" 2>/dev/null && ! mv -bf "${Cmd[@]}" /var/backups/ 2>/dev/null; then
	"${MYREALDIR}"/focopy.bash "${Cmd[@]}" /var/backups/
	rm -rf "${Cmd[@]}"
fi

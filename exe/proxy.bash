#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly NAME=$(basename ${ME})

readonly ACT=${1:-'restart'}

# -D Tells SSH that we want a SOCKS tunnel on the specified port number (you can choose a number between 1025-65536)
# -f Forks the process to the background
# -C Compresses the data before sending it
# -q Uses quiet mode
# -N Tells SSH that no command will be sent once the tunnel is up
#readonly PORT=$(shuf -i 1025-65536 -n 1)
readonly PROXYNAME=${2:-defproxy}
sudo chmod 1777 /tmp/log
readonly LOGDIR=$("${MYREALDIR}"/ensure-path.bash /tmp/log/$USER)
sudo chmod 1777 /tmp/log/$USER
readonly LOG="${LOGDIR}/${NAME}.log"
if [ -e ~/.ssh/config ] && grep ControlPath ~/.ssh/config; then
	readonly CONF=~/.ssh/config
else
	readonly CONF=/etc/ssh/ssh_config
fi
readonly PORT=$(sudo awk -vRS='Host' "/${PROXYNAME}/" "${CONF}" | awk '/DynamicForward/ {print $2}')
readonly CNTRL_PATH=$(sudo awk -vRS='\n\n' '/defproxy/' "${CONF}" | awk '/ControlPath/ {print $NF}')
readonly SOCKETSDIR=$("${MYREALDIR}"/ensure-path.bash $(dirname "${CNTRL_PATH}"))
readonly KEEPALIVE_CHECK_INTERVAL=1 # Seconds
[[ "${SOCKETSDIR}" == '' ]] && exit 1
[[ "${LOGDIR}" == '' ]] && exit 2

if [[ "$UID" == '0' ]]; then
	echo "${NAME}" should not be run as root
	exit 3
fi

"${MYREALDIR}"/ensure-path.bash "${LOGDIR}" "${SOCKETSDIR}"

function start() {
	#ssh -D ${PORT} -f -C -N ${PROXYNAME} &>> ${LOG}
	date &>> "${LOG}"
	#sudo "${MYREALDIR}"/apt-ensure.bash network-manager sshuttle | tee -a "${LOG}"
	#sudo "${MYREALDIR}"/apt-ensure.bash network-manager autossh | tee -a "${LOG}"
	#nm-online 2>&1  | tee -a "${LOG}"
	"${MYREALDIR}"/concheck.bash || exit 4
	ssh -g -v -f -C -nNT "${PROXYNAME}" &>> "${LOG}" # based on ssh config
	if ! status; then
		ssh -R 0.0.0.0:${PORT}:localhost:${PORT} -v -f -C -N ${PROXYNAME} &>> ${LOG} || exit 5
		if ! status; then
			autossh -M0 -f -vgnNT ${PROXYNAME} &>> ${LOG} || exit 6
			if ! status; then
				autossh -f -vgnNT ${PROXYNAME} &>> ${LOG} || exit 7
				if ! status; then
					(sshuttle -l "${PORT}" ${PROXYNAME} 2>&1 | tee -a "${LOG}") || exit 8
				fi
			fi
		fi
	fi
	#autossh -M 0 -o "ServerAliveInterval 10" -o "ServerAliveCountMax 2" -L 9999:localhost:19999 server@example.com
	#autossh -f -nNT -i ~/keypair.pem -R 2000:localhost:22 username@myoutsidebox.com
	#sshuttle --listen=$PORT --dns --remote $PHOST 0.0.0.0/0
	status || exit 6
}

function starteternal() {
	"${MYREALDIR}"/continuous.bash "${MYREALDIR}"/proxy-healthcheck.bash "${KEEPALIVE_CHECK_INTERVAL}" "${ME}" &> "${LOG}"
}

function stop() {
	date &>> ${LOG}
	killall autossh || true
	killall sshuttle || true
	ssh -O exit ${PROXYNAME} || true
	status
}

function status() {
	date &>> ${LOG}
	ssh -O check ${PROXYNAME} || true
	ps -ef | grep ssh | grep -v grep
	tail ${LOG}
	sudo netstat -tulpn | grep ssh | grep -v 22
	healthcheck
}

function restart() {
	if healthcheck; then
		status
	else
		stop || true
		start
	fi
}

function healthcheck() {
	"${MYREALDIR}"/concheck.bash -5x localhost:${PORT}
}

function install() {
	CPUQUOTA=75 NICE=0 IOCLASS=realtime IONICE=0 "${MYREALDIR}"/continuous.bash install "${ME}" starteternal
}

(echo autossh sshuttle ssh httping netstat | "${MYREALDIR}"/apt-ensure.bash autossh sshuttle openssh-client httping net-tools) | tee -a "${LOG}"

${ACT}

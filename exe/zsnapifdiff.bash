#!/usr/bin/env bash

set -e

readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly DATASET=${1:-rpool}
readonly SNAPNAME=${2}
readonly SNAPNAMELENRESTRICT=80
readonly LOGD="/tmp/log"
readonly ERRLOGF="${LOGD}/$(basename $0).error.log"
readonly DIFF_F="${LOGD}/$(basename $0).diff.log"
readonly DIFFTIMEOUT="0.25s"
readonly DIFFTIMEOUT_LABEL="long_diff_more_than_${DIFFTIMEOUT}"
readonly FIRSTSNAPSHOT_LABEL='1st_snapshot'
readonly LASTSNAPSHOT=$(zfs list -t snapshot -o name -S creation -H ${DATASET} | head -1)

readonly NICE=${3:-"19"}
readonly IOCLASS=${4:-"3"}

ionice -c${IOCLASS} -p $$
renice -n "${NICE}" $$

"${MYREALDIR}"/ensure-path.bash "${LOGD}"

date +%F_%T

function getdiff() {
	if [[ ${LASTSNAPSHOT} == '' ]]; then
		echo "${FIRSTSNAPSHOT_LABEL}"
		exit
	fi
	nice -19 ionice -c3 -n7 timeout ${DIFFTIMEOUT} zfs diff ${LASTSNAPSHOT} 2> "${ERRLOGF}" 1> "${DIFF_F}" || if [ $? -eq 124 ]; then
		cat "${DIFF_F}"
		echo "${DIFFTIMEOUT_LABEL}"
		exit
	elif [[ $(cat "${ERRLOGF}") =~ ^"Cannot diff an unmounted snapshot".* ]]; then
		zfs mount ${DATASET} || exit
		getdiff
		zfs umount ${DATASET}
		exit
        fi
	cat "${DIFF_F}"
}

readonly FILTEREDDIFF=$(getdiff | tr -cd '[:alnum:]._-' | cut -c 1-${SNAPNAMELENRESTRICT} )

echo -e "${DATASET}\t${FILTEREDDIFF}\t${LASTSNAPSHOT}\n" | tee -a "${DIFF_F}"

if [[ "${FILTEREDDIFF}" == '' ]]; then
	echo "there is no diff" | tee -a "${DIFF_F}"
else
	echo "there is a diff"
	readonly SHORTNAME=$(echo ${DATASET}@${SNAPNAME:-${FILTEREDDIFF}} | cut -c "1-${SNAPNAMELENRESTRICT}")
	if ! zfs snapshot ${SHORTNAME}; then
		readonly LONGERNAME=$(echo ${DATASET}@$(date +%F)_${SNAPNAME:-${FILTEREDDIFF}} | cut -c "1-${SNAPNAMELENRESTRICT}")
		if ! zfs snapshot ${LONGERNAME}; then
			readonly LONGESTNAME=$(echo ${DATASET}@$(date +%F)_${SNAPNAME:-${FILTEREDDIFF}} | cut -c "1-${SNAPNAMELENRESTRICT}")
			zfs snapshot ${LONGESTNAME}
		fi
	fi
fi

date +%F_%T | tee -a "${DIFF_F}"

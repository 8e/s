#!/usr/bin/env bash

set -e
if [ ${DEBUG} ]; then 
	        set -x
fi      
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

#function quit() {
#	git commit || true
#	git pull
#	"${MYREALDIR}"/apt-dedup.bash "$(pwd)"
#	git push
#}
#
#trap quit EXIT

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	tsp "${MYREALDIR}"/apt-dedup.bash &
	echo git vim tsp | "${MYREALDIR}"/apt-ensure.bash git vim-nox task-spooler
fi
#git status
#tsp "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/bfg.bash &>/dev/null
sudo chown "${DEFUSER}":"${DEFUSER}" -c ~/.git* 2>/dev/null || sudo chown "${DEFUSER}" ~/.git*

pushd $("${MYREALDIR}"/git-root.bash)

	git add -A

	if ! git commit ${*}; then
		"${MYREALDIR}"/git-rehash-staged.bash
		"${MYREALDIR}"/git-config.bash
		git commit || true
	fi

	tsp "${MYREALIR}"/ltd.bash git prune &
	if [[ "${FORCEPUSH}" != 'true' ]]; then
		git pull || git pull --allow-unrelated-histories || "${MYREALDIR}"/git-config.bash
		tsp "${MYREALDIR}"/apt-dedup.bash "$(pwd)" & &>/dev/null
		git push
	else
		git push --force
	fi


	#git reflog expire --expire=now --all && git gc --prune=now --aggressive

#	if [[ "${FORCEPUSH}" != 'true' ]]; then
#		FORCEPUSH=true "${ME}"
#	fi

	git log -n 3

popd

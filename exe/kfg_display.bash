#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

if [[ ${UID} == 0 ]]; then
	su - "${DEFUSER}" -c "DISPLAY=:0.0 /bin/systemsettings5 kcm_kscreen" &
else
	DISPLAY=:0.0 /bin/systemsettings5 kcm_kscreen &
fi

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

if ! [ -e "${*}" ]; then
	exit 1
elif [[ "${*}" =~ \/root\/.* ]]; then
	echo root
elif [[ "${*}" =~ \/home\/.* ]]; then
	echo "${*}" | cut -d/ -f3
else
	sudo stat -c '%U' "${*}"
fi      

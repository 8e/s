#!/usr/bin/env bash

set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd >/dev/null

readonly REPO="${1}"
shift
readonly BN=$(basename "${REPO}")
readonly NAME="${BN%.*}"
readonly LOGF=/tmp/$(basename "${0}").log
exec 3>&1 4>&2 1> "${LOGF}" 2>&1

if [[ "${GIR}" == '' ]]; then
	pushd "${MYREALDIR}"  >/dev/null
		readonly GIR=$(git rev-parse --show-toplevel)
	popd >/dev/null
fi
[[ "${GIR}" != '' ]] || exit 1
[[ "${INSTALLPARENT}" != '' ]] || readonly INSTALLPARENT=$(realpath "${GIR}"/..)
pushd "${INSTALLPARENT}" >/dev/null
	[ -d "${NAME}" ] || [ -L "${NAME}" ] || git clone --recursive "${REPO}" "${@}"
popd >/dev/null

set +x

exec 1>&3 2>&4

rm "${LOGF}"

echo $(realpath "${INSTALLPARENT}"/"${NAME}")

#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
	source ../defaults.rc.bash

	(which awk || ./apt-ensure.bash gawk) >/dev/null
	
	sudo find /etc/apt/apt.conf.d/ -iname '*~' -delete

	readonly FROM_CONF=$(awk -F'[ ;]' -vOFS='' '/^Dir::Cache::Archives/ {$1=$NF=""; print}' /etc/apt/apt.conf.d/*)
	if [[ "${FROM_CONF}" == '' ]]; then
		readonly RESULT='/var/cache/apt/archives'
	else
		readonly RESULT="${FROM_CONF}"
	fi

	./ensure-path.bash "${RESULT}"

popd >/dev/null

#!/usr/bin/env bash
set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd >/dev/null

readonly DefaultPresentList=("${MYREALDIR}"/../lib/{apt.list,basic.list,minimal.list,withgui.list})
readonly PURGELIST="${MYREALDIR}/../lib/purge.list"
readonly BACKEND="${MYREALDIR}/apt.bash"
if [[ "${ENFORCE_PURGE}" == '' ]]; then
	readonly ENFORCE_PURGE=false
fi

while (( ${#} > 0 )); do
	case ${1} in
		\-*)
			opt=${1}
			shift # past option
			AptListsOptions+=${opt}
		;;
		*\-upgrade | install)
			readonly ACT=${1}
			shift # past action
			if (( ${#} == 0 )); then 
				if [ -e custom.list ]; then
					readonly PresentList=(${DefaultPresentList[*]} custom.list)
				else
					readonly PresentList=(${DefaultPresentList[*]})
				fi
			else
				readonly PresentList=(${*})
			fi

			"${BACKEND}" markauto $(sort -u ${PresentList[*]} "${PURGELIST}") || true

			if "${ENFORCE_PURGE}" && [ "${ENFORCE_PURGE}" ]; then
				"${BACKEND}" ${AptListsOptions[*]} ${ACT} $(sed 's/$/+/g' ${PresentList[*]}) $(sed 's/$/_/g' "${PURGELIST}") || "${BACKEND}" ${AptListsOptions[*]} ${ACT} $(sort -u ${PresentList[*]})
			else
				"${BACKEND}" ${AptListsOptions[*]} ${ACT} $(sed 's/$/+/g' ${PresentList[*]}) || "${BACKEND}" ${AptListsOptions[*]} ${ACT} $(sort -u ${PresentList[*]})
			fi

			readonly CODE=$?

			("${BACKEND}" markauto $(sort -u "${PURGELIST}") || true; apt -y autoremove) &>/dev/null &

			exit ${CODE}
		;;
		*)
			readonly ACT=${1}
			shift # past action
			"${BACKEND}" ${AptListsOptions[*]} ${ACT} ${*}
			CODE=$?
			apt -y autoremove &>/dev/null &
			exit ${CODE}
		;;
	esac
done

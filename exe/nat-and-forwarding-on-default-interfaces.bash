#!/usr/bin/env bash

set -e
if [ ${DEBUG} ]; then
	set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}"
	source ../defaults.rc.bash

	./apt-ensure.bash grep moreutils iptables-persistent

	declare -a DefaultRoutes=($(ip -o -brief route | grep default))
	pushd /sys/class/net
		readonly INTERFACES_PATTERN=$(echo * | awk -vOFS="|" '{$1=$1; print}')
	popd
	declare -a DefaultInterfaces=($(printf "%s\n" "${DefaultRoutes[@]}" | ifne egrep -w "${INTERFACES_PATTERN}"))

	# Enable IP forwarding
	./sysctl-set-one.bash net.ipv4.ip_forward 1
	./sysctl-set-one.bash net.ipv6.conf.all.forwarding 1
	sudo sysctl -p

	# Configure NAT
	for ifa in "${DefaultInterfaces[@]}"; do
		sudo iptables -t nat -A POSTROUTING -o $ifa -j MASQUERADE
		sudo ip6tables -t nat -A POSTROUTING -o $ifa -j MASQUERADE
	done

	"${MYREALDIR}"/apt-ensure.bash iptables-persistent

popd

#!/usr/bin/env bash

set -e
if [ ${DEBUG} ]; then 
	        set -x
fi      
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

if [ "${DEBUG}" ]; then
	"${MYREALDIR}"/ltd.bash sudo "${MYREALDIR}"/dedup.bash lib/apt lib/trusted.gpg.d /etc/apt layers/*/*/etc/apt "${MYREALDIR}"/../lib/apt "${MYREALDIR}"/../lib/trusted.gpg.d "${MYREALDIR}"/../layers/*/*/etc/apt /var/cache/apt/archives
	"${MYREALDIR}"/ltd.bash sudo "${MYREALDIR}"/deledupes.bash lib/trusted.gpg.d "${MYREALDIR}"/../lib/trusted.gpg.d /etc/apt/trusted.gpg.d
else
	"${MYREALDIR}"/ltd.bash sudo "${MYREALDIR}"/dedup.bash lib/apt lib/trusted.gpg.d /etc/apt layers/*/*/etc/apt "${MYREALDIR}"/../lib/apt "${MYREALDIR}"/../lib/trusted.gpg.d "${MYREALDIR}"/../layers/*/*/etc/apt /var/cache/apt/archives &>/dev/null
	"${MYREALDIR}"/ltd.bash sudo "${MYREALDIR}"/deledupes.bash lib/trusted.gpg.d "${MYREALDIR}"/../lib/trusted.gpg.d /etc/apt/trusted.gpg.d &>/dev/null
fi

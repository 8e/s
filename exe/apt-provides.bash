#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

(
dpkg -S "${@}"  &
dpkg-query --search "${@}" &
apt-file search "${@}" &
wait
) 2>/dev/null | sort -u

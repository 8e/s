#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/z-holds.bash | awk '{print "zfs release -r "$2" "$1}' | "${MYREALDIR}"/xargs.bash -l bash -xc

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly NAME=$(basename ${ME})
readonly LOGDIR=$("${MYREALDIR}"/ensure-path.bash /tmp/log/$USER)

function install() {
	readonly DAEMON_NAME=$(basename "${1}")
	readonly DAEMON_PATH=/etc/systemd/system/"${DAEMON_NAME}".service
	readonly LOGF="${LOGDIR}"/"${DAEMON_NAME}".log
	readonly DAEMON_FILE_CONTENTS="[Unit]
Description="${@}" Continuous Agent
Wants=network.target
After=network.target
After=tmp.mount
Requires=tmp.mount
After=caches.service
Wants=caches.service
ConditionACPower=true

[Service]
Type=${TYPE:-'idle'}
ExecStart="${ME}" "${@}" 2>&1 >> ${LOGF}
Restart=always
SuccessExitStatus=3 4
RestartForceExitStatus=3 4
User="$USER"
Group="$USER"
Nice=${NICE:-19}
IOSchedulingClass=${IOCLASS:-'idle'}
IOSchedulingPriority=${IONICE:-7}
CPUQuota=${CPUQUOTA:-3}%

# https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html
# # A higher weight means more CPU time, a lower weight means less. The allowed range is 1 to 10000
CPUWeight=1
StartupCPUWeight=1

# # Specify the throttling limit on memory usage of the executed processes in this unit. Memory usage may go above the limit if unavoidable, but the processes are heavily slowed down and memory is taken away aggressively in such cases. This is the main mechanism to control memory usage of a unit.
# # Takes a memory size in bytes. If the value is suffixed with K, M, G or T, the specified memory size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively. Alternatively, a percentage value may be specified, which is taken relative to the installed physical memory on the system. This setting is supported only if the unified control group hierarchy is used and disables MemoryLimit=.
MemoryHigh=5%

# # Specify the absolute limit on swap usage of the executed processes in this unit.
# # Takes a swap size in bytes. If the value is suffixed with K, M, G or T, the specified swap size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively.
MemorySwapMax=1G

${HARDENING:-'# Hardening                                                                      
SystemCallArchitectures=native                                                   
MemoryDenyWriteExecute=true                                                      
NoNewPrivileges=true
PrivateDevices=true
PrivateNetwork=true
ProtectSystem=true'}

[Install]
WantedBy=graphical.target
WantedBy=default.target
WantedBy=multi-user.target
"
	[ -e "${LOGF}" ] || touch "${LOGF}"

	echo -n "${DAEMON_FILE_CONTENTS}" | "${MYREALDIR}"/ensure-contents.bash "${DAEMON_PATH}"

	sudo systemctl daemon-reload
	sudo systemctl restart "${DAEMON_NAME}"
	sudo systemctl status "${DAEMON_NAME}"

}

while [ ${#} -gt 0 ]; do
	case ${1} in
		'install')
                        shift # past action
			install ${*}
			exit 0
		;;
		*)
			delay="${1}"
                        shift # past param
			if [[ ${delay} =~ x[0-9]+ ]]; then
				readonly MULT=${delay#?} # Pluck off first char
				while true; do
					start=`date +%s`
					${*}
					end=`date +%s`
					runtime=$((end-start))
					sleep $(( runtime * MULT ))
				done
			elif "${MYREALDIR}"/is_posint.bash "${delay}"; then
				while true; do
					${*}
					sleep "${delay}"
				done
			elif [ -x "${delay}" ]; then
				readonly HEALTHCHECK="${delay}"
				delay="${1}"
				shift
				while true; do
					if "${HEALTHCHECK}"; then
						sleep ${delay}
					else
						${*}
					fi
				done
			fi
		;;
	esac
done

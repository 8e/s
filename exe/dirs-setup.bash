#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

sudo echo -ne "/usr/share/wallpapers\n/var/lib/apt\n/var/cache/apt/archives\n/etc/cron.weekly\n/var/log\n" | ensure-symlink.bash /tmp

#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-true}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
#readonly URLRE="https://.*"
if [[ $PORT == '' ]]; then
	readonly PORT=443
fi

"${MYREALDIR}"/apt-ensure.bash openssl
if [ -e "${@}" ]; then
	cat "${@}"
else
	# http://stackoverflow.com/questions/36609136/ddg#55694389
	openssl s_client -showcerts -servername "${@}" -connect "${@}:$PORT"
fi | openssl x509 -enddate -noout #https://stackoverflow.com/questions/21297853/how-to-determine-ssl-cert-expiration-date-from-a-pem-encoded-certificate

#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly ZVOL_BASENAME="${1}"
shift
readonly POOL=${POOL:-'rpool'}
readonly DATASETVOL="${POOL}/volumes/${ZVOL_BASENAME}"
readonly BLOCKSIZE=${BLOCKSIZE:-$(zfs list -H -o recordsize "${POOL}")}
"${MYREALDIR}"/apt-ensure-cmd.bash bc bc
readonly ZVOL_SIZE=${ZVOL_SIZE:-$( bc <<< "$( ${MYREALDIR}/zpool-size.bash ${POOL} ) * 0.94 " )}

zfs create -psV "${ZVOL_SIZE}" -o volblocksize="${BLOCKSIZE}" "${@}" "${DATASETVOL}"

readonly BLOCKDEV="/dev/zvol/${DATASETVOL}"
if [ "${DEBUG}" ]; then
	fdisk -l "${BLOCKDEV}"
	lsblk "${BLOCKDEV}"
	blkid "${BLOCKDEV}"
fi
echo "${BLOCKDEV}"

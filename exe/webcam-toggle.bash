#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
	source ../defaults.rc.bash
popd >/dev/null

sudo modprobe -r uvcvideo || true
sudo modprobe uvcvideo

sudo modprobe -r gspca_main || true
sudo modprobe gspca_main

function appeared() {
	sudo lsof /dev/video* || (( $(sudo lspci | wc -l) > 20 )) || (( $(sudo lsusb | wc -l) > 11 )) || (( $(sudo lshw -c video | wc -l) > 25 ))
}

"${MYREALDIR}"/webcam-toggle || true
appeared || "${MYREALDIR}"/webcam-toggle || true
appeared || for k in XF86WebCam XF86Launch1 XF86Launch2 XF86Launch3 XF86Launch4 F5 F6 F7 F8 F9 XF86Launch4 XF86Launch5 XF86Launch6 XF86Launch7 XF86Launch8 XF86Launch9 XF86MonBrightnessUp XF86Tools XF86Explorer; do
	xdotool key $k
	appeared || xvkbd -text "\[$k]"
	appeared && exit 0
done

xdotool keydown --clearmodifiers 0xee
xdotool keyup 0xee
appeared || xdotool key XF86WebCam
appeared || xvkbd -text '\[XF86WebCam]'
appeared

#!/usr/bin/env bash

set -e
DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

# https://duckduckgo.com/?t=lm&q=kidle_inj+cpu

# http://askubuntu.com/questions/584636/ddg#586077

sudo rmmod intel_powerclamp

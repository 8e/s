#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash
set -x
readonly POOL=${1}
shift
readonly CMD=${1}
shift

function expo() {
	local i=1
	while zpool status | egrep "${1}"; do
		#if (( i > 2 )); then
		#fi
		tsp zpool clear "${POOL}"
		zpool clear "${POOL}" "${1}" || true
		zpool "${CMD}" "${POOL}" "${1}" || true
		sleep $i
		i=$(( i * 2 ))
	done
}

zpool clear "${POOL}"
zpool resilver "${POOL}"

for d in "${@}"; do
	expo "${d}" &
done
wait

#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure.bash lshw gawk

function cd_device_is_there() {
	# https://askubuntu.com/questions/184652/how-to-check-if-a-cdrom-is-in-the-tray-remotely-via-ssh
	sudo lshw | awk '/\*-cd/,/con/' | sed -e 's/^[ \t]*//'
}

if cd_device_is_there; then
	"${MYREALDIR}"/apt-ensure.bash $(cat "${MYREALDIR}"/../lib/compact-disc.list)
fi

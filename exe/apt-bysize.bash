#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

which hr || "${MYREALDIR}"/hr_install.bash

# https://www.2daygeek.com/how-to-list-installed-packages-by-size-largest-on-linux/

(dpkg-query --show --showformat='${Installed-Size}\t${Package}\n' | awk '{print $1*1024, $2}' | hr; dpkg -H | sed -e 's/^[[:space:]]*//'; aptitude search "~i" --display-format "%I %p" --sort installsize) | sort -hu | awk '$2!=prev {prev=$2; print}'




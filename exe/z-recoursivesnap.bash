#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

readonly NAMESUFF=${1:-$(date +%T)}
readonly NAME="$(date +%F_%H)_${NAMESUFF}"
readonly SNAPTYPES="filesystem,volume,bookmark"

time for ds in $(zfs list -H -o name,com.sun:auto-snapshot,canmount,type -t "${SNAPTYPES}" | awk '$2=="true" && $3!="off" {print $1}'); do
	zfs snapshot ${ds}@${NAME} &
done

([[ -L /usr/libexec/zsys-system-autosnapshot ]] && [[ $(realpath /usr/libexec/zsys-system-autosnapshot ) == "${ME}" ]]) || ln -sfv "${ME}" /usr/libexec/zsys-system-autosnapshot


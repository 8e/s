#/usr/bin/env bash

set -xe

readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly BACKEND="${MYREALDIR}/sort-u-splinter-inplace.bash"
readonly LISTF="${1}"

shift

echo "${*}" >> "${LISTF}"

bash -xe "${BACKEND}" "${LISTF}"

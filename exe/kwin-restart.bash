#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

# https://www.addictivetips.com/ubuntu-linux-tips/fix-a-frozen-kde-window-manager/
#
export $(dbus-launch)

killall kwin &
killall kwin_x11 &

sleep 1.33

killall -KILL kwin &
killall -KILL kwin_x11 &

sleep 1.33

kwin --replace &
plasmashell --replace

#!/usr/bin/env sh

set -e
sed 's/./&\n/g' | LC_COLLATE=C sort -u | tr -d '\n'

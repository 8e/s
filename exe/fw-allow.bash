#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash

	which ufw || ./apt-ensure.bash ufw
	sudo ufw enable

	for h in "${@}"; do
	       	sudo ufw allow from "${h}"
		sudo ufw allow to "${h}"
	done
	./fw-status.bash

popd >/dev/null

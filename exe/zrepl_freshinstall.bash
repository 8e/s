#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly ZREPL_APT_KEY_URL=https://zrepl.cschwarz.com/apt/apt-key.asc
readonly ZREPL_APT_KEY_DST=/etc/apt/trusted.gpg.d/zrepl.gpg
readonly ZREPL_APT_REPO_FILE=/etc/apt/sources.list.d/zrepl.list

# Install dependencies for subsequent commands
"${MYREALDIR}"/apt-ensure.bash curl gnupg lsb-release
# Deploy the zrepl apt key.
curl -fsSL "${ZREPL_APT_KEY_URL}" | tee | gpg --dearmor | sudo tee "${ZREPL_APT_KEY_DST}"

readonly ARCH="$(dpkg --print-architecture)"
readonly CODENAME="$(lsb_release -i -s | tr '[:upper:]' '[:lower:]') $(lsb_release -c -s | tr '[:upper:]' '[:lower:]')"

# Add the zrepl apt repo.
echo "Using Distro and Codename: $CODENAME"
echo "deb [arch=$ARCH signed-by=${ZREPL_APT_KEY_DST}] https://zrepl.cschwarz.com/apt/$CODENAME main" | sudo tee -a "${ZREPL_APT_REPO_FILE}"

"${MYREALDIR}"/apt-update.bash

"${MYREALDIR}"/apt-ensure.bash tmux zrepl

#!/usr/bin/env bash

set -e

# https://github.com/mbroz/cryptsetup/blob/master/lib/utils_crypt.c
declare -a Algorithms=(aead hmac-sha1 hmac-sha256 hmac-sha512 poly1305 crc32 crc32c aes-xts-plain64 aes-xts-plain64:sha256)

Algorithms+=($(curl https://raw.githubusercontent.com/mbroz/cryptsetup/master/lib/utils_crypt.c 2>/dev/null | grep -oe '"[[:lower:][:digit:]-]\+"' | sort -u | grep -v '"s"' | tr -d '"'))

printf '%s\n' "${Algorithms[@]}" | sort -u # | grep -v 'empty\|null\|none'

# 2020-12-14_15:05:29
#Integrity:      hmac-sha1       +       Encryption:     aes-xts-plain64 =success
#Integrity:      hmac-sha1       +       Encryption:     aes-xts-plain64:sha256  =success
#Integrity:      hmac-sha256     +       Encryption:     aes-xts-plain64 =success
#Integrity:      hmac-sha256     +       Encryption:     aes-xts-plain64:sha256  =success
#Integrity:      hmac-sha512     +       Encryption:     aes-xts-plain64 =success
#Integrity:      hmac-sha512     +       Encryption:     aes-xts-plain64:sha256  =success

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
	source ../defaults.rc.bash
popd >/dev/null

df -H --output=source "${@:-/}" | awk -vFS='[ /]' 'NR==2 {print $1}'

#!/usr/bin/env bash
set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash

	while ! zpool export -f rpool; do fuser -k /mnt/z /dev/zfs; zfs umount /mnt/z; lsof /mnt/z /dev/zfs; lsof | awk -vOFS="\n" '/zfs/ {print $2, $3}' | sort -u |grep '^[[:digit:]]\+$' | ./xblissargs.bash -l kill -KILL; rmmod -f zfs; modprobe -rf zfs; done

popd >/dev/null

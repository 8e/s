#!/usr/bin/env bash

set -ex

readonly MYREALDIR=$(dirname $(realpath ${0}))

"${MYREALDIR}"/git-installrepo.bash https://github.com/felizchachacha/rclone-mount-manager.git

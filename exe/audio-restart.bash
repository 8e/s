#!/usr/bin/env bash
set -ex
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

sudo usermod -a -G audio,pulse-access root &
sudo usermod -a -G audio,pulse-access $USER &
sudo usermod -a -G audio,pulse-access $DEFUSER &
if which pulseaudio; then
	readonly CMD=pulseaudio
elif which pipewire-pulse; then
	readonly CMD=pipewire-pulse
else
	echo "there is no pulseaudio or pipewire-pulse, what should we start?"
	exit 1
fi

groups | grep audio
which pacmd || sudo "${MYREALDIR}"/apt-enure.bash pulseaudio-utils
killall /usr/bin/pipewire-pulse &
sudo pkill jack &
sudo fuser -k /dev/snd/* &
pulseaudio -k &
killall -KILL /usr/bin/pipewire-pulse &


wait
start-pulseaudio-kde &
start-pulseaudio-x11 &
"${CMD}" --start --daemonize=true &

wait

#curl --silent -H "Authorization: Bearer $ZT_API_KEY" "https://my.zerotier.com/api/network/${ZT_NETWORK_ID}/member" | jq -r '.[] | "\(.config.ipAssignments[]) \(.name)"' | sudo tee "${FHOSTS}"

pacmd unload-module module-udev-detect &
#sudo pacmd unload-module module-udev-detect &
systemctl --user daemon-reload &

wait

systemctl restart --user pulseaudio &
wait

pacmd load-module module-udev-detect
pacmd load-module module-bluetooth-policy
pacmd load-module module-bluetooth-discover

amixer -D pulse sset Master toggle &
wait

"${CMD}" --check
# Ensure that PulseAudio is not in the "suspend" mode. Run the following command:
# pacmd list-sinks
# Look for the state field in the output. If it shows "SUSPENDED", you can try to unsuspend it with:
# pacmd suspend-sink <sink_name_or_index> 0
# Replace <sink_name_or_index> with the name or index of the sink that is suspended.

pacmd list-sinks
aplay -l
pacmd list-sink-inputs

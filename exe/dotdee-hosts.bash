#!/usr/bin/env bash

set -e
DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

${MYREALDIR}/apt-ensure-cmd.bash dotdee dotdee

if dotdee -l | grep '/etc/hosts'; then
	sudo dotdee --update /etc/hosts
else
	sudo dotdee --setup /etc/hosts "update-alternatives: using /etc/dotdee/etc/hosts to provide /etc/hosts (etc:hosts) in auto mode." || (sudo dotdee --undo /etc/hosts && sudo dotdee --setup /etc/hosts "update-alternatives: using /etc/dotdee/etc/hosts to provide /etc/hosts (etc:hosts) in auto mode.")
fi

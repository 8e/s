#!/bin/bash 

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

ufw enable &>/dev/null &

if which tsp; then
	tsp "${MYREALDIR}"/apt-update.bash
elif which screen; then
	"${MYREALDIR}"/screen.bash "${MYREALDIR}"/apt-update.bash
fi

pushd "${MYREALDIR}"/../lib

	"${MYREALDIR}"/apt.bash purge -o DPkg::Lock::Timeout=-1 -y nano
	if ! "${MYREALDIR}"/apt.bash install -y $(sort -u apt.list basic.list |grep -v mint); then
		"${MYREALDIR}"/apt.bash install -y $(sort -u basic.list apt.list |grep -v mint)
		"${MYREALDIR}"/apt.bash install -fy
	fi

popd

ufw enable &

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

if ! "${MYREALDIR}/focopy.py3" || cp -rl --force --backup "${@}" &>/dev/null || cp -r --force --backup "${@}" &>/dev/null || cp -r --force --backup "${@}" &>/dev/null || cp -r --backup --remove-destination "${@}" &>/dev/null; then
	"${MYREALDIR}"/apt-ensure-cmd.bash rsync rsync
	rsync -schHPXlitA --backup-dir=/var/backups "${@}"
fi

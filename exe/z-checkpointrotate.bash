#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

sudo zpool checkpoint -d "${@}" || true
sleep 1
sudo zpool checkpoint "${@}"
"${MYREALDIR}"/z-status.bash "${@}"

#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))
readonly BACKDEV=${1}
readonly MNTOPTS=${2:-rw,noatime}
readonly MNTNAME=/mnt/$(basename "${BACKDEV}")
readonly DISLOCKERMNT=/dev/dislocker/$(basename "${BACKDEV}")

pushd "${MYREALDIR}"
	which dislocker || ./apt.bash -y install dislocker
	./apt-is-installed.bash ntfs-3g || ./apt.bash -y install ntfs-3g
	./ensure-path.bash "${DISLOCKERMNT}"
	dislocker -vo nonempty -u "${BACKDEV}" "${DISLOCKERMNT}" || dislocker -vo nonempty -p "${BACKDEV}" "${DISLOCKERMNT}"
	./ensure-path.bash "${MNTNAME}"
	ntfsfix --clear-bad-sectors "${DISLOCKERMNT}"/dislocker-file
	ntfs-3g.probe --readwrite "${DISLOCKERMNT}"/dislocker-file
	mount.ntfs-3g -o "loop,${MNTOPTS}" "${DISLOCKERMNT}"/dislocker-file "${MNTNAME}"
popd

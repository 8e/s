#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

if [ -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5 ]; then
	sudo killall kioslave5 &
	sudo killall krunner &
	sudo chmod -v -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5
	sudo killall -KILL kioslave5 &
	sudo killall -KILL krunner &
else 
	sudo chmod -v +x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5
fi

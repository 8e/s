#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

#set -o pipefail
#shopt -s lastpipe

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly REVISION=${1}
shift
readonly G_ROOT=$(git rev-parse --show-toplevel)
readonly RELPATH=$(realpath --relative-to="${G_ROOT}" "${@}")

pushd "${G_ROOT}" >/dev/null
	git fetch
	git diff -z --name-only "${REVISION}" "${RELPATH}" | "${MYREALDIR}"/xblissargs.bash -0 -I{} "${MYREALDIR}"/git-compare-datetime.bash "${REVISION}" {} 2>/dev/null
popd >/dev/null

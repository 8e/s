#!/usr/bin/env bash

set -ex

cp -lfv ./'Cubase 13' "/Applications/Cubase 13.app/Contents/MacOS/Cubase 13" || cp -fv ./'Cubase 13' "/Applications/Cubase 13.app/Contents/MacOS/Cubase 13"

function hex() {
echo ''$1'' | perl -0777pe 's|([0-9a-zA-Z]{2}+(?![^\(]*\)))|\\x${1}|gs'
}
function prep() {
sudo xattr -cr "$1"
sudo xattr -r -d com.apple.quarantine "$1"
sudo codesign --force --sign - "$1"
}

prep "/Applications/Cubase 13.app/Contents/MacOS/Cubase 13"
"/Applications/Cubase 13.app/Contents/MacOS/Cubase 13"

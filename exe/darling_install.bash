#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))

"${MYREALDIR}"/apt-ensure.bash curl gawk

pushd "${MYREALDIR}"
	which setsebool && setsebool -P mmap_low_allowed 1

	./dpkg-loosen.bash darling darling-dkms &
	#./apt.bash -y install cmake clang bison flex xz-utils libfuse-dev libudev-dev pkg-config libc6-dev:i386 linux-headers-generic gcc-multilib libcap2-bin libcairo2-dev libgl1-mesa-dev libtiff5-dev libfreetype6-dev libfreetype6-dev:i386 git libelf-dev libxml2-dev libegl1-mesa-dev libfontconfig1-dev libbsd-dev

	#pushd $(git-installrepo.bash https://github.com/darlinghq/darling.git)
	#	mkdir build && cd build
	#	cmake ..
	#	make
	#	make install
	#	make lkm
	#	make lkm_install
	#popd

	# https://github.com/darlinghq/darling/releases
	./apt-urls.bash $(curl -q https://github.com/darlinghq/darling/releases 2>/dev/null | awk -vFS='"' 'BEGIN {i=1; N=3} i==N {exit} /<a href=.*deb/ {Urls[i++]="https://github.com"$2} END { while (i >= 0) print Urls[--i] }')

popd

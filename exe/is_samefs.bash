#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

readonly MP=$("${MYREALDIR}"/get-mountpoint.bash "${1}")
shift
for path in "${@}"; do
	[[ $("${MYREALDIR}"/get-mountpoint.bash "${path}") == "${MP}" ]] && exit 1
done
exit 0

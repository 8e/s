#!/usr/bin/env bash

set -ex

readonly MYDIR=$(dirname $(realpath "${0}"))

which ldmtool || "${MYDIR}"/apt.bash install ldmtool ntfs-3g scrounge-ntfs

ldmtool scan

ldmtool create all

find /dev/mapper/ldm*



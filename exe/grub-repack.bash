#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))

pushd "${MYREALDIR}"

	for p in $(cat ../lib/grubboot.list); do

		if ./apt-is-installed.bash ${p}; then
			"${MYREALDIR}"/apt.bash
			dpkg-reconfigure ${p}
		else
			./apt.bash -y install ${p}
		fi

	done

popd

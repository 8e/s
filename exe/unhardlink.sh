#!/usr/bin/env sh

set -e
for i in "$@"; do
	temp="$(mktemp -d -- "/tmp/${i%/*}_hardlnk-XXXXXXXX")"
	[ -e "$temp" ] && cp -ip "$i" "$temp/tempcopy" && mv "$temp/tempcopy" "$i" && rmdir "$temp"
done

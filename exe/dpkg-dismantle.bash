#!/usr/bin/env bash

set -ex

readonly D="/var/lib/dpkg/info"

for package in ${@}; do
	for f in "${D}"/"${package}".*; do
		if [[ $(file "${f}") =~ "script, ASCII text executable" ]]; then
			cp "${f}" /var/backups/
			echo "exit 0" > "${f}"
		fi
	done
done

#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly LOGDIR=$("${MYREALDIR}"/ensure-path.bash "/tmp/log/$USER/$(basename ${ME})")
readonly LOGF="${LOGDIR}/$$.log"
readonly BASE_URL="https://github.com/ripmeapp/ripme/releases"
readonly JARPLACE="/opt/ripme.jar"
pushd ${MYREALDIR}
	echo java md5sum html2text curl | ./apt-ensure.bash default-jre-headless coreutils html2text curl
	./ensure-path.bash $(dirname "${JARPLACE}")
popd

function get_local_version() {
	[ -e "${JARPLACE}" ] || return 1
	local version=$(java -jar "${JARPLACE}" -v)
	[[ "${version}" == '' ]] && return 2
	echo "${version}"
}

function get_latest_version() {
	curl "${BASE_URL}" | html2text | grep '^[0-9\.]\+\:.\+' | sort -uVr | awk -F':' '{print$1; exit}'
}

function install() {
	local ver=$(get_latest_version)
	echo "Latest version: $ver"
	readonly URL="${BASE_URL}/download/${ver}/ripme.jar"
	[ -e "${JARPLACE}" ] && mv -v "${JARPLACE}" /var/backups
	wget -O "${JARPLACE}" "${URL}"
	get_local_version
}

case ${1} in
	install)
		shift
		install
	;;
	latest)
		shift
		get_latest_version
	;;
	version)
		shift
		get_local_version
	;;
	*)
		get_local_version || sudo "${ME}" install
		java -jar "${JARPLACE}" "${@}" | tee "${LOGF}"
	;;
esac

(${DEBUG} || rm -v "${LOGF}") || true

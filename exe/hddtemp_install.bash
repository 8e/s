#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-urls.bash latest hddtemp http://archive.ubuntu.com/ubuntu/pool/universe/h/hddtemp/

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly SCRSESNAME=$(basename "${ME}")

if [[ $MNT == '' ]]; then
	readonly MNT="/mnt/z"
fi

readonly RBINDS=(dev media proc run sys tmp 'var/cache/apt/archives' 'var/lib/apt' 'var/log' 'etc/cron.weekly')

for rbm in "${RBINDS[@]}"; do
	target="${MNT}/${rbm}"
	"${MYREALDIR}"/ensure-path.bash "${target}"
	mount --rbind -v "/${rbm}" "${target}"
done

chroot ${MNT} "${@:-/usr/bin/zsh}"

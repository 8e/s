#!/usr/bin/env bash

set -e

readonly TMPF=$(mktemp)
cp -p -- "${*}" "${TMPF}"
mv -f -- "${TMPF}" "${*}"

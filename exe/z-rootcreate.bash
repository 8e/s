#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source ../defaults.rc.bash
source ../z-ensure-poolosid.rc.bash

"${MYREALDIR}"/z-create.bash "${@}"

# https://www.reddit.com/r/zfs/comments/mj4nfa/ubuntu_server_2104_native_encrypted_root_on_zfs/
# https://github.com/Sithuk/ubuntu-server-zfsbootmenu/blob/main/ubuntu_server_encrypted_root_zfs.sh
# https://www.reddit.com/r/zfs/comments/zqe1xn/announcing_zfsbootmenu_210/
# https://github.com/zbm-dev/zfsbootmenu/wiki/Debian-Buster-installation-with-ESP-on-the-zpool-disk

zfs create -o canmount=off -o mountpoint=none "${POOL}"/ROOT

##zfs create -o canmount=off -o mountpoint=none "${POOL}"/USERDATA

# NOTE: It is important to set the property canmount=noauto on any file systems with mountpoint=/ (that is, on any additional boot environments you create).
zfs create -o canmount=noauto -o mountpoint=/ -o com.ubuntu.zsys:bootfs=yes -o com.ubuntu.zsys:last-used=$(date +%s) "${POOL}"/ROOT/$OSID

zfs mount "${POOL}"/ROOT/$OSID &
zpool set bootfs="${POOL}"/ROOT/$OSID "${POOL}"

zfs create -o compression=lz4 -o dedup=off -o canmount=on -o mountpoint=/var/git "${POOL}"/git

##zfs create -o com.ubuntu.zsys:bootfs=no -o com.ubuntu.zsys:bootfs-datasets="${POOL}"/ROOT/$OSID -o canmount=on -o mountpoint=/home "${POOL}"/USERDATA/$OSID

#zfs create -o canmount=on -o mountpoint=/root "${POOL}"/USERDATA/$OSID/root

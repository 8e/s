#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

find ${@} -exec apt-file search '{}' \; | awk -F':' '{print $1}' | sort -u

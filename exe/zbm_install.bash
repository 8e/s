#!/usr/bin/env bash

# https://gitlab.com/Sithuk/ubuntu-server-zfsbootmenu/-/blob/main/ubuntu_server_encrypted_root_zfs.sh

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly TO_REFIND="15" #Timeout in seconds for rEFInd boot screen until default choice selected.

pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
        source ../z-ensure-poolosid.rc.bash
	./apt-ensure-cmd.bash task-spooler tsp

	##Check for root priviliges
	if [ "$(id -u)" -ne 0 ]; then
	   echo "Please run as root."
	   exit 1
	fi

	##Check for EFI boot environment
	if [ -d /sys/firmware/efi ]; then
	   echo "Boot environment check passed. Found EFI boot environment."
	elif ${EFI_BYPASS} && [ ${EFI_BYPASS} ]; then
		echo "You asked to bypass efi boot env not being found"
	else
	   echo "Boot environment check failed. EFI boot environment not found. Script $ME requires EFI."
	   exit 1
	fi

	if [[ "${POOL}" == '' ]]; then
		if [[ "${1}" == '' ]]; then
			read -e -p "Enter pool name: " POOL
		else
			readonly POOL="${1}"
		fi
	fi
	 
	if [[ $OSID == '' ]]; then
		if [[ "${2}" == '' ]]; then
			read -e -p "Enter OS ID (host id command outputs $(hostid) and /etc/hostid is $(cat /etc/hostid || true): " OSID
		else
			readonly OSID="${2}"
		fi
	fi

	readonly CMDLINE_LINUX_DEFAULT="POOL=${POOL} zbm.import_policy=hostid zbm.set_hostid ${CMDLINE_LINUX_DEFAULT} spl_hostid=${OSID} zbm.prefer=${POOL}"

	readonly ZBM_LOCAL_GIT=$(./git-installrepo.bash https://github.com/zbm-dev/zfsbootmenu.git)

	./ensure-path.bash /etc/dracut.conf.d
	./focopy.bash ../lib/dracut.conf.d/* /etc/dracut.conf.d/

	##Install zfsbootmenu dependencies
	#aptitude install -y $(sort -u "${MYREALDIR}"/../lib/zbm.list "${MYREALDIR}"/../lib/boot.list)
	./apt-ensure.bash $(sort -u "${MYREALDIR}"/../lib/zbm.list "${MYREALDIR}"/../lib/boot.list wget curl git)
	#./apt-lists.bash install "${MYREALDIR}"/../lib/zbm.list "${MYREALDIR}"/../lib/boot.list

	echo 'REMAKE_INITRD=yes' | ./ensure-keyvals.bash /etc/dkms/zfs.conf
	echo 'LOAD_KEXEC=true' | ./ensure-keyvals.bash /etc/default/kexec

	tsp cpan 'YAML::PP'

	echo /usr/share/zfsbootmenu | ./ensure-symlink.bash "${ZBM_LOCAL_GIT}"/zfsbootmenu

popd >/dev/null

pushd "${ZBM_LOCAL_GIT}"
#	bash -x ./zfsbootmenu/install-helpers.sh
#	make install
#
#	# Install perl dependencies if necessary
#	# https://github.com/zbm-dev/zfsbootmenu/blob/master/testing/helpers/zbm-populate.sh
#	if [ -z "${SKIP_PERL}" ]; then
#		  cpanm --notest --installdeps .
#	fi

	# https://github.com/zbm-dev/zfsbootmenu/wiki/Debian-Buster-installation-with-ESP-on-the-zpool-disk
	# https://github.com/Sithuk/ubuntu-server-zfsbootmenu/blob/main/ubuntu_server_encrypted_root_zfs.sh

	##configure zfsbootmenu

# Adjust the configuration for convenient builds
#if [ -f /etc/zfsbootmenu/config.yaml ]; then
#  sed -e 's/Versions:.*/Versions: false/' \
#      -e 's/ManageImages:.*/ManageImages: true/' \
#      -e 's@ImageDir:.*@ImageDir: /zfsbootmenu/build@' \
#      -e '/BootMountPoint:/d' -i /etc/zfsbootmenu/config.yaml
#fi

	##Omit systemd dracut modules to prevent ZBM boot breaking
	cat <<-EOF >> /etc/zfsbootmenu/dracut.conf.d/zfsbootmenu.conf
omit_dracutmodules+=" systemd systemd-initrd dracut-systemd "
EOF

	# Replace key parts with symlinks to reflect source changes
	#$(realpath bin/generate-zbm) | "${MYREALDIR}"/ensure-symlink.bash /usr/local/bin/generate-zbm
	echo /usr/local/bin/generate-zbm | "${MYREALDIR}"/ensure-symlink.bash $(realpath bin/generate-zbm)
	#$(realpath 90zfsbootmenu) | "${MYREALDIR}"/ensure-symlink.bash /usr/lib/dracut/modules.d/90zfsbootmenu
	echo /usr/lib/dracut/modules.d/90zfsbootmenu | "${MYREALDIR}"/ensure-symlink.bash $(realpath 90zfsbootmenu)

	cpan 'YAML::PP'

	mount ${EFIDIR} || (( $? == 32 ))
	"${MYREALDIR}"/ensure-path.bash ${EFIDIR}/EFI/ubuntu
	if [ -d ${EFIDIR}/EFI/ubuntu ] || [ -d ${EFIDIR}/EFI/debian ]; then
		mv -v ${EFIDIR}/EFI/debian/* ${EFIDIR}/EFI/ubuntu/
		rmdir -v ${EFIDIR}/EFI/debian
		cp -av ${EFIDIR}/EFI/ubuntu/ ${EFIDIR}/EFI/debian
	fi
		
	pushd ${EFIDIR}/EFI/ubuntu
		curl -LJO https://get.zfsbootmenu.org/efi || if  (( $? == 23 )); then
			wget --continue https://get.zfsbootmenu.org/efi
		fi
	popd

	##Adjust timer on initial rEFInd screen
	sed -i "s,^timeout .*,timeout $TO_REFIND," ${EFIDIR}/EFI/*/refind.conf

	if grep '^REMAKE_INITRD=' /etc/dkms/zfs.conf; then
		[[ $(grep '^REMAKE_INITRD=' /etc/dkms/zfs.conf) == "REMAKE_INITRD=yes" ]] || $EDITOR /etc/dkms/zfs.conf
	else
		echo "REMAKE_INITRD=yes" >> /etc/dkms/zfs.conf
	fi

	#ls /usr/lib/modules
		
	"${MYREALDIR}"/zbm_update.bash	

popd

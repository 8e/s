#!/usr/bin/env bash

[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

aptitude versions $(cut -d/ -f1 "${MYREALDIR}"/../layers/zfs/custom.list "${MYREALDIR}"/../lib/zfs.list | sort -u)
"${MYREALDIR}"/zfs-modver.bash

#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly LOGDIR=$("${MYREALDIR}"/ensure-path.bash "/tmp/log/$USER/$(basename ${ME})")
readonly LOGF="${LOGDIR}/$$.log"
readonly BASE_URL="https://repo1.maven.org/maven2/com/madgag/bfg/"
#readonly DWNLD_LINK="https://repo1.maven.org/maven2/com/madgag/bfg/1.13.0/bfg-1.13.0.jar"
readonly JARPLACE="/opt/bfg.jar"
pushd ${MYREALDIR}
	echo java md5sum html2text curl | ./apt-ensure.bash default-jre-headless coreutils html2text curl
	./ensure-path.bash $(dirname "${JARPLACE}")
popd

function get_local_version() {
	[ -e "${JARPLACE}" ] || return 1
	local version=$(java -jar "${JARPLACE}" | awk '{print $2; exit}')
	[[ "${version}" == '' ]] && return 2
	echo "${version}"
}

function get_latest_version() {
	local r
	#[[ $(curl "${BASE_URL}"/maven-metadata.xml | md5sum | cut -f1 -d' ') == $(curl "${BASE_URL}"/maven-metadata.xml.md5) ]] || exit 1
	# curl https://repo1.maven.org/maven2/com/madgag/bfg/ | html2text | awk '/.+\/.+/ && ( $NF == "-" ) {print $1}' | sort -V | tail -1
	r=$("${MYREALDIR}"/curl.bash --quiet --digest md5=$("${MYREALDIR}"/curl.bash "${BASE_URL}/maven-metadata.xml.md5") "${BASE_URL}"/maven-metadata.xml | awk -vFS='[><]' '/latest/ {print $3}')
	if [[ "${r}" == '' ]]; then
		r=$(curl "${BASE_URL}" | html2text | awk '/.+\/.+/ && ( $NF == "-" ) {print $1}' | sort -V | tail -1 | sed 's/\/$//')
	fi
	echo "${r}"
}

function install() {
	local ver=$(get_latest_version)
	echo "Latest version: $ver"
	readonly URL="${BASE_URL}${ver}/bfg-${ver}.jar"
	"${MYREALDIR}"/curl.bash --quiet --digest md5=$("${MYREALDIR}"/curl.bash "${BASE_URL}${ver}/bfg-${ver}.jar.md5") -C - --output "${JARPLACE}" "${URL}" || wget --continue -O "${JARPLACE}" "${URL}"
	"${MYREALDIR}"/focopy.bash "${JARPLACE}" /var/backups/bfg-"${ver}".jar
	get_local_version
}

case ${1} in
	install)
		shift
		install
	;;
	latest)
		shift
		get_latest_version
	;;
	version)
		shift
		get_local_version
	;;
	'')
		"${ME}" -B 1
	;;
	*)
		get_local_version || sudo "${ME}" install
		java -jar "${JARPLACE}" "${@}" | tee "${LOGF}"
		readonly GITD=$(dirname $(awk -vFS=' : ' '/^Using repo :/{print $2}' "${LOGF}"))
		if [ -d "${GITD}" ]; then
			pushd "${GITD}"
				git prune
				git reflog expire --expire=now --all && git gc --prune=now --aggressive
			popd
		fi
	;;
esac

(${DEBUG} || rm -v "${LOGF}") || true

#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
set -o pipefail

readonly F="${*:-'.'}"

function get_blockdev_mounted() {
	(df --output=source "${*}" || return $?) | (tail -n +2 || return $?)
}

function check_in_fstab() {
	awk "\$2==\"${*}\" {print \$1}" /etc/fstab
}

function get_blockdev_from_fstab() {
	local path=$(realpath "${*}")
	local dev=$(check_in_fstab "${path}")
	while [[ "${dev}" == '' ]]; do
		path=$(dirname ${path})
		local dev=$(check_in_fstab "${path}")
	done
	realpath "${dev}"
}

get_blockdev_from_fstab "${F}" || get_blockdev_mounted "${F}"

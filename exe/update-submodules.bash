#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly MYDIR=$(dirname ${0})

pushd ${MYDIR}
	which git || apt-get -y install git
	git submodule update --init
	git pull --recurse-submodules
	git submodule update --recursive
popd

#!/usr/bin/env bash

# https://gitlab.com/Sithuk/ubuntu-server-zfsbootmenu/-/blob/main/ubuntu_server_encrypted_root_zfs.sh

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi

pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash

	if [[ "${2}" != '' ]]; then
		readonly OSID="${2}"
		readonly POOL="${1}"
	else
		source ../z-ensure-poolosid.rc.bash
	fi

	./linux-cmdline-update.bash

popd

#initial_boot_order="$(efibootmgr | grep "BootOrder" | cut -d " " -f 2)"

###Add backup ESPs to the EFI boot manager
#loop_counter="$(mktemp)"
#echo 0 > "$loop_counter" ##Assign starting counter value.
#for disk in "${@}"; do
#	i="$(cat "$loop_counter")"
#	echo "$i"
#	if [ "$i" -eq 0 ];
#	then
#		true
#	else
#		device_name="$(readlink -f /dev/disk/by-id/"${disk}")"
#		efibootmgr --create --disk "${disk}" --label "rEFInd Boot Manager Backup $i" --loader \\EFI\\refind\\refind_x64.efi
#	fi
#	i=$((i + 1)) ##Increment counter.
#	echo "$i" > "$loop_counter"
#done
#
###Adjust ESP boot order
#declare -a EspNums=( $(efibootmgr | grep -v "Backup" | grep -w "rEFInd Boot Manager" | cut -d " " -f 1 | sed 's,Boot0*,,;s,*,,') )
#readonly last_index=$(( "${#EspNums[@]}" - 1 ))
#readonly primary_esp_num=${EspNums[0]}
#readonly last_esp_num=${EspNums[last_index]}
##$(( "$primary_esp_num" + "$num_disks" ))
#			
#i="$primary_esp_num"
#while [ "$i" -ne "$last_esp_num" ]; do
#	if [ "$i" -eq "$primary_esp_num" ]; then
#		echo "$primary_esp_num," > /tmp/revised_boot_order.txt
#	else
#		sed -i "s/$/$i,/g" /tmp/revised_boot_order.txt
#	fi
#	i=$((i + 1))
#done 
#sed -i "s/$/$initial_boot_order/g" /tmp/revised_boot_order.txt
#revised_boot_order="$(cat /tmp/revised_boot_order.txt)"
#efibootmgr -o "$revised_boot_order"

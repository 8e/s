#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly INTO=1

source "${MYREALDIR}"/../defaults.rc.bash

function lets_wait() {
	local i=$INTO
	while sudo fuser /var/lib/dpkg/lock* || lsof /var/lib/dpkg/lock* || sudo fuser /var/lib/apt/lists/lock* || lsof /var/lib/apt/lists/lock* || (ps -ef | egrep -v "sh|dnsmasq|file|cmd|mount|tmp|key|update|tsp|methods|grep|fuser|lsof|tee|$0 $*" | egrep -w 'apt|aptitude|dpkg|cupt'); do
		echo "waiting for lock(s) $i sec"
		ps -ef | awk '/unattended-upgr/ {print $2}' | while read p; do
			kill $p &
		done
		if [ -f /var/log/unattended-upgrades/unattended-upgrades.log ]; then
			while sudo fuser /var/log/unattended-upgrades/unattended-upgrades.log || lsof /var/log/unattended-upgrades/unattended-upgrades.log; do
				sleep $i
			done
			rm -v /var/log/unattended-upgrades/unattended-upgrades.log >/dev/null  &
		fi
		ps -ef | awk '/unattended-upgr/ {print $2}' | while read p; do
			kill -KILL $p &
		done
		sleep $i
		i=$(( $i * 2 ))
	done 2>/dev/null
}

if [[ "$OSTYPE" != "linux-gnu"* ]]; then
	readonly backend=brew
elif [[ "${BACKEND}" == '' ]] && which cupt >/dev/null; then
	readonly BACKEND=("${MYREALDIR}"/cupt.bash)
elif which aptitude >/dev/null; then
	if which ionice >/dev/null; then
		readonly BACKEND=(nice -n -20 ionice -c0 aptitude -o DPkg::Lock::Timeout=-1 --full-resolver --allow-new-upgrades --allow-new-installs)
	else
		readonly BACKEND=(nice -n -20 aptitude -o DPkg::Lock::Timeout=-1 --full-resolver --allow-new-upgrades --allow-new-installs)
	fi
else
	readonly BACKEND=(apt -o DPkg::Lock::Timeout=-1 --allow-downgrades)
fi

which ${BACKEND[0]} || exit 1

[ -e /usr/share/initramfs-tools/hooks/plymouth ] && sed -i 's/^set -e$//g' /usr/share/initramfs-tools/hooks/plymouth &

which update-initramfs >/dev/null || sudo "${MYREALDIR}"/focopy.bash /bin/true /usr/sbin/update-initramfs

which fuser lsof >/dev/null || sudo ${BACKEND[*]} -y install psmisc lsof

(( ${#} == 0 )) || (
	lets_wait
	sudo dpkg --configure -a 2>/dev/null || true
	lets_wait
	"${ME}"
	lets_wait
	sudo ${BACKEND[*]} install -yf 2>&1
	lets_wait
	"${ME}"
	lets_wait
	sudo dpkg --configure -a
	lets_wait
	echo ${BACKEND[*]} ${*}
	sudo ${BACKEND[*]} ${*} 2>&1 ) | (grep -ve '^W.*Target.*is configured multiple times.*\|^W.*The key.*are ignored as the file has an unsupported filetype' || true)

[ -e /usr/share/initramfs-tools/hooks/plymouth ] && sed -i 's/^set -e$//g' /usr/share/initramfs-tools/hooks/plymouth &

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

tsp
exit

[[ "$OSTYPE" == "linux-gnu"* ]] && "${MYREALDIR}"/apt-ensure-cmd.bash task-spooler tsp

for p in $("${MYREALDIR}"/git-staged.bash); do
	tsp -d "$(pwd)" "${MYREALDIR}"/git-fixhash.bash "${p}"
done
tsp -f true

#!/usr/bin/env bash

set -ex

readonly MYREALDIR=$(dirname $(realpath ${0}))

readonly TARGETD=$("${MYREALDIR}"/git-installrepo.bash https://bitbucket.org/mburr/hr.git)

"${MYREALDIR}"/apt-ensure.bash libbsd-dev bmake #freebsd-mk
pushd "${TARGETD}"
	# bmake: /usr/share/bmake/mk-netbsd/bsd.prog.mk
	#freebsd-mk: /usr/share/mk-freebsd/bsd.prog.mk
	# .include <bsd.prog.mk>
	for f in /usr/share/bmake/mk-netbsd/bsd.*.mk; do
		[ -e $(basename "${f}") ] || ln -sv "${f}"
	done
	bmake
	mv -v hr /usr/local/bin/
	#bmake install
popd
"${MYREALDIR}"/apt.bash purge -y bmake libbsd-dev

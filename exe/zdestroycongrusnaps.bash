#!/usr/bin/env bash

set -xe

readonly SNAPSHOTTABLE_DS=$(zfs list -o name,com.sun:auto-snapshot,type -t filesystem,volume,bookmark |  awk '$2=="true" {print $1}')
readonly TF=$(mktemp)
readonly ERRLOGF=$(mktemp)
readonly DIFFTIMEOUT="0.3s"

function compare_snapshot_pair() {
	local dataset="${1%@*}"
	local canmount=$(zfs get -H canmount "${dataset}" || true)
	local exit_code=0
	[[ "${canmount}" == 'off' ]] && zfs destroy "${1}"
	[ ${#} -eq 2 ] || return 0
	if [[ "${dataset}" == "${2%@*}" ]]; then
		[[ "${canmount}" == 'off' ]] && zfs destroy "${2}" && return
	else
		echo "Datasets ${dataset} != ${2%@*}"
		return 1
	fi
	timeout ${DIFFTIMEOUT} zfs diff ${1} ${2} 1> ${TF} 2> "${ERRLOGF}" || exit_code=$?

		if [ ${exit_code} -eq 124 ] || [[ -s ${TF} ]]; then
			echo "there is a diff"
		elif [[ $(cat "${ERRLOGF}") =~ ^"Cannot diff an unmounted snapshot".* ]]; then
			if zfs mount ${dataset}; then
				compare_snapshot_pair ${1} ${2}
				zfs umount ${dataset}
			else
				zfs destroy ${1}
				zfs destroy ${2}
			fi
		else
			echo nodiff
			amount_of_firsts_siblings=$(zfs list -t snapshot | grep -- "${1#*@}" | wc -l)
			amount_of_seconds_siblings=$(zfs list -t snapshot | grep -- "${2#*@}" | wc -l)
			if (( amount_of_firsts_siblings > amount_of_seconds_siblings )); then
				zfs destroy ${2}
			else
				zfs destroy ${1}
			fi
		fi

	rm -f ${TF} ${ERRLOGF}
}

for p in ${SNAPSHOTTABLE_DS}; do
	echo ${p}
	snapslist=$(zfs list -t snapshot -o name -s creation ${p} -H)
	if [[ "${snapslist}" == '' ]]; then
		exit 0
	else
		echo "${snapslist}" | ifne xargs -L2 echo | while read line; do
			compare_snapshot_pair $line
		done
	fi
done


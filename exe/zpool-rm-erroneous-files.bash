#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash

	which ifne awk || ./apt-ensure.bash moreutils gawk task-spooler

	zpool status -v ${*} | ifne awk -vRS='errors: Permanent errors have been detected in the following files:' 'NR==1 {RS="\n"; $1=""} NR!=1  && ! /^[[:space:]]*$/ {$1=$1; print}' | sed 's|^/dir_[[:digit:]]+||g' | ifne ./xblissargs.bash -l rm -rvf &
	zpool status -v ${*} | ifne awk -vRS='errors: Permanent errors have been detected in the following files:' 'NR==1 {RS="\n"; $1=""} NR!=1  && ! /^[[:space:]]*$/ {$1=$1; print}' | sed 's|^/dir_[[:digit:]]+||g' | while read p; do
		tsp rm -rvf "${p}"
	done

	zpool status -v ${*} | ifne awk -vRS='errors: Permanent errors have been detected in the following files:' 'NR==1 {RS="\n"; $1=""} NR!=1  && ! /^[[:space:]]*$/ {$1=$1; print}' | grep 'dir_[[:digit:]]\+' | sed 's|dir_[[:digit:]]\+|*|g; s| \+.*|*|g' | while read line; do echo find /mnt/z -ipath "$line/*"; done | sort -u

	# rm -rvf `zpool status -v ${*} | ifne awk -vRS='errors: Permanent errors have been detected in the following files:' 'NR==1 {RS="\n"; $1=""} NR!=1  && ! /^[[:space:]]*$/ {$1=$1; print}' | awk -vFS=/ '/dir_[[:digit:]]+/ {print $1"/"$2"/"$3"/"$4"/"$5"/"$6"/"$7}' |sort -u`

popd >/dev/null

zpool status -v

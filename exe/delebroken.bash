#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
	source ../defaults.rc.bash
popd >/dev/null

"${MYREALDIR}"/find-broken-symlinks.sh | "${MYREALDIR}"/xargs.bash -l tsp rm -v 

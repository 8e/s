#!/usr/bin/env bash

set -ex

readonly MYREALDIR=$(dirname $(realpath ${0}))
readonly URL=https://cdn.download.clearlinux.org/releases/33040/clear/x86_64/os/Packages/
readonly KEEPD="${MYREALDIR}"/../../../soft/linux/clearlinux
readonly LIST="${KEEPD}"/packages.list
readonly BACKEND="${MYREALDIR}/apt.bash"
readonly CACHED=/var/cache/apt/archives
readonly BUILDD=/tmp/$(basename ${0})
readonly LOGF="/var/log/$(basename ${0}).log"

"${MYREALDIR}"/apt-ensure.bash alien html2text rpm git curl flex bison libssl-dev util-linux rsync git make gcc build-essential

function list() {
	"${MYREALDIR}"/ensure-path.bash "${KEEPD}"
	curl "${URL}" | tee "${LIST}" | html2text
}

function get_package_url() {
	local package_name=${1}
	echo "${URL}"$(curl "${URL}" | awk -F\" "/>${package_name}-[-.0-9]+.x86_64.rpm<\/a>/ {print \$2}")
}

function get_package() {
	local package_name=${1}
	local logtmpfile=$(mktemp /tmp/$(basename "${package_name}").XXXXXX)
	local package_url=$(get_package_url ${package_name})
	"${MYREALDIR}"/ensure-path.bash "${BUILDD}" >> "${LOGF}"
	pushd "${BUILDD}" >> "${LOGF}"
		wget -c "${package_url}" 2>&1 | tee ${logtmpfile} >> "${LOGF}"
		local downloaded_fname=$(tail -2 ${logtmpfile} | awk -vFS='‘|’' '/saved/ {print $2}')
		[[ "${downloaded_fname}" == '' ]] && downloaded_fname=$(basename "${package_url}")
		rm ${logtmpfile} &
	popd >> "${LOGF}"
	echo "${downloaded_fname}"
}

function process_package() {
	local package_name=${1}
	local package_file=$(get_package ${package_name})
	"${MYREALDIR}"/ensure-path.bash "${BUILDD}"
	pushd "${BUILDD}"
		alien -i "${package_file}" -v
	popd
}

function ProcessPackages() {
	for p in ${Packages[*]}; do
		process_package $p
	done
}

while [ ${#} -gt 0 ]; do
        case ${1} in
                \-*)
                        opt=${1}
                        shift # past option
                        AptListsOptions+=${opt}
                ;;

                list)
			shift
			list
			exit 0
		;;

                url)
                        shift # past action
			readonly Packages=${*}
			for p in ${Packages[*]}; do
				get_package_url ${p}
			done
			exit 0
		;;

                search)
			shift
			list | grep ${*}
			exit 0
		;;

                *upgrade | install)
                        shift # past action
			readonly Packages=${*}
			"${BACKEND}" install -fy || true
                        which "${BACKEND}" && "${BACKEND}" markauto ${Packages[*]} || true
			rpm --rebuilddb
			pushd "${BUILDD}"
				"${MYREALDIR}"/ensure-path.bash "${KEEPD}"
				cp -a "${KEEPD}"/* ./
				find -type d -mindepth 1 -maxdepth 1 -execdir rm -rvf '{}' \;
				ProcessPackages ${Packages[*]}
				rsync --open-noatime -svauhHPxlit --progress * "${KEEPD}"/
				find -type d -mindepth 1 -maxdepth 1 -execdir rm -rvf '{}' \;
				cp -fl "${KEEPD}"/* "${CACHED}"/ 2>/dev/null || rsync --open-noatime -svauhHPxlit --progress * "${CACHED}"/
				rm -v *
			popd
			"${MYREALDIR}"/apt-update.bash
			"${BACKEND}" install -fy
                ;;

                *)
			exit 1
                ;;
        esac
done


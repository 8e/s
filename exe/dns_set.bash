#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly CONFD=/etc/resolvconf/resolv.conf.d/
readonly RESOLV_FILE=/run/resolvconf/resolv.conf
readonly PREVCONTENT=$(cat /etc/resolv.conf | true)
declare -a PrevServers=$(awk '/nameserver/ {print $2}' /etc/resolv.conf | true)
declare -a ResolvLinks=(/run/systemd/resolve/stub-resolv.conf /var/run/NetworkManager/resolv.conf /etc/resolv.conf) 
if which resolvconf; then
	declare -a IFaces=($(ip link | awk -F'[: ]' '{print $3}' | egrep -v '^[[:space:]]*(lo)?[[:space:]]*$'))
fi

function addserver() {
	local name="${*}"
	if ping -w 1 "${name}" | httping -c 1 -t 1s "${name}"; then  
		echo "nameserver ${name}" | sudo tee -a "$RESOLV_FILE" | sudo tee "${CONFD}/${name}"
		grep "DNS=${name}" /etc/systemd/resolved.conf.d/custom.conf || echo "DNS=${name}" | sudo tee -a /etc/systemd/resolved.conf.d/custom.conf
		if which resolvconf; then
			for iface in "${IFaces[@]}"; do
				echo "nameserver ${name}" | sudo resolvconf -a $iface
			done
		fi
	fi
}

tsp resolvectl flush-caches
sudo "${MYREALDIR}"/ensure-path.bash "${CONFD}"
sudo "${MYREALDIR}"/ensure-path.bash "/etc/systemd/resolved.conf.d"
#sudo "${MYREALDIR}"/dlt.bash /etc/resolv.conf || true

## Some examples of DNS servers which may be used for DNS= and FallbackDNS=:
## Cloudflare: 1.1.1.1#cloudflare-dns.com 1.0.0.1#cloudflare-dns.com 2606:4700:4700::1111#cloudflare-dns.com 2606:4700:4700::1001#cloudflare-dns.com
## Google:     8.8.8.8#dns.google 8.8.4.4#dns.google 2001:4860:4860::8888#dns.google 2001:4860:4860::8844#dns.google
## Quad9:      9.9.9.9#dns.quad9.net 149.112.112.112#dns.quad9.net 2620:fe::fe#dns.quad9.net 2620:fe::9#dns.quad9.net
(( $# > 0 )) || set -- 1.1.1.1 1.0.0.1 2606:4700:4700::1111 2606:4700:4700::1001 8.8.8.8 8.8.4.4 2001:4860:4860::8888 2001:4860:4860::8844 9.9.9.9 149.112.112.112 2620:fe::fe 2620:fe::9

for s in "${@}"; do 
	echo "${PrevServers[@]}" | grep "${s}" || addserver "${s}"
done

(( "${#PrevServers[@]}" > 0 )) && for s in "${PrevServers[@]}"; do 
	addserver "${s}"
done
#echo "${PREVCONTENT}" | sudo tee -a $RESOLV_FILE

for resolv_lnk in "${ResolvLinks[@]}"; do
	sudo "${MYREALDIR}"/ensure-path.bash $(dirname "${resolv_lnk}")
	echo "${resolv_lnk}" | sudo "${MYREALDIR}/ensure-symlink.bash" "$RESOLV_FILE"
done

if which resolvectl; then
	resolvectl statistics
	resolvectl status
elif which resolvconf; then
	resolvconf -l
fi

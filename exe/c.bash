#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

[[ $DBUS_SESSION_BUS_PID != '' ]] || export $(dbus-launch)

if which chromium &>/dev/null; then
	readonly DEFCMD=chromium
else
	readonly DEFCMD=chromium-browser
fi
readonly PROG=${*:-"${DEFCMD}"}
"${MYREALDIR}"/is_int.bash "${NICENESS}" || readonly NICENESS=-11

sudo renice -n "${NICENESS}" $$
sudo ionice -c 0 -n 0 -p $$

/usr/bin/env nice -n "${NICENESS}" ionice -c0 ${PROG} --log-file=/tmp/log/$USER/$1.$$.log --disable-hang-monitor --disable-crashpad --disable-breakpad --disable-crash-reporter --disable-dinosaur-easter-egg --disable-logging --disable-logging-redirect --disable-login-animations --disable-login-screen-apps --no-referrers --disable-oopr-debug-crash-dump --disable-pnacl-crash-throttling --disable-pnacl-crash-throttling --no-report-upload --no-crash-upload --no-pings --disk-cache-size=0 --media-cache-size=0 --no-sandbox &
p=$!
sudo renice -n "${NICENESS}" ${p}
sudo ionice -c 0 -n 0 -p ${p}

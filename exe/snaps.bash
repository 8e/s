#!/usr/bin/env bash
set -xe
readonly ME=$(realpath "${0}")
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

declare -a Snaps=(chromium chromium-ffmpeg telegram-desktop pocket-browser opera beebeep '--beta --devmode bbcp' '--classic slack' 'viber-mtd --edge' '--classic aws-cli' 'doctl')

echo tsp snap | "${MYREALDIR}"/apt-ensure.bash task-spooler snapd

for snapline in "${Snaps[@]}"; do
	sudo -u "${DEFUSER}" tsp snap ${1:-install} $snapline 
done

#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x
readonly ME=$(realpath $(which "${0}"))
[ -e /var/git ] || ln -sv $(dirname "${ME}")/../../ /var/git
[ -e /var/git/s ] || ln -sv $(dirname "${ME}")/.. /var/git
readonly MYREALDIR=/var/git/s/exe

source "${MYREALDIR}"/../defaults.rc.bash

readonly TARGDIR="/usr/sbin"
readonly ORIGPLACE="${TARGDIR}"/grub-probe.orig
readonly COWEXP=".*: error: failed to get canonical path of \`/cow'."
readonly THEPATH="${TARGDIR}"/"grub-probe"
readonly LOGF=/var/log/$(basename ${ME})

exec &> >(tee "${LOGF}")

declare -a Params=()

function install() {
	set -x
	for f in $(find /var/lib/dpkg/info/plymouth*p* -executable); do 
		cp "${f}" /var/backups/ -v
		echo "exit 0" > "${f}"
	done
	ps -ef | grep 'purge -y plymouth' | grep -v grep || if which aptitude>/dev/null; then
		"${MYREALDIR}"/apt.bash purge -y plymouth~s
	else
		"${MYREALDIR}"/apt.bash purge -y plymouth-theme-ubuntustudio
	fi
	diff -dq "${ME}" "${THEPATH}" || if (( $? == 1)); then
		if [ -e "${ORIGPLACE}" ]; then
			file "${THEPATH}" | grep ELF && "${MYREALDIR}"/focopy.bash "${THEPATH}" "${ORIGPLACE}" 
		else
			"${MYREALDIR}"/focopy.bash "${THEPATH}" "${ORIGPLACE}"
		fi
		"${MYREALDIR}"/focopy.bash "${ME}" "${THEPATH}"
	fi
}

while [ ${#} -gt 0 ]; do
	case ${1} in
		'install')
			install
			exit 0
		;;
		'/')
			echo "zfs"
			exit 0
		;;
		*)
			Params+=("${1}")
                        shift # past param
		;;
	esac
done

readonly ORIGOUT=$("${ORIGPLACE}" ${Params[@]} 2>&1 )

if [[ "${ORIGOUT}" =~ ${COWEXP} ]]; then
	exit 0
else
	"${ORIGPLACE}" ${Params[@]}
fi

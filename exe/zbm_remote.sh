#!/bin/sh
		##https://github.com/zbm-dev/zfsbootmenu/wiki/Remote-Access-to-ZBM
		apt install -y dracut-network dropbear
		
		git -C /tmp clone 'https://github.com/dracut-crypt-ssh/dracut-crypt-ssh.git'
		mkdir /usr/lib/dracut/modules.d/60crypt-ssh
		cp /tmp/dracut-crypt-ssh/modules/60crypt-ssh/* /usr/lib/dracut/modules.d/60crypt-ssh/
		rm /usr/lib/dracut/modules.d/60crypt-ssh/Makefile
		
		##comment out references to /helper/ folder from module-setup.sh
		sed -i \
			-e 's,  inst "\$moddir"/helper/console_auth /bin/console_auth,  #inst "\$moddir"/helper/console_auth /bin/console_auth,' \
			-e 's,  inst "\$moddir"/helper/console_peek.sh /bin/console_peek,  #inst "\$moddir"/helper/console_peek.sh /bin/console_peek,' \
			-e 's,  inst "\$moddir"/helper/unlock /bin/unlock,  #inst "\$moddir"/helper/unlock /bin/unlock,' \
			-e 's,  inst "\$moddir"/helper/unlock-reap-success.sh /sbin/unlock-reap-success,  #inst "\$moddir"/helper/unlock-reap-success.sh /sbin/unlock-reap-success,' \
			"$modulesetup"
		
		##create host keys
		mkdir -p /etc/dropbear
		ssh-keygen -t rsa -m PEM -f /etc/dropbear/ssh_host_rsa_key -N ""
		ssh-keygen -t ecdsa -m PEM -f /etc/dropbear/ssh_host_ecdsa_key -N ""
		
		mkdir -p /etc/cmdline.d
		echo "ip=dhcp rd.neednet=1" > /etc/cmdline.d/dracut-network.conf ##Replace "dhcp" with specific IP if needed.
		
		##add remote session welcome message
		cat <<-EOF >/etc/zfsbootmenu/dracut.conf.d/banner.txt
			Welcome to the ZFSBootMenu initramfs shell. Enter "zbm" to start ZFSBootMenu.
		EOF
		chmod 755 /etc/zfsbootmenu/dracut.conf.d/banner.txt
		
		sed -i 's,  /sbin/dropbear -s -j -k -p \${dropbear_port} -P /tmp/dropbear.pid,  /sbin/dropbear -s -j -k -p \${dropbear_port} -P /tmp/dropbear.pid -b /etc/banner.txt,' /usr/lib/dracut/modules.d/60crypt-ssh/dropbear-start.sh
		
		##Copy files into initramfs
		sed -i '$ s,^},,' "$modulesetup"
		echo "  ##Copy dropbear welcome message" | tee -a "$modulesetup"
		echo "  inst /etc/zfsbootmenu/dracut.conf.d/banner.txt /etc/banner.txt" | tee -a "$modulesetup"
		echo "}" | tee -a "$modulesetup"
		
		cat <<-EOF >/etc/zfsbootmenu/dracut.conf.d/dropbear.conf
			## Enable dropbear ssh server and pull in network configuration args
			##The default configuration will start dropbear on TCP port 222.
			##This can be overridden with the dropbear_port configuration option.
			##You do not want the server listening on the default port 22.
			##Clients that expect to find your normal host keys when connecting to an SSH server on port 22 will \
			##refuse to connect when they find different keys provided by dropbear.
			add_dracutmodules+=" crypt-ssh "
			install_optional_items+=" /etc/cmdline.d/dracut-network.conf "
			## Copy system keys for consistent access
			dropbear_rsa_key=/etc/dropbear/ssh_host_rsa_key
			dropbear_ecdsa_key=/etc/dropbear/ssh_host_ecdsa_key
			##Access by authorized keys only. No password.
			##By default, the list of authorized keys is taken from /root/.ssh/authorized_keys on the host.
			##Remember to "generate-zbm" after adding the remote user key to the authorized_keys file. 
			##The last line is optional and assumes the specified user provides an authorized_keys file \
			##that will determine remote access to the ZFSBootMenu image.
			##Note that login to dropbear is "root" regardless of which authorized_keys is used.
			#dropbear_acl=/home/${user}/.ssh/authorized_keys
		EOF
		
		##Increase ZFSBootMenu timer to allow for remote connection
		sed -i 's,zbm.timeout=$timeout_zbm_no_remote_access,zbm.timeout=$timeout_zbm_remote_access,' /boot/efi/EFI/ubuntu/refind_linux.conf
		
		systemctl stop dropbear
		systemctl disable dropbear
		
		generate-zbm --debug

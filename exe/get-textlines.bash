#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
set -o pipefail

shopt -s checkwinsize
# https://unix.stackexchange.com/questions/184009/how-do-i-find-number-of-vertical-lines-available-in-the-terminal

function maybefinish() {
	if [[ ${LINES} != '' ]]; then
		echo ${LINES}
		exit 0
	fi
}

maybefinish
LINES=$(tput lines)
maybefinish
LINES=$(stty size | cut '-d ' -f1)
maybefinish
LINES=$(stty -a | tr \; \\012 | grep rows | tr -d ' rows')
maybefinish

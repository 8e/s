#!/usr/bin/env bash

set -e
DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

if [[ "${BACKUP_ROOT}" == '' ]]; then
	readonly BACKUP_ROOT="/var/backups"
fi
readonly DATETIME="$(date '+%F')"
readonly BACKUP_PATH="${BACKUP_ROOT}/${DATETIME}"
readonly LATEST_LINK="${BACKUP_ROOT}/latest"

set -o errexit
set -o nounset
set -o pipefail

ionice -c3 -n6 -p $$
renice -n 18 $$

"${MYREALDIR}"/ensure-path.bash "${BACKUP_ROOT}"

declare -a Opts=(-svauchHPXlitA --open-noatime --delete --link-dest="${LATEST_LINK}" ${*} "${BACKUP_PATH}")

nice -19 ionice -c3 -n7 "${MYREALDIR}"/rsync.bash --ignore-errors --itemize-changes --inplace --backup-dir="${BACKUP_ROOT}" "${Opts[@]}"

rm -rf "${LATEST_LINK}"
ln -sv "${BACKUP_PATH}" "${LATEST_LINK}"

#/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

for f in ${*}; do
	tmpfile=$(mktemp /tmp/$(basename $0).XXXXXX)
	sort -u "${f}" > ${tmpfile}
	declare -a HardLinks=$("${MYREALDIR}"/hardlinks-to.bash "${f}")
	mv ${tmpfile} ${f}
	if (( "${#HardLinks[@]}" > 1 )); then
		for hl in "${HardLinks[@]}"; do
			cp -lvf "${f}" "${hl}"
		done
	fi
        pushd $(dirname ${f})
                git add $(basename ${f}) "${HardLinks[@]}"
        popd
done

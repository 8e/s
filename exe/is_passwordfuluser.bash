#!/usr/bin/env bash

set -e

readonly ME=$(realpath $(which ${0}))
readonly MYREALDIR="$(dirname ${ME})"
[ $DEBUG ] && set -x

source "${MYREALDIR}"/../defaults.rc.bash


function status() {
	passwd --status ${1} | awk '$2==NP {exit 1} END {exit 0}'
}

function shadow() {
	awk -F: "\$1==\"${1}\" { if (\$2==\"*\") exit 1; else exit 0; }" /etc/shadow
}

status $1
shadow $1

#!/usr/bin/env bash

# Function to display usage instructions
usage() {
    echo "Usage: $0 <database> [key]"
    echo "Supported databases: passwd, group"
    exit 1
}

# Check if database is provided
if [[ -z "$1" ]]; then
    usage
elif (( ${#} == 1 )); then
		database=passwd
    key=$1
else
		database="$1"
		key="$2"
fi

case "$database" in
    passwd)
        if [[ -z "$key" ]]; then
            # List all users (equivalent to cat /etc/passwd)
            dscl . -list /Users | while read user; do
                username=$(dscl . -read "/Users/$user" RecordName 2>/dev/null | awk '{print $2}')
                uid=$(dscl . -read "/Users/$user" UniqueID 2>/dev/null | awk '{print $2}')
                gid=$(dscl . -read "/Users/$user" PrimaryGroupID 2>/dev/null | awk '{print $2}')
                realname=$(dscl . -read "/Users/$user" RealName 2>/dev/null | sed -e 's/^RealName: //' | tr '\n' ' ' | sed 's/ $//')
                homedir=$(dscl . -read "/Users/$user" NFSHomeDirectory 2>/dev/null | awk '{print $2}')
                shell=$(dscl . -read "/Users/$user" UserShell 2>/dev/null | awk '{print $2}')

                if [[ -n "$username" ]]; then
                    echo "$username:*:$uid:$gid:$realname:$homedir:$shell"
                fi
            done
        else
            # Display specific user details
            if dscl . -read "/Users/$key" &>/dev/null; then
                username=$(dscl . -read "/Users/$key" RecordName | awk '{print $2}')
                uid=$(dscl . -read "/Users/$key" UniqueID | awk '{print $2}')
                gid=$(dscl . -read "/Users/$key" PrimaryGroupID | awk '{print $2}')
                realname=$(dscl . -read "/Users/$key" RealName | sed -e 's/^RealName: //' | tr '\n' ' ' | sed 's/ $//')
                homedir=$(dscl . -read "/Users/$key" NFSHomeDirectory | awk '{print $2}')
                shell=$(dscl . -read "/Users/$key" UserShell | awk '{print $2}')

                echo "$username:*:$uid:$gid:$realname:$homedir:$shell"
            elif dscl . -list /Users | grep "${key}"; then
								true
						else
								
                echo "No such user: $key"
                exit 2
            fi
        fi
        ;;
    group)
        if [[ -z "$key" ]]; then
            # List all groups (equivalent to cat /etc/group)
            dscl . -list /Groups | while read group; do
                groupname=$(dscl . -read "/Groups/$group" RecordName 2>/dev/null | awk '{print $2}')
                gid=$(dscl . -read "/Groups/$group" PrimaryGroupID 2>/dev/null | awk '{print $2}')
                members=$(dscl . -read "/Groups/$group" GroupMembership 2>/dev/null | sed -e 's/^GroupMembership: //' | tr '\n' ', ' | sed 's/, $//')

                if [[ -n "$groupname" ]]; then
                    echo "$groupname:*:$gid:$members"
                fi
            done
        else
            # Display specific group details
            if dscl . -read "/Groups/$key" &>/dev/null; then
                groupname=$(dscl . -read "/Groups/$key" RecordName | awk '{print $2}')
                gid=$(dscl . -read "/Groups/$key" PrimaryGroupID | awk '{print $2}')
                members=$(dscl . -read "/Groups/$key" GroupMembership | sed -e 's/^GroupMembership: //' | tr '\n' ', ' | sed 's/, $//')

                echo "$groupname:*:$gid:$members"
						elif dscl . -list /Groups | grep "${key}"; then
								true
            else
                echo "No such group: $key"
                exit 2
            fi
        fi
        ;;
    *)
        echo "Unsupported database: $database"
        usage
        ;;
esac

exit 0

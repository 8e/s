#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly POOL=${1:-rpool}
readonly INPOOL_LISTF=$("${MYREALDIR}"/ensure-path.bash /tmp/caches/"${USER}")/$(basename "${ME}")."${POOL}".list

zpool list -Hvo name "${POOL}" | awk '/ata/ {print $1}' > "${INPOOL_LISTF}"
ls -A "${@}" /dev/disk/by-id | grep -vf "${INPOOL_LISTF}"  | egrep -v wwn
rm "${INPOOL_LISTF}"

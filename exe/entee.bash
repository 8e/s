#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}

${DEBUG} && set -x
readonly ME="$(realpath $(which ${0}))"
readonly MYREALDIR="$(dirname ${ME})"

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	(which ifne | "${MYREALDIR}"/apt-ensure.bash moreutils) >/dev/null
fi

case "${1}" in
	'-a')
		shift
		declare -a Cmd=(tee -a)
	;;

	'-na'|'-an')
		shift
		declare -a Cmd=(cat \>\>)
	;;

	'-n')
		shift
		declare -a Cmd=(cat \>)
	;;

	*)
		declare -a Cmd=(tee)
	;;
esac
whereto="${*}"
bash -c "'${MYREALDIR}'/ensure-path.bash -n '$(dirname "${whereto}")'; ifne ${Cmd[*]} '${whereto}'"
#${Cmd[*]} "${whereto}"

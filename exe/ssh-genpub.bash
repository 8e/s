#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

openssl rsa -in "${*}" -pubout -out "${*}".pub || ssh-keygen -y -f "${*}" > "${*}".pub

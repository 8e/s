#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))

dd if=/dev/zero of="${1}" bs=${2} count=${3:-1}

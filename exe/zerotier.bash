#!/bin/sh

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

"${MYREALDIR}"/apt-ensure.bash zerotier-one libssl1.1

sudo systemctl restart zerotier-one
if [ ! -e /var/lib/zerotier-one/zerotier-one.pid ] || ! ps -p $(cat /var/lib/zerotier-one/zerotier-one.pid); then
	sudo systemctl restart zerotier-one
fi
if [ ! -e /var/lib/zerotier-one/zerotier-one.pid ] || ! ps -p $(cat /var/lib/zerotier-one/zerotier-one.pid); then
	sudo /usr/sbin/zerotier-one -d
fi
sudo /usr/sbin/zerotier-cli join ${1}
sudo /usr/sbin/zerotier-cli info

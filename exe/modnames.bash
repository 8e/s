#!/bin/bash 

set -e

find /lib/modules/$(uname -r) -type f -name '*.ko*' | awk -F'/|\\.' '{print $(NF-1)}'

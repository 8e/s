#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

pushd "${MYREALDIR}"

	sudo ./apt-update.bash

	sudo aptitude dist-upgrade -y

popd

#!/usr/bin/env python3

import os
import sys
import operator
from datetime import datetime

def get_size(path):
    total = 0
    if os.path.isdir(path) and not os.path.islink(path):
        with os.scandir(path) as it:
            for entry in it:
                if entry.is_file() or os.path.islink(entry):
                    total += entry.stat().st_size
                elif entry.is_dir():
                    total += get_size(entry.path)
    elif os.path.isfile(path) or os.path.islink(path):
        total = os.stat(path).st_size
    return total

def get_depth(path):
    return path.count(os.path.sep)

def get_mtime(path):
    return os.stat(path).st_mtime

sort_key_funcs = {
    'size': get_size,
    'depth': get_depth,
    'age': get_mtime
}

def sort_files(paths, sort_key, reverse=False):
    sort_func = sort_key_funcs.get(sort_key)
    if sort_func is None:
        print(f"Unknown sort key: {sort_key}")
        sys.exit(1)

    path_stat_pairs = []
    for path in paths:
        abs_path = os.path.abspath(path)
        try:
            if os.path.isdir(abs_path) and not os.path.islink(abs_path):
                for dirpath, dirnames, filenames in os.walk(abs_path):
                    for filename in filenames:
                        filepath = os.path.join(dirpath, filename)
                        stat = sort_func(filepath)
                        path_stat_pairs.append((filepath, stat))
            else:
                stat = sort_func(abs_path)
                path_stat_pairs.append((abs_path, stat))
        except FileNotFoundError:
            pass

    path_stat_pairs.sort(key=operator.itemgetter(1), reverse=reverse)
    for path, stat in path_stat_pairs:
        print(path)

def main():
    if len(sys.argv) != 3:
        print(f"Usage: {sys.argv[0]} asc|desc size|depth|age")
        sys.exit(1)

    order, sort_key = sys.argv[1:]
    reverse = (order.lower() == 'desc')
    paths = [line.strip() for line in sys.stdin]

    sort_files(paths, sort_key, reverse)

if __name__ == '__main__':
    main()

#!/usr/bin/env bash

set -ex
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

declare -a Executioner=($(which bash) '-xem')
declare -a PaShebang=('#!' ${Executioner[@]})
readonly TMPDIR=$("${MYREALDIR}"/ensure-path.bash /tmp/caches/"${USER}")
(( ${UID} != 0 )) || chmod 1777 /tmp/caches

if [[ "${SCREENN}" == '' ]]; then
	readonly SCREENN=$(basename "${*}")
fi

if [[ "${WINDOWN}" == '' ]]; then
	readonly WINDOWN=$(basename "${*}")
fi

source "${MYREALDIR}"/../defaults.rc.bash

function write_jobf() {
        local jobf="${*}"
        echo "${PaShebang[@]}" > "${jobf}"
        echo "source \"${MYREALDIR}\"/../defaults.rc.bash" >> "${jobf}"
        cat >> "${jobf}"
        ${DEBUG} || (echo "rm -v \"\$0\" &" >> "${jobf}")
	echo "sudo screen -wipe &" >> "${jobf}"
	echo "screen -wipe &" >> "${jobf}"
        chmod +x "${jobf}"
}

sudo chmod o+rwxt /run /var/run &

sudo "${MYREALDIR}"/ensure-path.bash /tmp/caches/root /run/screen &

sudo chmod u+rwx,g+rwx,o+rwxt /run /var/run /run/screen &

"${MYREALDIR}"/apt-ensure.bash screen

if (( ${#} == 1 )); then
	readonly CMD="${*}"
else
	readonly CMD="${TMPDIR}"/$(basename "${ME}").$$.bash
	if (( ${#} == 0 )); then
		write_jobf "${CMD}"
	else
		echo "${*}" | write_jobf "${CMD}"
	fi
fi

screen -S "${SCREENN}" -t "${WINDOWN}" -Adm "${CMD}" || screen -S "${SCREENN}" -X screen -t "${WINDOWN}" -Adm "${CMD}"
	
sudo screen -wipe &
screen -wipe &

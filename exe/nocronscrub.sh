#!/bin/sh

set -xe

[ -e /etc/cron.d/zfsutils-linux ] && mv /etc/cron.d/zfsutils-linux /var/backups/
[ -e /etc/cron.d/e2scrub_all ] && mv /etc/cron.d/e2scrub_all /var/backups/

systemctl restart cron &

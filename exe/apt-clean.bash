#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

apt-get -o DPkg::Lock::Timeout=-1 --yes autoremove

# If not using /hostcache, clean the cache
[ -d /hostcache ] || apt-get -o DPkg::Lock::Timeout=-1 clean

if which aptitude; then
	aptitude -o DPkg::Lock::Timeout=-1 -y clean
	aptitude -o DPkg::Lock::Timeout=-1 -y autoclean &
fi

rm -rvf /var/cache/apt/archives/* &

"${MYREALDIR}"/apt-update.bash

wait

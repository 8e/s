#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly MYDIR="$(dirname $(realpath ${0}))"

"${MYDIR}"/apt-ensure.bash sysfsutils zfsutils-linux

zpool export -av &

systemctl stop zed zfs-load-module.service zfs-mount.service zfs-share.service zfs-volume-wait.service zrepl.service zsys-commit.service zsysd.socket zfs-zed zfs-import.target zfs-volumes.target zfs.target zsys-gc.timer &

for device in /sys/block/sd*; do
     (udevadm info --query=property --path=$device | grep -q "^ID_BUS=usb") && removable_devs_expression+="|$(basename $device)" || true
done

pushd "${MYDIR}"
	./zfs-params_set.bash
	for m in  zfs zzstd  icp zcommon znvpair zavl spl; do 
		if lsmod | grep $m; then
		       rmmod $m || fuser -k /dev/zfs
		fi
	done
#	if ! modprobe -r zfs; then
#		pushd /lib/modules/$(uname -r)/kernel/zfs
#			
#		popd
#	fi
	./zfs-params_set.bash
popd

[ -d /lib/modules/$(uname -r)/extra ] && pushd /lib/modules/$(uname -r)/extra || [ -e /lib/modules/$(uname -r)/updates ] && pushd /lib/modules/$(uname -r)/updates/dkms
	for modpath in 	spl/spl/spl.ko 	zstd/zzstd/zzstd.ko 	lua/zlua/zlua.ko 	unicode/zunicode/zunicode.ko 	nvpair/znvpair/znvpair.ko 	zcommon/zcommon/zcommon.ko 	icp/icp/icp.ko 	avl/avl/zavl.ko 	zfs/zfs/zfs.ko; do
		bn=$(basename ${modpath})
		modname=${bn%.*}
		lsmod | grep ${modname} || if [ -e ${modpath} ]; then
			insmod ${modpath}
		else
			modprobe ${modname} || [[ "${modname}" == 'zzstd' ]] && true
		fi
	done
popd || true

for device in /sys/block/sd*; do
     (udevadm info --query=property --path=$device | grep -q "^ID_BUS=usb") && removable_devs_expression+="|$(basename $device)" || true
done

systemctl start zfs-zed zed zsysd &

systool -vm zfs

zpool --version

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

declare -a DBPubKeys=($(find ~/.ssh/ -type f -not -name '*known_hosts*' -not -name '*keys*' -not -name '*config*' -name '*ecdsa*.pub' -perm '0600'))
for dppubkey in ${DBPubKeys[@]}; do
	dbclient -s -i "${dppubkey}" "${@}" || continue
done

declare -a OpenSshPrivKeys=($(find ~/.ssh/ /etc/ssh -type f -not -name '*known_hosts*' -not -name '*keys*' -not -name '*config*' -not -name '*.pub' -not -name '*dropbear*' -perm '0600'))

for ossh_priv_key in "${OpenSshPrivKeys[@]}"; do
	ssh -o 'PasswordAuthentication=no' -i "${ossh_priv_key}" "${@}" || continue
done

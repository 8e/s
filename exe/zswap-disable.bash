#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly SYSTEMD_FILE_CONTENTS="# https://www.addictivetips.com/ubuntu-linux-tips/enable-zswap-on-linux/
swapfc_enabled=0
"

"${MYREALDIR}"/zswap-status.bash || true

echo N | sudo tee /sys/module/zswap/parameters/enabled

sudo "${MYREALDIR}"/ensure-path.bash $(dirname "${ZSWAP_SYSTEMD_FILE_PATH}")

echo -n "${SYSTEMD_FILE_CONTENTS}" | sudo "${MYREALDIR}"/ensure-contents.bash "${ZSWAP_SYSTEMD_FILE_PATH}"
sudo systemctl daemon-reload &

"${MYREALDIR}"/zswap-status.bash

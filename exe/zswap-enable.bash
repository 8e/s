#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

declare -a Compressors=(deflate lz4hc lzo lzo-rle 842 zstd lz4)
declare -a Allocators=(zbud zsmalloc z3fold)
readonly SYSTEMD_FILE_CONTENTS="# https://www.addictivetips.com/ubuntu-linux-tips/enable-zswap-on-linux/
swapfc_enabled=1
"

if [[ "${ZSWAP_COMPRESSION}" == '' ]]; then
	readonly ZSWAP_COMPRESSION=${1:-$(printf '%s\n' "${Compressors[@]}" | sort -u | "${MYREALDIR}"/dialog.bash "Zswap compressor:")}
fi

if [[ "${ZSWAP_POOL_ALLOCATOR}" == '' ]]; then
	readonly ZSWAP_POOL_ALLOCATOR=${2:-$(printf '%s\n' "${Allocators[@]}" | sort -u | "${MYREALDIR}"/dialog.bash "Zswap pool allocator:")}
fi

if [[ "${ZSWAP_MAX_POOL_PERCENT}" == '' ]]; then
	if "${MYREALDIR}"/is_posint.bash "${3}"; then
		readonly ZSWAP_MAX_POOL_PERCENT="${3}"
	else
		read -e -p "Enter max pool percentage: " ZSWAP_MAX_POOL_PERCENT
	fi
fi

if [[ "${ZSWAP_ACCEPT_THRESHOLD_PERCENT}" == '' ]]; then
	if "${MYREALDIR}"/is_posint.bash "${4}"; then
		readonly ZSWAP_MAX_POOL_PERCENT="${4}"
	else
		read -e -p "Enter accept threshold percentage: " ZSWAP_ACCEPT_THRESHOLD_PERCENT
	fi
fi

if [[ "${SWAPPINESS}" == '' ]]; then
	if "${MYREALDIR}"/is_posint.bash "${5}"; then
		readonly SWAPPINESS="${5}"
	else
		read -e -p "Enter swappiness: " SWAPPINESS
	fi
fi

"${MYREALDIR}"/sysctl-set-one.bash "vm.swappiness" "${SWAPPINESS}"

echo "${ZSWAP_POOL_ALLOCATOR}" | sudo tee /sys/module/zswap/parameters/zpool
echo "${ZSWAP_COMPRESSION}" | sudo tee /sys/module/zswap/parameters/compressor
echo "${ZSWAP_MAX_POOL_PERCENT}" | sudo tee /sys/module/zswap/parameters/max_pool_percent
echo "${ZSWAP_ACCEPT_THRESHOLD_PERCENT}" | sudo tee /sys/module/zswap/parameters/accept_threshold_percent
echo Y | sudo tee /sys/module/zswap/parameters/enabled

sudo "${MYREALDIR}"/ensure-path.bash $(dirname "${ZSWAP_SYSTEMD_FILE_PATH}")

echo -n "${SYSTEMD_FILE_CONTENTS}" | sudo "${MYREALDIR}"/ensure-contents.bash "${ZSWAP_SYSTEMD_FILE_PATH}"
sudo systemctl daemon-reload &

"${MYREALDIR}"/zswap-status.bash

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly LOGF=/tmp/$(basename "${0}").log

function is_there() {
	ps --no-headers -ef | grep -w apt | grep -w update | egrep -wv "${$}|grep|tail|key|file|$(basename ${0})"
}

function too_much() {
	(( $(ps --no-headers -ef | grep "$(basename ${0})" | grep -v grep | wc -l) > 3 )) || (( $(ps --no-headers -ef | grep apt | grep update | egrep -viw 'grep|file|key|tsp' | wc -l) > 5 ))
}

function expo() {
	local i=1
	while is_there; do
		echo "$(ps --no-headers -p ${$}): waiting $i seconds..."
		sleep $i
		i=$(( $i * 2 ))
	done
	if which tsp &>/dev/null; then
		tsp "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/apt.bash update
	elif which screen &>/dev/null; then
		"${MYREALDIR}"/screen.bash "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/apt.bash update
	else
		"${MYREALDIR}"/ltd.bash "${MYREALDIR}"/apt.bash update
	fi
}

ps --no-headers -p $$ | sudo tee "${LOGF}"
date | sudo tee -a "${LOGF}"

"${MYREALDIR}"/connect.bash
"${MYREALDIR}"/dns_set.bash

if too_much; then
	exit 0
elif is_there; then
	#	expo | sudo tee -a"${LOGF}" &
	exit 0
else
	which cupt && cupt update &
	"${MYREALDIR}"/apt.bash update || true
	wait || true
fi

if which apt-file >/dev/null && ! ps -ef | grep -w "apt-file" | grep -v grep >/dev/null; then
	if which tsp >/dev/null; then
		tsp "${MYREALDIR}"/ltd.bash sudo apt-file update
		tsp "${MYREALDIR}"/ltd.bash sudo hamachi check-update
	elif which screen >/dev/null; then
		"${MYREALDIR}"/screen.bash "${MYREALDIR}"/ltd.bash sudo apt-file update
		"${MYREALDIR}"/screen.bash "${MYREALDIR}"/ltd.bash sudo hamachi check-update
	fi
fi
sudo fwupdmgr get-upgrades || true

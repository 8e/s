#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly CLEANER="${MYREALDIR}"/dlt.bash

[ $# -eq 0 ] && set . --

#find ${WHERE} -regex '^.+\.[:digit:]+\..+' | awk -vFS='.' '{
find "${@}" -exec realpath {} \; | ifne "${MYREALDIR}"/prev-vers.awk

#!/usr/bin/env bash

set -x

readonly MYREALDIR="$(dirname $(realpath ${0}))"

declare -A ZfsParams
 
# https://forums.freebsd.org/threads/zfs-scrub-resilver-performance.55540/
# Now my sysctl.conf is
#Code:
#
#vfs.zfs.scrub_delay=0
#vfs.zfs.top_maxinflight=256
#vfs.zfs.resilver_min_time_ms=10000
#vfs.zfs.resilver_delay=0
#vfs.zfs.vdev.scrub_max_active: 8
#vfs.zfs.vdev.scrub_min_active: 4
#vfs.zfs.txg.timeout: 15
#
#
#
#And my performance is abysmal
#3.17T scanned out of 26.8T at 14.8M/s, 463h22m to go
#

ZfsParams["recover"]="0"
ZfsParams["spa_config_path"]=""
ZfsParams["max_missing_tvds"]="1"
#vfs.zfs.l2arc_write_boost=160000000             # Set the L2ARC warmup writes to 160 MBps
ZfsParams["l2arc_write_boost"]=160000000
#vfs.zfs.l2arc_write_max=320000000               # Set the L2ARC writes to 320 MBps
ZfsParams["l2arc_write_max"]=320000000
#  If you set vfs.zfs.l2arc_write_max this high, you may want to set vfs.zfs.l2arc_feed_min_ms=1000. Otherwise, the maximum l2arc write speed would be 1600MBps.
ZfsParams["l2arc_feed_min_ms"]=1000
#vfs.zfs.top_maxinflight=128                     # Up the number of in-flight I/O (default 32)
ZfsParams["top_maxinflight"]=$(( $("${MYREALDIR}"/cpus.sh) ))
#vfs.zfs.vdev.max_pending=24                     # Set the queue depth (number of I/O) for each vdev
ZfsParams["vdev.max_pending"]=$(( $("${MYREALDIR}"/cpus.sh) ))
# vfs.zfs.resilver_min_time_ms=5000               # Up the length ofo time a resilver process takes in each TXG (default 3000)
# ZfsParams["resilver_min_time_ms"]=6000
ZfsParams["resilver_min_time_ms"]=20000
#vfs.zfs.resilver_delay=0                        # Prioritise resilver over normal writes (default 2)
ZfsParams["resilver_delay"]=5
#vfs.zfs.scrub_delay=0                           # Prioritise scrub    over normal writes (default 4)
ZfsParams["scrub_delay"]=5
#ZfsParams["vdev_scheduler"]="none"
ZfsParams["scrub_limit"]=8

##If Ram = 4GB, set the value to 512M
##If Ram = 8GB, set the value to 1024M
##Assuming 12GB of memory
#vfs.zfs.arc_min="1536M"
#vfs.zfs.arc_max="5504M"
## 5376/2+(1024*12/2-512)/2

#Assuming 32GB of memory
#ZfsParams["arc_min"]="4294967296"

ZfsParams["arc_min"]="$(( $(${MYREALDIR}/ramsizekb.bash) * 1024 / 8 ))"

# ZfsParams["arc_max"]="16106127360"

# Ram x 0.5 - 512 MB
ZfsParams["arc_max"]="$(( $(${MYREALDIR}/ramsizekb.bash) * 1024 / 2 - 512*1024*1024))"

#Ram x 2
vm.kmem_size_max="$(( $(${MYREALDIR}/ramsizekb.bash) * 2 / ( 1024 * 1024 ) ))G"

#Ram x 1.5
vm.kmem_size="$(( $(${MYREALDIR}/ramsizekb.bash) * 3 / ( 2 * 1024 * 1024 ) ))G"

#The following were copied from FreeBSD ZFS Tuning Guide
#https://wiki.freebsd.org/ZFSTuningGuide

# Disable ZFS prefetching
# http://southbrain.com/south/2008/04/the-nightmare-comes-slowly-zfs.html
# Increases overall speed of ZFS, but when disk flushing/writes occur,
# system is less responsive (due to extreme disk I/O).
# NOTE: Systems with 4 GB of RAM or more have prefetch enabled by default.
ZfsParams["prefetch_disable"]="0"

# Decrease ZFS txg timeout value from 30 (default) to 5 seconds.  This
# should increase throughput and decrease the "bursty" stalls that
# happen during immense I/O with ZFS.
# http://lists.freebsd.org/pipermail/freebsd-fs/2009-December/007343.html
# http://lists.freebsd.org/pipermail/freebsd-fs/2009-December/007355.html
# default in FreeBSD since ZFS v28
ZfsParams["txg.timeout"]="5"

# Increase number of vnodes; we've seen vfs.numvnodes reach 115,000
# at times.  Default max is a little over 200,000.  Playing it safe...
# If numvnodes reaches maxvnode performance substantially decreases.
ZfsParams["kern.maxvnodes"]=250000

# Set TXG write limit to a lower threshold.  This helps "level out"
# the throughput rate (see "zpool iostat").  A value of 256MB works well
# for systems with 4 GB of RAM, while 1 GB works well for us w/ 8 GB on
# disks which have 64 MB cache.

# NOTE: in v27 or below , this tunable is called 'vfs.zfs.txg.write_limit_override'.
ZfsParams["write_limit_override"]=2G

for key in ${!ZfsParams[@]}; do
	val=${ZfsParams[${key}]}
	sysctl vfs.zfs.${key}=${val}
	echo "${val}" > /sys/module/zfs/parameters/zfs_${key}
	echo "options zfs zfs_${key}=${val}" > /etc/modprobe.d/zfs_${key}.conf
done	



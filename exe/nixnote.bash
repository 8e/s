#!/usr/bin/env bash

set -ex

readonly MYREALDIR="$(dirname $(realpath ${0}))"
readonly LOGF=/tmp/${USER}/nixnote.log

"${MYREALDIR}"/ensure-path.bash $(dirname "${LOGF}")
#sudo renice -n -20 -p $$ > "${LOGF}"
sudo renice -n 10 -p $$ > "${LOGF}"

"${MYREALDIR}"/continuous.bash 1 "${MYREALDIR}"/nixnote-iteration.bash | tee -a "${LOGF}"

#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd >/dev/null
"${MYREALDIR}"/find-samename-subdirs.bash $* | "${MYREALDIR}"/xblissargs.bash -l rmdir -pv 2>/dev/null

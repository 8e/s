#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

zfs list -H -o name,mountpoint | awk '($2=="/") {print $1}' | awk -vFS="/ROOT/" '{print $2}'

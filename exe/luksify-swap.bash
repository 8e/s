#!/usr/bin/env bash

set -xe

if [ -e ${1} ]; then
	readonly BACKDEV=${1}
elif [ -e $(pwd)/$(basename ${1}) ]; then
	readonly BACKDEV=(pwd)/$(basename ${1})
else
	echo "Block device not found"
	exit 1
fi
readonly MYREALDIR=$(dirname $(realpath ${0}))

readonly INTALGO=${2:-$("${MYREALDIR}"/print-cryptsetup-algorithms.bash | "${MYREALDIR}"/dialog.bash "Integrity check algorithm:")}
readonly ENC_CIPH=${3:-$("${MYREALDIR}"/print-cryptsetup-algorithms.bash | "${MYREALDIR}"/dialog.bash "Encryption Cipher:")}

readonly MAPPERNAME=$(basename ${BACKDEV}).luks
readonly LBL='swap'
readonly PAGESZ=$(getconf PAGESIZE)



#readonly DEFKF=/etc/luks/"$(basename ${BACKDEV}).key"
#readonly KEYOPT=${4}
#if [[ "${KEYOPT}" == '' ]]; then
#	readonly KEYF="${DEFKF}"
#fi

function ensure_mdadm() {
	[[ $(realpath "${BACKDEV}") =~ .*md[[:digit:]]+$ ]] || exit 1
	which mdadm || "${MYREALDIR}"/apt.bash install mdadm
	mdadm --detail "${BACKDEV}"
	return $?
}

#pvcreate -v --zero=y pvmetadatacopies=3 --dataalignment=4K /dev/disk/by-id/ata-*-part2
#vgcreate --physicalextentsize=4K --zero=y --pvmetadatacopies=2 --autobackup y --vgmetadatacopies=3 --dataalignment=4K lvm-vg-swap /dev/disk/by-id/ata-*-part2
#lvcreate --activate=y --wipesignatures=y --zero=y --type raid10 --mirrors 1 --stripes 2 --regionsize 4K --raidintegrity=y --raidintegritymode=bitmap --raidintegrityblocksize=4096 --stripesize=4K --name lvm-lv-swap --alloc anywhere -l 45%FREE lvm-vg-swap -vv

lvdisplay "${BACKDEV}" || if [[ $(lsblk -o PARTTYPE -ln "${BACKDEV}" | tr -d '[:space:]') != '0x82' ]] && [[ $(lsblk -o PARTTYPE -ln "${BACKDEV}") != '0657fd6d-a4ab-43c4-84e5-0933c84b4f4f' ]] && [[ $(lsblk -o PARTTYPE -ln "${BACKDEV}") != 'cafecafe-9b03-4f30-b4c6-5ec00ceff106' ]] && ! ensure_mdadm; then
	#echo "${BACKDEV} is an md array"
	#echo "${BACKDEV} does not have “swap“ partition code (0x82)"
	exit 2
fi

which cryptsetup || "${MYREALDIR}"/apt.bash install cryptsetup
"${MYREALDIR}"/apt-ensure.bash cryptsetup-initramfs


if [[ "${KEYOPT}" == 'file' ]]; then
	"${MYREALDIR}"/ensure-path.bash /etc/luks

	[ -e ${KEYF} ] || tr -cd '[:alnum:]' < /dev/urandom | dd of="${KEYF}" bs="${PAGESZ}" count=1 && chmod 0600 "${KEYF}"

	#Then the correct corresponding entry in /etc/crypttab would be something like
	#cswap /dev/disk/by-id/ata-SAMSUNG_SSD_830_Series_S0XYNEAC762041-part5 /dev/urandom swap,cipher=aes-cbc-essiv:sha256,size=256
	# <name> <device> <password> <options>
	# <target name> <source device>         <key file>      <options>

	cryptsetup -v luksFormat --key-file "${KEYF}" --type luks2 "${BACKDEV}" --cipher "${ENC_CIPH}" --label "${MAPPERNAME}" --sector-size ${PAGESZ} --integrity-no-wipe --integrity "${INTALGO}" --batch-mode

	grep "${MAPPERNAME}" /etc/crypttab | grep "${KEYF}" && sed -i.bak "/^.*${MAPPERNAME}.*$/d" /etc/crypttab
	# ADD trim,discard one supported
	echo -e "${MAPPERNAME}\t${BACKDEV}\t${KEYF}\tswap,luks2,luks,allow-discards,cipher=${ENC_CIPH},size=${PAGESZ},integrity=${INTALGO}" | tee -a /etc/crypttab
	cryptsetup luksAddKey "${BACKDEV}" "${KEYF}" --key-file "${KEYF}"

else
	#cryptsetup luksFormat -c aes-xts-plain64 -s 512 -h sha256 ${DISK}-part4
	## cryptsetup luksFormat --type luks2 "${BACKDEV}" --cipher "${ENC_CIPH}" --integrity hmac-sha256
	## cryptsetup luksFormat --type luks2 <device> --cipher "${ENC_CIPH}" --integrity hmac-sha256
	## cryptsetup luksFormat --type luks2 <device> --cipher chacha20-random --integrity poly1305
	cryptsetup -v luksFormat --type luks2 "${BACKDEV}" --cipher "${ENC_CIPH}" --label "${MAPPERNAME}" --sector-size ${PAGESZ} --integrity-no-wipe --integrity "${INTALGO}" --batch-mode
	grep "${MAPPERNAME}" /etc/crypttab && sed -i.bak "/^.*${MAPPERNAME}.*$/d" /etc/crypttab
	echo -e "${MAPPERNAME}\t${BACKDEV}\tnone\tswap,luks2,luks,allow-discards,cipher=${ENC_CIPH},size=${PAGESZ},integrity=${INTALGO}" | tee -a /etc/crypttab
fi

#/usr/lib/systemd/system-generators/systemd-cryptsetup-generator
#/usr/lib/systemd/system-generators/systemd-cryptsetup-generator /run/systemd/generator
#systemctl daemon-reload
#systemctl restart cryptsetup.target


cryptdisks_start "${MAPPERNAME}"

cryptsetup luksDump "${BACKDEV}"

cryptsetup -v status "${MAPPERNAME}"

# mdadm --create /dev/md/swap --metadata=1.2 --level=raid10 --raid-devices=2 /dev/mapper/*.luks

#cryptsetup luksOpen "${BACKDEV}" "${MAPPERNAME}"

#cryptsetup -v status "${MAPPERNAME}"

#cryptsetup -v luksAddKey "${MAPPERNAME}" "${KEYF}"

mkswap -p ${PAGESZ} -L "${LBL}" /dev/mapper/"${MAPPERNAME}"

echo "/dev/mapper/${MAPPERNAME}" > /etc/initramfs-tools/conf.d/resume

#<file system> <mount point> <type> <options> <dump> <pass>
grep -- "^${LBL}" /etc/fstab && sed -i.bak "|^.*LABEL=${LBL}.*$|d" /etc/fstab
echo -e "LABEL=${LBL}\tnone\tswap\tsw,pri=32767,discard\t0\t0" | tee -a /etc/fstab

ln -svf /dev/mapper/"${MAPPERNAME}" /dev/disk/by-label/"${LBL}"

swapon -av -d

if [[ $( cat /etc/initramfs-tools/conf.d/resume ) != /dev/mapper/"${MAPPERNAME}" ]]; then
	mv /etc/initramfs-tools/conf.d/resume /var/backups/
	echo /dev/mapper/"${MAPPERNAME}" > /etc/initramfs-tools/conf.d/resume
fi

# https://wiki.archlinux.org/index.php/Dm-crypt/Swap_encryption#With_suspend-to-disk_support
#
#/etc/initcpio/hooks/openswap
#
#run_hook ()
#{
#    ## Optional: To avoid race conditions
#    x=0;
#    while [ ! -b /dev/mapper/<root-device> ] && [ $x -le 10 ]; do
#       x=$((x+1))
#       sleep .2
#    done
#    ## End of optional
#
#    mkdir crypto_key_device
#    mount /dev/mapper/<root-device> crypto_key_device
#    cryptsetup open --allow-discards --key-file crypto_key_device/<path-to-the-key> /dev/<device> swapDevice
#    umount crypto_key_device
#}


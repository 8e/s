#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

("${MYREALDIR}"/apt-ensure-cmd.bash git git) >/dev/null
readonly D=${*:-'.'}
pushd "${D}" > /dev/null
	git symbolic-ref --short HEAD
popd > /dev/null

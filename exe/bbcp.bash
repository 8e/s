#!/usr/bin/env bash

# https://www.slac.stanford.edu/~abh/bbcp

# https://docs.nersc.gov/services/bbcp/

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly CORES=$("${MYREALDIR}"/cpus.sh)
readonly RAMINKB=$(grep MemTotal /proc/meminfo | awk '{print $2}')
readonly FREERAMB=$(free -b | awk '/^Mem/ {print $4}')

function min() {
	if [ ${#} -gt 2 ]; then
		local two=$(min ${1} ${2})
		shift 2
		echo $(min ${two} ${*})
	else
		echo $(( ${1} < ${2} ? ${1} : ${2} ))
	fi
}

function max() {
	if [ ${#} -gt 2 ]; then
		local two=$(max ${1} ${2})
		shift 2
		echo $(max ${two} ${*})
	else
		echo $(( ${1} > ${2} ? ${1} : ${2} ))
	fi
}

readonly LAUNCHED_MES=$(ps -ef | grep "$(basename ${ME})" | grep -v grep | wc -l)
readonly STREAMS=$(max $(( ${CORES} / 2 - ${LAUNCHED_MES} )) 1)
readonly IOB=$(min $(( FREERAMB / STREAMS )) ${RAMINKB} )
readonly TCPW=$(min $(( FREERAMB / STREAMS )) ${RAMINKB} 425984)

# Some examples of usage are:
#bbcp -P 2 -V -w 8m -s 16 /local/path/bigfile.tar remotesystem:/remote/path/bigfile.tar
#
#-V verbose output
#-P 2 display progress every two seconds
#-s 16 create 16 parallel network streams (or threads)
#-w sets to 8 MB the size of the disk input/output (I/O) buffers
#
# To resume files in case of a lost connection add the -a and -k switch.
# if a firewall is blocking the communication between source and destination, use the -z option

# 1. Adler32 ('much faster' than 2)
#2. CRC32
#3. MD5 hash (*very much* slower than 2)
#
#In order of reliability:
#
#1. MD5 hash
#2. CRC32 (significantly less reliable than 1)
#3. Adler32 ('almost as reliable' as 2) 
#%         computes the checksum on the source node. By default, the checksum is computed on the target node.

ionice -c3 -p $$
renice -n 19 $$

if [[ $1 == 'serve' ]]; then
	shift
	declare -a Opts=()
else
	#declare -a Opts=(--recursive --preserve --progress 2 --vverbose --verbose --windowsz "${TCPW}" --buffsz "${IOB}" --streams "${STREAMS}" --compress 0 --omit --append --keep --symlinks keep -e --checksum %a32 --gross -T "ssh –x –a –oFallBackToRsh=no %4 %I –l %U %H $(basename ${ME}) serve 2>&1 > /tmp/$(basename ${ME}).log")
	declare -a Opts=(--recursive --preserve --progress 2 --vverbose --verbose --windowsz "${TCPW}" --buffsz "${IOB}" --streams "${STREAMS}" --compress 0 --omit --append --keep --symlinks keep -e --checksum %a32 --gross)
	#declare -a Opts=(--recursive --preserve --progress 2 --vverbose --verbose --windowsz "${TCPW}" --buffsz "${IOB}" --streams "${STREAMS}" --compress 0 --omit --append --keep --symlinks keep -e --checksum %a32 --gross -T "ssh –x –a –oFallBackToRsh=no %4 %I –l %U %H $(basename ${ME}) serve")
fi
#Opts+=(--debug)

bbcp ${Opts[*]} ${*} || bbcp -z ${Opts[*]} ${*}


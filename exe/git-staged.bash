#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

pushd $("${MYREALDIR}"/git-root.bash)
	git status | awk -vFS=':[ ]+' '{print $2}' | grep -v '^$'
popd

#!/usr/bin/env bash

set -ex

readonly MYDIR="$(dirname $(realpath ${0}))"
readonly DEFUSER="$(cat ${MYDIR}/../lib/DEFUSER)"

if [[ ${UID} == 0 ]]; then
	su - "${DEFUSER}" -c "DISPLAY=:0.0 /bin/systemsettings5 kcm_kscreen" &
else
	DISPLAY=:0.0 /bin/systemsettings5 kcm_kscreen &>/dev/null &
fi



#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly DEVDIR=$(dirname $(grep -RIl "${*}" /sys/bus/usb/devices/*/product))
readonly TARGFILE="${DEVDIR}/bConfigurationValue"

ls -al "${TARGFILE}"
echo '0' | sudo tee "${TARGFILE}"

cat "${TARGFILE}"

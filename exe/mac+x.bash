#!/usr/bin/env bash

sudo chmod -R 755 "${@}"
 
sudo xattr -dr com.apple.quarantine "${@}"

sudo spctl --master-disable

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

sudo chgrp "${DEFUSER}" ${@}  &
sudo chmod g+rwX ${@} &

# if [[ " ${array[*]} " =~ " ${value} " ]]; then
    # whatever you want to do when array contains value
#fi

#if [[ ! " ${array[*]} " =~ " ${value} " ]]; then
    # whatever you want to do when array doesn't contain value
#fi

# https://stackoverflow.com/questions/3685970/check-if-a-bash-array-contains-a-value/15394738#15394738
# if [[ " ${@} " =~ " -R " ]] || [[ " ${@} " =~ " --recoursive " ]]; then


[ ${#} -eq 0 ] && exit 1

while [ ${#} -gt 0 ]; do

        case ${1} in

                '-R' | '--recoursive')
                        shift
			RECOUR=true
                ;;

                *)
                        Params+=(${1})
                        shift
                        break
                ;;
        esac

	shift # past param

done

if $RECOUR; then
	sudo find "${Params[@]}" -type d -execdir chmod g+s '{}' \; &
fi

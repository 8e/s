#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd >/dev/null

readonly MAXJOBS='75%'
readonly LOGDIR=$("${MYREALDIR}"/ensure-path.bash /tmp/log/$(basename ${ME}).${USER})

if (( ${#} == 0 )); then
	echo arguemnts needed
	exit 1
fi

"${MYREALDIR}"/dedup.bash --delete --no-prompt "${@}"

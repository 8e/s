#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

function find_for_one_file() {
	local fname=${*}
	local base=$(basename ${fname%%.*})
	local parentdir=$(dirname ${fname})
	find "${parentdir}" -name "${base}.*" | while read line; do
		#[[ "${line}" != "${fname}" ]] && [[ $(file --brief --mime "${fname}") == $(file --brief --mime "${line}") ]] && echo ${line}
		if [[ $(realpath "${line}") != $(realpath "${fname}") ]]; then
			echo ${line}
		fi
	done
}

for fname in "${@}"; do
	find_for_one_file "${fname}"
done | sort -u

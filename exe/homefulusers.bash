#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	"${MYREALDIR}"/apt-ensure.bash moreutils >/dev/null
	getent passwd | awk -F\: '/\/home/ {print $6}' | ifne sort -u | "${MYREALDIR}"/xblissargs.bash -I{} bash -c 'if [ -e "{}" ]; then echo "{}"; fi' | "${MYREALDIR}"/xblissargs.bash -I{} bash -c "getent passwd | awk -F\: '\$6==\"{}\" {print \$1}'"
else
	"${MYREALDIR}"/mac_getent.bash passwd | awk -F\: '/\/home/ {print $6}' | sort -u | "${MYREALDIR}"/xblissargs.bash -I{} bash -c 'if [ -e "{}" ]; then echo "{}"; fi' | "${MYREALDIR}"/xblissargs.bash -I{} bash -c "${MYREALDIR}/mac_getent.bash passwd | awk -F\: '\$6==\"{}\" {print \$1}'"
fi

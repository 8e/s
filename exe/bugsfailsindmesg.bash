#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

sudo dmesg | egrep -i 'bug|fail' | grep -v '2ban\|nofail\|Failed to parse message, ignoring.'

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

case "${1}" in
        '-n')
                shift
		readonly ECHO=false
        ;;

        *)
		readonly ECHO=true
        ;;
esac

function create() {
	local u="${USER}"
	if ! mkdir -p "${*}"; then
		sudo mkdir -p "${*}"
		sudo chown -R "${USER}":"${u}" "${*}"
	fi
}

if [[ "${*}" == '' ]]; then
	while read path; do
		([ -d "${path}" ] || [ -L "${path}" ]) || create "${path}"
	done
else
	for path in "${@}"; do 
		([ -d "${path}" ] || [ -L "${path}" ]) || create "${path}"
	done
fi

"${ECHO}" && echo "${path}" || exit 0

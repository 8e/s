#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly CACHED=$("${MYREALDIR}"/apt-cachedir.bash)
readonly PARTIALD="${CACHED}/partial"
readonly URLRE="(https?|ftp|file)://.*"

function process_package_url_line() {
	local package_name=${1}
	local deburl=${2}
	if "${MYREALDIR}"/apt-is-installed.bash "${package_name}"; then
		echo "${package_name} is already installed"
	else
		local expected_fname=$(basename "${deburl}")
		"${MYREALDIR}"/apt.bash
		if [ -e ${CACHED}/${expected_fname} ] && ! dpkg -i ${CACHED}/${expected_fname}; then
			"${MYREALDIR}"/apt.bash -f install -y
			dpkg -i ${CACHED}/${expected_fname}
		elif [ -e ${CACHED}/${package_name}.deb ] && ! dpkg -i ${CACHED}/${package_name}.deb; then
			"${MYREALDIR}"/apt.bash -f install -y
			dpkg -i ${CACHED}/${package_name}.deb
		elif [ -e ${PARTIALD}/${expected_fname} ] && ! dpkg -i ${PARTIALD}/${expected_fname}; then
			"${MYREALDIR}"/apt.bash -f install -y
			dpkg -i ${PARTIALD}/${expected_fname}
			mv -v ${PARTIALD}/${expected_fname} ${CACHED}/
		elif [ -e ${PARTIALD}/${package_name}.deb ] && ! dpkg -i ${PARTIALD}/${package_name}.deb; then
			"${MYREALDIR}"/apt.bash -f install -y
			dpkg -i ${PARTIALD}/${package_name}.deb
			mv -v ${PARTIALD}/${expected_fname} ${CACHED}/
		else 
			"${MYREALDIR}"/ensure-path.bash "${PARTIALD}"
			pushd ${PARTIALD}
				local downloaded_fname=$("${MYREALDIR}"/dwnld_fname.bash "${deburl}")
				[[ "$downloaded_fname" != '' ]] || exit 1
				if ! mv -v "${downloaded_fname}" ${CACHED}/; then
					local ec=$?
					echo "${downloaded_fname}" ${CACHED}/
					exit "${ec}"
				fi
			popd
			pushd ${CACHED}
				if [ -e ${expected_fname} ] && ! dpkg -i ${CACHED}/${expected_fname}; then
					"${MYREALDIR}"/apt.bash -f install -y
					dpkg -i ${CACHED}/${expected_fname}
				elif [ -e ${package_name}.deb ] && ! dpkg -i ${CACHED}/${package_name}.deb; then
					"${MYREALDIR}"/apt.bash -f install -y
					dpkg -i ${CACHED}/${package_name}.deb
				elif [ -e ${downloaded_fname} ] && ! dpkg -i ${downloaded_fname}; then
				       	"${MYREALDIR}"/apt.bash -f install -y
					dpkg -i ${downloaded_fname}
				fi
				[[ "${downloaded_fname}" == "${package_name}.deb" ]] || ln -v "${downloaded_fname}" "${package_name}.deb" &
				[[ "${downloaded_fname}" == "${expected_fname}" ]] || ln -v "${downloaded_fname}" "${expected_fname}" &
			popd
		fi
	fi
}

while ! [ -z "${1}" ]; do
	arg="${1}"
	shift
	if [[ "${arg}" == 'latest' ]]; then
		package_name="${1}"
		shift
		deburl=$("${MYREALDIR}"/apt-latesturl.bash "${*}")
		"${ME}" "${package_name}" "${deburl}"
		exit
	elif [[ "${arg}" =~ ${URLRE} ]]; then
		deburl="${arg}"
		expected_fname=$(basename "${deburl}")
		expected_package_name="${expected_fname%.*}"
		process_package_url_line ${expected_package_name} ${deburl}

	elif [ -f "${arg}" ]; then
		listfile="${arg}"
		cat "${listfile}" | while read package_url_line; do
			process_package_url_line ${package_url_line}
		done <&0
	else
		expected_package_name="${arg}"
		deburl="${*}"
		shift
		process_package_url_line ${expected_package_name} ${deburl}
		#	echo "${arg} is not an url and not a list file"
	fi
done

"${MYREALDIR}"/apt.bash -f install -y 

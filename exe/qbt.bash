#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

#readonly NICE=10
#readonly IONICE=3
#
#renice -n "${NICE}" $$
#ionice -c "${IONICE}" -n 7 -p $$
#
#/usr/bin/env nice -n "${NICE}" ionice -c"${IONICE}" qbittorrent &
#p=$!
#renice -n "${NICE}" $p
#ionice -c "${IONICE}" -n 7 -p $p

ps -ef | grep 'qbittorrent-nox' | grep -v grep && sudo systemctl stop qbittorrent.service
tsp "${MYREALDIR}"/ltd.bash qbittorrent

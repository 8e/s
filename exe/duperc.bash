#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd >/dev/null

readonly P="${@:-/}"

sync
sudo sync

function sing() {
	local osduperc=$(df -H --output=pcent "${*}" | awk -vRS='[% ]' '/[[:digit:]]/ {print; exit}')

	if "${MYREALDIR}"/is_zfs.bash "${*}"; then
		zpool sync &
		sudo zpool sync &
		echo $(( ( osduperc + $("${MYREALDIR}"/z-cap.bash "${*}") ) / 2 ))
	else
		echo $osduperc
	fi
}

while (( ${#} > 0 )); do
	sing $1
	shift
done | sort -u

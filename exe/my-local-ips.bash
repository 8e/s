#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

if $(( ${#} > 0 )) then
	for d in "${@}"; do
		ip addr show dev "${d}" | awk '/inet/ {print $2}'
	done
else
	ip addr show | awk '/inet/ {print $2}'
fi

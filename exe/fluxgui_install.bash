#!/usr/bin/env bash

set -xe

# https://github.com/xflux-gui/fluxgui

readonly MYREALDIR="$(dirname $(realpath ${0}))"
source "${MYREALDIR}"/../defaults.rc.bash
readonly INSTALLD=$(sudo su "${DEFUSER}" - -c "${MYREALDIR}/git-installrepo.bash https://github.com/xflux-gui/fluxgui.git")

pushd "${INSTALLD}"

	if [[ $1 == 'uninstall' ]]; then
		
		if [[ $2 == '--home' ]]; then
			# EXCLUSIVE OR uninstall in your home directory
			xargs rm -vr < installed.txt
			glib-compile-schemas "$(dirname "$(grep apps.fluxgui.gschema.xml installed.txt)")"
		else
			# EITHER uninstall globally
			#
			# The 'installed.txt' is generated when you install. Reinstall first if you
			# as described above if you don't have an 'installed.txt' file.
			sudo xargs rm -vr < installed.txt
			sudo glib-compile-schemas "$(dirname "$(grep apps.fluxgui.gschema.xml installed.txt)")"
		fi
		
	else
		./download-xflux.py
		if [[ $1 == '--home' ]]; then
			# EXCLUSIVE OR, install in your home directory
			#
			# The fluxgui program installs
			# into ~/.local/bin, so be sure to add that to your PATH if installing
			# locally. In particular, autostarting fluxgui in Gnome will not work
			# if the locally installed fluxgui is not on your PATH.
			sudo su "${DEFUSER}" -c "./setup.py install --user --record installed.txt"
			sudo su "${DEFUSER}" -c "ln -svf \"${INSTALLD}\" \"${MYREALDIR}\"/../lib/home/"
		else
			sudo ./setup.py install ${*} --record installed.txt
		fi
	fi


popd


#!/usr/bin/env bash

set -e
DEBUG=${DEBUG:-false}
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure-cmd.bash rsync rsync

declare -a BasicOpts=(-svauhHPXlitA --inplace --append --open-noatime --ignore-errors)

"${MYREALDIR}"/screen.bash "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/cleanup.bash rsync-insensitive-exclude "${BasicOpts[@]}" ${*}

"${MYREALDIR}"/ltd.bash "${MYREALDIR}"/bbcp.bash "${Opts[@]}" ${*}

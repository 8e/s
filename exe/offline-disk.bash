#!/usr/bin/env bash

set -x

readonly MYREALDIR=$(dirname $(realpath ${0}))


for dsk in ${*}; do
	[ -e /dev/${dsk} ] || dsk=$(basename $(realpath "${dsk}"))
	zpool sync &
	sync
	hdparm -Y ${dsk} &
	udisksctl power-off -b /dev/${dsk} &
	udisks --detach /dev/${dsk} &
	scsi-spin --force --down ${dsk} &
	eject ${dsk} &
	"${MYREALDIR}"/ensure-path.bash /sys/block/${dsk}/device
	echo 'offline' > /sys/block/${dsk}/device/state
	echo 1 > /sys/block/${dsk}/device/delete 
done


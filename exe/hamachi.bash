#!/bin/bash 

set -e
DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly NETW="25.0.0.0/8"

function hamachilogin() {
	while ! output=$(sudo hamachi login) || [[ "${output}" == 'Logging in .. failed, busy' ]]; do
		case "$output" in

			'Already logged in.')
				return 0
			;;

			*)
				echo -n "${output}"
				sleep 1
			;;

		esac
	done
}

which hamachi || "${MYREALDIR}"/apt-urls.bash latest logmein-hamachi https://www.vpn.net/linux

/etc/init.d/logmein-hamachi restart
if ! ps -p $(cat /var/run/logmein-hamachi/hamachid.pid); then
	"${MYREALDIR}"/apt.bash
	sudo dpkg-reconfigure -plow logmein-hamachi
	sudo /etc/init.d/logmein-hamachi restart
fi

while ! ps -p $(cat /var/run/logmein-hamachi/hamachid.pid); do
	sudo /etc/init.d/logmein-hamachi start
done

sudo hamachi check-update || true

hamachilogin

if [[ "$(hostname)" == 'ubuntu-studio' ]]; then
	"${MYREALDIR}"/hostname_set.bash
fi

output=$(hamachi set-nick "$(hostname)" || true)
case "${output}" in
	#'Setting nickname .. failed, busy')
	#	read -e -p "Enter hamachi nickname for this host: " ha_name
	#	hamachi set-nick "${ha_name}"
	#;;

	'This is an online operation and you are not logged in')
		hamachilogin
	;;
esac

if [ $# -gt 0 ]; then
	readonly NETID="${@}"
else
	read -e -p "Enter hamachi nickname for this host: " NETID
fi

sudo hamachi join "${NETID}"
ip route | grep "${NETW}" || ip route add "${NETW}" dev ham0
sudo "${MYREALDIR}"/hamachi-2.bash

sudo hamachi list

#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

(which git | "${MYREALDIR}"/apt-ensure.bash git) >/dev/null
readonly D="${*:-.}"

pushd "${D}" > /dev/null


	readonly BRANCH=$("${MYREALDIR}"/git-branch.bash)
	readonly REMOTE=$(git config --get remote.origin.url)

	"${MYREALDIR}"/dlt.bash -rvf .git
	git init -b "${BRANCH}" --initial-branch="${BRANCH}" 
	git remote add origin "${REMOTE}"
	git remote -v
	git-config.bash
	git add -A
	git status
	git commit -m "$(date +%F)"
	git push --force --set-upstream origin "${BRANCH}"
	gits.bash

popd > /dev/null

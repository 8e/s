#!/usr/bin/env bash

set -ex

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly POOLNAME=$(df -hT /boot | tail -1 | cut -d/ -f1)

pushd "${MYREALDIR}"

	./zsnapifdiff.bash "${POOLNAME}"

	cp -af /etc/fstab /var/backups/

	grep '/boot/efi' /etc/fstab || echo ${DISK}-part1 /boot/efi vfat defaults 0 0 >> /etc/fstab

	grep '/boot/grub' /etc/fstab || echo /boot/efi/grub /boot/grub none defaults,bind 0 0 >> /etc/fstab

	grub-probe /boot || ./grub-probe.bash install
	
	./linux-cmdline-update.bash

	# For a mirror or raidz topology:
	# systemctl mask grub-initrd-fallback.service

popd

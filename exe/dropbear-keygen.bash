#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly TYPE=${2:-ecdsa}
readonly KEY=${1:-~/.ssh/id_dropbear_${TYPE}}
dropbearkey -t "${TYPE}" -f "${KEY}" | grep "^${TYPE}" > "${KEY}".pub
chmod -v 0700 ~/.ssh
chmod -v 0600 ~/.ssh/* "${KEY}"
echo "${KEY}"

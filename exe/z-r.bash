#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

tsp "${MYREALDIR}"/z-releaseall.bash "${@}"
tsp "${MYREALDIR}"/z-rotatesnaps.bash "${@}"
tsp "${MYREALDIR}"/zpool-rm-erroneous-files.bash "${@}"
tsp "${MYREALDIR}"/z-checkpointrotate.bash "${@}"
tsp -f true
"${MYREALDIR}"/z-status.bash

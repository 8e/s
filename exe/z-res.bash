#!/usr/bin/env bash

set -xe

readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly RESCUE_PERCENTS=5
readonly MIN_RESC_P=1
readonly POOLS=${1:-$(zpool list -H -o name)}

function min() {
	if [ ${#} -gt 2 ]; then
		local two=$(min ${1} ${2})
		shift 2
		echo $(min ${two} ${*})
	else
		echo $(( ${1} < ${2} ? ${1} : ${2} ))
	fi
}

function max() {
	if [ ${#} -gt 2 ]; then
		local two=$(max ${1} ${2})
		shift 2
		echo $(max ${two} ${*})
	else
		echo $(( ${1} > ${2} ? ${1} : ${2} ))
	fi
}

function getswapsize() {
	local res=$(awk -vFS=\: '/swap/ {print $1}' /etc/fstab | xargs blockdev --getsize64 2>/dev/null || true)
	"${MYREALDIR}"/is_posint.bash "${res}" || echo 0
}

function forapool() {
	local pool=$1
	local poolsize=$(zpool list -Hpo size ${pool})
	local swapsize=$(getswapsize)
	local value=$(( poolsize * RESCUE_PERCENTS / 100 - swapsize ))
	local another_val=$(( poolsize * MIN_RESC_P / 100 ))
	local res=$(max $value $another_val)
	zfs set reservation="${res}" "${pool}"
	zfs set refreservation="${res}" "${pool}"
}

for p in ${POOLS}; do
	forapool ${p}
done

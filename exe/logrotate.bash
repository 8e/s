#!/usr/bin/env bash

#!/usr/bin/env bash
set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

function nologcompress(){
        ## Disable log compression
        grep -RIlE "(^|[^#y])(compress|delaycompress)" /etc/logrotate* | "${MYREALDIR}"/xargs.bash sed -i -r "s/(^|[^#y])(compress|delaycompress)/\1nocompress/"

	for file in /etc/logrotate.d/* ; do
		if grep -Eq "(^|[^#y])compress" "$file" ; then
			sed -i -r "s/(^|[^#y])(compress)/\1#\2/" "$file"
		fi
	done


}

pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
	./focopy.bash /etc/logrotate* /var/backups/ 
	./focopy.bash ../lib/logrotate/* /
	nologcompress
popd >/dev/null

#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

source "${MYREALDIR}"/../defaults.rc.bash

reset
"${MYREALDIR}"/is_passwordfuluser.bash root || passwd root

if id "${DEFUSER}" &>/dev/null; then
	reset
	"${MYREALDIR}"/is_passwordfuluser.bash "${DEFUSER}" || passwd "${DEFUSER}"
fi

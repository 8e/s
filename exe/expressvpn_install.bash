#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly BASH_SCRIPT="${*}"

"${MYREALDIR}"/apt-urls.bash latest https://www.expressvpn.works/clients/linux

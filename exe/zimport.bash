#!/usr/bin/env bash

set -xe

readonly T_ROOT=${1:-/mnt/z}
readonly MYREALDIR=$(dirname $(realpath ${0}))

[[ ${T_ROOT} == '' ]] && exit 1

"${MYREALDIR}"/zfs-restart.bash

zpool import -d /dev/disk/by-id -NfR "${T_ROOT}" rpool

zfs load-key rpool

zpool import -d /dev/disk/by-id -NfR "${T_ROOT}" bpool &

"${MYREALDIR}"/zfs-mount.bash

zfs mount

#!/usr/bin/env bash

#set -x

set -e

readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly SNAPNAME=${1}
readonly SNAPTYPES="filesystem,volume,bookmark"
readonly THRESHOLD_SEC=$(( 60 * 60 * 8 ))

readonly NICE=${2:-"19"}
readonly IOCLASS=${3:-"3"}

ionice -c${IOCLASS} -p $$
renice -n "${NICE}" $$

date +%F_%T

if ( ! ( "${MYREALDIR}/zsys-latest.bash" | "${MYREALDIR}/datediff.bash" ) 2>/dev/null ) || (( $( "${MYREALDIR}/zsys-latest.bash" | "${MYREALDIR}/datediff.bash" ) > ${THRESHOLD_SEC} )); then
	"${MYREALDIR}/zrecoursivesnap.bash"
else
	time for dataset in $(zfs list -H -o name,com.sun:auto-snapshot,canmount,type -t "${SNAPTYPES}" | awk '$2=="true" && $3!="off" {print $1}'); do
		"${MYREALDIR}"/zsnapifdiff.bash ${dataset} ${SNAPNAME}
	done
fi

for dir in /etc/etckeeper/pre-install.d /etc/etckeeper/post-install.d; do
	[ -d "${dir}" ] || if [ -L "${dir}" ] && ! [ -L "${dir}"/$(basename "${ME}") ]; then
		pushd "${dir}"
			ln -sv "${ME}"
		popd
	fi
done

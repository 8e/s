#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly SELF_HOSTS_LINE="127.0.0.1 localhost ${HN}"
readonly OLDHOSTNAME=${OLDHOSTNAME:-${2:-'ubuntu-studio'}}

if [[ $(hostname) != "${OLDHOSTNAME}" ]] && (( ${#} == 0 )); then
	echo "Hostname already set: $(hostname)"
	exit 0
elif [[ "${HN}" == '' ]]; then
	if [[ "${1}" == '' ]]; then
	reset
		read -e -p "Enter hostname: " HN
	else
		readonly HN="${1}"
	fi
fi

if [[ "${HN}" == '' ]]; then
	echo "Hostname cannot be empty"
	exit 1
fi

hostname ${HN}

"${MYREALDIR}"/dotdee-hosts.bash

sed -i "s/${OLDHOSTNAME}/${HN}/g" /etc/hostname /etc/hosts /etc/dotdee/etc/hosts.d/*

[[ $(cat /etc/dotdee/etc/hosts.d/self) == "${SELF_HOSTS_LINE}" ]] || echo "${SELF_HOSTS_LINE}" > /etc/dotdee/etc/hosts.d/01-self

hamachi set-nick "${HN}" &

dotdee -u /etc/hosts

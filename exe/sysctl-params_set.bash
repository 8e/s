#!/usr/bin/env bash

set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash

	declare -A Params

	Params["vm.swappiness"]=${SWAPPINESS}
	Params["fs.inotify.max_user_watches"]=1048576
	Params["net.ipv6.conf.all.disable_ipv6"]=0
	Params["net.ipv6.conf.default.disable_ipv6"]=0
	Params["net.ipv6.conf.lo.disable_ipv6"]=0
	Params["net.ipv6.conf.ham0.disable_ipv6"]=0
	Params["net.mptcp.mptcp.enabled"]=1

	# #disable ipv6
	#net.ipv6.conf.all.disable_ipv6 = 1
	#net.ipv6.conf.default.disable_ipv6 = 1
	#net.ipv6.conf.lo.disable_ipv6 = 1
	#net.ipv6.conf.eth0.disable_ipv6 = 1

	for key in ${!Params[@]}; do
		val=${Params[${key}]}
		if which screen; then
			./screen.bash ./sysctl-set-one.bash "${key}" "${val}"
		else
			./sysctl-set-one.bash "${key}" "${val}"
		fi
		sleep 0.33
	done	

	if which screen; then
		screen -wipe &>/dev/null &
		screen -ls || true
	fi

popd >/dev/null

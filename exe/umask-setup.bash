#!/usr/bin/env bash

set -xe

sed -i.bak 's/^UMASK.*2/UMASK\t\t002/g' /etc/login.defs

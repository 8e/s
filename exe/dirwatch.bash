#!/usr/bin/env bash

set -e

readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
declare -a Past=()

#inotifywait -qmr -e CLOSE_WRITE --format %w%f "${@}" | awk '!past[$0]++'
prev=''
inotifywait -qmre MOVED_TO -e attrib -e CLOSE_WRITE --format %w%f "${@}" | while read f; do
	if [[ "${f}" != "${prev}" ]]; then
		echo "${f}"
		prev="${f}"
	fi
done

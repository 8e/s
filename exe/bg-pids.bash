#!/usr/bin/env bash

set -e

BG_PIDS_TEMPFILE=$(mktemp /tmp/$(basename $0).$$.XXXXXX)

function bg_pid_cleanup() {
	rm "${BG_PIDS_TEMPFILE}"
}
trap bg_pid_cleanup EXIT

ps --ppid $$ | awk '$1 != "PID" && $3" "$4 != "00:00:00 ps" && $3" "$4 != "00:00:00 awk" {print $1}' > "${BG_PIDS_TEMPFILE}"
jobs -p >> "${BG_PIDS_TEMPFILE}"

sort -u "${BG_PIDS_TEMPFILE}"

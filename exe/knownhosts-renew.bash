#!/usr/bin/env bash

set -e
DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

readonly HOSTS="${@}"

for hd in /root /home/*; do
	u=$("${MYREALDIR}"/owner.bash "${hd}")
	nhf="${hd}"/.ssh/known_hosts
	sudo -u "${u}" "${MYREALDIR}"/ensure-path.bash "${hd}"/.ssh
	sudo -u "${u}" touch "${nhf}"
	sudo -u "${u}" [ -e "${nhf}" ] && echo "${HOSTS}" | xargs -d '\n' -l sudo -u "${u}" ssh-keygen -f "${nhf}" -R
	sudo -u "${u}" [ -e "${nhf}" ] && echo "${HOSTS}" | xargs -d '\n' -l sudo -u "${u}" ssh-keyscan -f "${nhf}" -H
	sudo chown -Rv "${u}":"${u}" "${hd}"/.ssh
done

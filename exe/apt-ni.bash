#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

"${MYREALDIR}"/apt-ensure.bash aptitude &>/dev/null
aptitude -o DPkg::Lock::Timeout=-1 search "${@}" | grep -v '^i' | cut -d' ' -f3

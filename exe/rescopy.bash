#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

[ ${DEBUG} ] && set -x
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR="$(dirname ${ME})"
readonly P_SEMAPHORENAME=$(basename "${ME}").${USER}
readonly CORES=$("${MYREALDIR}"/cpus.sh)
readonly JOBS=$(( ( ( ${CORES} + 1 ) ** 3 ) / ( 81 / 13 ) + 1 ))
readonly LOGDIR=$("${MYREALDIR}"/ensure-path.bash /tmp/log/$(basename ${ME}).${USER})
readonly LOGF="${LOGDIR}/$$.log"
export DELIVER=true
export REMOVE_SAME=true
export IGNORE=true

declare -a BackEnds=('mv -vfu' 'cp -favu' "${MYREALDIR}/rsync.bash -u --ignore-errors --remove-source-files" "${MYREALDIR}/relmv.zsh")
#declare -a BackEnds=('mv -nvu')
#declare -a BackEnds=('cp -navu')
#declare -a BackEnds=("${MYREALDIR}/bbcp.bash --debug --" "${MYREALDIR}/bbcp.bash --debug -z --")
#declare -a BackEnds=("${MYREALDIR}/bbcp.bash --debug --" "${MYREALDIR}/bbcp.bash --debug -z --")
#declare -a BackEnds=("${MYREALDIR}/bbcp.bash --debug --" "${MYREALDIR}/bbcp.bash --debug -z --")
#declare -a BackEnds=("A${MYREALDIR}/bbcp.bash --debug --" "${MYREALDIR}/bbcp.bash --debug -z --" 'rsync --open-noatime -svauchHPXlitAS --numeric-ids --ignore-errors --info=progress2 --partial -- ')

source "${MYREALDIR}"/../defaults.rc.bash

function cleanup() {
	${DEBUG} || (rm "${LOGDIR}"/*${$}.* || true)
}

function spawn() {
	"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}"
}

function src2rel() {
	ifne sed "s|^${src_root}||g; s|^/||g"
}

function filt() {
	ifne grep -v '/.$\|/..$\|/.\*\|/\*\|\*$' | ifne sed 's|/\./|/|g'
}

function list_deeper() {
	while read f; do
		printf '%q\n' "${f}"/* || true
		printf '%q\n' "${f}"/.* || true
	done | filt
}

function process_relpath() {
	local targroot="${*}"
	local targpath
	local targparent
	local sourcepath
	while read relpart; do
		echo relative part: "${relpart}"
		targpath="${targroot}/${relpart}"
		targparent=$(dirname "${targpath}")/
		sourcepath="${src_root}/${relpart}"
		sourceparent=$(dirname "${sourcepath}")
		echo source: "${sourcepath}" → Target parent: "${targparent}"
		printf "%s\n" "${BackEnds[@]}" | "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/rediff-apply-source.bash "${sourceparent}" "${targparent}" &
		for be in "${BackEnds[@]}"; do
			echo "using backend: ${be}"
			echo "${be} \"${sourcepath}\" \"${targparent}\"" | spawn
			echo "echo ${be} | '${MYREALDIR}'/ltd.bash '${MYREALDIR}'/rediff-apply-source.bash '${sourceparent}' '${targparent}'" | spawn
		done
		echo "echo \"${sourcepath}\" | \"${ME}\" 'deeper' \"${src_root}\" \"${targroot}\"" | spawn
		(chmod --reference="${sourcepath}" "${targpath}" || true) &
	done
}

trap cleanup EXIT
echo tsp rsync ifne | "${MYREALDIR}"/apt-ensure.bash task-spooler rsync moreutils

if [[ "${1}" == 'deeper' ]]; then
	shift
	readonly SOURCE_ROOT=${1}
	readonly TARG_ROOT=${2}
	readonly src_root="${SOURCE_ROOT}"
	list_deeper | src2rel | ifne tee "${LOGDIR}"/deeper.$$.list | ifne "${ME}" - "${SOURCE_ROOT}" "${TARG_ROOT}"
elif [[ "${1}" == '-' ]]; then
	shift
	readonly SOURCE_ROOT=${1}
	readonly TARG_ROOT=${2}
	if [[ ${SOURCE_ROOT} == '-' ]]; then
		while read src; do
			src_root=$(dirname "${src}")
			echo "${src}" | src2rel | process_relpath "${TARG_ROOT}"
		done
	else
		readonly src_root="${SOURCE_ROOT}"
		src2rel | process_relpath "${TARG_ROOT}"
	fi
elif [[ "${1}" == 'finish' ]]; then
	"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" finish
elif [[ "${1}" == 'kill' ]]; then
	"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" kill
else
	"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" create "${JOBS}" || true
	readonly SOURCE_ROOT=${1}
	readonly TARG_ROOT=${2}
	readonly src_root="${SOURCE_ROOT}"
	echo "." | process_relpath "${TARG_ROOT}"
fi

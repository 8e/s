#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

#set -o pipefail
#shopt -s lastpipe

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly REVISION=${1}
shift
readonly G_ROOT=$(git rev-parse --show-toplevel)
readonly RELPATH=$(realpath --relative-to="${G_ROOT}" "${@}")
readonly DELIM='|'
declare -a BaseActions=(skeep take quit)
declare -a LocallyExistingPathActions=("${BaseActions[@]}" patch edit delete remove-submodule backup)
readonly TMPF_ACTION=/tmp/$(basename ${0}).${USER}.$$.action.tmp
readonly TMPF_ACTIONS_SCRIPT=/tmp/$(basename ${0}).${USER}.$$.actions.bash
readonly TMPF_LOG=/tmp/$(basename ${0}).${USER}.$$.log
readonly BACKUPSRELPATH=./backups
readonly SHEBANG="#!/usr/bin/env bash -xi" #bashdb

sudo "${MYREALDIR}"/apt-ensure.bash moreutils

## https://unix.stackexchange.com/questions/440816/how-can-i-install-bashdb-on-ubuntu-18-04
## http://archive.ubuntu.com/ubuntu/pool/universe/b/bashdb/bashdb_4.2.0.8-1.1_all.deb
## https://askubuntu.com/questions/1306394/bashdb-in-ubuntu-20-04
#which bashdb >/dev/null || sudo "${MYREALDIR}"/apt-urls.bash http://archive.ubuntu.com/ubuntu/pool/universe/b/bashdb/bashdb_4.2.0.8-1.1_all.deb

function finishme() {
	rm "${TMPF_ACTION}" "${TMPF_LOG}" "${TMPF_ACTIONS_SCRIPT}" || true
}

${DEBUG} || trap finishme EXIT

function spawn() {
	 screen -d -m -L -Logfile "${TMPF_LOG}" "${@}"
}

function filt() {
	ifne grep -v '^backups/.*\|^ERROR|ERROR|$' | ifne grep -vf .gitignore
}

function dial() {
	local our_dt="${1}"
	shift
	local their_dr="${1}"
	shift
	local relation="${1}"
	shift
	local path="${@}"
	local title="Our: ${our_dt} ${relation} Their: ${their_dt}"
	if [ -e "${path}" ]; then
		local subtitle="Locally existing: ${path}"
		printf '%s\n' "${LocallyExistingPathActions[@]}" | "${MYREALDIR}"/dialog.bash "${title}" "${subtitle}" > "${TMPF_ACTION}"
	else
		local subtitle="Locally absent: ${path}"
		printf '%s\n' "${BaseActions[@]}" | "${MYREALDIR}"/dialog.bash "${title}" "${subtitle}" > "${TMPF_ACTION}"
	fi
}

function process_list() {
	local line
	local our_dt
	local their_dr
	local path
	while read line; do
		our_dt=$(echo "${line}" | cut -d\| -f1)
		their_dt=$(echo "${line}" | cut -d\| -f2)
		path=$(echo "${line}" | cut -d\| -f3)
		
		if [[ "${path}" == '' ]]; then
			continue
		elif ([[ "${our_dt}" = '' ]] || [[ "${our_dt}" = 'ERROR' ]]) && ([[ "${their_dt}" == '' ]] || [[ "${their_dt}" == 'ERROR' ]]); then
			dial "${our_dt}" "${their_dt}" 'both-error-case' "${path}"
		elif [[ "${our_dt}" > "${their_dt}" ]] || [[ "${their_dt}" == '' ]] || [[ "${their_dt}" == 'ERROR' ]]; then
			echo skeep > "${TMPF_ACTION}"
		elif [[ "${our_dt}" < "${their_dt}" ]]; then
			dial "${our_dt}" "${their_dt}" '<' "${path}"
		elif [[ "${our_dt}" == "${their_dt}" ]]; then
			dial "${our_dt}" "${their_dt}" '=' "${path}"
		else
			echo "Unknown case: Our: '${our_dt}' Their: '${their_dt}' Path: '${path}'"
			exit 3
		fi

		case $(cat "${TMPF_ACTION}") in

			'quit')
				return 0
			;;

			'skeep')
				true
			;;

			'take')
				echo "git checkout ${REVISION} '${path}'" >> "${TMPF_ACTIONS_SCRIPT}"
				echo git add "'${path}' &" >> "${TMPF_ACTIONS_SCRIPT}"
			;;

			'patch')
				echo "git checkout -p ${REVISION} '${path}'" >> "${TMPF_ACTIONS_SCRIPT}"
				echo git add "'${path}' &" >> "${TMPF_ACTIONS_SCRIPT}"
			;;

			'edit')
				echo "\"\${EDITOR}\" '${path}'" >> "${TMPF_ACTIONS_SCRIPT}"
				echo git add "'${path}' &" >> "${TMPF_ACTIONS_SCRIPT}"
			;;

			'remove-submodule')
				echo "'${MYREALDIR}'/git-remove-submodule.bash '${path}'" >> "${TMPF_ACTIONS_SCRIPT}"
				echo git add "'${path}' &" >> "${TMPF_ACTIONS_SCRIPT}"
				echo git add ".gitmodules &" >> "${TMPF_ACTIONS_SCRIPT}"
			;;

			'backup')
				echo "mv -rv '${path}' \"\$('${MYREALDIR}'/ensure-path.bash '${BACKUPSRELPATH}')\"/ &" >> "${TMPF_ACTIONS_SCRIPT}"
				echo git add "'${path}' '${BACKUPSRELPATH}' &" >> "${TMPF_ACTIONS_SCRIPT}"
			;;

			'delete')
				echo "'${MYREALDIR}'/dlt.bash -rv '${path}' &" >> "${TMPF_ACTIONS_SCRIPT}"
				echo git add "'${path}' &" >> "${TMPF_ACTIONS_SCRIPT}"
			;;

			*)
				echo "Unknown action: ${action}"
				exit 2
			;;
		esac
		
		rm "${TMPF_ACTION}"

	done


}


pushd "${G_ROOT}" >/dev/null
	echo "${SHEBANG}" > "${TMPF_ACTIONS_SCRIPT}"
	git fetch
	"${MYREALDIR}"/git-diff-list-datetime.bash "${REVISION}" "${RELPATH}" | filt | ifne sort -u | ifne tee "${TMPF_LOG}" | process_list
	[[ $(cat "${TMPF_ACTIONS_SCRIPT}") == "${SHEBANG}" ]] || (chmod +x "${TMPF_ACTIONS_SCRIPT}" && "${TMPF_ACTIONS_SCRIPT}")
	$DEBUG || rm "${TMPF_ACTIONS_SCRIPT}"
popd >/dev/null

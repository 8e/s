#!/usr/bin/env bash

set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
	
	echo -e "/etc/profile.d/defaults.rc.bash\n/etc/zsh/defaults.rc.bash" | ./ensure-symlink.bash "$(realpath ../defaults.rc.bash)"

	./dns_set.bash

	systemctl daemon-reload

	systemctl enable zfs.target
	systemctl enable zfs-import-cache
	systemctl enable zfs-mount
	systemctl enable zfs-import.target
	systemctl enable zautosnap.bash.service
	systemctl enable systemd-networkd.service
	systemctl enable systemd-resolved.service 
	systemctl enable clamav-daemon.service
	systemctl enable clamav-freshclam.service
	#systemctl enable cudo-miner.service
	#systemctl disable syncthing

popd >/dev/null

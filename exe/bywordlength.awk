#!/usr/bin/gawk -f

BEGIN {
	RS = " "
}

{ print length(), $0 | "sort -nr | cut -d\" \"  -f2" }

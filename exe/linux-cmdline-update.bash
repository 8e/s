#!/usr/bin/env bash

# https://gitlab.com/Sithuk/ubuntu-server-zfsbootmenu/-/blob/main/ubuntu_server_encrypted_root_zfs.sh

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi

pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash

        if [[ "${2}" != '' ]]; then
                readonly OSID="${2}"
                readonly POOL="${1}"
        else
                source ../z-ensure-poolosid.rc.bash
        fi

	tsp ./zsnapifdiff.bash "${POOL}" 

	readonly CMDLINE_LINUX_DEFAULT="POOL=${POOL} zbm.import_policy=hostid zbm.set_hostid ${CMDLINE_LINUX_DEFAULT} spl_hostid=${OSID} zbm.prefer=${POOL}"

	touch /etc/zfsbootmenu/config.yaml ${EFIDIR}/EFI/ubuntu/refind_linux.conf /etc/default/grub

	zfs set org.zfsbootmenu:commandline="${CMDLINE_LINUX_DEFAULT}" "${POOL}"/ROOT
	mount -av || true
	mount | grep boot
	#mount ${EFIDIR} || (( $? == 32 ))
	./ensure-path.bash ${EFIDIR}/EFI/ubuntu
	./ensure-path.bash /etc/zfsbootmenu/dracut.conf.d
	./ensure-path.bash /etc/dracut.conf.d

	cat <<EOF > /etc/zfsbootmenu/config.yaml
Global:
  ManageImages: true
  BootMountPoint: ${EFIDIR}
  DracutConfDir: /etc/zfsbootmenu/dracut.conf.d
Components:
  ImageDir: ${EFIDIR}/EFI/ubuntu
  Versions: 3
  Enabled: true
  syslinux:
    Config: /boot/syslinux/syslinux.cfg
    Enabled: false
EFI:
  ImageDir: ${EFIDIR}/EFI/ubuntu
  Versions: 2
  Enabled: false
Kernel:
  CommandLine: "${CMDLINE_LINUX_DEFAULT}"
EOF

	##Update refind_linux.conf
	##zfsbootmenu command-line parameters:
	##https://github.com/zbm-dev/zfsbootmenu/blob/master/pod/zfsbootmenu.7.pod
	cat << EOF > ${EFIDIR}/EFI/ubuntu/refind_linux.conf
"Boot default"  "${CMDLINE_LINUX_DEFAULT} zbm.timeout=0"
"Boot to menu"  "${CMDLINE_LINUX_DEFAULT} zbm.timeout=-1 zbm.show"1
EOF

	# https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1862822
	sed -i.bak "s/.*GRUB_DEFAULT=.*/GRUB_DEFAULT=saved/g; s/.*GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX=\"\"/g; s/.*GRUB_TIMEOUT_STYLE=.*/GRUB_TIMEOUT_STYLE=countdown/g; s/.*GRUB_TIMEOUT=.*/GRUB_TIMEOUT=10/g; s/.*GRUB_TERMINAL=.*/GRUB_TERMINAL=console/g; s/.*GRUB_RECORDFAIL_TIMEOUT=.*/GRUB_RECORDFAIL_TIMEOUT=10/g; s/.*GRUB_DISABLE_OS_PROBER=.*/GRUB_DISABLE_OS_PROBER=true/g; s/.*GRUB_DISABLE_RECOVERY=.*/GRUB_DISABLE_RECOVERY=false/g" /etc/default/grub

	./z-cachefile.bash "${POOL}"

	# Generate ZFSBootMenu
	generate-zbm
	refind-install
	refind-mkdefault
	# copy modules for running kernel from /lib/modules to /mnt/z/lib/modules
	# copy running kernel and inird.img from /media/cdrom/casper to /mnt/z/boot
	update-initramfs -k all -c
	dracut --force
	#grub-mkconfig -o /boot/grub/grub.cfg
	#update-grub

	./unison efis

popd

#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly CHECKPATH="${*}"
readonly TMPF=/tmp/$(basename "${ME}")

"${MYREALDIR}"/apt-ensure.bash moreutils >/dev/null
"${MYREALDIR}"/get-ro-fss.sh | ifne cut -d' ' -f2 > "${TMPF}"
echo "${CHECKPATH}" | grep --file "${TMPF}"

#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly KEYSD=/etc/apt/trusted.gpg.d
readonly KEYSCACHED=${MYREALDIR}/../lib/apt/trusted.gpg.d
readonly URLRE="(https?|ftp|file)://.*"
readonly KeyServers=("keyserver.ubuntu.com" "keyserver.linuxmint.com" "ha.pool.sks-keyservers.net")

function gpgexec() {
	tsp gpg ${*}
}

function process_url() {
	local keyname=${1}
	if [[ "${keyname}" == '' ]]; then
		echo "keyname is empty with process_url. Args: ${*}"
		exit 2
	fi
	shift
	local url="${*}"

	if [[ "${keyfile}" == '' ]]; then
		exit 5
	fi

	pushd "${KEYSD}"
		local keyfile=$("${MYREALDIR}"/dwnld_fname.bash "${url}")
		[ -e "${keyfile}" ] || exit 4
		process_file "${keyname}" "${keyfile}"
	popd
}

function process_file() {
	local keyname="${1}"
	local keyfile="${2}"
	local gpg_keyring_f="${keyname}.gpg"

	if [[ "${keyname}" == '' ]]; then
		echo "keyname is empty"
		exit 1
	fi
	if [[ "${keyfile}" == '' ]]; then
		echo "keyfile is empty"
		exit 6
	fi

	if ! [ -e "${keyfile}" ] && ! [[ "${keyfile}" =~ .+gpg~+ ]]; then
	       echo "keyfile ${keyfile} does not exist"
       	       exit 3
	fi

	tsp apt-key add "${keyfile}" # adds to central storage
	#"${MYREALDIR}"/ltd.bash apt-key add "${keyfile}" 2>&1 | grep -v 'Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).' & # adds to central storage
	pushd "${KEYSD}"
		if [[ $(file --brief "${keyfile}") == 'PGP public key block Public-Key (old)' ]]; then
			cat "${keyfile}" | gpg --yes --dearmor > "${gpg_keyring_f}"
		else
			"${MYREALDIR}"/focopy.bash "${keyfile}" "${gpg_keyring_f}"
		fi
		tsp apt-key add "${gpg_keyring_f}" # adds to central storage
		#"${MYREALDIR}"/ltd.bash apt-key add "${gpg_keyring_f}" 2>&1 | grep -v 'Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).' & # adds to central storage
		tsp gpg --yes --no-default-keyring --keyring "${gpg_keyring_f}" --import "${gpg_keyring_f}"
		#gpgexec --yes --no-default-keyring --keyring "${tempkeyring}" --export --output "${gpg_keyring_f}" &

	popd
}

function remove_unsupported() {
	apt-key list 2>&1 | awk '/^W: .* unsupported/ {print $7}' | ifne "${MYREALDIR}"/xargs.bash tsp rm
}

function sync_dirs()
{
	chmod 0644 "${KEYSCACHED}"/* "${KEYSD}"/*
	tsp "${MYREALDIR}"/dedup.bash "${KEYSD}"
	tsp "${MYREALDIR}"/dedup.bash "${KEYSCACHED}"
	tsp "${MYREALDIR}"/dlt.bash "${KEYSCACHED}"/*~ "${KEYSD}"/*~
	remove_unsupported
	"${MYREALDIR}"/focopy.bash -u "${KEYSCACHED}"/* "${KEYSD}"/
	remove_unsupported
	"${MYREALDIR}"/focopy.bash -u "${KEYSD}"/* "${KEYSCACHED}"/
}

function recv_keys() {
	for serv in ${KeyServers[@]}; do
		tsp apt-key adv --keyserver "${serv}" --recv-keys
		#"${MYREALDIR}"/ltd.bash apt-key adv --keyserver "${serv}" --recv-keys 2>&1 | grep -v 'Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).' &>/dev/null &
	done
}

function process_existing() {
	readonly ExistingKeyFiles=( $(ls -A1 /cdrom/dists/*/*.gpg || true; ls -A1 "${KEYSD}"/*.gpg || true; ls -A1 "${KEYSCACHED}"/*.gpg || true ) )
	if (( ${#ExistingKeyFiles[@]} == 0 )); then
		echo "No existing key files?"
		exit 10
	fi
	for keyfile in "${ExistingKeyFiles[@]}"; do
		base=$(basename "${keyfile}")
		keyname="${base%.*}"
		if [[ "${keyname}" == '' ]]; then
			echo "keyname is empty in a loop"
			exit 7
		fi
		if [[ "${keyname}" == '' ]]; then
			echo "keyfile is empty in a loop"
			exit 8
		fi
		process_file "${keyname}" "${keyfile}"
	done
}

echo ifne wget tsp | "${MYREALDIR}"/apt-ensure.bash $(cat "${MYREALDIR}"/../lib/apt.list) moreutils wget task-spooler
tsp -S $("${MYREALDIR}"/cpus.sh)

declare -A Keys

if (( ${#} > 0 )); then
	if [[ "${1}" == 'existing' ]]; then
		process_existing
		shift
	fi
	i=1
	while (( ${#} > 0 )); do
		shift
		Keys["$i"]=$1
		i=$(( i + 1 ))
	done
else
	if ls -1A  *keys.bash.dict; then
	       for keyf in *keys.bash.dict; do
			source "${keyf}"
		done
	else
		for keyf in "${MYREALDIR}"/../layers/*/*keys.bash.dict; do
			source "${keyf}"
		done
	fi
fi

# Perform an update working similarly to the update command above, but get the archive keyring from a URI instead and validate it against a master key. This requires an installed wget(1) and an APT build configured to have
# a server to fetch from and a master keyring to validate. APT in Debian does not support this command, relying on update instead, but Ubuntu's APT does.
tsp apt-key net-update
#"${MYREALDIR}"/ltd.bash apt-key net-update 2>&1 | grep -v 'Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).' &>/dev/null & # may exit with code 1

tsp apt-key update
#apt-key update 2>&1 | grep -v 'Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).'

tsp "${MYREALDIR}"/deledupes.bash "${KEYSD}"
tsp "${MYREALDIR}"/deledupes.bash "${KEYSCACHED}"

recv_keys &>/dev/null || true
sync_dirs &>/dev/null

pushd "${KEYSD}"
	for keyname in "${!Keys[@]}"; do
		v=${Keys["${keyname}"]}

		# https://askubuntu.com/questions/1286545/what-commands-exactly-should-replace-the-deprecated-apt-key
		if [[ "${v}" =~ ${URLRE} ]]; then
			process_url "${keyname}" "${v}" &
		else
			keyfile="${keyname}.asc"
			gpg_keyring_f="${keyname}.gpg"
			for serv in ${KeyServers[@]}; do
				# apt-key adds to central storage
				tsp "${MYREALDIR}"/ltd.bash apt-key adv --keyserver "${serv}" --recv-keys "${v}"
				#tsp -u
				#apt-key adv --keyserver "${serv}" --recv-keys "${v}" 2>&1 | grep -v 'Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).' &>/dev/null &
				#apt-key adv --keyserver "${serv}" --recv-keys "${v}" 2>&1 | grep -v 'Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).' &>/dev/null &
			done
		fi

	done
	wait
	tsp -f true
popd

#for key in $("${MYREALDIR}"/apt-update.bash 2>&1 | awk '/^W: GPG error: / {print $NF}' | sort -u); do
#	for serv in ${KeyServers[@]}; do
#		apt-key adv --keyserver "${serv}" --recv-keys "${key}" 2>&1 | grep -v 'Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).' & # adds to central storage && break
#	done
#done

recv_keys
sync_dirs &>/dev/null &

tsp -f true

"${MYREALDIR}"/apt-update.bash

#tsp "${ME}" existing

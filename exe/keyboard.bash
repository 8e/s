#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd >/dev/null

function install() {
	if [ -e ~/.xinitrc ]; then
		grep "${ME}" -- ~/.xinitrc || echo "${ME}" >> ~/.xinitrc
	fi
}

function setmap() {
	setxkbmap -option grp:switch,grp:alt_shift_toggle,grp_led:scroll us,ru &
	setxkbmap -option grp:switch,grp:alt_shift_toggle,grp_led:scroll birman-us,birman-ru &
	setxkbmap -option grp:switch,grp:alt_shift_toggle,grp_led:scroll typo-birman-en,typo-birman-ru &
	kfg_kb
}

setupcon
setmap

while [ ${#} -gt 0 ]; do
        case ${1} in
                'install')
                        install
                        exit 0
                ;;
                *)
                ;;
        esac
done

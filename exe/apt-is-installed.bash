#!/usr/bin/env bash

set -e

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	dpkg-query -W -f='${Status}' ${*}  | grep "ok installed" &>/dev/null
else
	brew list ${*} &> /dev/null
fi
exit $?

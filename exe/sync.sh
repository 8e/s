#!/bin/sh

set -e

if [ $DEBUG ]; then
	set -x
fi

/usr/bin/env zpool sync &
/usr/bin/env sync

#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

readonly KEY=${1}
readonly VALUE=${2}

source "${MYREALDIR}"/../defaults.rc.bash

if [[ "${KEY}" == '' ]] || [[ "${VALUE}" == '' ]]; then
	exit 1
fi

function writeafresh() {
	echo "${KEY}=${VALUE}" | sudo tee /etc/sysctl.d/${KEY}.conf
}

echo sysctl ifne | "${MYREALDIR}"/apt-ensure.bash procps moreutils
sudo sysctl ${KEY}=${VALUE}

declare -a ContConfigs=($(grep -RIl -e "^${KEY}" /etc/sysctl* || true))
if (( "${#ContConfigs}" == 0 )); then
	writeafresh
else
	declare -a OtherSettings=("$(awk "/^${KEY}/ && ! /${KEY} *= *${VALUE}$/" "${ContConfigs[@]}" || true | ifne sort -u)")
	if (( "${#OtherSettings}" != 0 )); then
		sudo "${EDITOR}" $(printf '%s\n' "${OtherSettings[@]}" | "${MYREALDIR}"/xblissargs.bash -I{} grep -RIl -e {} "${ContConfigs[@]}")
	elif ! grep "/^${KEY} *= *${VALUE}$/" "${ContConfigs[@]}"; then
		writeafresh
	fi
fi

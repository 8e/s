#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly DAEMON_NAME=caches
readonly DAEMON_PATH=/etc/systemd/system/"${DAEMON_NAME}".service
readonly LOGF="/tmp/log/$(basename ${ME}).log"
readonly BASED=/tmp/caches
readonly TMPFS_OPTIONS=$(sudo systemctl show tmp.mount | awk -vFS='^Options=' '/^Options/ {print $2}')
readonly SAVED=/var/backups/caches
readonly MNT_OPTS=strictatime
readonly EXCLUDE='trash|stversion|wine'

function linktarget() {
	ls -l "${*}" | awk -vFS='-> ' '{print $NF}'
}

function cache_owner() {
	local what="${*}"
	if echo "${what}" | grep "^/root" >>"${LOGF}"; then
		u=root
	else
		u="${DEFUSER}"
	fi
	echo "${u}"
	sudo chown -Rc "${u}":"${u}" "${what}" >> "${LOGF}"
}

#function mount_fstab() {
#	readonly FSTAB=$(sudo "${MYREALDIR}"/ensure-path.bash /etc/fstab.d)/caches
#	local what="${*}"
#	if grep -F "${what}" "${FSTAB}" | grep -v '^#'; then
#		sudo mount --fstab "${FSTAB}" "${what}"
#	else
#		#<file system> <mount point> <type> <options> <dump> <pass>
#		echo -e "tmpfs\t$(echo ${what} | sed 's| |\\\\040|g')\ttmpfs\t${MNT_OPTS}\t0\t0" | sudo tee -a "${FSTAB}"
#		sudo mount --fstab "${FSTAB}" "${what}"
#	fi
#}
#
#function our_fstab_mounts() {
#	("${MYREALDIR}"/zswap-status.bash || "${MYREALDIR}"/zswap-enable.bash) &>/dev/null &
#	[ -e "${FSTAB}" ] || sudo touch "${FSTAB}"
#	[[ $(sudo "${MYREALDIR}"/mode.bash "${FSTAB}") == '644' ]] || sudo chmod 644 "${FSTAB}"
#	if [ -e "${FSTAB}" ]; then
#		sudo mount -av --fstab "${FSTAB}"
#	fi
#}
#function fstab_list() {
#	awk -vOFS='' '{$1=$NF=$(NF-1)=$(NF-2)=$(NF-3)=""; print}' "${FSTAB}" | sed 's|040| |g'
#}

function mounts_list() {
	mount | awk -vOFS="" '($1=="tmpfs") && ($3!="/tmp") && ($3!="/dev/shm") && ($3!="/run") && ($3!="/run/lock") && ($3!="/etc/cron.hourly") && ($3!="/etc/cron.daily") && ($3!="/etc/cron.weekly") {$1=$2=$NF=$(NF-1)=$(NF-2)=""; print}'
}

function mount_sharedbind() {
	local what="${*}"
	local u=$(cache_owner "${what}")
	local mode=$("${MYREALDIR}"/mode.bash "${what}")

	if [[ $(basename "${what}") == 'ScriptCache' ]]; then
		sudo mount -t tmpfs -o "${MNT_OPTS}",X-mount.mkdir="${mode}",uid=${u},user=${u},owner=${u} "${what}"
	else
		sudo mount --rbind -o "${MNT_OPTS}",X-mount.mkdir="${mode}",uid=${u},user=${u},owner=${u} "${BASED}/${u}" "${what}"
	fi
}

function filt() {
	ifne egrep -i "${EXCLUDE}" || true
}

function mountcache() {
	local what="${*}"
	local u=$(cache_owner "${what}")

	if ! [ -e "${what}" ]; then
		echo "cannot ensure mounting "${what}" — it does not exist!"
		exit 3
	elif df -t tmpfs "${what}" 2>/dev/null; then
		return 0
	elif mount | grep -F "${what}"; then
		return 0
	elif realpath "${what}" | filt >/dev/null; then
		return 0
	else
		mount_sharedbind "${what}"
	fi
 	
	local savedpath=$(get_saved_path "${what}")
	echo "${savedpath}"

	if [ -e "${savedpath}" ]; then
		sudo -u "${u}" cp -a "${savedpath}"/ "${what}"
	fi
}

function get_saved_parent() {
	local what="${*}"
	echo $("${MYREALDIR}"/ensure-path.bash "${SAVED}/$(dirname "${what}")")
}

function get_saved_path() {
	local what="${*}"
	echo $(get_saved_parent "${what}")/$(basename "${what}")
}


function save_all() {
	mounts_list | while read what; do
		savecache "${what}" &
	done
	wait
	tsp "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/dedup.bash "${SAVED}"
	tsp chown -cR "${DEFUSER}":"${DEFUSER}" "${SAVED}"
}

function movecache() {
	local what="${*}"
	local u=$(cache_owner "${what}")
	savecache "${what}"
	sudo -u "${u}" "${MYREALDIR}"/dlt.bash "${what}"
}

function savecache() {
	local what="${*}"
	local u=$(cache_owner "${what}")

	local savedparent=$(get_saved_parent "${what}")
	echo "${savedparent}"

	sudo chown -Rc "${u}":"${u}" "${savedparent}" &
	sudo -u "${u}" rsync -svauhHPXlitA --append --inplace --link-dest="${savedparent}" --link-dest="${SAVED}" "${what}" "${savedparent}"
}

function cachefy() { # mount version
	local what="${*}"
	local u=$(cache_owner "${what}")

	if realpath "${what}" | egrep -i 'trash|stversion|wine' >/dev/null; then
		return 0 # skip
	elif [[ $(basename "${what}") =~ .*Trash.* ]]; then
		echo "${what}" | sudo -u "${u}" "${MYREALDIR}"/ensure-symlink.bash /var/backups
		return 0
	elif ! [ -e "${what}" ]; then
		echo "cannot do anything with "${what}" — it does not exist!"
		exit 4
	elif df -t tmpfs "${what}" 2>/dev/null; then
		return 0
	elif mount | grep -F "${what}"; then
		return 0
	elif [ -L "${what}" ]; then
		local target=$(linktarget "${what}")
		sudo -u "${u}" "${MYREALDIR}"/ensure-path.bash "${target}"
		if ! df -t tmpfs "${target}" 2>/dev/null; then

			echo "'${what}' and its target '${target}' are not on tmpfs"
			ls -ald "${what}" "${target}"
			df -hT "${what}" "${target}"
			du -chs "${what}" "${target}"
			exit 2

		fi
	elif [ -d "${what}" ]; then

		if ! "${MYREALDIR}"/dirempty.bash "${what}"; then
			
			movecache "${what}"

			sudo -u "${u}" "${MYREALDIR}"/ensure-path.bash "${what}"
			sudo chown --reference="${savedpath}" -v "${what}"

		fi

		mountcache "${what}"

	else

		echo "Interesting case with ${what}"
		ls -ald "${what}"
		df -hT "${what}"
		du -chs "${what}"
		exit 1

	fi
}

#function cachefy_symlink_one() { # Symlink version
#	local what="${*}"
#	local u=$(get_owner "${what}")
#	target="${BASED}"/"${u}"
#	if df -t tmpfs "${what}"; then
#		return 0
#	fi
#	echo "echo \"${what}\" | sudo \"${MYREALDIR}\"/ensure-symlink.bash \"${target}\""
#	sudo "${MYREALDIR}"/ensure-path.bash "${BASED}"
#	sudo chmod 1777 "${BASED}"
#	if [ -e "${what}" ]; then
#		if [ -d "${target}" ]; then
#			"${MYREALDIR}"/dlt.bash -Rv "${what}"
#		else
#			mv "${what}" "${target}"
#		fi
#		echo "${what}" | sudo "${MYREALDIR}"/ensure-symlink.bash "${target}"
#	elif [ ! -e "${what}" ]; then
#		"${MYREALDIR}"/ensure-path.bash "${target}"
#		echo "${what}" | sudo "${MYREALDIR}"/ensure-symlink.bash "${target}"
#	fi
#	sudo chown -R "${u}":"${u}" "${target}" "${what}"
#}

function install() {
	[ -e "${LOGF}" ] || touch "${LOGF}"

	echo -n "[Unit]
Description=Cache Dirs
After=tmp.mount
Requires=tmp.mount
After=zfs-mount.service
Requires=zfs-mount.service
Before=sddm.service

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart="${ME}"
Nice=-20
IOSchedulingPriority=0

[Install]
WantedBy=default.target
WantedBy=multi-user.target
WantedBy=graphical.target" | "${MYREALDIR}"/ensure-contents.bash "${DAEMON_PATH}"
	sudo systemctl daemon-reload
	sudo systemctl enable "${DAEMON_NAME}"
	sudo systemctl status "${DAEMON_NAME}"
}

function cachefy_all() {
	if [ -e "${LOGF}" ]; then
		true
	else
		sudo "${MYREALDIR}"/ensure-path.bash "$(dirname ${LOGF})"
		sudo chmod -c g+rws,o+rwt "$(dirname ${LOGF})"
		touch "${LOGF}"
	fi

	if [[ $( realpath /var/tmp ) != /tmp ]]; then
		sudo "${MYREALDIR}"/dlt.bash -Rv /var/tmp
		echo /var/tmp | sudo "${MYREALDIR}"/ensure-symlink.bash /tmp
	fi

	#our_fstab_mounts &
	for hd in /root /home/* /home/*/.config; do
		u=$(cache_owner "${hd}")
		#u=$(basename "${hd}")
		sudo "${MYREALDIR}"/cleanup.bash find "${hd}" -xdev '(' -type d -or -type l ')' | filt | ifne sudo -Eu "${u}" "${MYREALDIR}"/xargs.bash -l sudo -Eu "${u}" "${ME}"
		# -exec interferes with the reslts for some reason # sudo "${MYREALDIR}"/cleanup.bash find "${hd}" "${hd}"/.config -xdev '(' -type d -or -type l ')' | sort -u " :exec echo sudo -Eu $(basename "${hd}") "${ME}" '{}' \;
	done

	# should be on same fs
	#sudo "${MYREALDIR}"/ensure-path.bash /tmp/caches/_apt
	#echo /var/cache/apt/archives/partial | sudo "${MYREALDIR}"/ensure-symlink.bash /tmp/caches/_apt
	#sudo chown _apt:root /tmp/caches/_apt
	#sudo chmod 0700 /tmp/caches/_apt
}

if ! [ -d /run/user/$UID ]; then
	sudo mkdir -pv /run/user/$UID
	sudo chown -c $UID:$UID /run/user/$UID
fi
echo xargs ifne rsync | "${MYREALDIR}"/apt-ensure.bash coreutils moreutils rsync &
sudo "${MYREALDIR}"/ensure-path.bash "$(dirname ${LOGF})"
sudo "${MYREALDIR}"/ensure-path.bash /tmp/{caches,log}/{root,"${USER}","${DEFUSER}"} /var/run /run/screen
sudo chown -c "${USER}:${USER}" /tmp/{caches,log}/"${USER}"
sudo chown -c "${DEFUSER}:${DEFUSER}" /tmp/{caches,log}/"${DEFUSER}"
sudo chmod -c g+rwsx,o+rwxt /tmp/caches /tmp/log
sudo chmod -c u+rwx,g+rwx,o+rwxt /run /var/run /run/screen
sudo chmod -c g+rws,o+rwt /tmp/{caches,log}/* "$(dirname ${LOGF})"
sudo chmod g+rw,o+rwt "${LOGF}" &
sudo chmod -c 1777 /run/user /tmp/caches /tmp/log /tmp
echo /root/.cache | "${MYREALDIR}/ensure-symlink.bash" "/tmp/caches/root"
echo /home/"${USER}"/.cache | sudo -u "${USER}" "${MYREALDIR}/ensure-symlink.bash" "/tmp/caches/${USER}"
echo /home/"${DEFUSER}"/.cache | sudo -u "${DEFUSER}" "${MYREALDIR}/ensure-symlink.bash" "/tmp/caches/${DEFUSER}"

if [ ${#} -eq 0 ]; then
	cachefy_all
elif [[ "${*}" == 'install' ]]; then
	install
elif [[ "${*}" == 'save_all' ]]; then
	save_all
elif [[ "${*}" == 'mounts' ]]; then
	mounts_list
elif [[ "${1}" == 'owner' ]]; then
	shift
	cacheowner "${*}"
else
	cachefy "${*}"
fi

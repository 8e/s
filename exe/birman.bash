#!/usr/bin/env bash

set -ex

readonly MYREALDIR=$(dirname $(realpath ${0}))

targetd=$("${MYREALDIR}"/git-installrepo.bash https://github.com/isqua/xkb-birman-layout.git)

pushd "${targetd}"
	# Create directory for your own layouts
	"${MYREALDIR}"/ensure-path.bash ~/.config/xkb "${MYREALDIR}"/../lib/home/.config/xkb

	# Put this layout to your home directory
	ln -svf $(realpath home/config/xkb/symbols) "${MYREALDIR}"/../lib/home/.config/xkb/

	# Add this XCompose settings to your XCompose
	ln -svf $(realpath home/XCompose) "${MYREALDIR}"/../lib/home/.XCompose

	echo 'setxkbmap "birman-us,birman-ru" "birman,birman" "grp:switch,grp:alt_shift_toggle,grp_led:scroll" -print | xkbcomp -I${HOME}/.config/xkb - $DISPLAY' >> "${MYREALDIR}"/../lib/home/.xinitrc
	#setxkbmap -option grp:switch,grp:alt_shift_toggle,grp_led:scroll birman-us,birman-ru
	
popd

targetd=$("${MYREALDIR}"/git-installrepo.bash https://github.com/neochief/birman-typography-layouts-for-ubuntu.git)
pushd "${targetd}"
	bash -x ./install.sh || true
popd

"${MYREALDIR}"/keyboard.bash install


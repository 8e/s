#!/usr/bin/env bash

readonly MYREALDIR=$(dirname $(realpath ${0}))

dpkg -i /datos/soft/linux/zsys_0.5.7_$(dpkg --print-architecture).deb

"${MYREALDIR}"/dpkg-loosen.bash zsys

ls -1A /var/lib/dpkg/info/zsys.*inst && source /var/lib/dpkg/info/zsys.*inst

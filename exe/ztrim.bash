#!/usr/bin/env bash

set -xe

zpool list -o name -H | xargs -l zpool trim ${*}

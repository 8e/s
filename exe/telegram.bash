#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
	source ../defaults.rc.bash
popd >/dev/null

sudo renice -n -10 $$
sudo ionice -c 0 -n 0 -p $$

declare -a SubDirs=(dumps temp cache media_cache)
declare -a Dirs=(user_data 'user_data#2' 'user_data#3' 'user_data#4' temp_data 'temp_data#2' 'temp_data#3' 'temp_data#4')

function do_onedir() {
	local name="${1}"
	shift
	local targparent="${*}"
	local target="${targparent}/$name"
	"${MYREALDIR}"/ensure-path.bash "${targparent}"
	if [ -L "${target}" ]; then
		rm -v "${target}"
	elif [ -d "${target}" ]; then
		rm -rv "${target}"
	fi
	if [ -e "${name}" ] && [[ $(realpath "${name}") != "${target}" ]]; then
		if ! mv "${name}" "${targparent}"; then
			realpath "${name}" "${targparent}"
			exit 1
		fi
		echo "${name}" | "${MYREALDIR}"/ensure-symlink.bash "${target}"
	elif [ ! -e "${name}" ]; then
		"${MYREALDIR}"/ensure-path.bash "${target}"
		"${MYREALDIR}"/ensure-path.bash $(realpath $(dirname "${name}"))
		echo "${name}" | "${MYREALDIR}"/ensure-symlink.bash "${target}"
	fi
}

function do_subdirs() {
	local targparent="${*}"
	for subd in ${SubDirs[*]}; do
		do_onedir "${subd}" "${targparent}"
	done
}

function do_dirs() {
	local targparent="${*}"
	for d in ${Dirs[*]}; do
		do_onedir "${d}" "${targparent}"
		pushd "${d}"
			do_subdirs "${targparent}"
		popd
	done
}

for twd in "${@}";  do

	"${MYREALDIR}"/ensure-path.bash /tmp/caches/"${USER}"

	pushd "${twd}/tdata"

		do_dirs /tmp/caches/${USER}/Telegrams/$(basename "${twd}")

	popd

	#[ -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5 ] || sudo chmod +x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5
	#DISPLAY=:0.0 /opt/Telegram/Updater -startintray -workdir "${twd}" &
	/opt/Telegram/Telegram -startintray -workdir "${twd}" &
	#[ -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5 ] && sudo chmod -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5
	p=$!
	sudo renice -n -10 ${p}
	sudo ionice -c 0 -n 0 -p ${p}

done

#!/usr/bin/env bash

set -e
DEBUG=${DEBUG:-false}
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure-cmd.bash rsync rsync

declare -a Opts=(-svauhHPXlitA --inplace --append --open-noatime --ignore-errors)

"${MYREALDIR}"/screen.bash "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/cleanup.bash rsync-insensitive-exclude "${Opts[@]}" ${*}

"${MYREALDIR}"/ltd.bash rsync "${Opts[@]}" -c ${*}

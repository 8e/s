#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))
source "${MYREALDIR}"/../defaults.rc.bash

#"${MYREALDIR}"/vundle-install.bash
#"${MYREALDIR}"/birman.bash

pushd "${MYREALDIR}"/..
	pushd lib/home/
		# IMPORTANT: please note that you might override an existing
		# configuration file in the current working directory! =>
		wget -O .zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
		# Optionally also grab the user configration:
		wget -O .zshrc.local  https://git.grml.org/f/grml-etc-core/etc/skel/.zshrc
	popd

	rsync -svahHP --open-noatime --backup --progress lib/home/ /etc/skel
	rsync -svahHP --open-noatime --backup --progress lib/home/ /root
	rsync -svahHP --open-noatime --backup --progress lib/home/ /home/"${DEFUSER}"
	#exe/ensure-path.bash /root/.ssh /home/"${DEFUSER}"/.ssh
	#chmod 0700 /etc/skel/.ssh /root/.ssh /home/"${DEFUSER}"/.ssh
	#cat ./lib/PUB | tee /root/.ssh/authorized_keys > /home/"${DEFUSER}"/.ssh/authorized_keys
	#chmod 0600 /etc/skel/ssh/authorized_keys /root/.ssh/authorized_keys /home/"${DEFUSER}"/.ssh/authorized_keys
	id "${DEFUSER}" &>/dev/null || "${MYREALDIR}"/admadd.bash "${DEFUSER}"
	chown -R "${DEFUSER}":"${DEFUSER}" /home/"${DEFUSER}"

	which zsh && chsh -s $(which zsh)
	which zsh && chsh -s $(which zsh) "${DEFUSER}"

	systemctl restart ssh &

popd

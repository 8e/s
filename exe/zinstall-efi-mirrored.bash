#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly MYDIR=$(dirname $(realpath ${0}))
readonly OSID=$(cat ${MYDIR}/../lib/OSID)
readonly DISK=${1}
readonly SWAPSIZE="$(( $(${MYDIR}/ramsizekb.bash) * 1.5 ))K"
[[ "${DISK}" == '' ]] && exit 1
[[ "${OSID}" == '' ]] && exit 2

${MYDIR}/apt-lists.bash -y install ${MYDIR}/../lib/installer.list
${MYDIR}/apt-ensure.bash mdadm cryptsetup

systemctl stop zed

sgdisk --zap-all $DISK  #DANGEROUS

# For both legacy and EFI booting:
sgdisk     -n1:1M:+512M   -t1:EF00 $DISK

# For legacy (BIOS) booting:
sgdisk -a1 -n5:24K:+1000K -t5:EF02 $DISK

# Mirrored swap
# sgdisk     -n2:0:+500M    -t2:FD00 $DISK # mdadm label
#sgdisk     -n2:0:+500M    -t2:8200 $DISK # swap label

#mdadm --create /dev/md0 --chunk=4K  --metadata=1.2 --level=raid10 --raid-devices=2 --verbose ${DISK}-part2 missing
#mkswap -f /dev/md0 -p $(getconf PAGESIZE) -L swap
### 

# sgdisk     -n2:0:0      -t2:BF00 $DISK
sgdisk     -n3:0:+2G      -t3:BE00 $DISK

sgdisk     -n4:0:0        -t4:BF00 $DISK

fdisk -l $DISK

while ! ls -1 ${DISK}-part4; do
	(partprobe $DISK; kpartx $DISK) || true
	sleep 1.33
done

exe/z-rootcreate.bash -R /mnt/z ${DISK}-part4

exe/z-createboot.bash -R /mnt/z ${DISK}-part3

debootstrap "${DISTRO}" /mnt/z

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly AGENT="/etc/hetrixtools/hetrixtools_agent.sh"
readonly DELAY=x1
readonly DAEMON_NAME=hetrixtools_agent
readonly DAEMON_PATH=/etc/systemd/system/"${DAEMON_NAME}".service
readonly DAEMONS=syncthing,hetrixtools_agent,ssh,docker,sddm,NetworkManager,networking,zfs-zed,snapd,zfs
readonly PORTS=22,8384,22000
readonly LOGF=/var/log/hetrixtools_agent.log
readonly DAEMON_FILE_CONTENTS="
[Unit]
Description=Hetrixtools Agent
After=network.target
After=tmp.mount
Requires=tmp.mount
After=caches.service
Wants=caches.service

[Service]
Type=idle
ExecStart=$(realpath ${MYREALDIR}/continuous.bash) ${DELAY} ${AGENT} 2>&1 >> ${LOGF}
Restart=on-failure
SuccessExitStatus=3 4
RestartForceExitStatus=3 4
User=root
Nice=18
IOSchedulingClass=idle
IOSchedulingPriority=7

# https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html
CPUQuota=3%
# A higher weight means more CPU time, a lower weight means less. The allowed range is 1 to 10000
CPUWeight=1
StartupCPUWeight=1

# Specify the throttling limit on memory usage of the executed processes in this unit. Memory usage may go above the limit if unavoidable, but the processes are heavily slowed down and memory is taken away aggressively in such cases. This is the main mechanism to control memory usage of a unit.
# Takes a memory size in bytes. If the value is suffixed with K, M, G or T, the specified memory size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively. Alternatively, a percentage value may be specified, which is taken relative to the installed physical memory on the system. This setting is supported only if the unified control group hierarchy is used and disables MemoryLimit=.
MemoryHigh=5%

# Specify the absolute limit on swap usage of the executed processes in this unit.
# Takes a swap size in bytes. If the value is suffixed with K, M, G or T, the specified swap size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively.
MemorySwapMax=1G

# Hardening                                                                      
SystemCallArchitectures=native                                                   
MemoryDenyWriteExecute=true                                                      
NoNewPrivileges=true
PrivateDevices=true
PrivateNetwork=true
ProtectSystem=true

[Install]
WantedBy=graphical.target
WantedBy=default.target
WantedBy=multi-user.target
"

if [[ $HETRIXTOOLS_ID == '' ]]; then
        read -e -p "Enter hetrix tools ID: " HETRIXTOOLS_ID
fi

if [[ "${HETRIXTOOLS_ID}" == '' ]]; then
        echo "NO HETRIXTOOLS_ID provided"
        echo "skipping HetrixTools agent setup"
        exit 0
fi

pushd /tmp
	wget -c https://raw.github.com/hetrixtools/agent/master/hetrixtools_install.sh
	bash hetrixtools_install.sh "${HETRIXTOOLS_ID}" 1 "${DAEMONS}" 0 1 1 "${PORTS}"
popd

${MYREALDIR}/cron-removejob.bash hetrix

[ -e "${LOGF}" ] || touch "${LOGF}"
[[ $(realpath /etc/hetrixtools/hetrixtools_agent.log) == "${LOGF}" ]] || ln -svf "${LOGF}" /etc/hetrixtools/hetrixtools_agent.log

echo -n "${DAEMON_FILE_CONTENTS}" | "${MYREALDIR}"/ensure-contents.bash "${DAEMON_PATH}"
sudo systemctl daemon-reload

sudo systemctl start "${DAEMON_NAME}"
sudo systemctl status "${DAEMON_NAME}"

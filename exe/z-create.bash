#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

if [[ $POOLNAME == '' ]]; then
        read -e -p "Enter pool name: " POOLNAME
fi

declare -A ZEverys

declare -A ZFeats
ZFeats["async_destroy"]='enabled'
ZFeats["lz4_compress"]='enabled'
ZFeats["embedded_data"]='enabled'
ZFeats["empty_bpobj"]='enabled'
ZFeats["enabled_txg"]='enabled'
ZFeats["extensible_dataset"]='enabled'
ZFeats["filesystem_limits"]='enabled'
ZFeats["hole_birth"]='enabled'
ZFeats["large_blocks"]='enabled'
ZFeats["large_dnode"]='enabled'
ZFeats["bookmarks"]='enabled'
ZFeats["spacemap_histogram"]='enabled'
ZFeats["userobj_accounting"]='enabled'
ZFeats["resilver_defer"]='enabled'
ZFeats["zpool_checkpoint"]='enabled'
ZFeats["allocation_classes"]='enabled'
ZFeats["project_quota"]='enabled'
ZFeats["resilver_defer"]='enabled'
ZFeats["spacemap_v2"]='enabled'

declare	-A ZPoolOpts
ZPoolOpts["ashift"]=12
ZPoolOpts["autotrim"]='on'

declare	-A ZfsOpts
ZfsOpts['canmount']='off'
ZfsOpts['mountpoint']='none'
ZfsOpts['com.sun:auto-snapshot']='true'
ZfsOpts["acltype"]='posixacl'
ZfsOpts["compression"]='zstd' # 'zstd-19'
ZfsOpts["normalization"]='formD'
ZfsOpts["checksum"]='on' # https://openzfs.github.io/openzfs-docs/Basic%20Concepts/Checksums.html
ZfsOpts["atime"]='off'
ZfsOpts["xattr"]='sa' # or "on"
ZfsOpts["snapdir"]='hidden'
# no boot:
ZfsOpts["encryption"]='aes-256-gcm'
ZfsOpts["keylocation"]='prompt'
ZfsOpts["keyformat"]='passphrase'
ZfsOpts["dnodesize"]='auto'
# custom:
ZfsOpts["recordsize"]='512K'
ZfsOpts["dedup"]='on'
ZfsOpts['snapshot_limit']='1000'

declare -a Options=()

for key in ${!ZEverys[@]}; do
	ZFeats["${key}"]=${ZEverys["${key}"]}
	ZPoolOpts["${key}"]=${ZEverys["${key}"]}
	ZfsOpts["${key}"]=${ZEverys["${key}"]}
done

for feature in ${!ZFeats[@]}; do
	ZPoolOpts["feature@${feature}"]=${ZFeats["${feature}"]}
done

for poolopt in ${!ZPoolOpts[@]}; do
	Options+=(-o "${poolopt}"="${ZPoolOpts[${poolopt}]}")
done

for zfsopt in ${!ZfsOpts[@]}; do
	Options+=(-O "${zfsopt}"="${ZfsOpts[${zfsopt}]}")
done

echo -e "\nSample usage: ${0} -o altroot=/mnt/z -R /mnt/z mirror ada0p3 ada1p3\n"
CMD="zpool create ${Options[@]} ${POOLNAME} ${*}"

echo -e "\n${CMD}\n"

${CMD}

[[ $(echo -e 'yes\nno' | "${MYREALDIR}"/dialog.bash "Create space reservation?") == 'no' ]] || "${MYREALDIR}"/z-res.bash ${POOLNAME}

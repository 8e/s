#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

set -o pipefail

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

which teamviewer || sudo "${MYREALDIR}"/apt-urls.bash https://download.teamviewer.com/download/linux/teamviewer_$(dpkg --print-architecture).deb

source "${MYREALDIR}"/../defaults.rc.bash

teamviewer daemon enable
teamviewer repo
teamviewer daemon start
teamviewer info
#readonly TVID=$(teamviewer info | awk '/TeamViewer ID/ {print $NF}')
#teamviewer daemon status # interactive

su - "${DEFUSER}" -c "DISPLAY=:0.0 teamviewer" &

#if (( TVID > 0 )); then
#	read -e -p "Enter teamviewer temporary password (at least 8 characters): " TP
#	teamviewer passwd ${TP}
#	teamviewer daemon restart
#	echo -e "Now you can try:
#	ID:\t${TVID}
#	with a temp password:\t${TP}
#	"
#else
#	su - "${DEFUSER}" -c "DISPLAY=:0.0 teamviewer" &
#fi

## commented above won't connect
#
#teamviewer passwd "${*}"
#teamviewer setup
#

#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))

if [ ${#} -eq 0 ]; then
	declare -a Pools=(rpool bpool)
else
	declare -a Pools=("${@}")
fi

for pl in "${Pools[@]}"; do
	"${MYREALDIR}"/zinit-pool.bash $pl
	"${MYREALDIR}"/zfs-mount.bash $pl
done

"${MYREALDIR}"/mount-rbind-chroot.bash

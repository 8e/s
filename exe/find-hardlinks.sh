#!/usr/bin/env sh

set -ex
find ${*} -type f -links +1

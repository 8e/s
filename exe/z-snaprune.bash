#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

zfs list -H -t snapshot -o name | grep "^${1:-rpool}" | xargs -l zfs destroy -v

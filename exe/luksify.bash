#!/usr/bin/env bash

set -xe

if [ -e ${1} ]; then
	readonly BACKDEV=${1}
elif [ -e $(pwd)/$(basename ${1}) ]; then
	readonly BACKDEV=(pwd)/$(basename ${1})
else
	echo "Block device not found"
	exit 1
fi

#read -e -p "Enter mapper name for ${BACKDEV}: " MAPPERNAME
readonly MAPPERNAME="$(basename "${BACKDEV}").luks"

readonly MYREALDIR=$(dirname $(realpath ${0}))

readonly INTALGO=${2:-$("${MYREALDIR}"/print-cryptsetup-algorithms.bash | "${MYREALDIR}"/dialog.bash "Integrity check algorithm:")}
readonly ENC_CIPH=${3:-$("${MYREALDIR}"/print-cryptsetup-algorithms.bash | "${MYREALDIR}"/dialog.bash "Encryption Cipher:")}

readonly KEYSIZE=$(getconf PAGESIZE)
readonly SECTORSIZE=$(getconf PAGESIZE)
readonly DEFKF="/etc/luks/${MAPPERNAME}.key"

readonly KEYOPT=${4}
if [[ "${KEYOPT}" == '' ]]; then
	readonly KEYF="${DEFKF}"
fi

which cryptsetup || "${MYREALDIR}"/apt.bash install cryptsetup

if [[ "${KEYOPT}" == '' ]]; then
	"${MYREALDIR}"/ensure-path.bash /etc/luks

	[ -e ${KEYF} ] || tr -cd '[:alnum:]' < /dev/urandom | dd of="${KEYF}" bs="${KEYSIZE}" count=1 && chmod 0600 "${KEYF}"

	cryptsetup -v luksFormat --key-file "${KEYF}" --type luks2 "${BACKDEV}" --cipher "${ENC_CIPH}" --label "${MAPPERNAME}" --sector-size ${SECTORSIZE} --integrity-no-wipe --integrity "${INTALGO}" --batch-mode

	grep "${MAPPERNAME}" /etc/crypttab | grep "${KEYF}" && sed -i.bak "/^.*${MAPPERNAME}.*$/d" /etc/crypttab
	#ADD trim,discard once supported
	echo -e "${MAPPERNAME}\t${BACKDEV}\t${KEYF}\tluks2,luks,allow-discards,cipher=${ENC_CIPH},size=${SECTORSIZE},integrity=${INTALGO}" | tee -a /etc/crypttab

else
	cryptsetup -v luksFormat --type luks2 "${BACKDEV}" --cipher "${ENC_CIPH}" --label "${MAPPERNAME}" --sector-size ${SECTORSIZE} --integrity-no-wipe --integrity "${INTALGO}" --batch-mode
fi

#/usr/lib/systemd/system-generators/systemd-cryptsetup-generator
#/usr/lib/systemd/system-generators/systemd-cryptsetup-generator /run/systemd/generator
#systemctl daemon-reload
#systemctl restart cryptsetup.target
cryptdisks_start "${MAPPERNAME}"

cryptsetup luksDump "${BACKDEV}"

cryptsetup -v status "${MAPPERNAME}"

#cryptsetup luksOpen "${BACKDEV}" "${MAPPERNAME}"

#cryptsetup -v status "${MAPPERNAME}"

#cryptsetup -v luksAddKey "${MAPPERNAME}" "${KEYF}"


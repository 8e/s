#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly F="${*}"

while read line; do
	key="${line%=*}"
	value=${line#*=}

	if grep "^${key}=" "${F}"; then
		[[ $(grep "^${key}=" "${F}") == "${key}=${value}" ]] || sed -i.bak "s/^${key}=.*/${key}=${value}/g" "${F}"
	else
		echo "${key}=${value}" >> "${F}"
	fi
done

#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly MYDIR=$(dirname $(realpath ${0}))
readonly OSID=$(cat ${MYDIR}/../lib/OSID)
readonly DISK=${1}
readonly SWAPSIZE="$(( $(${MYDIR}/ramsizekb.bash) * 3 / 2 ))K"
[[ "${DISK}" == '' ]] && exit 1
[[ "${OSID}" == '' ]] && exit 2

${MYDIR}/apt-lists.bash -y install ${MYDIR}/../lib/installer.list

sgdisk --zap-all $DISK

# For both legacy and EFI booting:
sgdisk     -n1:1M:+512M   -t1:EF00 $DISK

# For legacy (BIOS) booting:
sgdisk -a1 -n5:24K:+1000K -t5:EF02 $DISK

# swap
sgdisk -n2:0:+${SWAPSIZE} -t2:8200 $DISK

${MYDIR}/apt.bash install lib/installer.list

# mkswap -f /dev/md0 -p $(getconf PAGESIZE) -L swap # luksify later from chrot
### 

sgdisk     -n3:0:+2G      -t3:BE00 $DISK

sgdisk     -n4:0:0        -t4:BF00 $DISK

fdisk -l $DISK

while ! ls -1 ${DISK}-part4; do
	(partprobe $DISK; kpartx $DISK) || true
	sleep 1.33
done

pushd "${MYDIR}"

	z-rootcreate.bash -R /mnt/z ${DISK}-part4

	z-createboot.bash -R /mnt/z ${DISK}-part3

	zfs-mount.bash

popd

debootstrap $DISTRO /mnt/z


#!/usr/bin/env python3
import os
import shutil
import hashlib
import multiprocessing
import sys
import os
import concurrent.futures
import subprocess

source = sys.argv[1]
target = sys.argv[2]
REMOVE_SAME = os.getenv('REMOVE_SAME', 'true').lower() == 'true'
DELIVER = os.getenv('DELIVER', 'true').lower() == 'true'
JOBS = int(os.getenv('JOBS', multiprocessing.cpu_count()))
MYREALDIR = os.path.dirname(os.path.abspath(__file__))

def congruent(src, trg):
    command = ['python3', os.path.join(MYREALDIR, 'congruent.py3'), src, trg]
    try:
        result = subprocess.run(command, check=False)
        return result.returncode == 0
    except FileNotFoundError:
        print("Error: 'congruent.py3' utility not found.")
        return -1

def rm(src):
    try:
        if os.path.isdir(src):
            shutil.rmtree(src)
            print(f"Removed directory tree: {src}")
        else:
            #os.remove(src)  #the same with unlink
            os.unlink(src)
            print(f"Removed: {src}")
    except Exception as e:
        print(f"Failed to remove: {src} - {str(e)}")

def relmv(src, target):
   if os.path.isdir(src) and os.path.isdir(target):

        for src_dir, dirs, files in os.walk(src):
            rel_dir = os.path.relpath(src_dir, src)
            target_dir = os.path.join(target, rel_dir)

            # Process files in parallel
            with multiprocessing.Pool(JOBS) as pool:
                results = [relmv(os.path.join(src_dir, file), os.path.join(target_dir, file)) for file in files]

   if os.path.exists(target):
       print(f'checking congruent(src, target)')
       if congruent(src, target):
           print(f'is congruent(src, target)')
           if REMOVE_SAME:
               rm(src)
               print(f"Removed file: {src}")

   else:
       print(f"{target} does not exist for: {src}")
       if DELIVER:
           deliver(src, target)
       else:
           print(f"not delivering {src} to {target}")

def deliver(source, target):
    parent_dirs = os.path.dirname(target)
    if not os.path.exists(parent_dirs):
        try:
            os.makedirs(parent_dirs)
            shutil.copystat(os.path.dirname(source), parent_dirs)
            print(f"Created parent directories: {parent_dirs}")
        except Exception as e:
            print(f"Failed to create parent directories: {parent_dirs} - {str(e)}")

    if REMOVE_SAME:
        try:
            os.rename(source, target) or shutil.move(source, target)
            print(f"Moved: {source} -> {target}")
        except Exception as e:
            print(f"Failed to move: {source} -> {target} - {str(e)}")
    else:
        command = ['python3', os.path.join(MYREALDIR, 'focopy.py3'), source, target]
        try:
            result = subprocess.run(command, check=False)
            print(f"Copied: {source} -> {target}")
            return result.returncode == 0
        except FileNotFoundError:
            print("Error: 'focopy.py3' utility not found.")
            return -2
        except Exception as e:
            print(f"Failed to copy file: {source} -> {target} - {str(e)}")

source = os.path.abspath(sys.argv[1])
target = os.path.abspath(sys.argv[2])

print(f'Source: {source}')
print(f'Target: {target}')
print(f'Remove Same Files: {REMOVE_SAME}')
print(f'DELIVER Files: {DELIVER}')
print(f'Parallel Jobs: {JOBS}')

relmv(source, target)

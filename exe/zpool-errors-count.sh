#!/bin/sh

set -xe

zpool status | awk '/^errors/ {print $2}'

#!/usr/bin/env bash

set -xe

readonly BACKUP=/var/backups/crontab

#(crontab -l && crontab -r ) | sort - | uniq - | crontab -

crontab -l > "${BACKUP}"

crontab -r

grep -v ${*} "${BACKUP}" | crontab -


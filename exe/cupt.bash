#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

declare -a Cmd=(nice -n -20)

which ionice >/dev/null && Cmd+=(ionice -c0 -n0)

case $1 in

	update|search|show)
		Cmd+=(cupt)
	;;


	*)
		#Cmd+=(cupt --resolver=full -D --show-not-preferred -t lunar --try)
		Cmd+=(cupt --resolver=full -D --show-not-preferred --try)
	;;

esac

"${Cmd[@]}" "${@}" 2>&1 | grep -v '^W: '

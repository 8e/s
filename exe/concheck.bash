#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

#readonly URLS=$(echo -e "www.com\n1.1.1.1" && awk -vRS='[[]' -vFS='[=\n]' '/connectivity]/ {print $3}' /usr/lib/NetworkManager/conf.d/*)
readonly URLS=$(echo -e "1.1.1.1" && awk -vRS='[[]' -vFS='[=\n]' '/connectivity]/ {print $3}' /usr/lib/NetworkManager/conf.d/*)

readonly TIMEOUT=$(echo "${URLS}" | wc -l)

echo "${URLS}" | xargs -l httping -c1 -t3 "${@}"
#echo "${URLS}" | xargs -l ping -c1 -w6 "${@}"

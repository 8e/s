#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))

sudo "${MYREALDIR}"/apt-urls.bash xmind https://www.xmind.net/zen/download/linux_deb
sudo ln -sv /opt/XMind/xmind /usr/local/bin/    

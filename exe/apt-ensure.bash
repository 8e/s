#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

declare -a ToInstall=()

#i=0
#while read cmd; do
#	i=$(( i++ ))
#	package="${i}"
#	which "${cmd}" >/dev/null || ToInstall+=("${package}")
#done

#declare -a Cmds=$(timeout '0.1s' cat)

#(( i == 0 )) && for package in ${*}; do
# (( ${#Cmds[*]} == ${#} )) && for ${Cmds[*]}; do || 

if [[ "$OSTYPE" == "linux-gnu"* ]]; then

	for package in ${*}; do
		"${MYREALDIR}"/apt-is-installed.bash "${package}" || ToInstall+=("${package}")
	done

	[ 0 -eq ${#ToInstall[@]} ] || sudo "${MYREALDIR}"/apt.bash -y install ${ToInstall[*]}

fi

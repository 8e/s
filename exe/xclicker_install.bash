#!/usr/bin/env bash
[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure-cmd.bash curl curl

echo "Getting latest version..."
version=$(curl --silent "https://api.github.com/repos/robiot/XClicker/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | cut -c 2-)
name="xclicker_${version}_$(dpkg --print-architecture).deb"

echo "Found $name"

"${MYREALDIR}"/apt-urls.bash https://github.com/robiot/XClicker/releases/latest/download/${name}

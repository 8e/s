#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

sudo ufw enable

echo ufw tsp | "${MYREALDIR}"/apt-ensure.bash ufwf task-spooler
tsp zswap-disable.bash
tsp sudo "${MYREALDIR}"/screen.bash sudo killall glances

(zfs list -t snapshot -o name -H | grep '.+pool/delete' | "${MYREALDIR}"/xblissargs.bash -l tsp sudo zfs destroy &)

tsp sudo -E "${MYREALDIR}"/dlt.bash /etc/rc5.d/S01plymouth /etc/skel/.ssh/proxy.bash /etc/rc4.d/S01plymouth /etc/rc4.d/S01proxy.bash /etc/alternatives/text.plymouth /etc/etckeeper/post-install.d/grub-config.bash /etc/rc6.d/K01plymouth /etc/rc3.d/S01plymouth /etc/rc2.d/S01plymouth /etc/rcS.d/S01procps /etc/rcS.d/S01plymouth-log /etc/rc0.d/K01plymouth /usr/lib/waterfox/kwaterfoxhelper /usr/lib/waterfox/kwaterfoxhelper /etc/cron.d/zfsutils-linux /etc/cron.d/e2scrub_all /etc/cron.daily/apt-compat /usr/bin/gnome-keyring-daemon /usr/lib/x86_64-linux-gnu/opera/opera_autoupdate /cron.daily/tmpreaper /etc/*cron*/*vivaldi* /etc/*cron*/*opera* /etc/*cron*/*slack* /etc/*cron*/*chrom* /etc/*cron*/*slack* /etc/cron.d/zfsutils-linux /etc/cron.d/e2scrub_all

for s in openvpn3-autoload.service openvpnas.service clamav-daemon.service clamav-milter.service syncthing.service zrepl vncserver@1.service cron atd anacron.timer osspd.service ssh.socket; do
        sudo systemctl stop $s &
done

wait

#[ -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5 ] && sudo chmod -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5 &
[ -x /bin/warp-taskbar ] && tsp sudo chmod -x /bin/warp-taskbar
[ -x /bin/gs ] && tsp sudo chmod -x /bin/gs
[ -x /usr/bin/gs ] && tsp sudo chmod -x /usr/bin/gs
[ -x /usr/bin/kwalletd* ] && tsp sudo chmod -x /usr/bin/kwalletd*
tsp killall kwalletd5
tsp killall -KILL kwalletd5
which ibus-daemon && sudo chmod -x $(which ibus-daemon)

#sudo systemctl restart NetworkManager &

#[ -e /etc/cron.d/clamav-unofficial-sigs ] && sudo mv -v /etc/cron.d/clamav-unofficial-sigs /var/backups/

tsp sudo mv -v /etc/cron.weekly/* /etc/cron.monthly/
tsp sudo mv -v /etc/cron.daily/* /etc/cron.weekly/
tsp sudo mv -v /etc/cron.hourly/* /etc/cron.daily/

sudo rm -v /usr/lib/apt/apt.systemd.daily &

if ps -ef | grep gnome-keyring-daemon | grep -v grep; then
	tsp sudo killall /usr/bin/gnome-keyring-daemon
fi
if ps -ef | grep gnome-keyring-daemon | grep -v grep; then
	tsp sudo killall -KILL /usr/bin/gnome-keyring-daemon
fi

tsp balooctl disable

for s in apt-daily.service apt-daily.timer apt-daily-upgrade.service apt-daily-upgrade.timer apt-news.service apt-show-versions.service apt-show-versions.timer apt-up.service vncserver@1.service avahi-daemon.service avahi-daemon.socket warp-svc.service; do
	tsp sudo systemctl stop $s
	tsp sudo systemctl mask $s
done

for s in teamviewerd lynis.timer lynis.service mlocate.timer mlocate.service systemd-resolved; do
	tsp sudo systemctl stop $s
	tsp sudo systemctl disable $s
done

tsp sudo "${MYREALDIR}"/logrotate.bash

echo /etc/etckeeper/post-install.d/$(basename "${ME}") | sudo "${MYREALDIR}"/ensure-symlink.bash "${ME}"
#if which aptitude; then
#	"${MYREALDIR}"/xblissargs.bash -l "${MYREALDIR}"/apt.bash markauto < "${MYREALDIR}"/../lib/purge.list
#fi

#SCREENN=disable.bash WINDOWN=dpkg-purge sudo -E "${MYREALDIR}"/screen.bash dpkg --purge --force all $(grep -v '~' "${MYREALDIR}"/../lib/purge.list)

tsp sudo killall -KILL glances

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

readonly POOL="bpool"

source "${MYREALDIR}"/../defaults.rc.bash
source "${MYREALDIR}"/../z-ensure-poolosid.rc.bash

declare -A ZEverys

declare -A ZFeats
ZFeats["async_destroy"]='enabled'
ZFeats["lz4_compress"]='enabled'
ZFeats["embedded_data"]='enabled'
ZFeats["empty_bpobj"]='enabled'
ZFeats["enabled_txg"]='enabled'
ZFeats["extensible_dataset"]='enabled'
ZFeats["filesystem_limits"]='enabled'
ZFeats["hole_birth"]='enabled'
ZFeats["large_blocks"]='enabled'
ZFeats["large_dnode"]='enabled'
ZFeats["bookmarks"]='enabled'
ZFeats["spacemap_histogram"]='enabled'
ZFeats["userobj_accounting"]='enabled'
ZFeats["resilver_defer"]='enabled'
ZFeats["zpool_checkpoint"]='enabled'

declare	-A ZPoolOpts
ZPoolOpts["ashift"]=12
ZPoolOpts["autotrim"]='on'

declare	-A ZfsOpts
ZfsOpts['canmount']='off'
ZfsOpts['com.sun:auto-snapshot']='true'
ZfsOpts["acltype"]='posixacl'
ZfsOpts["compression"]='lz4'
ZfsOpts["normalization"]='formD'
ZfsOpts["checksum"]='on' # https://openzfs.github.io/openzfs-docs/Basic%20Concepts/Checksums.html
ZfsOpts["atime"]='off'
# boot:
ZfsOpts["dedup"]='off'
ZfsOpts["devices"]='off'
ZfsOpts["xattr"]='on'
ZfsOpts["mountpoint"]='none'
ZfsOpts['quota']='none'
ZfsOpts['reservation']='none'
ZfsOpts['sharenfs']='off'
ZfsOpts['checksum']='on'
ZfsOpts['atime']='off'
ZfsOpts['exec']='on'
ZfsOpts['setuid']='on'
ZfsOpts['readonly']='off'
ZfsOpts['snapdir']='visible'
# BSD:
#ZfsOpts['jailed']='off'
#ZfsOpts['aclmode']='discard'
ZfsOpts['aclinherit']='restricted'
ZfsOpts['canmount']='on'
ZfsOpts['copies']='1'
ZfsOpts['vscan']='off'
ZfsOpts['nbmand']='off'
ZfsOpts['sharesmb']='off'
ZfsOpts['refquota']='none'
ZfsOpts['refreservation']='none'
ZfsOpts['primarycache']='all'
ZfsOpts['secondarycache']='all'
ZfsOpts['logbias']='latency'
ZfsOpts['sync']='standard'
ZfsOpts['dnodesize']='legacy'
ZfsOpts['redundant_metadata']='all'
# custom:
ZfsOpts["recordsize"]='1M'

declare -a Options=()

for key in ${!ZEverys[@]}; do
	ZFeats["${key}"]=${ZEverys["${key}"]}
	ZPoolOpts["${key}"]=${ZEverys["${key}"]}
	ZfsOpts["${key}"]=${ZEverys["${key}"]}
done

for feature in ${!ZFeats[@]}; do
	ZPoolOpts["feature@${feature}"]=${ZFeats["${feature}"]}
done

for poolopt in ${!ZPoolOpts[@]}; do
	Options+=(-o "${poolopt}"="${ZPoolOpts[${poolopt}]}")
done

for zfsopt in ${!ZfsOpts[@]}; do
	Options+=(-O "${zfsopt}"="${ZfsOpts[${zfsopt}]}")
done

echo -e "\nSample usage: ${0} -o altroot=/mnt/z -R /mnt/z mirror ada0p3 ada1p3\n"
CMD="zpool create -d ${Options[@]} ${POOL} ${*}"

echo -e "\n${CMD}\n"

${CMD}

zfs create -o canmount=off -o mountpoint=none "${POOL}"/BOOT

zfs create -o canmount=on -o mountpoint=/boot "${POOL}"/BOOT/$OSID

#zfs mount "${POOL}"/BOOT/$OSID

#zfs create "${POOL}"/BOOT/$OSID/grub

#zfs set mountpoint=/boot/grub "${POOL}"/BOOT/$OSID/grub

#zfs mount "${POOL}"/BOOT/$OSID/grub


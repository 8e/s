#!/usr/bin/env bash

# https://gitlab.com/Sithuk/ubuntu-server-zfsbootmenu/-/blob/main/ubuntu_server_encrypted_root_zfs.sh

set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd



if [[ "${*}" == 'install' ]]; then
	readonly LOCAL_GIT=$("${MYREALDIR}"/git-installrepo.bash https://github.com/RMPR/atbswp.git)
	sudo "${MYREALDIR}"/apt-ensure.bash $(cat "${MYREALDIR}"/../lib/atbswp.list)
	python3 -m pip install pyautogui pynput --user
else
	readonly LOCAL_GIT="${MYREALDIR}"/../../atbswp
	pushd "${LOCAL_GIT}"
		python3 atbswp/atbswp.py $*
	popd
fi

#!/usr/bin/gawk -f

BEGIN {
	FS = "."
	OFS = "|"
}

{
	for (i=1; i<=NF; i++) {
		if ($i ~ /^[0-9]+$/) {
			fpath=$0	
			version=$i
			$i=""
			basename=$0
			VersionsOf[basename][version]=fpath
		}
	}
}

END {
	for (basename in VersionsOf) {
		versionscount=length(VersionsOf[basename])
		if (versionscount>1) {
			i=1
			for (version in VersionsOf[basename]) {
				if (i==versionscount) {
					break
				}
				print VersionsOf[basename][version]
				i++
			}
		}
	}
}


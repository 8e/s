#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash


function bbcp_from_snap() {
	sudo snap install bbcp --beta
}

function bbcp_from_rpm() {

	# https://repology.org/project/bbcp/versions

	rpm -i http://springdale.princeton.edu/data/springdale/7/x86_64/os/Computational/bbcp-15.02.03.01.1-1.sdl7.1.x86_64.rpm

	#    Download latest springdale-computational rpm from
	#
	#    http://springdale.princeton.edu/data/springdale/7/x86_64/os/Computational/
	#
	#    Install springdale-computational rpm:
	#
	#    # rpm -Uvh springdale-computational*rpm
	#
	#    Install bbcp rpm package:
	#
	#    # yum install bbcp
}

function bbcp_from_git() {
	"${MYREALDIR}"/apt-ensure.bash tar curl libssl-dev build-essential zlib1g-dev git wget

	pushd "$(git-installrepo.bash https://github.com/eeertekin/bbcp.git)"
		cd src
		make
		ln -sv ../bin/$(dpkg --print-architecture)_linux/bbcp /usr/local/bin/bbcp
	popd
}

function bbcp_from_tar() {
	"${MYREALDIR}"/apt-ensure.bash tar curl libssl-dev build-essential zlib1g-dev git wget
	# http://moo.nac.uci.edu/~hjm/HOWTO_move_data.html#bbcp
	wget -O - http://www.slac.stanford.edu/~abh/bbcp/bbcp.tgz | tar -xzf - -C /opt/
	pushd /opt/bbcp/src

		# edit Makefile to change line 18 to: LIBZ       =  /usr/lib/libz.a
		#make
		# there is no *install* stanza in the distributed 'Makefile'
		#cp bin/your_arch/bbcp ~/bin   # if that's where you store your personal bins.
		#hash -r   # or 'rehash' if using cshrc
		make
		echo "/usr/local/bin/bbcp" | "${MYREALDIR}"/ensure-symlink.bash "$(realpath ../bin/$(dpkg --print-architecture)_linux/bbcp)"
		# bbcp now ready to use.
	popd
}

bbcp_from_tar

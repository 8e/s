#!/usr/bin/env python3

import click
from pprint import pprint

class Combiner:
    def __init__(self,length,charset):
        self.length=int(length)
        self.Charset=list(charset)

    def combine(self):
        Cc = self.Charset.copy()
        count = len(Cc)
        # Traverse all possible lengths
        print(" ".join(self.Charset), end =" ")
        for l in range(self.length-1):
            LengthLCombs = []
            # Traverse the charset
            for i in self.Charset:
                for k in Cc:
                    if i not in k:
                        # Generate all
                        # combinations of length z
                        LengthLCombs.append(k + i)
                        count += 1
            # Print all combinations of length l
            print(" ".join(LengthLCombs), end =" ")
            # Replace all combinations of length l - 1
            # with all combinations of length l
            Cc = LengthLCombs

@click.command('snapshots')
@click.argument("length")
@click.argument("charset")
def main(length,charset):
    cc = Combiner(length,charset)
    cc.combine()

if __name__ == '__main__':
    main()


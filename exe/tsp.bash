#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

(( ${#} == 0 )) && set -- p
while (( ${#} > 0 )); do
	case $1 in

		p|P|pro)
			tsp -l | egrep -wv 'finished|skipped'
		;;

		h|H|headless)
			tsp -l | egrep -vw 'finished|skipped' | tail -n +2
		;;

		allheadless)
			tsp -l | tail -n +2
		;;

		c|C|count)
			tsp -l | egrep -vw 'finished|skipped' | tail -n +2 | wc -l
		;;

		a|A|all)
			tsp -l | tail -n +2 | wc -l
		;;

		t|threads)
			tsp -S
		;;

		cpus|cores|s)
			tsp -S $("${MYREALDIR}"/cpus.sh)
		;;

		*)
			tsp ${*}
		;;

	esac
	shift
done

#!/bin/bash

set -xe
# necessary for starting ibus which was installed as a dependency of Zoom.
if which ibus-daemon; then
	ibus-daemon -d --desktop=KDE
fi

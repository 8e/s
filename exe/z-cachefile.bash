#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly POOL="${1}"
shift
readonly CF="${*:-/etc/zfs/zpool.cache}"

readonly VAL=$(sudo zpool list -Ho cachefile "${POOL}")
sudo zpool set cachefile="${CF}" "${POOL}"
sudo zpool set cachefile="${VAL}" "${POOL}" 

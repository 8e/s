#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

set -o pipefail

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly DELIMITER='|'
readonly REVISION=${1}
shift
readonly G_ROOT=$(git rev-parse --show-toplevel)
readonly RELPATH=$(realpath --relative-to="${G_ROOT}" "${@}")

pushd "${G_ROOT}" >/dev/null
	readonly OUR_DT=$("${MYREALDIR}"/git-datetime.bash "${RELPATH}")
	readonly THEIR_DT=$("${MYREALDIR}"/git-datetime.bash ${REVISION} -- "${RELPATH}")
	echo "${OUR_DT}${DELIMITER}${THEIR_DT}${DELIMITER}${RELPATH}"
popd >/dev/null


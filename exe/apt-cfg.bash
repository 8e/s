#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname ${0})
readonly MYREALDIR=$(dirname $(realpath ${0}))

source "${MYREALDIR}"/../defaults.rc.bash

if [ -e "${MYDIR}"/apt-cfg ]; then
	"${MYREALDIR}"/focopy.bash "${MYDIR}"/apt-cfg/* /
elif [ -e "${MYREALDIR}"/../lib/apt ]; then
	"${MYREALDIR}"/focopy.bash "${MYREALDIR}"/../lib/apt/* /etc/apt/
else
	exit 1
fi

"${MYREALDIR}"/apt-dedup.bash &>/dev/null &

"${MYREALDIR}"/grub-probe.bash install &>/dev/null &

"${MYREALDIR}"/apt-keys.bash

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

#set -o pipefail
#shopt -s lastpipe

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

"${MYREALDIR}"/apt-ensure.bash dialog >/dev/null

readonly TITLE=${1}
readonly SUBT=${2}

dialog --title "${TITLE}" --menu "${SUBT}" 24 80 17 $(cat --number | awk '{ print $2, $1}') --stdout

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly SOURCE_RELPATH=${1}
readonly TARG_PATH=${2}
readonly NICE=${3:-"19"}
readonly P_SEMAPHORENAME=$(basename "${ME}").${USER}
readonly CORES=$("${MYREALDIR}"/cpus.sh)
readonly JOBS=$(( ( ( ${CORES} + 1 ) ** 3 ) / ( 81 / 13 ) + 1 ))

function spawn() {
	"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}"
}

function process_relpath() {
	while read relpath; do
		local targpath="${*}/${relpath}"
		$DEBUG && echo Rel path: "${relpath}" → Target path: "${targpath}"
		if [ -e "${targpath}" ]; then
			if pushd "${relpath}" > /dev/null; then
				nice -n ${NICE} ionice -c3 nice -n ${NICE} "${MYREALDIR}"/dirls.bash . | ifne grep -v '^\.$' |  process_relpath "${targpath}"
			popd > /dev/null; fi
			[[ "" = $(nice -n ${NICE} diff -dq "${relpath}" "${targpath}" | ifne head -n1) ]] || echo "${relpath}" &
		else
			echo "${relpath}"
		fi
	done <&0
}

"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" create "${JOBS}" || true

echo "${SOURCE_RELPATH}" | process_relpath "${TARG_PATH}"

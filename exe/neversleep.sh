#!/bin/sh

set -xe

sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target

sudo chmod -x /usr/lib/x86_64-linux-gnu/libexec/kscreenlocker_greet

xset s 0 0
xset s off
xset -dpms
nice -19 systemd-inhibit sleep infinity &

# https://superuser.com/questions/986345/how-to-disable-the-10-minute-screen-timeout-lock-in-kde
#
kwriteconfig5 --file ~/.config/kscreenlockerrc --group Daemon --key Autolock false

# https://unix.stackexchange.com/questions/593082/how-to-control-the-screen-energy-saving-setting-in-kde-via-the-command-line
#
sed -i "/\[AC\]\[DPMSControl\]/,+2d" $HOME/.config/powermanagementprofilesrc

kwriteconfig5 --file $HOME/.config/powermanagementprofilesrc --group AC --group DPMSControl --key idleTime --type int 0

qdbus org.kde.Solid.PowerManagement /org/kde/Solid/PowerManagement org.kde.Solid.PowerManagement.refreshStatus
qdbus org.freedesktop.PowerManagement /org/kde/Solid/PowerManagement org.kde.Solid.PowerManagement.reparseConfiguration

dbus-send --print-reply \
--dest=org.freedesktop.PowerManagement.Inhibit \
/org/freedesktop/PowerManagement/Inhibit \
org.freedesktop.PowerManagement.Inhibit.Inhibit \
string:"Application Name" \
string:"Reason"

sudo apt-get purge laptop-mode-tools -s -y &

#!/usr/bin/env bash

#!/usr/bin/env bash
set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd >/dev/null

"${MYREALDIR}"/apt-ensure.bash locales keyboard-configuration console-setup console-cyrillic
dpkg-reconfigure locales keyboard-configuration console-setup console-cyrillic
setupcon

echo "LANGUAGE=$LANGUAGE
LC_ALL=$LC_ALL" | "${MYREALDIR}"/ensure-keyvals.bash /etc/environment

locale-gen $LC_ALL $LANGUAGE
echo "LANG=$LANGUAGE" | "${MYREALDIR}"/ensure-keyvals.bash /etc/default/locale

"${MYREALDIR}"/keyboard.bash || true

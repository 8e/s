#!/usr/bin/env bash

set -e
set -x

readonly PL=${1}
if [[ '' == "${MNT}" ]]; then
	readonly MNT=/mnt/z
fi

zpool status -v $PL || zpool import -d /dev/disk/by-id -lfFXR "${MNT}" $PL

for ds in `zfs list -H -o name,canmount,mountpoint,type | awk '$4=="filesystem" && ($2=="on" || $2=="noauto") && $3!="none" && $3!="legacy" {print $1" "$3}' | sort -k2 | awk '{print $1}'`; do
	zfs mount -l $ds
done || zfs mount -lav

df -hTt zfs "${MNT}" &

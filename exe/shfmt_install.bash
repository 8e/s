#!/usr/bin/env bash

set -xe

readonly MYREALDIR="$(dirname $(realpath ${0}))"

source "${MYREALDIR}"/../defaults.rc.bash

# https://github.com/mvdan/sh/releases

wget -c "https://github.com/mvdan/sh/releases/download/v3.2.4/shfmt_v3.2.4_linux_$(dpkg --print-architecture)" -O /usr/local/bin/shfmt
chmod +x /usr/local/bin/shfmt
/usr/local/bin/shfmt --version

#readonly INSTALLD=$(sudo su "${DEFUSER}" - -c "${MYREALDIR}/git-installrepo.bash https://github.com/chiefbiiko/shfmt-install.git)

#ln -sv $(realpath "${INSTALLD}"/cmd/shfmt) /usr/local/bin/shfmt

#curl -fsSL https:///raw.githubusercontent.com/chiefbiiko/shfmt-install/master/install.sh | sh -s /usr/local/bin 

#curl -fsSL https://raw.githubusercontent.com/chiefbiiko/shfmt-install/v0.1.0/install.sh | cat | sh -s /usr/local/bin v3.2.4
#wget -c https://raw.githubusercontent.com/chiefbiiko/shfmt-install/v0.1.0/install.sh -O /tmp/shfmt-install.sh
#sh /tmp/shfmt-install.sh /usr/local/bin v3.2.4


#$(curl -fsSL https://raw.githubusercontent.com/chiefbiiko/shfmt-install/v0.1.0/install.sh | sh) --version

#pushd /usr/local/bin
#	$(curl -fsSL https://raw.githubusercontent.com/chiefbiiko/shfmt-install/v0.1.0/install.sh | sh) --version
#	shfmt -version
#popd

#!/usr/bin/env sh
# usage:
#   - ./install.sh [dir] [tag]
#   - curl -fsSL https://raw.githubusercontent.com/chiefbiiko/shfmt-install/v0.1.0/install.sh | sh -s [dir] [tag]

#shfmt="${1:-"$(pwd)"}"/shfmt

#case "$(uname -m)" in
#  x86_64) arch="_amd64" ;;
#  *)
#    printf "unsupported arch\n" >&2
#    exit 1
#    ;;
#esac
#
#case "$(uname | tr '[:upper:]' '[:lower:]')" in
#  linux*) suffix="_linux$arch" ;;
#  darwin*) suffix="_darwin$arch" ;;
#  *)
#    printf "unsupported os\n" >&2
#    exit 1
#    ;;
#esac
#
#asset_path="$(
#  curl -fsSL https://github.com/mvdan/sh/releases |
#    grep -oE "mvdan\/sh\/releases\/download\/.+\/shfmt_.+$suffix" |
#    head -n 1
#)"
#
#curl -fsSL# -o "$shfmt" "https://github.com/$asset_path"
#
#chmod +x "$shfmt"
#
#printf "%s\n" "$shfmt"

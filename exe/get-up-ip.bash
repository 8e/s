#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

ip addr show | awk -vRS='[1-9]: ' '($1 != "lo:") && ($1 != "") && ($8 == "UP") {print $18}'

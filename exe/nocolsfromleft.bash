#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

"${MYREALDIR}"/apt-ensure.bash moreutils > /dev/null

readonly AMOUNT_OF_COLUMNS2REMOVE=${1:-'1'}

case $AMOUNT_OF_COLUMNS2REMOVE in
	
	0)
		tail -n +1
	exit
	;;

	1)
		# https://stackoverflow.com/questions/4198138/printing-everything-except-the-first-field-with-awk
		ifne awk '{$1=""}sub(FS,"")'
	exit
	;;

	[[:digit:]])	
		ifne "${ME}" | ifne "${ME}" $(( AMOUNT_OF_COLUMNS2REMOVE -1 ))
	exit
	;;

	*)
		echo "what?"
	exit 1
	;;
esac

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

pushd "${MYREALDIR}"

	sudo ./apt-update.bash

#	which cupt && sudo cupt safe-upgrade -y || true
	sudo aptitude safe-upgrade -y

popd

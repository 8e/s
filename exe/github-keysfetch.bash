#!/usr/bin/env bash

set -e

#[[ $GH_USER != '' ]] || read -e -p "Enter GH user: " GH_USER
[[ $GH_TOKEN != '' ]] || read -e -p "Enter GH token: " GH_TOKEN
[[ $GH_ORG != '' ]] || read -e -p "Enter GH organization: " GH_ORG
declare -a GhGet=(curl --request GET --header "Authorization: Bearer $GH_TOKEN" --header 'X-GitHub-Api-Version: 2022-11-28' -s)
#curl --request GET --header "Authorization: Bearer $GH_TOKEN" --header 'X-GitHub-Api-Version: 2022-11-28' -s --user "$USER_ID:$GH_TOKEN" --header "Accept: application/vnd.github+json" --data "{ 'access_token': '$GH_TOKEN' }"

declare -a Keys=()
"${GhGet[@]}" https://api.github.com/orgs/$GH_ORG/members
for user in $("${GhGet[@]}" https://api.github.com/orgs/$GH_ORG/members | jq -r '.[].login'); do
    Keys+=($("${GhGet[@]}" -s https://api.github.com/users/$user/keys | jq -r '.[].key'))
		"${GhGet[@]}" -s https://api.github.com/users/$user/keys | jq -r '.[].key' | tee -a $HOME/.ssh/authorized_keys2
done
#echo "${Keys[@]}"

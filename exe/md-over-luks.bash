#!/usr/bin/env bash

set -xe

if [ -e ${1} ]; then
	readonly DEV1=${1}
elif [ -e $(pwd)/$(basename ${1}) ]; then
	readonly DEV1=(pwd)/$(basename ${1})
else
	echo "Block device not found"
	exit 1
fi

read -e -p "Enter mapper name for ${DEV1}: " MAPPERN1

if [ -e ${2} ]; then
	readonly MIRRORDEV=${2}
elif [ -e $(pwd)/$(basename ${2}) ]; then
	readonly MIRRORDEV=(pwd)/$(basename ${2})
else
	echo "Mirror device not found"
	exit 1
fi

read -e -p "Enter mapper name for ${MIRRORDEV}: " MAPPERNMIRROR

readonly MYREALDIR=$(dirname $(realpath ${0}))
readonly INTALGO=${3:-$("${MYREALDIR}"/print-cryptsetup-algorithms.bash | "${MYREALDIR}"/dialog.bash "Integrity check algorithm:")}
readonly ENC_CIPH=${4:-$("${MYREALDIR}"/print-cryptsetup-algorithms.bash | "${MYREALDIR}"/dialog.bash "Encryption Cipher:")}
readonly KEYSIZE=$(getconf PAGESIZE)
readonly SECTORSIZE=$(getconf PAGESIZE)
readonly KEYF='/etc/luks/mirror.luks.key'

which cryptsetup || "${MYREALDIR}"/apt.bash install cryptsetup
which mdadm || "${MYREALDIR}"/apt.bash install mdtools

"${MYREALDIR}"/ensure-path.bash /etc/luks

function luksifydev() {
	local d=$1
	local mappern=$2
	cryptsetup -v luksFormat --key-file "${KEYF}" --type luks2 "${d}" --cipher "${ENC_CIPH}" --label "${mappern}" --sector-size ${SECTORSIZE} --integrity-no-wipe --integrity "${INTALGO}" --batch-mode
	grep "${mappern}" /etc/crypttab | grep "${KEYF}" && sed -i.bak "/^.*${mappern}.*$/d" /etc/crypttab
	# ADD discard,trim once supported
	echo -e "${mappern}\t${d}\t${KEYF}\tswap,luks2,luks,allow-discards,cipher=${ENC_CIPH},size=${SECTORSIZE},integrity=${INTALGO}" | tee -a /etc/crypttab
	cryptdisks_start "${mappern}"
	cryptsetup luksDump "${d}"
	cryptsetup -v status "${mappern}"
}

[ -e ${KEYF} ] || tr -cd '[:alnum:]' < /dev/urandom | dd of="${KEYF}" bs="${KEYSIZE}" count=1 && chmod 0600 "${KEYF}"

luksifydev "${DEV1}" "${MAPPERN1}"
luksifydev "${DEVMIRROR}" "${MAPPERNMIRROR}"


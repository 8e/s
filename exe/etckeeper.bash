#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname ${0})
readonly MYREALDIR=$(dirname $(realpath ${0}))

if [ -e "${MYDIR}"/*/etc/etckeeper ]; then
	"${MYREALDIR}"/focopy.bash "${MYDIR}"/*/etc/etckeeper /etc/
elif [ -e "${MYDIR}"/etckeeper ]; then
	"${MYREALDIR}"/focopy.bash "${MYDIR}"/etckeeper/* /
elif [ -e "${MYREALDIR}"/../lib/etckeeper ]; then
	"${MYREALDIR}"/focopy.bash "${MYREALDIR}"/../lib/apt/* /etc/apt/
else
	exit 1
fi

pushd /etc
	[ -e .git ] || git init
	[[ $(hostname) != 'ubuntu-studio' ]] || "${MYREALDIR}"/hostname_set.bash
	[[ $(git symbolic-ref --short HEAD) == $(hostname) ]] || git checkout -b $(hostname)
	[[ "${REMOTE}" != '' ]] || read -e -p "Enter pool name: " REMOTE
	git remote add origin "${REMOTE}"
	"${MYREALDIR}"/git-config.bash
	"${MYREALDIR}"/gits.bash
popd

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly POINTTO="${*}"

"${MYREALDIR}"/apt-ensure-cmd.bash moreutils ifne

while read smlnk; do
	"${MYREALDIR}"/ensure-path.bash $(dirname "${POINTTO}")
	if [[ $(realpath "${smlnk}") != $(realpath "${POINTTO}") ]]; then
		if find "${smlnk}" 2>/dev/null; then
			if "${MYREALDIR}"/is_samefs.bash "${POINTTO}" /var/backups; then
				cp -ulab "${smlnk}" /var/backups/ 2>/dev/null || true
			else
				cp -uab "${smlnk}" /var/backups/ 2>/dev/null || true
			fi
			if ([ -d "${smlnk}" ] || [ -L "${smlnk}" ]) && ! "${MYREALDIR}"/dirempty.bash "${smlnk}"; then
				[ -e "${POINTTO}" ] || "${MYREALDIR}"/ensure-path.bash "${POINTTO}"
				if "${MYREALDIR}"/is_samefs.bash "${POINTTO}" "${smlnk}"; then
					"${MYREALDIR}"/dirls.bash "${smlnk}" | ifne "${MYREALDIR}"/xblissargs.bash -I{} cp -vula {} "${POINTTO}"/
				else
					"${MYREALDIR}"/dirls.bash "${smlnk}" | ifne "${MYREALDIR}"/xblissargs.bash -I{} cp -vua {} "${POINTTO}"/
				fi
			fi
			rm -vr "${smlnk}" || true
		elif [ ! -e $(dirname "${smlnk}") ]; then
			"${MYREALDIR}"/ensure-path.bash $(dirname "${smlnk}")
		fi
		ln -sv "${POINTTO}" "${smlnk}"
	fi
done

#!/usr/bin/env bash

#!/usr/bin/env bash

set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
	source ../z-ensure-poolosid.rc.bash
popd >/dev/null

if [[ $MNT == '' ]]; then # mountpoint
        readonly MNT=/mnt/z
fi


"${MYREALDIR}"/ensure-path.bash /etc/zfs/zfs-list.cache
touch /etc/zfs/zfs-list.cache/$POOL
ln -svf /usr/lib/zfs-linux/zed.d/history_event-zfs-list-cacher.sh /etc/zfs/zed.d
zed -F &

sleep 2.33

##Verify that zed updated the cache by making sure this is not empty:
##If it is empty, force a cache update and check again:
##Note can take a while. c.30 seconds for loop to succeed.
cat /etc/zfs/zfs-list.cache/$POOL

while [ ! -s /etc/zfs/zfs-list.cache/$POOL ]; do
	zfs set canmount=noauto $POOL/ROOT/$OSID
	sleep 1.33
done
cat /etc/zfs/zfs-list.cache/$POOL

##Stop zed:
pkill -9 "zed*" || true
kill %% || true 

##Fix the paths to eliminate mountpoint:
sed -Ei "s|$MNT/?|/|" /etc/zfs/zfs-list.cache/$POOL

cat /etc/zfs/zfs-list.cache/$POOL

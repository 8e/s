#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly CS_CMD=crc32
readonly F="${*}"
readonly MINSIZE=$(( $(ls -s "${F}" | cut -f1 -d' ') / 2 ))
readonly CS=$("${CS_CMD}" "${F}" | cut -f1 -d' ')
readonly MAXSIZE=$(( "${MINSIZE}" * 10 ))

find . -type f -size -"${MAXSIZE}"k -size +"${MINSIZE}"k -exec "${CS_CMD}" '{}' \+ | awk "/^${CS}/ {print \$2}"

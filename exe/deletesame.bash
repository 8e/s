#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/find-same.bash "${*}" | "${MYREALDIR}"/xargs.bash "${MYREALDIR}"/dlt.bash

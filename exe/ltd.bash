#!/usr/bin/env zsh
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash &>/dev/null

#set -o pipefail

pushd "${MYREALDIR}" >/dev/null
	./is_posint.bash "${CPULI}" || readonly CPULI=3
	./is_posint.bash "${NICE}" || readonly NICE=19
	./is_posint.bash "${IONIC}" || readonly IONIC=3
	./is_posint.bash "${ION}" || readonly ION=7
popd >/dev/null

(echo nice ionice cpulimit ifne | "${MYREALDIR}"/apt-ensure.bash cpulimit util-linux coreutils moreutils) > /dev/null

#CPU=1 for some reason breakes pipes!!!
#cpulimit for some reason hides the exit code!!!
#nice -n ${NICE} ionice -t -c"${IONIC}" -n"${ION}" "${@}"

nice -n ${NICE} ionice -t -c"${IONIC}" -n"${ION}" "${@}" &
readonly P="$!"
#ps -p ${P} >/dev/null && sudo cpulimit --quiet --background cpu=1 --limit="${CPULI}" --pid="${P}" | (ifne grep -v 'Child process is finished, exiting...' || true)
ps -p ${P} >/dev/null && sudo cpulimit --quiet --background --limit="${CPULI}" --pid="${P}" | (ifne grep -v 'Child process is finished, exiting...' || true)
fg 2>/dev/null || wait ${P}

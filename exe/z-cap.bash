#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
	source ../defaults.rc.bash
popd >/dev/null

readonly P="${@:-/}"
if [ -e "${P}" ]; then
	readonly POOL=$("${MYREALDIR}"/z-pool.bash "${P}")
else
	readonly POOL="${P}"
fi
zpool list -o capacity "${POOL}" -H  | cut -d'%' -f1

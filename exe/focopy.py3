#!/usr/bin/env python3
import os
import shutil
import concurrent.futures
import multiprocessing
import sys

JOBS = int(os.getenv('JOBS', multiprocessing.cpu_count()))

def focopy(source, target):
    if target.endswith('/'):
        print(f'If target is a parenting directory, append the source filename')
        target = os.path.join(target, os.path.basename(source))

    if os.path.isdir(source):
        print(f'Create target directory if it does not exist')
        if not os.path.exists(target):
            os.makedirs(target)

        with concurrent.futures.ThreadPoolExecutor(max_workers=JOBS) as executor:
            for item in os.listdir(source):
                source_path = os.path.join(source, item)
                target_path = os.path.join(target, item)
                executor.submit(focopy, source_path, target_path)

    else:
        try:

            if os.path.exists(target):
                print(f'Move existing files to /var/backups and overwrite them')
                backup_dir = '/var/backups'
                backup_target = os.path.join(backup_dir, os.path.basename(target))
                os.makedirs(backup_dir, exist_ok=True)
                shutil.move(target, backup_target)

            if target.endswith('/'):
                print(f'If target is a parenting directory, append the source filename')
                target = os.path.join(target, os.path.basename(source))

            if os.path.islink(source):
                link_target = os.readlink(source)
                print(f'Copy symlink without following it')
                print(f"Creating symlink: {target} -> {link_target}")
                os.symlink(link_target, target)

            elif os.path.exists(target) and os.stat(source).st_dev == os.stat(target).st_dev:
                print(f'Same filesystem, create hard link')
                os.link(source, target)

            elif os.path.exists(os.path.dirname(target)) and os.stat(source).st_dev == os.stat(os.path.dirname(target)).st_dev:
                print(f'Target does not exist, compare source filesystem with parent directory of target')
                print(f"Creating hard link: {target}")
                os.link(source, target)

            else:
                print(f'Different filesystem, use regular copy')
                print(f"Copying: {target}")
                shutil.copy2(source, target)

            print(f"Copying file attributes: {target}")
            shutil.copystat(source, target)

            print(f'Set the same file permissions, timestamps, and ownership as the source')
            shutil.copystat(source, target)

        except Exception as e:
            print(f"Error copying {source} to {target}: {e}")

    print(f"Finished copying {source} to {target}")

if sys.argv[2].endswith('/'):
    print(f'If target is a parenting directory, append the source filename')
    target = os.path.join(os.path.abspath(sys.argv[2]), os.path.basename(sys.argv[1]))
else:
    target = os.path.abspath(sys.argv[2])
src = os.path.abspath(sys.argv[1])

print(f'{src} to {target}')

focopy(src, target)

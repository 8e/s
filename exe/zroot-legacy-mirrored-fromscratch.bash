#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly DISK=${1}
readonly MYDIR=$(dirname $(realpath ${0}))
readonly OSID=$(cat ${MYDIR}/lib/OSID)

[[ "${DISK}" == '' ]] && exit 1

sgdisk --zap-all $DISK  #DANGEROUS

sgdisk -a1 -n5:24K:+1000K -t5:EF02 $DISK

# Mirrored swap
sgdisk     -n2:0:+500M    -t2:FD00 $DISK
"${MYREALDIR}"/apt-ensure.bash mdadm cryptsetup
mdadm --create /dev/md0 --chunk=4K  --metadata=1.2 --level=raid10 --raid-devices=2 --verbose ${DISK}-part2 missing
mkswap -f /dev/md0 -p $(getconf PAGESIZE) -L swap
### 

sgdisk     -n3:0:+2G      -t3:BE00 $DISK

sgdisk     -n4:0:0        -t4:BF00 $DISK

fdisk -l $DISK

while ! ls -1 ${DISK}-part4; do
	(partprobe $DISK; kpartx $DISK) || true
	sleep 1.33
done

exe/z-rootcreate.bash -R /mnt/z ${DISK}-part4

exe/z-createboot.bash -R /mnt/z ${DISK}-part3

debootstrap "${DISTRO}" /mnt/z


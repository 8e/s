#!/usr/bin/env bash

set -ex

readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly NOWDATE="$(date '+%F %T')"

if [ ${#} -eq 0 ]; then
	first="${NOWDATE}"
	while read dateline; do
		second="${dateline}"
		echo $(( $(date -d "${first}" '+%s') - $(date -d "${second}" '+%s') ))
		first="${second}"
	done
else
	printf "%s\n" "${*}" | "${ME}"
fi



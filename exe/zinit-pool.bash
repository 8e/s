#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
	        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash

	if [[ "${MNT}" == '' ]]; then
		readonly MNT=/mnt/z
	fi

	zpool import -R "${MNT}" -mlfd /dev/disk/by-id ${*} || true
	zpool resilver ${*} &
	./ltd.bash zpool trim ${*} &
	./ltd.bash zpool scrub ${*} &

	./zfs-mount.bash ${*}

popd >/dev/null

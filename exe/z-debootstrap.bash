#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
	source ../z-ensure-poolosid.rc.bash

	readonly TMPDR=/tmp/$(basename "${0}").$$
	if [[ "${DEFUSER}" == '' ]]; then
		exit 1
	fi
	if [[ "${OSID}" == '' ]]; then
		exit 2
	fi
	if [[ "${MNT}" == '' ]]; then
		exit 3
	fi

	# Install base system
	debootstrap $DISTRO ${MNT}

	./focopy.bash -av --backup /etc/hostid $MNT/etc
	./focopy.bash -av --backup /etc/resolv.conf $MNT/etc/

	ln -svf --backup /proc/self/mounts $MNT/etc/mtab
	echo -e "tmpfs\t/var/log\ttmpfs\tnoatime,owner,X-mount.mkdir=775\t0\t0" | tee -a $MNT/etc/fstab
	echo -e "tmpfs\t/var/lib/apt\ttmpfs\tnoatime,owner,X-mount.mkdir=775\t0\t0" | tee -a $MNT/etc/fstab
	echo -e "tmpfs\t/var/cache/apt/archives\ttmpfs\tnoatime,owner,X-mount.mkdir=775\t0\t0" | tee -a $MNT/etc/fstab
	echo -e "tmpfs\t/etc/cron.weekly\ttmpfs\tnoatime,owner,X-mount.mkdir=775\t0\t0" | tee -a $MNT/etc/fstab
	./ensure-path.bash $MNT/etc/fstab.d/

	readonly REPODIRNAME=$(basename `git rev-parse --show-toplevel`)
	./ensure-path.bash $MNT/var/git/
	cp -r ../../"${REPODIRNAME}" $MNT/var/git/

	echo "source /var/git/${REPODIRNAME}/defaults.rc.bash" | tee -a $MNT/root/.bashrc | tee -a $MNT/root/.progile | tee -a $MNT/etc/skel/.bashrc | tee -a $MNT/etc/skel/.profile

 	ln -sv "/var/git/${REPODIRNAME}/defaults.rc.bash" $MNT/etc/profile.d/

	rm -rv "${MNT}"/var/log/* &
	rm -rv "${MNT}"/var/lib/apt/* &
	rm -rv "${MNT}"/var/cache/apt/archives/* &

	echo /var/backups | sudo ./ensure-symlink.bash ../datos/archive/backups
	echo /opt | ./ensure-symlink.bash datos/soft/linux
	echo /etc/cron.daily | ./ensure-symlink.bash cron.weekly
	echo /etc/cron.hourly | ./ensure-symlink.bash cron.daily
	echo /etc/apt | ./ensure-symlink.bash "/var/git/${REPODIRNAME}/lib/apt"
	echo /home/"${DEFUSER}"/.cache | sudo -u "${DEFUSER}" ./ensure-symlink.bash "/tmp/caches/${DEFUSER}"
	echo /root/.cache | ./ensure-symlink.bash "/tmp/caches/root"
	./focopy.bash /usr/lib/modules/$(uname -r) "${MNT}"/lib/modules/
	./focopy.bash /media/cdrom/casper/vmlinuz "${MNT}"/boot/vmlinuz-$(uname -r)
	./focopy.bash /media/cdrom/casper/initrd "${MNT}"/boot/initrd.img-$(uname -r)

	./mount-rbind-chroot.bash bash --login -c "/var/git/${REPODIRNAME}/exe/apt-cfg.bash"

	chroot "${MNT}" "/var/git/${REPODIRNAME}/exe/apt-keys.bash"

	chroot "${MNT}" "/var/git/${REPODIRNAME}/exe/screen.bash /var/git/${REPODIRNAME}/exe/basic-install.bash"

	chroot "${MNT}" "POOL=${POOL} OSID=${OSID} DEFUSER=${DEFUSER} /var/git/${REPODIRNAME}/apply.bash chrooted"

popd

##Copy install log into new installation.
#pushd /var/log
#       DELIVER=true "${MYREALDIR}"/relmv.zsh . "${MNT}"/var/log
#popd


#!/usr/bin/env bash

readonly MYREALDIR=$(dirname $(realpath ${0}))
which git || "${MYREALDIR}"/apt.bash -y install git

git add -A
[ ${#} -eq 1 ] && set -- -m ${1}
EDITOR=vi git commit ${*} || "${MYREALDIR}"/git-config.bash && EDITOR=vi git commit ${*}
git push

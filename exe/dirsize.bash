#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
	which hr || ./hr_install.bash
popd >/dev/null

ls -alRs ${*} | awk '{totsize += $6 }; END { print totsize }' | pee "cat" "hr"

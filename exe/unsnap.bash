#!/usr/bin/env bash

set -e

[ ${DEBUG} ] && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

source "${MYREALDIR}"/../defaults.rc.bash

snap list | awk 'NR != 1 {print $1}' | xargs -l sudo snap remove

mount | awk '/snap/ {print $3}' | "${MYREALDIR}"/xblissargs.bash -I{} sudo umout -fv

sudo "${MYREALDIR}"/apt.bash purge snap

sudo rm -rf /var/cache/snapd &
sudo rm -rf /var/snap &
sudo rm -rf /snap &
sudo rm -rf /var/lib/snapd &

"${MYREALDIR}"/gethomes.bash | "${MYREALDIR}"/xblissargs.bash -I{} sudo rm -rf {}/snap

sudo apt-mark hold snapd

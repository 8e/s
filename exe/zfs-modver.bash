#!/bin/bash 

set -e

readonly MYREALDIR=$(dirname $(realpath ${0}))

find /lib/modules -type f -iname '*zfs*' -exec "${MYREALDIR}"/modver.bash '{}' \+ |sort

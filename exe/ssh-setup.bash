#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

systemctl stop sshd &
systemctl stop ssh &
systemctl disable sshd &
systemctl disable ssh &

"${MYREALDIR}"/passwd.bash

systemctl status dropbear || "${MYREALDIR}"/apt-ensure.bash sshuttle autossh ssh-tools sshguard sshoot sshpass ufw net-tools dropbear

#if ! systemctl status sshd; then
#	"${MYREALDIR}"/apt-ensure.bash openssh-server openssh-client mosh ssh-audit sshuttle pssh autossh libpam-ssh openssh-client openssh-known-hosts ssh-import-id ssh-tools sshguard sshoot sshpass ufw net-tools
#fi

"${MYREALDIR}"/focopy.bash "${MYREALDIR}"/../lib/ssh /etc/

"${MYREALDIR}"/hamachi-2.bash

if (( $("${MYREALDIR}"/fw-amount.bash) < 2 )); then
	"${MYREALDIR}"/get-up-ip.bash
	read -e -p "Enter from host: " FROM
	"${MYREALDIR}"/fw-allow.bash "${FROM}"
fi

systemctl restart dropbear
systemctl status dropbear
"${MYREALDIR}"/get-up-ip.bash
netstat -tulpn

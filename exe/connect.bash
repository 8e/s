#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly WD=$("${MYREALDIR}"/w-dev.bash)

if ! concheck.bash; then
	sudo ifdown "${WD}"
	sudo ifup "${WD}" || sudo netcardconfig
fi
if ! concheck.bash && ! sudo ifup "${WD}"; then 
	fuser -k /run/network/ifstate."${WD}" &>/dev/null &
	#reset
	sudo netcardconfig
fi
tsp "${MYREALDIR}"/proxy.bash

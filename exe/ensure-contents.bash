#!/usr/bin/env bash

set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly EPATH="${*}"

"${MYREALDIR}"/apt-ensure-cmd.bash moreutils ifne

readonly FILE_CONTENTS=$(ifne cat)

which md5sum | "${MYREALDIR}"/apt-ensure.bash coreutils
readonly CS="sha1sum" # should support pipe input, that's why it's not "crc32"

readonly CHECKSUM=$(echo -n "${FILE_CONTENTS}" | $CS)

function writetoit() {
	echo -n "${FILE_CONTENTS}" > "${EPATH}"
        sudo systemctl daemon-reload &
}

function update_existing_if_needed() {
	if [[ $(cat "${EPATH}" | $CS) != "${CHECKSUM}" ]]; then
		mv -bv "${EPATH}" /var/backups/
		writetoit
	fi
}

if [ -e "${EPATH}" ]; then
	update_existing_if_needed
else
	"${MYREALDIR}"/ensure-path.bash "$(dirname ${EPATH})"
	writetoit
fi

cat "${EPATH}"
echo
echo "↑${EPATH}↑ exists and has a $CS of ${CHECKSUM}"

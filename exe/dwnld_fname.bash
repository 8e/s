#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly LOGD=$("${MYREALDIR}"/ensure-path.bash /tmp/log)
readonly URL="${*}"
readonly EXPECTED=$(basename "${URL}")

"${MYREALDIR}"/apt-ensure-cmd.bash wget wget

readonly LOGTMPFILE="${LOGD}"/"${EXPECTED}.$$.log"
	wget -c "${URL}" &> "${LOGTMPFILE}"
	if grep 'The file is already fully retrieved; nothing to do.' "${LOGTMPFILE}" >/dev/null; then
		ls "${EXPECTED}"
	else
		fname=$(tail -2 ${LOGTMPFILE} | awk -vFS='‘|’' '/saved/ {print $2}')
		if [[ "${fname}" == '' ]]; then
			echo ${LOGTMPFILE}
			cat ${LOGTMPFILE}
			exit 1
		else
			echo "${fname}" 
		fi
	fi
rm ${LOGTMPFILE} &

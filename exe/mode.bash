#!/usr/bin/env bash

set -e
DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash
(which stat | "${MYREALDIR}"/apt-ensure.bash coreutils) >/dev/null

stat --format="%a" "${*}"

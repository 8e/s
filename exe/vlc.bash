#!/usr/bin/env bash

set -e
DEBUG=${DEBUG:-false}
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure-cmd.bash vlc vlc

declare -a Opts=(--no-playlist-autostart --recursive expand --media-library)

#if [[ "${USER}" == "${DEFUSER}" ]]; then
	vlc "${Opts[@]}" "${@}" &
#else
#	sudo -u $DEFUSER vlc "${Opts[@]}" "${@}" &
#fi

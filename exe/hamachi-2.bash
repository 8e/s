#!/usr/bin/env bash

set -e
DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

"${MYREALDIR}"/dotdee-hosts.bash

#hamachi list | awk '/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/ {print $4"\t"$3}' >> /etc/hosts

sudo hamachi list | awk '/^[[:blank:]]{5}\*/ {print $4"\t"$3}' | sudo tee /etc/dotdee/etc/hosts.d/74-hamachi_ipv4
echo -e "$(ip addr|awk -vFS='[/ ]' '/ham0/ && /inet/ {gsub(/\/[0-9][0-9]/,""); print $6}')\t$(hostname)" | sudo tee -a /etc/dotdee/etc/hosts.d/74-hamachi_ipv4
#sudo hamachi list | awk '/^[[:blank:]]{5}\*/ {print $8"\t"$3}' | sudo tee /etc/dotdee/etc/hosts.d/76-hamachi_ipv6

sudo dotdee -u /etc/hosts

readonly HOSTS=$(cut /etc/dotdee/etc/hosts.d/74-hamachi_ipv4 -f2)
"${MYREALDIR}"/knownhosts-renew.bash "${HOSTS[@]}"

"${MYREALDIR}"/fw-allow.bash $(cut /etc/dotdee/etc/hosts.d/74-hamachi_ipv4 -f1)

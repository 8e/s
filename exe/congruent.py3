#!/usr/bin/env python3
import os
import sys
import multiprocessing

JOBS = int(os.getenv('JOBS', multiprocessing.cpu_count()))


def get_file_type(path):
    file_type = "unknown"
    if os.path.isdir(path):
        file_type = "directory"
    elif os.path.islink(path):
        file_type = "symbolic link"
    elif os.path.isfile(path):
        file_type = "file"
    elif os.path.issocket(path):
        file_type = "socket"
    elif os.path.isfifo(path):
        file_type = "named pipe"
    elif os.path.ischardev(path):
        file_type = "character device"
    elif os.path.isblockdev(path):
        file_type = "block device"

    return file_type

def congruent(src, target):
    if os.path.isdir(src) and os.path.isdir(target):
        # Compare directory contents recursively
        for src_dir, dirs, files in os.walk(src):
            rel_dir = os.path.relpath(src_dir, src)
            target_dir = os.path.join(target, rel_dir)
            if not os.path.isdir(target_dir):
                print(f"Directory does not exist in target: {target_dir}")
                return False

            # Process files in parallel
            with multiprocessing.Pool(JOBS) as pool:
                results = [congruent(os.path.join(src_dir, file), os.path.join(target_dir, file)) for file in files]

            if not all(results):
                return False

        print(f"Directories are congruent: {src} and {target}")
        return True
    else:
        # Compare individual files

        # Check file types
        src_type = get_file_type(src)
        target_type = get_file_type(target)

        if src_type != target_type:
            print(f"Different file types: {src} ({ src_type }) and {target} ({ target_type })")
            return False
        elif os.path.islink(src):
            src_link = os.readlink(src)
            target_link = os.readlink(target)
            if src_link == target_link:
                print(f"Same symlink targets: {src} ({src_link}) and {target} ({target_link})")
                return True
            else:
                print(f"Different symlink targets: {src} ({src_link}) and {target} ({target_link})")
                return False
        elif os.path.samefile(src, target):
            # Check if files are hardlinks to the same inode
            print(f"Same file: {src} and {target}")
            return True

        # Check file sizes
        src_size = os.path.getsize(src)
        target_size = os.path.getsize(target)

        if src_size != target_size:
            print(f"Different file sizes: {src} ({src_size} bytes) and {target} ({target_size} bytes)")
            return False

        # Blockwise comparison
        block_size = os.statvfs(src).f_bsize
        with open(src, 'rb') as src_file, open(target, 'rb') as target_file:
            while True:
                src_data = src_file.read(block_size)
                target_data = target_file.read(block_size)
                if src_data != target_data:
                    print(f"Different file contents: {src} and {target}")
                    return False
                if not src_data:
                    break

        print(f"Same file contents: {src} and {target}")
        return True

# Check command-line arguments
if len(sys.argv) != 3:
    print("Usage: python congruent.py3 <source> <target>")
    sys.exit(2)

src = os.path.abspath(sys.argv[1])
target = os.path.abspath(sys.argv[2])

# Check if source and target exist
if not os.path.islink(src) and not os.path.exists(src):
    print(f"Source does not exist: {src}")
    sys.exit(3)

if not os.path.exists(target):
    print(f"Target does not exist: {target}")
    sys.exit(4)

if congruent(src, target):
    sys.exit(0)
else:
    sys.exit(1)

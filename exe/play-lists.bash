#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

#readonly TIMEINCSTEP=0.03
readonly OUT_PL=./$(basename "${ME}").m3u8
readonly MELI=~/.local/share/vlc/ml.m3u8
readonly PLEXE="${MYREALDIR}"/vlc.bash

if (( ${#} == 0 )); then
	declare -a PLists=(*.m3u8 ../*.m3u8)
else
	declare -a PLists=(${*})
fi

function playbyone() {
	i='0'
	while read line; do
		if [ -e "${line}" ]; then
			line=$(realpath "${line}")
			echo "${line}" | tee -a "${OUT_PL}" | tee -a "${MELI}"
			tsp "${MYREALDIR}"/ltd.bash "${PLEXE}" "${line}"
		#	echo "$i"
		#	sleep "$i"
		#	i=$(bc <<< "$i + $TIMEINCSTEP")
		fi
	done
}

"${MYREALDIR}"/apt-ensure-cmd.bash task-spooler tsp
tsp -S $("${MYREALDIR}"/cpus.sh)
[ -s "${OUT_PL}" ] && cat /dev/null > "${OUT_PL}"
tsp "${PLEXE}"
echo ${PLists[*]}
"${MYREALDIR}"/ltd.bash sort -Ru ${PLists[*]} | playbyone &
wait
tsp -f true

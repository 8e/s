#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

[ -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5 ] || sudo chmod +x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5
dolphin "$@"
[ -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5 ] && sudo chmod -x /usr/lib/x86_64-linux-gnu/libexec/kf5/kioslave5




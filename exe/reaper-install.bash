#!/usr/bin/env bash

set -xe

# https://www.reaper.fm/download.php#linux_download

readonly MYREALDIR="$(dirname $(realpath ${0}))"
source "${MYREALDIR}"/../defaults.rc.bash
readonly DISTROD=/"${MYREALDIR}"/../../../soft/linux/reaper_linux_x86_64
readonly INSTALLER="${DISTROD}/install-reaper.sh"

sudo "${MYREALDIR}"/apt-ensure.bash $(cat "${MYREALDIR}"/../lib/reaper.list)

regsvr32 wineasio.dll
wine64 regsvr32 wineasio.dll


pushd $(dirname "${DISTROD}"/../)

	[ -f reaper*_linux_x86_64.tar.xz ] && tar -xvf reaper*_linux_x86_64.tar.xz || true

	pushd "${DISTROD}"

		#make
		
		sudo "${INSTALLER}" --install "/opt" --quiet --integrate-desktop --usr-local-bin-symlink

		# https://reapack.com/user-guide
		cp -f reaper_reapack-x86_64.so ~/.config/REAPER/UserPlugins/

		#sudo su "${DEFUSER}" - -c "ln -svf $(realpath libSwell.so) /home/${DEFUSER}/reaper_linux_x86_64/REAPER/libSwell.so"
		#ln -svf $(realpath libSwell.so) /root/reaper_linux_x86_64/REAPER/libSwell.so
	
	popd

popd


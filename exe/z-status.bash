#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

zpool list -v

zpool status -v

zfs list -o name,compression,compressratio,dedup,used,avail,refer,checksum,reserv

zpool list -o name,free,ckpoint,cap,dedup,health -v

echo -n "snapshots:"
zfs list -t snapshot -H | wc -l


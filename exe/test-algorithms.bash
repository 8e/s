#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

readonly Algorithms=$("${MYREALDIR}"/print-cryptsetup-algorithms.bash)
declare -A IntAlgosCompat
declare -A EncCiphersCompat
readonly MYLOG=/var/log/$(basename ${0}).log
readonly LUKSILOG=/var/log/luksilog
readonly BACKDEV="${1}"
readonly TARGETDEV=${2:-/dev/mapper/$(basename "${BACKDEV}").luks}

function benchmark_blockdev() {
	set +e
	zpool sync &
	sync
	ioping -c 5 ${*}
	hdparm -tT ${*}
	#iozone -a ${*}
	## GNU dd syntax ##
	##########################################################
	##***[Adjust bs and count as per your needs and setup]**##
	##########################################################
	dd if=/dev/zero of=${*} bs=1G count=1 oflag=dsync
	dd if=/dev/zero of=${*} bs=64M count=1 oflag=dsync
	dd if=/dev/zero of=${*} bs=1M count=256 conv=fdatasync
	dd if=/dev/zero of=${*} bs=8k count=10k
	dd if=/dev/zero of=${*} bs=512 count=1000 oflag=dsync
	# https://docs.cloud.oracle.com/en-us/iaas/Content/Block/References/samplefiocommandslinux.htm
	# Run the following command directly to test random reads:
	fio --filename=${*} --direct=1 --rw=randread --bs=4k --ioengine=libaio --iodepth=256 --runtime=120 --numjobs=4 --time_based --group_reporting --name=iops-test-job --eta-newline=1 --readonly
	#Run the following command against the mount point to test file read/writes:
	fio --filename=${*} --size=1GB --direct=1 --rw=randrw --bs=4k --ioengine=libaio --iodepth=256 --runtime=120 --numjobs=4 --time_based --group_reporting --name=iops-test-job --eta-newline=1
	#Run the following command to test random read/writes:
	fio --filename=${*} --direct=1 --rw=randrw --bs=4k --ioengine=libaio --iodepth=256 --runtime=120 --numjobs=4 --time_based --group_reporting --name=iops-test-job --eta-newline=1
	#Run the following command to test sequential reads:
	fio --filename=${*} --direct=1 --rw=read --bs=4k --ioengine=libaio --iodepth=256 --runtime=120 --numjobs=4 --time_based --group_reporting --name=iops-test-job --eta-newline=1 --readonly
	#Run the following command to test random reads:
	fio --filename=${*} --direct=1 --rw=randread --bs=64k --ioengine=libaio --iodepth=64 --runtime=120 --numjobs=4 --time_based --group_reporting --name=throughput-test-job --eta-newline=1 --readonly
	#Run the following command against the mount point to test file read/writes:
	fio --filename=${*} --size=1GB --direct=1 --rw=randrw --bs=64k --ioengine=libaio --iodepth=64 --runtime=120 --numjobs=4 --time_based --group_reporting --name=throughput-test-job --eta-newline=1 
	#Run the following command to test random read/writes:
	fio --filename=${*} --direct=1 --rw=randrw --bs=64k --ioengine=libaio --iodepth=64 --runtime=120 --numjobs=4 --time_based --group_reporting --name=throughput-test-job --eta-newline=1
	#Run the following command to test sequential reads:
	fio --filename=${*} --direct=1 --rw=read --bs=64k --ioengine=libaio --iodepth=64 --runtime=120 --numjobs=4 --time_based --group_reporting --name=throughput-test-job --eta-newline=1 --readonly
	#Run the following command directly to test random reads for latency:
	fio --filename=${*} --direct=1 --rw=randread --bs=4k --ioengine=libaio --iodepth=1 --numjobs=1 --time_based --group_reporting --name=readlatency-test-job --runtime=120 --eta-newline=1 --readonly
	#Run the following command directly to test random read/writes for latency:
	fio --filename=${*} --direct=1 --rw=randrw --bs=4k --ioengine=libaio --iodepth=1 --numjobs=1 --time_based --group_reporting --name=rwlatency-test-job --runtime=120 --eta-newline=1 --readonly
	sync
	set -e
}

echo $(date +%F_%T) | tee "${MYLOG}"

which ddrescue || "${MYREALDIR}"/apt.bash install gddrescue
which ioping || "${MYREALDIR}"/apt.bash install ioping
which hdparm || "${MYREALDIR}"/apt.bash install hdparm
#which iozone || "${MYREALDIR}"/apt.bash install iozone3
which fio || "${MYREALDIR}"/apt.bash install fio

i=1
for intalgo in ${Algorithms[@]}; do
	for encciph in ${Algorithms[@]}; do
		if "${MYREALDIR}"/luksify.bash "${BACKDEV}" "${intalgo}" "${encciph}" > "${LUKSILOG}"; then
			echo -e "Integrity:\t${intalgo}\t+\tEncryption:\t${encciph}\t=success" | tee -a "${MYLOG}"
			algolog=/tmp/cryptswaptest.contwrite.int_${intalgo}.enc_${encciph}.log
			IntAlgosCompat["${intalgo}"]="${IntAlgosCompat[${intalgo}]} ${encciph}"
			EncCiphersCompat["${encciph}"]="${EncCiphersCompat[${encciph}]} ${intalgo}"
			#ddrescue --force /dev/urandom /dev/mapper/cryptswap "${algolog}" || true
			benchmark_blockdev "${TARGETDEV}" 2>&1 | tee "${algolog}" | tee -a "${MYLOG}"
			#cat "${algolog}" >> "${MYLOG}"
			cryptsetup luksClose "${TARGETDEV}"
			#swapon -av
			#swapoff -av
		fi
		(( i++ ))
	done
done

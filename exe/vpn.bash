#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

readonly CONNECTOR_CONFIG=$(echo /etc/*/*/connector.conf)

function disconnect() {
	openvpn3 session-manage --disconnect --path ${*} || openvpn3 session-manage --disconnect --config $(realpath ${*})
	onchange
} 

function connect() {
	openvpn3 session-start --config $(realpath ${*})
	onchange
}

function onchange() {
	pushd "${MYREALDIR}"
		./screen.bash /etc/openvpn/scripts/update-resolv-conf
		./screen.bash /etc/openvpn/scripts/update-systemd-resolved
		sudo -u "${DEFUSER}" ./screen.bash ./proxy.bash
		status
	popd
}

function status() {
	openvpn3 sessions-list ${*}
	pushd "${MYREALDIR}"
		./concheck.bash
		./exp.bash
	popd
}

function remove_one_imported_config() {
	openvpn3 config-remove --path ${*} || openvpn3 config-remove --config $(realpath ${*})
}

function remove_all_imported_configs() {
	local cp
	for cp in $(openvpn3 configs-list | grep net); do
		remove_one_imported_config $cp
	done
}

pushd "${MYREALDIR}"
	source ../defaults.rc.bash
popd

remove_all_imported_configs

while (( ${#} > 0 )); do
	case ${1} in

		'connector')
			shift # past action
			./nat-and-forwarding-on-default-interfaces.bash
			connect "${CONNECTOR_CONFIG}" ${*}
			exit
		;;

		'disconnector')
			shift # past action
			disconnect "${CONNECTOR_CONFIG}" ${*}
			exit
		;;

		'c'|'con'|'connect')
			shift # past action
			connect ${*}
			exit
		;;

		'd'|'discon'|'disconnect')
			shift # past action
			disconnect ${*}
			exit
		;;

		's'|'status')
			shift # past action
			status ${*}
		;;

		'e')
			shift # past action
			systemctl start expressvpn
			systemctl status expressvpn
			expressvpn connect $*
			status ${*}
		;;

		'de')
			shift # past action
			expressvpn disconnect
			systemctl stop expressvpn
			status ${*}
		;;

		*)
			openvpn3 ${*}
			exit
		;;

		esac
done

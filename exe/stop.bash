#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
	source ../defaults.rc.bash
popd >/dev/null

tsp kilall Telegram
tsp sudo systemctl stop cudo-miner
tsp sudo systemctl stop monero
tsp sudo systemctl stop qbittorrent
tsp sudo systemctl stop syncthing
tsp kilall qbittorrent
tsp rm -rvf /tmp/*.deb
tsp kilall -KILL Telegram

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly MAXJOBSFILE="${TMPDIR}/maxjobs"
readonly NICE="19"
readonly IONI="c3"
#readonly LOGF="${LOGSD}/$$.log"
readonly P_SEMAPHORENAME=$(basename "${ME}").${USER}.${1} #should be 1st param
shift
readonly TMPDIR=$("${MYREALDIR}"/ensure-path.bash /tmp/caches/"${P_SEMAPHORENAME}")
readonly JOBSD=$("${MYREALDIR}"/ensure-path.bash "${TMPDIR}/jobs")
readonly MGMTPIDSFILE="${TMPDIR}/pids"
declare -a Executioner=($(which bash) '-xem')
declare -a PaShebang=('#!'${Executioner[@]})
declare -a MgmtPids

#which parallel bash tmux screen inotifywait | "${MYREALDIR}"/apt-ensure.bash parallel bash inotify-tools screen tmux
which tsp bash inotifywait || "${MYREALDIR}"/apt-ensure.bash task-spooler bash inotify-tools

function maxjobs() {
	if "${MYREALDIR}"/is_posint.bash $1; then
		$(get_semcmd) -S $1
	else
		$(get_semcmd) -S $("${MYREALDIR}"/cpus.sh)
	fi
	$(get_semcmd) -S
}

function get_semcmd() {
	#echo /usr/bin/sem --semaphorename "${P_SEMAPHORENAME}" $(get_paopts) "${@}"
	echo /bin/tsp -L "${P_SEMAPHORENAME}" "${@}"
}

function write_jobf() {
	local jobf="${*}"
	echo "${PaShebang[@]}" > "${jobf}"
	#echo "exec > >(tee -a '$(get_logf "${jobf}")') 2>&1" >> "${jobf}"
	echo "source \"${MYREALDIR}\"/../defaults.rc.bash" >> "${jobf}"
	cat >> "${jobf}"
	#${DEBUG} || (echo "rm '${TMPDIR}'/*\$$.* '\$0' '$(get_logf "${jobf}")' || true &" >> "${jobf}")
	chmod +x "${jobf}"
}

function countj {
	#find "${TMPDIR}/" -path "${TMPDIR}/job.$$.*.bash" | wc -l
	echo "$(( $($(get_semcmd) -l | wc -l) - 1 ))"
	
}

function create() {
	[ -s "${MGMTPIDSFILE}" ] && exit 1
	local jobf="${JOBSD}/$$.0.bash"
	if [[ "${1}" != '' ]] && (( ${1} > 0 )); then
		maxjobs ${1}
		shift
	fi
	"${MYREALDIR}"/ensure-path.bash "${JOBSD}"
	echo "inotifywait -qmre attrib --format %w%f '${JOBSD}' | $(get_semcmd)" | SCREENN="${P_SEMAPHORENAME}" WINDOWN="${P_SEMAPHORENAME}" "${MYREALDIR}"/screen.bash
	#$(get_semcmd) "${jobf}"
	#$(get_semcmd) -p > "${MGMTPIDSFILE}"
	echo $$ >> "${MGMTPIDSFILE}"
	source "${MYREALDIR}/bg-pids.bash" >> "${MGMTPIDSFILE}"
}

function addjobs() {
	local jobf
	local j=$(countj)
	[ -s "${MGMTPIDSFILE}" ] || create "${@}"
	while read line; do
		j=$(( $j + 1 ))
		sleep $j 
		jobf="${JOBSD}/$$.$j.bash"
		echo "sleep $j; ${line}" | write_jobf "${jobf}"
		#echo "/usr/bin/env nice -"${NICE}" ionice -"${IONI}" screen -d -m -L -Logfile '$(get_logf "${jobf}")' '${jobf}'" | tee -a "${LOGF}"
	done
}


function mgmtpids() {
	MgmtPids=($(_out_mgmtpids))
	echo ${MgmtPids[*]}
}

function _out_mgmtpids() {
	if (( ${#MgmtPids[*]} == 0 )); then
	       cat "${MGMTPIDSFILE}"
	else
	       echo ${MgmtPids[*]}
	fi | tr " " "\n" | while read p; do
		(ps -p $p > /dev/null) && echo $p
	done
}

function stop() {
	finish &
	$(get_semcmd) -K &
	rm -rvf "${TMPDIR}" &
	for p in $(mgmtpids); do
		kill $p &
		kill -KILL $p &
	done
	#${DEBUG} || (rm -rvf "${TMPDIR}" || true; rm -rvf "${JOBSD}" || true; rm -rvf "${LOGSD}" || true)
}

function finish() {
	$(get_semcmd) -f true
	rm -rvf "${TMPDIR}" &
}

case "${1}" in
	create)
		shift
		create "${@}"
		exit
	;;
	stop|kill)
		shift
		stop
		exit
	;;
	wait|finish)
		shift
		finish
		wait
		exit
	;;
	*)
		[ -s "${MGMTPIDSFILE}" ] || create "${@}"
		addjobs "${@}"
		exit
	;;
esac


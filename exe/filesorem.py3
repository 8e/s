#!/usr/bin/env python3

import os
import sys
import operator
import subprocess
from datetime import datetime

def get_size(path):
    total = 0
    if os.path.isdir(path) and not os.path.islink(path):
        with os.scandir(path) as it:
            for entry in it:
                if entry.is_file() or os.path.islink(entry):
                    total += entry.stat().st_size
                elif entry.is_dir():
                    total += get_size(entry.path)
    elif os.path.isfile(path) or os.path.islink(path):
        total = os.stat(path).st_size
    return total

def get_depth(path):
    return path.count(os.path.sep)

def get_mtime(path):
    return os.stat(path).st_mtime

def get_disk_usage(path):
    stat = os.statvfs(path)
    total = stat.f_blocks * stat.f_frsize
    free = stat.f_bfree * stat.f_frsize
    used = total - free
    usage = used / total
    return usage

def get_zfs_usage(path):
    # Get the pool name
    command = ['df', '-h', path]
    output = subprocess.check_output(command).decode('utf-8').strip()
    pool_name = output.split('\n')[-1].split()[0].split('/')[0]

    # Get the usage
    command = ['zpool', 'list', '-Ho', 'cap', pool_name]
    output = subprocess.check_output(command).decode('utf-8').strip()
    usage = int(output.rstrip('%')) / 100
    return usage

sort_key_funcs = {
    'size': get_size,
    'depth': get_depth,
    'age': get_mtime
}

sort_aliases = {
    'biggest': ('size', 'desc'),
    'smallest': ('size', 'asc'),
    'deepest': ('depth', 'desc'),
    'oldest': ('age', 'desc'),
    'newest': ('age', 'asc'),
    'asis': (None, 'asc'),
    'reverse': (None, 'desc'),
}

def sort_files(paths, sort_alias, delete_count, threshhold):
    log_file = os.environ.get('DELLOGF', '/tmp/log/filesorem.log')
    sort_key, order = sort_aliases.get(sort_alias, (None, None))
    if sort_key is None and sort_alias not in ('asis', 'reverse'):
        print(f"Unknown sort alias: {sort_alias}")
        sys.exit(1)

    reverse = (order == 'desc')
    delete_count = int(delete_count)
    threshhold = int(threshhold.rstrip('%')) / 100

    path_stat_pairs = []
    for path in paths:
        abs_path = os.path.abspath(path)
        if abs_path not in path_stat_pairs:
            try:
                if os.path.isdir(abs_path) and not os.path.islink(abs_path):
                    for dirpath, dirnames, filenames in os.walk(abs_path):
                        for filename in filenames:
                            filepath = os.path.join(dirpath, filename)
                            stat = sort_key_funcs[sort_key](filepath) if sort_key else 0
                            path_stat_pairs.append((filepath, stat))
                else:
                    stat = sort_key_funcs[sort_key](abs_path) if sort_key else 0
                    path_stat_pairs.append((abs_path, stat))
            except FileNotFoundError:
                pass

    if sort_key is not None:
        path_stat_pairs.sort(key=operator.itemgetter(1), reverse=reverse)

    path_stat_pairs.sort(key=operator.itemgetter(1), reverse=reverse)

    with open(log_file, 'w') as f:
        for i, (path, stat) in enumerate(path_stat_pairs):
            if i >= delete_count:
                break

            try:
                mount_point = os.path.dirname(path)
                while not os.path.ismount(mount_point) and mount_point != '/':
                    mount_point = os.path.dirname(mount_point)
                fstype = os.popen(f'df -T "{mount_point}" | tail -1 | awk \'{{print $2}}\'').read().strip()

                if fstype == 'zfs':
                    usage = get_zfs_usage(path)
                else:
                    usage = get_disk_usage(path)

                if usage > threshhold:
                    os.remove(path)
                    f.write(f"Removed: {path}\n")
                else:
                    print(path)
            except OSError as e:
                print(f"Failed to remove {path}: {e}", file=sys.stderr)

        for path, stat in path_stat_pairs[i+1:]:
            print(path)

def main():
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        print(f"Usage: {sys.argv[0]} <sort_alias> <delete_count> [<threshhold>]")
        sys.exit(1)

    sort_alias = sys.argv[1]
    delete_count = sys.argv[2]
    threshhold = sys.argv[3] if len(sys.argv) == 4 else '100%'

    paths = [line.strip() for line in sys.stdin if line.strip()]
    sort_files(paths, sort_alias, delete_count, threshhold)

if __name__ == '__main__':
    main()


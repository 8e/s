#!/bin/bash
# NOTE: You might use this script to follow along and manually run the commands
#       on your terminal and not actually run it. I'm not responsible if it
#       fails on some step and you have to start over! Good luck building :)
#
# For more see issue https://github.com/telegramdesktop/tdesktop/issues/2519

echo "== [1/6] Instaling dependencies"
sudo apt-get install git g++ -y

echo "== [2/6] Installing development dependencies (common)"
sudo apt-get install libexif-dev liblzma-dev libz-dev libssl-dev libappindicator-dev libunity-dev libicu-dev libdee-dev -y

echo "== [3/6] Installing development dependencies (FFmpeg)"
sudo apt-get install dh-autoreconf autoconf automake build-essential libass-dev libfreetype6-dev libgpac-dev libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texi2html zlib1g-dev yasm -y

echo "== [4/6] Installing development dependencies (OpenAL Soft)"
sudo apt-get install cmake -y

echo "== [5/6] Installing development dependencies (libxkbcommon)"
sudo apt-get install xutils-dev bison python-xcbgen -y

echo "== [6/6] Installing development dependencies (QT)"
sudo apt-get install libxcb1-dev libxcb-image0-dev libxcb-keysyms1-dev libxcb-icccm4-dev libxcb-render-util0-dev libxcb-util0-dev libxrender-dev libasound-dev libpulse-dev libxcb-sync0-dev libxcb-xfixes0-dev libxcb-randr0-dev libx11-xcb-dev libffi-dev -y

echo "== Adding PPA for g++ version 6"
sudo add-apt-repository ppa:ubuntu-toolchain-r/test

sudo apt-get update
sudo apt-get install gcc-6 g++-6 -y
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 60
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-6 60

echo "== Setting up build folder ~/TBuild"
cd
mkdir TBuild && cd TBuild
mkdir Libraries
git clone --recursive https://github.com/telegramdesktop/tdesktop.git -b master

# Building libraries
cd Libraries

echo "== [ 1/10] Downloading and building zlib 1.2.8"
wget http://www.zlib.net/fossils/zlib-1.2.8.tar.gz
tar xf zlib-1.2.8.tar.gz
rm zlib-1.2.8.tar.gz
cd zlib-1.2.8
./configure
make -j4
sudo make install
cd ..

echo "== [ 2/10] Downloading and building Opus codec 1.1"
wget http://downloads.xiph.org/releases/opus/opus-1.1.tar.gz
tar xf opus-1.1.tar.gz
rm opus-1.1.tar.gz
cd opus-1.1
./configure
make -j4
sudo make install
cd ..

echo "== [ 3/10] Downloading and building FFmpeg"
git clone https://github.com/01org/libva.git
cd libva
./autogen.sh --enable-static
make -j4
sudo make install
cd ..

git clone git://anongit.freedesktop.org/vdpau/libvdpau
cd libvdpau
./autogen.sh --enable-static
make -j4
sudo make install
cd ..

git clone https://github.com/FFmpeg/FFmpeg.git ffmpeg
cd ffmpeg
git checkout release/3.2
./configure --prefix=/usr/local --disable-programs --disable-doc --disable-everything --enable-protocol=file --enable-libopus --enable-decoder=aac --enable-decoder=aac_latm --enable-decoder=aasc --enable-decoder=flac --enable-decoder=gif --enable-decoder=h264 --enable-decoder=h264_vdpau --enable-decoder=mp1 --enable-decoder=mp1float --enable-decoder=mp2 --enable-decoder=mp2float --enable-decoder=mp3 --enable-decoder=mp3adu --enable-decoder=mp3adufloat --enable-decoder=mp3float --enable-decoder=mp3on4 --enable-decoder=mp3on4float --enable-decoder=mpeg4 --enable-decoder=mpeg4_vdpau --enable-decoder=msmpeg4v2 --enable-decoder=msmpeg4v3 --enable-decoder=opus --enable-decoder=vorbis --enable-decoder=wavpack --enable-decoder=wmalossless --enable-decoder=wmapro --enable-decoder=wmav1 --enable-decoder=wmav2 --enable-decoder=wmavoice --enable-encoder=libopus --enable-hwaccel=h264_vaapi --enable-hwaccel=h264_vdpau --enable-hwaccel=mpeg4_vaapi --enable-hwaccel=mpeg4_vdpau --enable-parser=aac --enable-parser=aac_latm --enable-parser=flac --enable-parser=h264 --enable-parser=mpeg4video --enable-parser=mpegaudio --enable-parser=opus --enable-parser=vorbis --enable-demuxer=aac --enable-demuxer=flac --enable-demuxer=gif --enable-demuxer=h264 --enable-demuxer=mov --enable-demuxer=mp3 --enable-demuxer=ogg --enable-demuxer=wav --enable-muxer=ogg --enable-muxer=opus

make -j4
sudo make install
cd ..

echo "== [ 4/10] Downloading and building PortAudio"
wget http://www.portaudio.com/archives/pa_stable_v19_20140130.tgz
tar xf pa_stable_v19_20140130.tgz
rm pa_stable_v19_20140130.tgz
cd portaudio
./configure
make -j4
sudo make install
cd ..

echo "== [ 5/10] Downloading and building OpenAL Soft"
git clone git://repo.or.cz/openal-soft.git
cd openal-soft/build
cmake -D LIBTYPE:STRING=STATIC ..
make -j4
sudo make install
cd ../..

echo "== [ 6/10] Downloading and building OpenSSL"
git clone https://github.com/openssl/openssl
cd openssl
# https://github.com/telegramdesktop/tdesktop/issues/2519#issuecomment-253171003
# Use 1.0.2 and not 1.0.1
git checkout OpenSSL_1_0_2-stable
./config
make -j4
sudo make install
cd ..


echo "== [ 7/10] Downloading and building libxkbcommon (for Fcitx Qt plugin)"
git clone https://github.com/xkbcommon/libxkbcommon.git
cd libxkbcommon
./autogen.sh --disable-x11
make -j4
sudo make install
cd ..

echo "== [ 8/10] Downloading and building Qt 5.6.2"
git clone git://code.qt.io/qt/qt5.git qt5_6_2
cd qt5_6_2
git checkout 5.6

# I already spent an hour trying to fix the stupid init-repository.
# I gave up and I'm just echoing a Python script that does the job.
cat << EOF > fixinit.py
from shutil import copyfile

copyfile('init-repository', 'init-repository.old')
with open('init-repository') as f:
    lines = [l for l in f]

lines.insert(533, '        \$url =~ s/\/qt5\/..//;\n')
with open('init-repository', 'w') as f:
    f.write(''.join(lines))
EOF
python3 fixinit.py
rm fixinit.py

perl init-repository --module-subset=qtbase,qtimageformats -f
git checkout v5.6.2
cd qtimageformats && git checkout v5.6.2 && cd ..
cd qtbase && git checkout v5.6.2 && cd ..
cd qtbase && git apply ../../../tdesktop/Telegram/Patches/qtbase_5_6_2.diff && cd ..

./configure -prefix "/usr/local/tdesktop/Qt-5.6.2" -release -force-debug-info -opensource -confirm-license -qt-zlib -qt-libpng -qt-libjpeg -qt-freetype -qt-harfbuzz -qt-pcre -qt-xcb -qt-xkbcommon-x11 -no-opengl -no-gtkstyle -static -openssl-linked -nomake examples -nomake tests
make -j4
sudo make install
cd ..

echo "== [ 9/10] Downloading and building Google Breakpad"
git clone https://chromium.googlesource.com/breakpad/breakpad
git clone https://chromium.googlesource.com/linux-syscall-support breakpad/src/third_party/lss
cd breakpad
./configure --prefix=$PWD
make -j4
make install
cd ..

echo "== [10/10] Downloading and building GYP and CMake"
git clone https://chromium.googlesource.com/external/gyp
wget https://cmake.org/files/v3.6/cmake-3.6.2.tar.gz
tar xf cmake-3.6.2.tar.gz
rm cmake-3.6.2.tar.gz
cd gyp
git apply ../../tdesktop/Telegram/Patches/gyp.diff
cd ../cmake-3.6.2
./configure
make -j4
cd ..

echo "== Building Telegram"
cd ../tdesktop/Telegram
gyp/refresh.sh

# https://github.com/telegramdesktop/tdesktop/issues/2961#issuecomment-280071728
echo "Fixing issue #2961"
for libname in libicutu.a libicui18n.a libicuuc.a libicudata.a; do
    sudo ln -s /usr/lib/x86_64-linux-gnu/${libname} /usr/lib/${libname}
done

echo "OK, this should do"
echo "Go to ~/TBuild/tdesktop/out/Debug and run `make -j4`."
echo "Go to ~/TBuild/tdesktop/out/Release and run `make -j4`."
echo ""
echo "To debug from Qt Creator, open `CMakeLists.txt` at out/Debug"

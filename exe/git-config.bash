#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly MYDRIVE=$(df -P "${MYREALDIR}" | awk 'END {print $1}')

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	echo git vim | "${MYREALDIR}"/apt-ensure.bash git vim-nox task-spooler
	if which nano &>/dev/null; then
		sudo "${MYREALDIR}"/apt.bash purge -y nano
	fi
fi
readonly G_ROOT=$("${MYREALDIR}"/git-root.bash)

if [[ "${FORCEPUSH}" != 'true' ]]; then
	git fetch
fi

pushd "${G_ROOT}"
	git config --global --add safe.directory "${G_ROOT}"
	git config ${*} credential.helper store
	git config ${*} user.email "you@example.com"
	git config ${*} user.name "Your Name"
	git config ${*} push.followTags true
	git config pull.rebase false
	if [[ "$OSTYPE" == "linux-gnu"* ]] && "${MYREALDIR}"/get-ro-fss.sh | grep -- "${MYDRIVE}"; then
		echo "${MYDRIVE}" is read-only
	else
		readonly BRANCH=$("${MYREALDIR}"/git-branch.bash)
		git branch --set-upstream-to=origin/"${BRANCH}" || git push --set-upstream origin "${BRANCH}"
		git config push.followTags true
		git config credential.helper store
		#tsp "${MYREALDIR}"/git-rehash-staged.bash
		"${MYREALDIR}"/git-rehash-staged.bash
		tsp -f true
		if [[ "${FORCEPUSH}" != 'true' ]]; then
			git fetch
			git pull --allow-unrelated-histories
		fi
		tsp "${MYREALDIR}"/ltd.bash git prune
	fi
popd

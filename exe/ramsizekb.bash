#!/bin/bash 

set -e

grep MemTotal /proc/meminfo | awk '{print $2}'

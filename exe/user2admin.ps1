# Prompt for the old and new user's SID
$oldUserSID = Read-Host "Enter the SID of the old user (e.g., S-1-5-21-<YourOldSID>)"
$newUserSID = Read-Host "Enter the SID of the new user (e.g., S-1-5-21-<YourNewSID>)"

# Validate SID format
if (-not ($oldUserSID -match "^S-\d+(-\d+)+$")) {
    Write-Host "Invalid SID format for the old user. Exiting script."
    exit
}

if (-not ($newUserSID -match "^S-\d+(-\d+)+$")) {
    Write-Host "Invalid SID format for the new user. Exiting script."
    exit
}

Write-Host "Old user SID: $oldUserSID"
Write-Host "New user SID: $newUserSID"

# Define source and destination registry keys
$sourceProfile = "HKU\$oldUserSID\Software"  # The registry hive of the old user
$destinationProfile = "HKU\$newUserSID\Software"  # The registry hive of the new user

# Create a temporary folder for storing registry exports and backups
$TempFolder = "C:\Temp"
if (-not (Test-Path $TempFolder)) {
    New-Item -ItemType Directory -Path $TempFolder
}

# Function to export and import registry settings (ALL current user settings)
function Migrate-RegistrySettings {
    Write-Host "Migrating registry settings for all current user settings..."

    # Get the current user's registry hive paths under Software
    $registryPaths = @(
        "Microsoft\Windows\CurrentVersion\Explorer",      # Explorer settings (taskbar, desktop, etc.)
        "Microsoft\Windows\CurrentVersion\Themes",        # Theme settings (backgrounds, fonts)
        "Microsoft\Windows\CurrentVersion\Internet Settings",  # Internet explorer and settings
        "Control Panel\International"                     # Language and regional settings
    )

    foreach ($path in $registryPaths) {
        $sourceKey = "$sourceProfile\$path"
        $destinationKey = "$destinationProfile\$path"

        # Export the registry settings from the old profile
        Write-Host "Exporting registry key: $sourceKey"
        reg export $sourceKey "$TempFolder\RegistryBackup.reg" /y

        # Import the registry settings to the new profile
        Write-Host "Importing registry key to $destinationKey"
        reg import "$TempFolder\RegistryBackup.reg"

        # Clean up temporary file
        Remove-Item "$TempFolder\RegistryBackup.reg"
    }
}

# Function to migrate desktop background (Wallpaper)
function Migrate-DesktopBackground {
    Write-Host "Migrating desktop background settings..."

    # Define paths for wallpaper in both profiles
    $sourceWallpaper = "C:\Users\$oldUser\AppData\Roaming\Microsoft\Windows\Themes\TranscodedWallpaper"
    $destinationWallpaper = "C:\Users\$newUser\AppData\Roaming\Microsoft\Windows\Themes\TranscodedWallpaper"

    if (Test-Path $sourceWallpaper) {
        Write-Host "Copying desktop wallpaper..."
        Copy-Item $sourceWallpaper -Destination $destinationWallpaper -Force
    }
}

# Function to migrate AppData settings (this ensures we move app-specific settings)
function Create-JunctionForAppData {
    Write-Host "Creating junction for AppData to preserve app settings..."

    # Define source and destination AppData paths
    $sourceAppData = "C:\Users\$oldUser\AppData"
    $destinationAppData = "C:\Users\$newUser\AppData"

    if (-not (Test-Path $destinationAppData)) {
        New-Item -ItemType Junction -Path $destinationAppData -Target $sourceAppData
    }
}

# Function to migrate all necessary Current User (HKCU) settings
function Migrate-HKCU {
    Write-Host "Migrating all settings from HKEY_CURRENT_USER..."

    # Path to current user registry settings for the old profile
    $sourceHKCU = "HKU\$oldUserSID\Software\Microsoft\Windows\CurrentVersion\Explorer"
    $destinationHKCU = "HKU\$newUserSID\Software\Microsoft\Windows\CurrentVersion\Explorer"

    # Export all Current User settings (Explorer, Taskbar, etc.)
    Write-Host "Exporting registry key: $sourceHKCU"
    reg export $sourceHKCU "$TempFolder\CurrentUserSettings.reg" /y

    # Import the settings to the new profile
    Write-Host "Importing registry key to $destinationHKCU"
    reg import "$TempFolder\CurrentUserSettings.reg"

    # Clean up temporary file
    Remove-Item "$TempFolder\CurrentUserSettings.reg"
}

# Function to migrate all important system settings like language and region
function Migrate-LanguageAndRegionSettings {
    Write-Host "Migrating language and regional settings..."

    $sourceLanguageKey = "HKU\$oldUserSID\Control Panel\International"
    $destinationLanguageKey = "HKU\$newUserSID\Control Panel\International"

    # Export the region and language settings from the old profile
    reg export $sourceLanguageKey "$TempFolder\LanguageSettings.reg" /y

    # Import the settings to the new profile
    reg import "$TempFolder\LanguageSettings.reg"

    # Clean up temporary file
    Remove-Item "$TempFolder\LanguageSettings.reg"
}

# Function to ensure new user profile permissions
function Adjust-Permissions {
    Write-Host "Adjusting permissions for the new user profile..."

    # Set the appropriate permissions for the new user profile
    $acl = Get-Acl "C:\Users\$newUser"
    $acl.SetAccessRuleProtection($true, $false) # Disable inheritance
    Set-Acl "C:\Users\$newUser" $acl
}

# Run all migration steps
Migrate-RegistrySettings
Migrate-HKCU
Migrate-LanguageAndRegionSettings
Migrate-DesktopBackground
Create-JunctionForAppData
Adjust-Permissions

Write-Host "Profile migration completed successfully. Please restart the system to apply all changes."

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly USERNAME=$1
readonly NEWUID=$2
readonly OLDUID=${3:-$(id -u "${USERNAME}")}
readonly GROUPNAME=${4:-${USERNAME}}
readonly NEW_GID=${5:-${NEWUID}}
readonly OLD_GID=${6:-${OLDUID}}

cp -pb /etc/passwd /etc/group /var/backups/
usermod -u ${NEWUID} ${USERNAME}
groupmod -g ${NEW_GID} ${GROUPNAME}
find / -gid "${OLD_GID}" -not -path '*/.zfs/*' -exec chgrp -vh "${GROUPNAME}" {} \+ &
find / -uid "${OLDUID}" -not -path '*/.zfs/*' -exec chown -vh "${USERNAME}" {} \+

wait

#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
set -o pipefail

readonly F="${*:-'.'}"

# readonly DIR=${1:-'.'}
# readonly MYDRIVE=$(df -P "${DIR}" | awk 'END {print $1}')
# readonly MYMOUNTP=$(mount | grep "${MYDRIVE}" | cut -f3 -d' ')
# echo "${MYMOUNTP}"

function get_mounted() {
	if [ -L "${*}" ]; then
		local path=$(dirname "${*}")
	else
		local path="${*}"
	fi
	(df --output=target "${path}" || return $?) | (tail -n +2 || return $?)
}
function check_in_fstab() {
	awk "\$2==\"${*}\" {print \$2}" /etc/fstab
}

function get_from_fstab() {
	local path=$(realpath "${*}")
	local dev=$(check_in_fstab "${path}")
	while [[ "${dev}" == '' ]]; do
		path=$(dirname ${path})
		local dev=$(check_in_fstab "${path}")
	done
	realpath "${dev}"
}

[ -e "${F}" ] || exit 1
get_mounted "${F}" || get_from_fstab "${F}"

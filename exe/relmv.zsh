#!/usr/bin/env zsh

[ ${IGNORE} ] || set -e

if $DEBUG && [ ${DEBUG} ]; then
	set -x
fi

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
readonly JOBS=${JOBS:-$("${MYREALDIR}"/cpus.sh)}

source "${MYREALDIR}"/../defaults.rc.bash

readonly SOURCE_RELPATH="${1}"
shift
readonly TARG_PATH=$(realpath "${*}")

if [[ "${REMOVE_SAME}" == '' ]]; then # deleting on the source those files which are present the same target
	readonly REMOVE_SAME=true
fi
if [[ "${DELIVER}" == '' ]]; then # deliver to the target the files present on source and not present on target
	readonly DELIVER=true
fi
if [[ "${BACKUPDIR}" == '' ]]; then
	readonly BACKUPDIR=/var/backups/
fi

function maybe_remove() {
	if [ $REMOVE_SAME ]; then
		if $DEBUG && [ $DEBUG ]; then
			tsp "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/dlt.bash -vf "${*}"
		else
			tsp "${MYREALDIR}"/ltd.bash rm -vf "${*}"
		fi
	fi
}

function process_relpaths() {
	local relpath
	local real
	local targpath
	while read relpath; do
		if echo "${relpath}" | grep '\*'; then
			echo "Asterisk detected in relpath: ${relpath}"
			exit 4
		fi
		if echo "${@}" | grep '\*'; then
			echo "Asterisk detected in target"
			exit 5
		fi
		if [[ "${relpath}" == '.' ]]; then
			targpath="${@}"
		else
			targpath="${@}/${relpath}"
		fi
		echo Rel path: "${relpath}" → Target path: "${targpath}"
		if [[ $(basename $(dirname "${targpath}")) == $(basename "${targpath}") ]] || ([ -d "${targpath}" -o -L "${targpath}" ] && [[ $(basename $(realpath "${targpath}/..")) == $(basename $(realpath "${targpath}")) ]]) 2>&1; then
			exit 2 # DEBUG
		fi
		if [ -d "${relpath}" ]; then
			if [ -d "${targpath}" ] || ($DELIVER && [ ${DELIVER} ]); then
				"${MYREALDIR}"/ltd.bash "${MYREALDIR}"/ensure-path.bash "${targpath}"
				pushd "${relpath}"
					if "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/dirls.bash . | grep '\*'; then
						echo "Asterisk detected in listing"
						exit 3
					fi
					#"${MYREALDIR}"/ltd.bash "${MYREALDIR}"/dirls.bash . | process_relpaths "${targpath}"
					"${MYREALDIR}"/ltd.bash "${MYREALDIR}"/dirls.bash . | process_relpaths "${targpath}"
				popd
				if $REMOVE_SAME && [ ${REMOVE_SAME} ]; then
					real=$(realpath "${relpath}")
					echo "Checking if real path ${real} is empty"
				       	if echo "${real}" | "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/dirempty.bash; then
						echo "real path ${real} is empty, removing"
					       	tsp rmdir -v "${real}"
					else
						echo "real path ${real} isn't empty"
					fi
				fi
			elif [ -e "${targpath}" ]; then
				echo "${relpath}" is a dir and "${targpath}" is not
			else
				echo Skipping: "${relpath}" is a dir "${targpath}" does not exist
			fi
		elif [ -e "${targpath}" ]; then
			if [ ! -e "${relpath}" ]; then
				echo "${relpath}" does not exist
			elif [[ $(realpath "${relpath}") == $(realpath "${targpath}") ]]; then
				echo "${relpath}" IS the "${targpath}"
				maybe_remove "${relpath}" &
			elif [ -L "${relpath}" ] && [ -L "${targpath}" ] && [[ $(readlink "${relpath}") == $(readlink "${targpath}") ]]; then
				echo "${relpath}" and "${targpath}" are symlinks with same target
				maybe_remove "${relpath}" &
			elif [[ $("${MYREALDIR}"/ltd.bash file --brief --mime "${relpath}") != $("${MYREALDIR}"/ltd.bash file --brief --mime "${targpath}") ]]; then
				 echo "${relpath}" and "${targpath}" are of different type
			else
				if [[ "" = $("${MYREALDIR}"/ltd.bash diff -dq "${relpath}" "${targpath}" | ifne head -n1) ]]; then
					echo "${relpath}" and "${targpath}" have no diff
					maybe_remove "${relpath}" &
				elif [[ $("${MYREALDIR}"/ltd.bash crc32 "${relpath}") == $("${MYREALDIR}"/ltd.bash crc32 "${targpath}") ]]; then
					echo "${relpath}" and "${targpath}" have same checksum
					maybe_remove "${relpath}" &
				else
					echo "${relpath}" and "${targpath}" differ
				fi
			fi
		elif [ -e "${relpath}" ]; then
			echo "${relpath}" exists and "${targpath}" does not exist
			if $DELIVER && [ ${DELIVER} ]; then
				local targparent=$(dirname "${targpath}")/
				"${MYREALDIR}"/ensure-path.bash "${targparent}"
				tsp chown --reference=$(dirname "${relpath}") "${targparent}" -c
				tsp chmod --reference=$(dirname "${relpath}") "${targparent}" -c
				if ! [ $REMOVE_SAME ] || ! "${MYREALDIR}"/ltd.bash mv -v "${relpath}" "${targparent}"; then
					tsp "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/focopy.bash -a "${relpath}" "${targparent}"
				fi
			fi
			maybe_remove "${relpath}" &

		else
			echo "Both ${relpath} and ${targpath} do not exist"
		fi
	done <&0
}

echo ifne crc32 zsh tsp | "${MYREALDIR}"/apt-ensure.bash moreutils libarchive-zip-perl zsh task-spooler

tsp -S "${JOBS}"

if $DELIVER && [ $DELIVER ] && (( $JOBS > 1 )); then
	tsp "${MYREALDIR}"/ltd.bash "${MYREALDIR}"/rsync.bash --backup-dir="${BACKUPDIR}" --ignore-existing --remove-source-files --relative="${SOURCE_RELPATH}" "${SOURCE_RELPATH}" "${TARG_PATH}" #2>&1 | grep -v 'No such file or directory' )
fi

echo "${SOURCE_RELPATH}" | process_relpaths "${TARG_PATH}"

wait
tsp -f true

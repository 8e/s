#!/bin/bash 

set -e

readonly ME=$(realpath $(which ${0}))
readonly MYREALDIR="$(dirname ${ME})"
$DEBUG && set -x

source "${MYREALDIR}"/../defaults.rc.bash

pip install $(cat "${MYREALDIR}"/../lib/pips.list)

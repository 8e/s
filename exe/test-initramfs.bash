#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))
readonly DUMMY=/tmp/qemu-kvm.$$

"${MYREALDIR}"/apt-ensure.bash qemu-kvm qemu-utils
"${MYREALDIR}"/fillfile.bash "${DUMMY}" 1M


kvm -kernel ${1} -initrd ${2} -hda "${DUMMY}"

rm -rvf "${DUMMY}"


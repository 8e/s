#!/usr/bin/env bash

set -o pipefail

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly JOBS="${CPUS}"
declare -a Cmd=(ifne xargs --no-run-if-empty --max-procs=${JOBS})

(which xargs ifne | "${MYREALDIR}"/apt-ensure.bash coreutils moreutils) >/dev/null

[ ${#} -eq 0 ] && set -- ls

while [ ${#} -gt 0 ]; do
	case ${1} in

		'-d')
			shift # past param
			if [[ 'space' == "${1}" ]]; then
				readonly DELIM=' '
			else
				readonly DELIM="${1}"
			fi
			shift
			Cmd+=(-d"${DELIM}")
		;;

		'-0')
			shift # past param
			readonly DELIM=0
			Cmd+=(-0)
		;;

		\-*)
			Cmd+=(${1})
			shift
		;;

		*)
			break
		;;
	esac
done

[[ "${DELIM}" == '' ]] && Cmd+=(-d '\n')

ifne tail -n +0 | "${Cmd[@]}" $RUNNER "${@}" || true

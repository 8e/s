#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-true}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

"${MYREALDIR}"/ensure-path.bash /etc/dracut.conf.d
cp -r "${MYREALDIR}"/../lib/dracut.conf.d/* /etc/dracut.conf.d/

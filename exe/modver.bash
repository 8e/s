#!/bin/bash 

set -e

if [ ${#} -gt 1 ]; then 
	for modfile in ${*}; do
		echo -ne "${modfile}\t"
		${0} ${modfile}
	done
else
	modinfo ${1} | awk '/^version/ {print $NF}'
fi

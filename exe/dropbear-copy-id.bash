#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

if [[ "$2" == '' ]]; then
	if [ -e "$HOME/.ssh/id_ecdsa.pub" ]; then
		readonly KEY="$HOME/.ssh/id_ecdsa.pub"
	elif [ -e "$HOME/.ssh/id_dropbear_ecdsa.pub" ]; then
		readonly KEY="$HOME/.ssh/id_dropbear_ecdsa.pub"
	else
		exit 1
	fi
else
	readonly KEY="$2"
fi


cat "${KEY}" | dbclient "${1}" '[ -d ~/.ssh -o -L ~/.ssh ] || mkdir -pv ~/.ssh && cat - >> ~/.ssh/authorized_keys && chmod 0700 ~/.ssh && chmod 0600 ~/.ssh/*'

#dbclient -i "${KEY}" "${1}" true
#ssh -i "${KEY}" "${1}" true

#!/bin/bash 

set -e
readonly MYREALDIR=$(dirname $(realpath ${0}))
declare -a SelectedOpts=()
declare -a Modules=()
declare -a AwkFilters=()

(which systools | "${MYREALDIR}"/apt-ensure.bash sysfsutils) > /dev/null

function allopts() {
	systool -vm ${*}
}

function join() {
	printf "%s" "${*}"
}


readonly MYDIR=$(dirname $(realpath ${0}))

while [ ${#} -gt 0 ]; do
	case ${1} in
		-o)
			shift # past option
			SelectedOpts+=(${1})
			AwkFilters+=("/${1}/ {print \$(NF-1)}")
			shift # past value
		;;
		*)
			Modules+=${1}
			shift # past modname
		;;
	esac
done

if [ ${#AwkFilters} -eq 0 ]; then
	allopts ${Modules}
else
	allopts ${Modules} |  awk -vFS=\" "$(join ${AwkFilters[*]})"
fi

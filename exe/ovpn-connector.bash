#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}"
	source ../defaults.rc.bash

	which openvpn-connector-setup || ./apt-ensure.bash python3-openvpn-connector-setup

	# Run openvpn-connector-setup to install ovpn profile and connect to VPN.
	# You will be asked to enter setup token. You can get setup token from Linux
	# Connector configuration page in OpenVPN Cloud Portal
	sudo openvpn-connector-setup

popd

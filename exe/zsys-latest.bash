#!/usr/bin/env bash

set -e

readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

"${MYREALDIR}"/apt-ensure-cmd.bash zsys zsysd

sudo systemctl restart zsys
sudo systemctl status zsys

zsysctl machine show | awk -vFS='[ )(\t]' 'f {print $9" "$10; f=0 } /History/ { f=1 }'

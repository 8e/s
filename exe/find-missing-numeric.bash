#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly WORD="${1}"
shift
readonly DIR_PATH="${*:-.}"

# Set the pattern for the filename
readonly FILENAME_PATTERN=".*${WORD}[0-9]+\..+"
readonly FILENAME_REMATCH_PATTERN=".*${WORD}([0-9]+)\..+"

prev_number=0
find "$DIR_PATH" -type f -regex ".*${FILENAME_PATTERN}" | while read file; do
	if [[ "$(basename "$file")" =~ $FILENAME_REMATCH_PATTERN ]]; then
		echo "${BASH_REMATCH[1]}"
	else
		echo "${file}" is very interesting
		exit 1
	fi
done | sort -V | while read number; do
	if (( 10#$number - 10#$prev_number > 1 )); then
		echo $(( prev_number + 1 ))
		exit 0
	fi
	prev_number=$number
done

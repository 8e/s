#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly DAEMON_CONF_PATH=/etc/systemd/system/monero.service
readonly PIDFILE=/var/run/monero.pid
readonly CONFDIR=/home/"${DEFUSER}"/.config/monero
readonly CONFF="${CONFDIR}"/bitmonero.conf
readonly DLOGF="/tmp/log/${DEFUSER}/monerod.log"
readonly WAL=${1:-$(read -e -p WAL)}
readonly COMMANDLINE=/bin/monerod --restricted-rpc --non-interactive --bg-mining-enable --block-sync-size 3 --confirm-external-bind --config-file="${CONFF}" --pidfile "${PIDFILE}" --log-file="${DLOGF}" --start-mining ${WAL} --check-updates=update --detach --mining-threads $("${MYREALDIR}/cpus.sh") --bg-mining-min-idle-interval 5

"${MYREALDIR}"/apt-ensure.bash monero

if [ ! -e "${CONFF}" ]; then
	"${MYREALDIR}"/ensure-path.bash "${CONFDIR}"
	touch "${CONFF}"
fi

echo -n "[Unit]
Description=Monero Full Node
After=network.target caches.service network-online.target
Requires=caches.service network.target network-online.target
StartLimitIntervalSec=0
ConditionACPower=true

[Service]
Type=idle
User="${DEFUSER}"
Group="${DEFUSER}"
Nice=19
IOSchedulingClass=idle
IOSchedulingPriority=7
# https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html
#CPUQuota=3%
# A higher weight means more CPU time, a lower weight means less. The allowed range is 1 to 10000
#CPUWeight=1
StartupCPUWeight=1
PIDFile="${PIDFILE}"
ExecStart=${COMMANDLINE}
# --bg-mining-miner-target 75
Restart=always
RestartSec=40
WorkingDirectory=/tmp/caches/"${DEFUSER}"

# Specify the throttling limit on memory usage of the executed processes in this unit. Memory usage may go above the limit if unavoidable, but the processes are heavily slowed down and memory is taken away aggressively in such cases. This is the main mechanism to control memory usage of a unit.
# Takes a memory size in bytes. If the value is suffixed with K, M, G or T, the specified memory size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively. Alternatively, a percentage value may be specified, which is taken relative to the installed physical memory on the system. This setting is supported only if the unified control group hierarchy is used and disables MemoryLimit=.
#MemoryHigh=5%

# Specify the absolute limit on swap usage of the executed processes in this unit.
# Takes a swap size in bytes. If the value is suffixed with K, M, G or T, the specified swap size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively.
MemorySwapMax=1K
# Hardening                                                                      
SystemCallArchitectures=native                                                   
MemoryDenyWriteExecute=true                                                      
NoNewPrivileges=true
SystemCallFilter=~@resources:EPERM

[Install]
WantedBy=multi-user.target
WantedBy=default.target
WantedBy=graphical.target" | sudo "${MYREALDIR}"/ensure-contents.bash "${DAEMON_CONF_PATH}"

systemctl daemon-reload
systemctl enable monero.service

"${MYREALDIR}"/fw-allow.bash 18080
"${MYREALDIR}"/fw-allow.bash 18081

systemctl restart monero.service || ${COMMANDLINE}
systemctl status monero.service

netstat -tulpn |grep 'mone\|1808'

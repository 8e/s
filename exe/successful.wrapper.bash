#!/usr/bin/env bash

set -x

readonly ME="${0}"
readonly MEBASE="${ME%.*}"
readonly EXT="${ME##*.}"
readonly MYDIR="$(dirname $(realpath ${ME}))"
readonly ORIGPLACE="${MYDIR}"/"${MEBASE}".orig."${EXT}"

"${ORIGPLACE}" ${*} || true

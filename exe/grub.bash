#!/usr/bin/env bash

set -e

readonly MYREALDIR=$(dirname $(realpath ${0}))
readonly DISK=${*:-"/dev/disk/by-id/$(ls -1 /dev/disk/by-id |grep -v part | ${MYREALDIR}/dialog.bash 'Select disk for GRUB:')"}
readonly TMPDR=/tmp/$(basename "${0}").$$

pushd "${MYREALDIR}"

	which mkdosfs || ./apt.bash install -y dosfstools

#	if mount | grep '/boot/efi'; then
#		umount /boot/efi
#	fi

	if mount | grep '/boot/grub'; then
		umount /boot/grub
	else
		./ensure-path.bash /boot/grub
	fi

	#mkdosfs -F 32 -s 1 -n EFI ${DISK}-part1

	./grub-config.bash

	if [ -e /boot/efi ]; then
		mv -v /boot/efi "${TMPDR}"/
		mkdir /boot/efi
		mount /boot/efi
		[ -e  "${TMPDR}"/efi ] && rsync -svahHPxXlit --remove-source-files "${TMPDR}"/efi /boot/
	else
		mkdir /boot/efi
		mount /boot/efi
	fi
	./ensure-path.bash /boot/efi/grub

	grep '/boot/grub' /etc/fstab || echo /boot/efi/grub /boot/grub none defaults,bind 0 0 >> /etc/fstab

	if [ -e /boot/grub ]; then
		mv -v /boot/grub "${TMPDR}"/
		mkdir /boot/grub
		mount /boot/grub
		[ -e "${TMPDR}"/grub ] && rsync -svahHPxXlit --remove-source-files "${TMPDR}"/grub /boot/
	else
		mkdir /boot/grub
		mount /boot/grub
	fi

	./apt.bash reinstall $(cat ../lib/grubboot.list)
	./grub-repack.bash

	update-initramfs -c -k all

	grub-install "${DISK}" 

	grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=ubuntu --recheck --no-floppy

	# For a mirror or raidz topology:

	# systemctl mask grub-initrd-fallback.service

	./grub-config.bash

popd

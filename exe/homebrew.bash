#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname ${0})
source "${MYREALDIR}"/../defaults.rc.bash

readonly DWNLD_LINK="https://raw.githubusercontent.com/Homebrew/install/master/install.sh"
readonly TARGET_F="/tmp/homebrew_install.sh"

pushd "${MYDIR}"
	wget -c "${DWNLD_LINK}" -O "${TARGET_F}"
	chmod +x "${TARGET_F}"
	"${TARGET_F}" || if [[ $("${TARGET_F}") == "Don't run this as root!" ]]; then
		chown "${DEFUSER}":"${U}" "${TARGET_F}"
		su - "${DEFUSER}" -c "${TARGET_F}"
		su - "${DEFUSER}" -c "/home/linuxbrew/.linuxbrew/bin/brew shellenv >> ~${DEFUSER}/.zprofile"
		su - "${DEFUSER}" -c "brew install gcc"
	fi
	/home/linuxbrew/.linuxbrew/bin/brew shellenv >> ~/.zprofile
	/home/linuxbrew/.linuxbrew/bin/brew shellenv >> /etc/environment
	brew install gcc
	# brew install homebrew/cask/evernote # macOS only
popd

#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

readonly PACKAGE_NAME=$1
shift
readonly CMD="$*"

sudo which "$CMD" >/dev/null || "${MYREALDIR}"/apt-ensure.bash "${PACKAGE_NAME}"

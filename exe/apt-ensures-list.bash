#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

function exclude() {
	egrep -v 'ufwf|systools|status|[)(/#"&>|\$]|which|^-|[[:space:]]|packages|ensure|cryptsetup|miner|cut|echo|geoiplookup|ifne|inotifywait|mdadm|openvpn-connector-setup' | while read line; do
		which "${line}" &>/dev/null || echo "${line}"
	done
}

grep -RI 'apt.*ensure.*' "${MYREALDIR}"/*.bash | cut -d':' -f2 | bash sort-u-splinter-inplace.bash | exclude

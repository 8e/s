#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

set -o pipefail

readonly LEVEL=${LEVEL:-86} # %du
readonly PORTION=1
readonly P_SEMAPHORENAME=$(basename "${0}").${USER}
readonly TMPDIR_COMMON="$("${MYREALDIR}"/ensure-path.bash /tmp/${P_SEMAPHORENAME})"
readonly TMPDIR_MINE="${TMPDIR_COMMON}/$$"
readonly LOGF="${TMPDIR_MINE}"/$$.log
readonly FINDINGSF="${TMPDIR_MINE}"/findings.list
readonly DELLOGF="${TMPDIR_COMMON}"/del.log
readonly DIRSCACHE="${TMPDIR_COMMON}/dirs"
readonly JOBS=$(( ${CPUS} * 2 - 1 ))
readonly ZFS_LAST_SEARCH='images\|backup\|archive\|delete'
declare -A Excludes
Excludes["${TMPDIR_COMMON}/.*"]='regex'
Excludes["${TMPDIR_MINE}/.*"]='regex'
Excludes["${TMPDIR_COMMON}"]='path'
Excludes["${TMPDIR_MINE}"]='path'
Excludes["/var/tmp"]='path'
Excludes["/var/tmp/.*"]='regex'
Excludes["/tmp"]='path'
Excludes["/tmp/.*"]='regex'
Excludes["${MYREALDIR}"]='path'
Excludes["${MYREALDIR}/.*"]='regex'
Excludes["/etc/systemd"]='path'
Excludes["/etc/systemd/.*"]='regex'
Excludes["/mnt"]='path'
Excludes["/mnt/.*"]='regex'
Excludes["/media"]='path'
Excludes["/media/.*"]='regex'
Excludes["/var/cache/debconf/.*"]='regex'
Excludes["/var/cache/debconf"]='path'
Excludes["/var/cache/apt/.*.bin"]='regex'
Excludes["/proc/.*"]='regex'
Excludes["/proc"]='path'
Excludes["/dev/.*"]='regex'
Excludes["/dev"]='path'
Excludes["/sys/.*"]='regex'
Excludes["/sys"]='path'
Excludes["/run/.*"]='regex'
Excludes["/run"]='path'
Excludes["/var/run/.*"]='regex'
Excludes["/var/run"]='path'
Excludes["*/qBittorrent/BT_backup*"]='path'
Excludes[".*/qBittorrent/BT_backup.*"]='regex'
Excludes["."]='name'
Excludes[".."]='name'
Excludes['.*/var/lib/jenkins/workspace/core\(/.*\)?']='regex'
declare -a ExclFindOptions=()
readonly EXCLPATTERNSF="${TMPDIR_COMMON}/exclude.list"
for find_optvalue in "${!Excludes[@]}"; do
	find_optname=${Excludes[${find_optvalue}]}
	ExclFindOptions+=(-not -${find_optname} "${find_optvalue}")
	if [[ "${find_optname}" == 'regex' ]]; then
		echo "${find_optvalue}"
	else
		echo "^${find_optvalue}$"
	fi
done > "${EXCLPATTERNSF}"

readonly RSYNC_EXCLUDE_PATTERNS_FILE="${TMPDIR_COMMON}/rsync.exclude.list"
readonly RSYNCD_CONF_FILE=/etc/rsyncd.conf.d/$(basename "${ME}").inc
readonly RSYNCD_CONF_CONTENTS="[global]
exclude from = ${RSYNC_EXCLUDE_PATTERNS_FILE}
"
function filt() {
	ifne grep -vf "${EXCLPATTERNSF}" || true
}

function du_above_threshold() {
	tsp sync
	tsp sudo sync
	tsp zpool sync
	tsp sudo zpool sync
	bg_wait
	echo $("${MYREALDIR}"/duperc.bash "${FindRoots[@]}")
	exit 600 norec
	if $(( $("${MYREALDIR}"/duperc.bash "${FindRoots[@]}") > LEVEL )); then
		[[ "$*" != 'norec' ]] && iterate_backup_datasets norec
		(( $("${MYREALDIR}"/duperc.bash "${FindRoots[@]}") > LEVEL ))
	else
		finish
	fi
}

function isroot() {
	return ${UID}
}

function xargs_every() {
	ifne xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" -I{} "${MYREALDIR}"/bliss.sh "${@}" || true
}

function xargs_all() {
	ifne xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" "${MYREALDIR}"/bliss.sh "${@}" || true
}

function process_dirscache() {
	local d
	du_above_threshold
	if [ -s "${DIRSCACHE}" ]; then
		(cat "${DIRSCACHE}" | ifne "${MYREALDIR}"/dedup.bash -) &
		sort -u "${DIRSCACHE}" | while read d; do
			process_one_dir "${d}"
		done
		bg_wait
		tsp rm -v "${DIRSCACHE}"
	fi
}

function myfind() {
	find -H "${@}" -depth -mount -true "${ExclFindOptions[@]}"
}

function process_one_dir() {
	local d=$(realpath "${*}")
	du_above_threshold
	tsp "${MYREALDIR}/dedup.bash" "${d}"
	tsp "${MYREALDIR}/delebroken.bash" "${d}"
	find_empty_delete "${d}"
	possible_git_parent "${d}"
	[ -d "${d}" ] && myfind "${d}" -mindepth 1 | "${ME}" -
}

function clean_portion_sorted_by() {
	local by="${*}"
	"${MYREALDIR}"/filesorem.py3 "${by}" "${PORTION}" "${LEVEL}"
}

function eventually_clean_files_list() {
	local input_f="${TMPDIR_MINE}"/input.list
	local output_f="${TMPDIR_MINE}"/output.list
	ifne cat > "${input_f}"
	while [  -s "${input_f}" ]; do

		clean_portion_sorted_by biggest < "${input_f}" | clean_portion_sorted_by oldest | clean_portion_sorted_by smallest | clean_portion_sorted_by newest | clean_portion_sorted_by deepest > "${output_f}"

		mv "${output_f}" "${input_f}"

		[ -s "${DIRSCACHE}" ] && process_dirscache

		bg_wait

		iterate_backup_datasets

	done
	(rm -v "${input_f}" 2>&1 |"${MYREALDIR}"/entee.bash -na "${LOGF}" || true) &
}

function zfs_get_backup_datasets() {
	zfs list -t snapshot -H -o name
	(zfs list -H -o name | grep 'images\|backup\|delete' || true)
}

function zfs_delete_the_oldest_backup_dataset() {
	[[ "$*" != 'norec' ]] && du_above_threshold
	for dataset2destroy in $(zfs_get_backup_datasets | xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" zfs list -H -s creation -o name); do
		[[ "${dataset2destroy}" == '' ]] && break
		zfs_destroy "${dataset2destroy}"
		break
	done
}

function zfs_delete_the_biggest_backup_dataset() {
	[[ "$*" != 'norec' ]] && du_above_threshold
	for dataset2destroy in $(zfs_get_backup_datasets | xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" zfs list -H -s used -o name | sort -r); do
		[[ "${dataset2destroy}" == '' ]] && break
		zfs_destroy "${dataset2destroy}"
		break
	done
}

function zfs_delete_the_smallest_backup_dataset() {
	[[ "$*" != 'norec' ]] && du_above_threshold
	for dataset2destroy in $(zfs_get_backup_datasets | xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" zfs list -H -s used -o name); do
		[[ "${dataset2destroy}" == '' ]] && break
		zfs_destroy "${dataset2destroy}"
		break
	done
}

function find_and_list() {
	if ! myfind "${@}" 2> >("${MYREALDIR}"/entee.bash "${errlogf}" | ifne "${MYREALDIR}"/entee.bash -na "${FINDINGSF}"); then
		errcode=$?
		cat "${errlogf}"
		exit ${errcode}
	fi
	cat "${FINDINGSF}"
	rm "${FINDINGSF}" &>/dev/null &
}

function find_empty_delete() {
	tsp "${MYREALDIR}"/delempty.bash "${@}" "${ExclFindOptions[@]}" -mount
}

function DelFromDicts() {
	local Options=()
	local isfirst=true
	local errlogf="${TMPDIR_MINE}/find.error.log"
	local find_optvalue
	local FindCmd=()

	du_above_threshold

	for find_optvalue in "${!SearchOpts[@]}"; do
		echo "find opt value: ${find_optvalue}" | "${MYREALDIR}"/entee.bash -na "${LOGF}"
		find_optname=${SearchOpts[${find_optvalue}]}
		if ${isfirst}; then
			isfirst=false
		else
			Options+=(-or)
		fi
		Options+=(-${find_optname} "${find_optvalue}")
	done

	du_above_threshold

	if [ -s "${FINDINGSF}" ]; then
		cat "${FINDINGSF}" | (xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" -I{} find -H {} "${ExclFindOptions[@]}" -delete -mount -true || true) | "${MYREALDIR}"/entee.bash -na "${DELLOGF}"
		du_above_threshold
	fi

	find_and_list "${FindRoots[@]}" "${Options[@]}" | eventually_clean_files_list

}

function mypushd() {
	local newd="${@}"
	if [[ "${parent}" != '.' ]]; then
		pushd "${newd}" | "${MYREALDIR}"/entee.bash -na "${LOGF}"
	fi
}

function mypopd() {
	if [ ${#DIRSTACK[@]} -gt 1 ]; then
		popd | "${MYREALDIR}"/entee.bash -na "${LOGF}"
	fi
}

function possible_git_parent() {
	parentd="${@}"
	mypushd "${@}"
		if [ -d /.git ]; then
			du_above_threshold
			git prune
			du_above_threshold
			git gc --aggressive --prune=all # remove the old files
			tsp git push
			du_above_threshold
			tsp "${MYREALDIR}"/bfg.bash -B 1 .
			tsp git push --force
		fi
	mypopd
}

function process_path {
	local f=$(realpath "${*}")
	du_above_threshold
	if [ -d "${f}" ]; then
		process_one_dir "${f}"
		echo ${f} | "${MYREALDIR}"/entee.bash -na "${DIRSCACHE}"
	elif [ -e "${f}" ]; then
		(rm -v "${*}" | "${MYREALDIR}"/entee.bash -a "${DELLOGF}") &
	elif [[ ${1} == [:digit:]+ ]]; then
		shift
		(( ${1} > ${PORTION} )) && tsp "${ME}" zfsdatasets
		process_path "${*}"
	fi
}

function iterate_backup_datasets() {
	[[ "$*" != 'norec' ]] && du_above_threshold norec
	isroot && which zfs && if [[ $(zfs_get_backup_datasets | wc -l) != '0' ]]; then
		zfs_delete_the_oldest_backup_dataset $*
		zfs_delete_the_biggest_backup_dataset $*
		zfs_delete_the_smallest_backup_dataset $*
		echo "Snapshots: $(zfs list -t snapshot | wc -l)"
		"${MYREALDIR}"/z-checkpointrotate.bash nmpool
	fi | "${MYREALDIR}"/entee.bash -a "${LOGF}"
}

function zfs_destroy() {
	${DEBUG} && echo destroy "${@}"
	tsp zfs destroy "${@}"
}

function bg_wait() {
	#"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" wait
	tsp -f true
	wait
}

function finish_me() {
	"${DEBUG}" || (rm -r "${TMPDIR_MINE}" || true)
}

function finish() {
	bg_wait
	"${DEBUG}" || (rm -r "${TMPDIR_COMMON}" || true)
	exit 0
}

trap finish_me EXIT

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	(echo "ifne tsp" | "${MYREALDIR}"/apt-ensure.bash moreutils task-spooler) | "${MYREALDIR}"/entee.bash -na "${LOGF}"
fi
"${MYREALDIR}"/ensure-path.bash "${TMPDIR_MINE}" "${TMPDIR_COMMON}"  | "${MYREALDIR}"/entee.bash -na "${LOGF}"

mypushd "${TMPDIR_MINE}"

	date +%F_%T |"${MYREALDIR}"/entee.bash -na "${LOGF}"
	echo "pwd: $(pwd)" | "${MYREALDIR}"/entee.bash -na "${LOGF}"

	case "${1}" in
		
		'-')
			eventually_clean_files_list
		exit
		;;

		create|finish|kill|stop|wait)
			#"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" "${@}"
			tsp -S "${JOBS}"
		exit
		;;


		'myfind')
			shift
			myfind "${@}"
		exit
		;;

		# don't use with -delete! there's a bug it deletes everything
		# use | xargs.bash -l rm -rvf  instead
		'find')
			shift
			#"${ME}" findopts | xargs --no-run-if-empty -l --max-procs=${JOBS}  -H "${@}" -true | ifne sort -u | filt
			"${ME}" findopts | xargs "${ME}" myfind
		exit
		;;

		'findopts')
			shift
			awk -vFS='[]|[]' '/SearchOpts\[.*=/ {gsub("['\''=]","",$3); print "-"$3,$2}' "${ME}" | ifne sort -u | filt
		exit
		;;

		'patterns')
			shift
			awk -vFS='[]|[]' "/SearchOpts\[.*=/ {print \"${*}\"\$2}" "${ME}" | grep -v "'" | ifne sort -u | filt
		exit
		;;

		'ipatterns')
			shift
			# https://superuser.com/questions/256751/make-rsync-case-insensitive
			# https://forums.gentoo.org/viewtopic-t-1060394-start-0.html
			"${ME}" patterns |  perl -pe "s/([[:alpha:]])/[\U\$1\E\$1]/g; s/^/${*}/g;"
		exit
		;;

		'unison')
			shift
			"${ME}" patterns |  awk '{ print "ignore = Name", $1 }'
		exit
		;;

		'docker')
			shift
			docker builder prune --force
			docker container ls -a --filter status=exited --filter status=created && docker container prune --force
			docker image prune -a --force
			docker system prune --force

		exit
		;;

		'git-parent')
			shift
			possible_git_parent "${*:-.}"
		exit
		;;

		
		'gits')
			shift
			declare -a FindRoots=("${@}")
			du_above_threshold
			myfind "${FindRoots[@]}" -ipath '*/.git' -type d "${ExclFindOptions[@]}" -execdir tsp "${ME}" git-parent .. \;
		exit
		;;

		'rsync-exclude')
			shift
			which rsync | "${MYREALDIR}"/apt-ensure.bash rsync
			"${ME}" patterns | sort -u | sed 's/\*/\*\*/g' | nice -19 ionice -c3 rsync --open-noatime -prune-empty-dirs --ignore-errors --prune-empty-dirs --itemize-changes --inplace --backup-dir=/var/backups/ --exclude-from=- "${@}"
		exit
		;;

		'rsync-insensitive-exclude')
			shift
			which rsync | "${MYREALDIR}"/apt-ensure.bash rsync
			# https://superuser.com/questions/256751/make-rsync-case-insensitive
			# https://forums.gentoo.org/viewtopic-t-1060394-start-0.html
			("${ME}" patterns && "${ME}" ipatterns) | sort -u | sed 's/\*/\*\*/g' | nice -19 ionice -c3 rsync --open-noatime --fuzzy --prune-empty-dirs --ignore-errors --prune-empty-dirs --itemize-changes --inplace --backup-dir=/var/backups/ --exclude-from=- "${@}"
		exit
		;;

		'zfsdatasets')
			isroot && fs list -t snapshot -o name -H | grep '.+pool/delete' | "${MYREALDIR}"/xblissargs.bash -l tsp zfs destroy
			isroot && tsp "${MYREALDIR}"/zdestroycongrusnaps.bash
			iterate_backup_datasets
		exit
		;;

		'snapshots-reduce')
			zrepl zfs-abstraction release-stale
			du_above_threshold
			zrepl zfs-abstraction release-all
			if which zfs; then 
				for ds in $(zfs list -H -o name); do
					du_above_threshold
					"${MYREALDIR}"/zc-fs.bash receive -A "${ds}"
				done
				for i in `seq 1 $(( $(zfs list -t snapshot -H | wc -l) / 4 ))`; do
					du_above_threshold
					"${ME}" zfsdatasets
				done
			fi
		exit
		;;

		'')
			"${ME}" ipatterns > "${RSYNC_EXCLUDE_PATTERNS_FILE}" &

			"${MYREALDIR}"/ensure-path.bash $(dirname "${RSYNCD_CONF_FILE}")

			echo -n "${RSYNCD_CONF_CONTENTS}" | sudo "${MYREALDIR}"/ensure-contents.bash "${RSYNCD_CONF_FILE}"
			tsp sudo systemctl daemon-reload

			true # go on
		;;

		'old-versions')
			"${MYREALDIR}"/list-old-versions.bash "${FindRoots[@]}" "${ExclFindOptions[@]}" # | "${ME}" -

		exit
		;;

		'clean')
			shift
			process_path "${*}"
		exit
		;;

		*)
			declare -a FindRoots=("${@}")
		;;

	esac

	if [ ${#FindRoots[@]} -eq 0 ]; then
		declare -a FindRoots=($("${MYREALDIR}"/dirls.bash / | filt))
	fi

	du_above_threshold

	declare -A SearchOpts
	SearchOpts['efs*.tmp']='iname'
	SearchOpts['.wget-hsts']='iname'
	SearchOpts['apt.systemd.daily']='name'
	SearchOpts['*/ChatExport_*/*.html']='ipath'
	SearchOpts['*/ChatExport_*/images']='ipath'
	SearchOpts['*/ChatExport_*/images/*']='ipath'
	SearchOpts['*/ChatExport_*/css']='ipath'
	SearchOpts['*/ChatExport_*/css/*']='ipath'
	SearchOpts['*/ChatExport_*/js']='ipath'
	SearchOpts['*/ChatExport_*/js/*']='ipath'
	SearchOpts['*.DS_Store']='iname'
	SearchOpts['thumbs.db']='iname'
	SearchOpts['.thumbnails']='iname'
	SearchOpts["*RECYCLE.BIN"]='iname'
	SearchOpts['ViberDownloads']='iname'
	SearchOpts['RecentDocuments']='iname'
	SearchOpts['temp']='iname'
	SearchOpts['emoji']='iname'
	SearchOpts[".Trash*"]='iname'
	SearchOpts["trash"]='iname'
	SearchOpts["logfiles"]='iname'
	SearchOpts['Настраиваемые*шаблоны*Office']='iname'
	DelFromDicts

	du_above_threshold
	apt -y autoremove | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	apt -y autoclean | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	which cleanmgr && cleanmgr /autoclean | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	tsp "${MYREALDIR}"/apt-clean.bash
	which brew && brew cleanup
	for i in /var/log/mail.* ; do
		echo "" > $i
	done | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	which docker && docker builder prune --force | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	which docker && docker container ls -a --filter status=exited --filter status=created && docker container prune --force | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	#which docker && docker image prune -f -a --filter "until=48h" --force | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	which docker && docker image prune -a --force | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	which docker && docker system prune --force | "${MYREALDIR}"/entee.bash -a "${LOGF}"

	du_above_threshold


	declare -A SearchOpts
	SearchOpts[".*dump"]='iname'
	SearchOpts["dead.letter"]='iname'
	SearchOpts["ViberDownloads"]='iname'
	SearchOpts["*/inetpub/temp"]='ipath'
	SearchOpts["*/ADtemp"]='ipath'
	SearchOpts["Downloaded*Program*Files/*"]='iname'
	SearchOpts["*/Local/Temp/*"]='ipath'
	SearchOpts["*/My*Skype*Received*Files/*"]='ipath'
	SearchOpts["*/.dropbox.cache/*"]='ipath'
	SearchOpts["*/files/temp/*"]='ipath'
	SearchOpts["fb_temp"]='iname'
	SearchOpts["*/log/*"]='ipath'
	SearchOpts[".RecycleBin*"]='iname'
	SearchOpts['.$recycle_bin$*']='iname'
	SearchOpts["thumbnail_cache"]='iname'
	SearchOpts["musiccache"]='iname'
	SearchOpts["albumthumbs"]='iname'
	SearchOpts['RECYCLE.BIN']='iname'
	SearchOpts['thumbnail.db*']='iname'
	SearchOpts["*/var/lib/jenkins/.m2/repository/*"]='ipath'
	SearchOpts['*/*.bfg-report*']='ipath'
	SearchOpts['*.bfg-report']='iname'
	SearchOpts['desktop.ini']='iname'
	SearchOpts['.directory']='iname'
	DelFromDicts

	which cleanmgr && cleanmgr /autoclean /lowdisk | "${MYREALDIR}"/entee.bash -a "${LOGF}"

	du_above_threshold

	find_and_list '/var/lib/jenkins/workspace' -mindepth 1 -type f | eventually_clean_files_list
	du_above_threshold

	which cleanmgr && cleanmgr /autoclean /verylowdisk | "${MYREALDIR}"/entee.bash -a "${LOGF}"

	declare -A SearchOpts
	SearchOpts['*RECYCLE*']='iname'
	SearchOpts['Thumbnails']='iname'
	SearchOpts['Temporary']='iname'
	SearchOpts["*/Dropbox/My*"]='ipath'
	SearchOpts["*/Dropbox/Мои*"]='ipath'
	SearchOpts['LogFiles']='name'
	SearchOpts['logs']='iname'
	SearchOpts['Crash Reports']='iname'
	SearchOpts['Crashpad']='iname'
	SearchOpts['crashes']='iname'
	SearchOpts['crash_count.txt']='iname'
	SearchOpts['Code Cache']='iname'
	SearchOpts["themes_backup"]='iname'
	SearchOpts["crash_count.txt"]='iname'
	SearchOpts["*.backup"]='iname'
	SearchOpts["*/.backup/*"]='ipath'
	SearchOpts["*/backup/*"]='ipath'
	SearchOpts["*/backups/*"]='ipath'
	SearchOpts['*.bak']='iname'
	SearchOpts['*.log']='iname'
	SearchOpts['*.tmp']='iname'
	SearchOpts['*.*~']='iname'
	SearchOpts['nohup.out']='iname'
	SearchOpts["*/audio/music*/Spotify*"]='ipath'
	SearchOpts["*/var/cache/apt/archives/*"]='ipath'
	SearchOpts["*/soft/linux/deb/*"]='ipath'
	SearchOpts['LOG']='name'
	SearchOpts['*.old']='iname'
	SearchOpts['*.out']='iname'
	SearchOpts["*/Dropbox/My*"]='ipath'
	SearchOpts["*nixnote*"]='ipath'
	SearchOpts["*/.local/Trash*"]='ipath'
	SearchOpts[".zfs"]='name'
	DelFromDicts
	du_above_threshold

	tsp "${ME}" gits "${FindRoots[@]}"

	declare -A SearchOpts
	SearchOpts["*/*backup*"]='ipath'
	SearchOpts["*backup*"]='ipath'
	SearchOpts["*/*temp/*"]='ipath'
	SearchOpts["*/*tmp/*"]='ipath'
	SearchOpts['*.cue']='iname'
	SearchOpts['*/var/log/*']='ipath'
	SearchOpts['*/files/Download/*']='ipath'
	SearchOpts['*/var/mail/*']='ipath'
	SearchOpts['.*dump']='iname'
	SearchOpts['*/.stversions*']='ipath'
	SearchOpts['*/.zfs']='name'
	SearchOpts["*cache"]='iname'
	SearchOpts['lost+found']='iname'
	SearchOpts['*changelog*']='iname'
	SearchOpts["*/.cache/*"]='ipath'
	SearchOpts["**thumbnail*"]='ipath'
	SearchOpts["*cache2"]='iname'
	SearchOpts["*.save"]='iname'
	SearchOpts["HuaweiBackup"]='iname'
	SearchOpts["IntsigLog"]='iname'
	SearchOpts["*/IntsigLog/*"]='ipath'
	SearchOpts[".bz2"]='name'
	SearchOpts["temp_data*"]='name'
	SearchOpts["*_cache.*"]='iname'
	SearchOpts["*.tmb.*"]='iname'
	DelFromDicts
	du_above_threshold

	du_above_threshold
	declare -A SearchOpts
	SearchOpts["Service.*Worker"]='iname'
	SearchOpts["*Cache*"]='iname'
	SearchOpts[".sudo_as_admin_successful"]='iname'
	SearchOpts["*.parts"]='iname'
	SearchOpts["*_thumb.*"]='iname'
	SearchOpts['*конфликтующая копия*']='iname'
	SearchOpts['*конфликт синхронизации*']='iname'
	SearchOpts['*conflicting copy*']='iname'
	SearchOpts['*sync*conflict*']='iname'
	SearchOpts['*/*.sync*conflict-*']='ipath'
	SearchOpts['*.torrent']='ipath'
	SearchOpts['* (\?)']='iname'
	SearchOpts['"']='iname'
	SearchOpts['*/var/lib/flatpak*']='ipath'
	SearchOpts['*/share/flatpack*']='ipath'
	SearchOpts['*/*backup*/*']='ipath'
	SearchOpts['&']='name'
	SearchOpts['&&']='name'
	DelFromDicts
	du_above_threshold

	"${MYREALDIR}"/list-old-versions.bash "${FindRoots[@]}" "${ExclFindOptions[@]}" | "${ME}" -

	isroot && which zfs && while [[ $(zfs_get_backup_datasets | wc -l) != '0' ]]; do
		du_above_threshold
	done | "${MYREALDIR}"/entee.bash -a "${LOGF}"

	if which zfs; then for ds in $(zfs list -H -o name); do
		du_above_threshold
		tsp zrepl zfs-abstraction release-stale
		du_above_threshold
		tsp zrepl zfs-abstraction release-all
		du_above_threshold
		tsp zfs recv -A "${ds}"
	done; fi

	isroot && which zfs && while [[ $(zfs_get_backup_datasets | wc -l) != '0' ]]; do
		du_above_threshold
	done | "${MYREALDIR}"/entee.bash -a "${LOGF}"

	tsp "${MYREALDIR}"/rmdir-samename-subdirs.bash "${FindRoots[@]}"
	"${MYREALDIR}"/find-broken-symlinks.sh "${FindRoots[@]}" | eventually_clean_files_list

	declare -A SearchOpts
	SearchOpts["*.lock"]='iname'
	SearchOpts["*.dpkg-old"]='iname'
	SearchOpts["*/cache/*"]='ipath'
	SearchOpts['*/archive/*']='ipath'
	SearchOpts['*/apps/*']='ipath'
	SearchOpts['*/soft/*']='ipath'
	SearchOpts['*/man/*']='ipath'
	SearchOpts['*/hard/*']='ipath'
	SearchOpts['*/Dropbox/*']='path'
	SearchOpts['*/var/lib/jenkins/workspace/*']='path'
	SearchOpts['*/var/cache/*']='path'
	DelFromDicts
	du_above_threshold

	isroot && which zfs && zfs list -H -o name | grep "${ZFS_LAST_SEARCH}"
	isroot && which zfs; do
		du_above_threshold
		for dataset2destroy in $(zfs list -H -o name | grep "${ZFS_LAST_SEARCH}" | xargs --no-run-if-empty -l --max-procs=${JOBS} -d "\n" zfs list -H -s creation -o name); do
			[[ "${dataset2destroy}" == '' ]] && break
			zfs_destroy "${dataset2destroy}"
			break
		done
		du_above_threshold
		for dataset2destroy in $(zfs list -H -o name | grep "${ZFS_LAST_SEARCH}" | xargs --no-run-if-empty -l --max-procs=${JOBS}  -d "\n" zfs list -H -s used -o name | sort -r); do
			[[ "${dataset2destroy}" == '' ]] && break
			zfs_destroy "${dataset2destroy}"
			break
		done
		du_above_threshold
		for dataset2destroy in $(zfs list -H -o name | grep "${ZFS_LAST_SEARCH}" | xargs --no-run-if-empty -l --max-procs=${JOBS}  -d "\n" zfs list -H -s used -o name); do
			[[ "${dataset2destroy}" == '' ]] && break
			zfs_destroy "${dataset2destroy}"
			break
		done
	done

	if which zfs; then
		"${MYREALDIR}"/ztrim.bash -w
	fi

mypopd

tsp rm -rv "${TMPDIR_MINE}"

tsp rm -rv "${TMPDIR_COMMON}"

tsp updatedb

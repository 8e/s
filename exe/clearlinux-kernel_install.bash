#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

pushd /tmp
	wget -c https://cdn.download.clearlinux.org/releases/33030/clear/x86_64/os/Packages/gcc7-7.5.0-440.x86_64.rpm
	sudo aptitude install alien git curl flex bison libssl-dev
	sudo alien gcc7-7.5.0-440.x86_64.rpm
	sudo dpkg -i gcc7_7.5.0-441_amd64.deb
	gcc7 -v
	mkdir ~/patched_source
	git clone https://github.com/clearlinux-pkgs/linux ~/clearlinux_patches
	cd ~/clearlinux_patches
	git clone https://github.com/clearlinux-pkgs/linux ~/clearlinux_patches
	cd ~/clearlinux_patches
	./scripts/develop.sh linux.spec ~/patched_source
	git config --global user.email "you@example.com"
	./scripts/develop.sh linux.spec ~/patched_source
	cd /home/android/patched_source/linux*
	CC=/the/clearlinux/gcc make -j4 bindeb-pkg
	sudo dpkg -i ../*.deb
	"${MYREALDIR}"/zbm_update.bash
popd

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure.bash build-essential git libdbus-1-dev libx11-dev libxext-dev libxss-dev libssl-dev libgconf2-dev libgtk-3-dev libxkbcommon-dev libfontconfig1-dev libcairo2-dev libfreetype6-dev libharfbuzz-dev libicu-dev libjpeg-dev libpng-dev libtiff5-dev libwebp-dev libglib2.0-dev libdbus-glib-1-dev libpulse-dev libudev-dev libevent-dev libappindicator3-dev libasound2-dev libsystemd-dev libreadline-dev
# libgnome-keyring-dev
# libnm-glib-dev 

pushd $("${MYREALDIR}"/git-installrepo.bash https://github.com/telegramdesktop/tdesktop.git)
	./Telegram/Tools/linux/prepare-deps.sh
	qmake
	# Build Telegram Desktop client from source
	make -j$(nproc)

	# Run Telegram Desktop client
	./Telegram/Telegram
popd


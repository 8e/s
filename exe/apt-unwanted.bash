#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-true}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

pushd "${MYREALDIR}"/..

	bash exe/addsuspli.bash lib/purge.list "${@}"

	for package in "${@}"; do

		tsp sudo "${MYREALDIR}"/cupt.bash -y markauto $package
		tsp sudo aptitude -o DPkg::Lock::Timeout=-1 --full-resolver --allow-new-upgrades --allow-new-installs -y markauto $package
		tsp sudo dpkg --force all --purge $package
		
		for f in $(find lib layers -name notessential); do 
			grep -w "${package}" "${f}" || sed -i "s/^Package: \(.*\)/Package: \1 ${package}/g;" "${f}"
			git add "${f}"
		done

		grep -w "${package}" /etc/apt/preferences.d/notessential || sudo sed -i.bak "s|^Package: \(.*\)|Package: \1 ${package}|g" /etc/apt/preferences.d/notessential

	done


popd

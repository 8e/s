#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

set -o pipefail

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

# https://serverfault.com/questions/401437/how-to-retrieve-the-last-modification-date-of-all-files-in-a-git-repository
git fetch
TZ=UTC git --no-pager log -1 --date=iso-local '--format=%ai' "${@}" || echo ERROR
#git ls-tree -r --name-only "${@:-HEAD}" -z | TZ=UTC xargs -0 -I{} git --no-pager log -1 --date=iso-local --format="%ad {}" -- {}

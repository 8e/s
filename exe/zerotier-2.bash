#!/usr/bin/env bash

set -e

readonly MYREALDIR=$(dirname $(realpath ${0}))
readonly FHOSTS=/etc/dotdee/etc/hosts.d/75-zerotier

"${MYREALDIR}"/zerotier-list.bash | sudo tee "${FHOSTS}"

sudo dotdee -u /etc/hosts

"${MYREALDIR}"/knownhosts-renew.bash $(cat "${FHOSTS}")

"${MYREALDIR}"/fw-allow.bash $(cut "${FHOSTS}" -f1)

#!/usr/bin/env bash

set -e
if [ ${DEBUG} ]; then 
	        set -x
fi      
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

FORCEPUSH=true "${MYREALDIR}"/gits.bash -m ${*:-'fi'}

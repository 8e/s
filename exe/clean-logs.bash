#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
	source ../defaults.rc.bash
popd >/dev/null

tsp sudo journalctl --flush --rotate --vacuum-time=1s
tsp sudo journalctl --user --flush --rotate --vacuum-time=1s

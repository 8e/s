#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

readonly TARGET_ROOT=${1:-/mnt/z}
if [ ${#} -gt 1 ]; then
	shift
fi
readonly CMD=${CMD:-sudo cp -lvfa} #could be rsync -svauchHPXlitA --open-noatime

declare -a Opts=()
declare -a Packages=()

function filt() {
	ifne grep -v 'live\|casper' | ifne grep -vf "${MYREALDIR}"/../lib/purge.list
}

function copy_one_file() {
	set -e
	readonly DEBUG=${DEBUG:-false}
	${DEBUG} && set -x

	local s="${*}"
	local sparent=$(dirname "${s}")
	local targetf="${TARGET_ROOT}${s}"
	local tparentd=$(dirname "${targetf}")

	if [[ "${s}" == '' ]]; then
		echo "Empty source file name"
		exit 1
	fi
	if [ -e "${s}" ] && [ ! -e "${targetf}" ]; then
		"${MYREALDIR}"/ensure-path.bash "${tparentd}"
<<<<<<< HEADd
		sudo chmod --reference="${sparent}" "${tparentd}"
		if [ -d  "${s}" ]; then
			"${MYREALDIR}"/ensure-path.bash "${targetf}"
			sudo chmod --reference="${s}" "${targetf}"
		else
			${CMD} "${Opts[@]}" "${s}" "${targetf}"
			sudo chmod --reference="${s}" "${targetf}"
=======d
		sudo chmod --reference="${sparent}" "${tparentd}" &
		if [ -d  "${s}" ]; then
			"${MYREALDIR}"/ensure-path.bash "${targetf}"
			sudo chmod --reference="${s}" "${targetf}" &
		else
			${CMD} "${Opts[@]}" "${s}" "${targetf}"
			sudo chmod --reference="${s}" "${targetf}" &
>>>>>>>da2d47e51be76c5cae055a978a2e816140ab31a4
		fi
	fi
}

source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure.bash moreutils

while [ ${#} -gt 0 ]; do

	case ${1} in

                \-*)
                        Opts+=("${1}")
                ;;

                *)
			Packages+=("${1}")
                ;;

        esac

	shift

done

if [[ ${#Packages} == 0 ]]; then
	Packages=$(dpkg-query -f '${binary:Package}\n' -W | filt)
#else
#	Packages=$(printf '%s\n' "${Packages[@]}" | grep 
fi

if [[ ${#Opts} == 0 ]]; then
	Opts=(-lva)
fi

echo "Requested packages count: ${#Packages}"
#echo "Options:"
#printf '%s\n' "${Opts[@]}"
export -f copy_one_file
export MYREALDIR
export CMD
export TARGET_ROOT

i=1
for package in ${Packages[@]}; do

	echo -e "Package: ${package}\t($i/${#Packages})"

	( dpkg -L "${package}" || true ) | ifne sort -u | xargs -I{} -d'\n' --max-procs=$("${MYREALDIR}"/./cpus.sh) --no-run-if-empty bash -c "copy_one_file '{}'"

	(( i++ ))

#	for f in $(dpkg -L "${package}" || true); do
#		copy_one_file "${f}"
#	done

done


#!/usr/bin/env bash

set -x
[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

# Replace this with the path to your zvol device
readonly ZVOL_NAME="${1}"
readonly ZVOL_DEVICE=/dev/zvol/"${ZVOL_NAME}"

# Get the zvol blocksize in bytes
readonly ZVOL_BLOCKSIZE=$(zfs get -H -o value volblocksize "${ZVOL_NAME}")

# Convert the blocksize to bytes
readonly ZVOL_BLOCKSIZE_BYTES=$(echo ${ZVOL_BLOCKSIZE%K} | awk '{print $1 * 1024}')

# Convert the blocksize to a human-readable format
#readonly ZVOL_BLOCKSIZE_HUMAN=$(echo ${ZVOL_BLOCKSIZE} | numfmt --to=iec)
"${MYREALDIR}"/apt-ensure-cmd.bash bc bc
readonly ZVOL_BLOCKSIZE_HUMAN=$(echo "scale=0; ${ZVOL_BLOCKSIZE_BYTES}/1024" | bc)K

# If zvol blocksize is greater than 4096 bytes, set OCFS2 blocksize to 4KB
if (( ${ZVOL_BLOCKSIZE_BYTES} > 4096 )); then
	readonly OCFS2_BLOCKSIZE="4K"
else
	readonly OCFS2_BLOCKSIZE="${ZVOL_BLOCKSIZE_HUMAN}"
fi

# Maximum possible node slots for OCFS2
readonly MAX_NODE_SLOTS=255

# Filesystem label
readonly LABEL="${2:-ocfs2}"

# Create the OCFS2 filesystem
sudo mkfs.ocfs2 -b ${OCFS2_BLOCKSIZE} -C ${ZVOL_BLOCKSIZE_HUMAN} -L ${LABEL} -N ${MAX_NODE_SLOTS} --journal-options size=6M --fs-features=inline-data,sparse,xattr,indexed-dirs,discontig-bg --fs-feature-level=max-features --discard --force --verbose ${ZVOL_DEVICE}
sudo modprobe dlm
sudo modprobe ocfs2_dlmfs
sudo modprobe ocfs2
lsmod | egrep 'dlm|ocfs'

#!/usr/bin/env bash

set -xe

readonly MYREALDIR="$(dirname $(realpath ${0}))"
source "${MYREALDIR}"/../defaults.rc.bash

readonly HOMEPATH=.vim/bundle/Vundle.vim

readonly INSTALLD=$(sudo su "${DEFUSER}" - -c "${MYREALDIR}/git-installrepo.bash https://github.com/benmills/vim-bundle")

ln -svf "${INSTALLD}" "${MYREALDIR}"/../lib/home/

vim +PluginInstall +qall

sudo su "${DEFUSER}" - -c "vim +PluginInstall +qall"


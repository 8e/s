#!/usr/bin/env bash

set -xe

sed -i.bak "s|^PATH=\"\(.*\)\"\$|PATH=\"\1:$(pwd)\"|g"  /etc/environment

export PATH="${PATH}:$(pwd)"

echo "export PATH=${PATH}" | sudo tee -a /etc/profile.d/path /etc/bash.bashrc /etc/zsh/zshrc /etc/zsh/zprofile

#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

find ${*} -type f '(' -iname '*.*r*' -o -iname '*.*z*' ')' -not -iname '*.jpg' -not -iname '*.m*' -not -iname '*.avi' -not -iname '*.wmv' -not -iname '*.rtf' -not -iname '*.srt' -not -iname '*.vcard' -not -iname '*.ac3' -not -iname '*.*sh' -not -iname '*.srm' -not -iname '*.ts' -not -iname '*.ass' -not -iname '*.orf' -not -iname '*.lrcat*' -not -iname '*.DVDRip'

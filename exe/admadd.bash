#!/usr/bin/env bash

set -e

if [[ $DEFUID == '' ]]; then
	read -e -p "Enter default user ID: " DEFUID
fi
if [[ $DEFUID == '' ]]; then
	exit 1
fi

readonly U=${1}
shift

#grep lpadmin /etc/group || addgroup --system lpadmin
#grep lxd /etc/group || addgroup --system lxd

useradd ${U} --non-unique --uid $DEFUID -d /home/${U} -G adm,users,sudo,disk,audio --shell $(which zsh) ${*}

for g in cdrom dip plugdev lpadmin lxd pulse-access vboxusers docker input; do
	if getent group $g; then
		usermod -aG "${g}" "${U}"
	fi
done

passwd "${U}"

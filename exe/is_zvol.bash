#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

readonly BLOCKDEV="${*}"
find /dev/zvol -type l -execdir realpath '{}' \; | xargs -I{} find {} -type b | grep "^$(realpath ${BLOCKDEV})$"

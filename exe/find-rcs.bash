#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}" >/dev/null
        source ../defaults.rc.bash
popd >/dev/null

find /etc /home /root ${*} -type f \( -name '*rc' -o -name '*profile' \) | egrep -iv 'steam|gwenview|octaverc|pipewire|X11|mercurial|xdg|qjack'

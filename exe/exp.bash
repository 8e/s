#!/usr/bin/env bash

set -e
[ ${DEBUG} ] && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure-cmd.bash curl curl

function get_my_ip() {
	for url in "https://ifconfig.me." "https://ipinfo.io/ip"; do
		echo $(curl -sk "${url}")
	done | sort -u
}

readonly IP=$(get_my_ip)

echo "${IP}"

"${MYREALDIR}"/location.bash "${IP}"

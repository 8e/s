#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

[[ "${MODE}" != '' ]] || readonly MODE=${1:-headless} # headless or desktop

function download_sh_install() {
	wget -c https://download.cudo.org/tenants/135790374f46b0107c516a5f5e13069b/5e5f800fdf87209fdf8f9b61441e53a1/linux/x64/stable/install.sh -O cudominer_install.sh
	[ -x cudominer_install.sh ] || chmod +x cudominer_install.sh
	"${MYREALDIR}"/dpkg-loosen.bash cudo-miner-service cudo-miner-${MODE} &
	(./cudominer_install.sh --install-mode=${MODE} --install-cli --update-channel=stable --quiet
	"${MYREALDIR}"/dpkg-loosen.bash cudo-miner-service cudo-miner-${MODE}) &
}

	cudominercli enable
	cudominercli login fccc
	cudominercli info

pushd /opt
	download_sh_install
	"${MYREALDIR}"/apt-ensure.bash cudo-miner-cli cudo-miner-core cudo-miner-${MODE} cudo-miner-service # packages: cudo-miner-headless cudo-miner-desktop cudo-miner-core cudo-miner-cli cudo-miner-service

	echo -n "[Unit]
Description=Cudo Miner
StartLimitIntervalSec=0
ConditionACPower=true

[Service]
Type=simple
EnvironmentFile=-/etc/default/cudo-miner
EnvironmentFile=-/etc/sysconfig/cudo-miner
ExecStart=/usr/local/cudo-miner/cudo-miner-core
Restart=always
WorkingDirectory=/usr/local/cudo-miner
RestartSec=4
User="${DEFUSER}"
Group="${DEFUSER}"
Nice=19
IOSchedulingClass=idle
IOSchedulingPriority=7
# https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html
CPUQuota=3%
# A higher weight means more CPU time, a lower weight means less. The allowed range is 1 to 10000
CPUWeight=1
StartupCPUWeight=1

# Specify the throttling limit on memory usage of the executed processes in this unit. Memory usage may go above the limit if unavoidable, but the processes are heavily slowed down and memory is taken away aggressively in such cases. This is the main mechanism to control memory usage of a unit.
# Takes a memory size in bytes. If the value is suffixed with K, M, G or T, the specified memory size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively. Alternatively, a percentage value may be specified, which is taken relative to the installed physical memory on the system. This setting is supported only if the unified control group hierarchy is used and disables MemoryLimit=.
MemoryHigh=5%

# Specify the absolute limit on swap usage of the executed processes in this unit.
# Takes a swap size in bytes. If the value is suffixed with K, M, G or T, the specified swap size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively.
MemorySwapMax=1K
# Hardening                                                                      
SystemCallArchitectures=native                                                   
MemoryDenyWriteExecute=true                                                      
NoNewPrivileges=true
SystemCallFilter=~@resources:EPERM

[Install]
WantedBy=multi-user.target
WantedBy=default.target
WantedBy=graphical.target" | sudo "${MYREALDIR}"/ensure-contents.bash "${DAEMON_PATH}"
	cudominercli enable
	cudominercli login fccc
	cudominercli info

popd

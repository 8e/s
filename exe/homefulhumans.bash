#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	"${MYREALDIR}"/apt-ensure.bash moreutils >/dev/null
	"${MYREALDIR}"/homefulusers.bash | ifne grep -v syncthing
else
	"${MYREALDIR}"/homefulusers.bash | grep -v syncthing
fi


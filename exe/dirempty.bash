#!/usr/bin/env bash

set -e

if [ ${#} -eq 0 ]; then
	read LINE
else
	readonly LINE="${*}"
fi

readonly REAL=$(realpath "${LINE}")
if [ -d "${REAL}" ]; then
	(( $(ls -A "${LINE}" | wc -l ) == 0 ))
else
	echo "${REAL}" is not a dir >&2
	exit 100
fi

#!/bin/bash

# Define dracut script location
DRACUT_SCRIPT="/usr/bin/dracut"

# Create a backup of the original dracut.sh script
if [ -f "$DRACUT_SCRIPT" ]; then
    if [ ! -f "${DRACUT_SCRIPT}.bak" ]; then
        echo "Creating a backup of the original dracut.sh at ${DRACUT_SCRIPT}.bak"
        cp "$DRACUT_SCRIPT" "${DRACUT_SCRIPT}.bak"
    else
        echo "Backup already exists at ${DRACUT_SCRIPT}.bak"
    fi

    # Apply the patch to modify dracut.sh
    echo "Applying the patch to dracut.sh"
    patch "$DRACUT_SCRIPT" << 'EOF'
--- a/dracut.sh
+++ b/dracut.sh
@@ -996,7 +996,7 @@ export SYSTEMD_IGNORE_CHROOT="1"
 # and the tools need this
 unset TZ

-DRACUT_PATH=${DRACUT_PATH:-/sbin /bin /usr/sbin /usr/bin}
+DRACUT_PATH=${DRACUT_PATH:-/sbin /bin /usr/sbin /usr/bin /usr/local/sbin /usr/local/bin}

 for i in $DRACUT_PATH; do
     rl=$i
     if [ -L "$dracutsysrootdir$i" ]; then
@@ -1006,6 +1006,7 @@ for i in $DRACUT_PATH; do
         NPATH+=":$rl"
     fi
 done
+
 [[ -z $dracutsysrootdir ]] && export PATH="${NPATH#:}"
 unset NPATH
EOF

    echo "Patch applied successfully."

else
    echo "dracut.sh not found at $DRACUT_SCRIPT. Please verify the path."
    exit 1
fi

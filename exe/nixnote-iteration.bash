#!/usr/bin/env bash

set -ex

readonly MYREALDIR="$(dirname $(realpath ${0}))"
readonly LOGF=/tmp/${USER}/nixnote.log
readonly PIDF=/tmp/${USER}/nixnote.pid

function launch_nn() {
	"${MYREALDIR}"/ensure-path.bash $(dirname "${LOGF}")
	date +%F_%T > "${LOGF}"
	nohup nixnote2 --logLevel=2 --startMinimized 2>&1 > "${LOGF}" &
	local pid_nn=${!:-$(pidof nixnote2)}
	#sudo renice -n -19 -p ${pid_nn} >> "${LOGF}"
	echo ${pid_nn} | tee "${PIDF}"
}

#sudo renice -n -20 -p $$ > "${LOGF}"

if grep ERROR "${LOGF}"; then
	if [ -f "${PIDF}" ] && pid_nn=$(cat "${PIDF}") && ps -p ${pid_nn}; then
		kill ${pid_nn}
		sleep 2
		if ps -p ${pid_nn}; then
			kill -KILL ${pid_nn}
		else
			launch_nn
		fi
	elif pkill nixnote2; then
		sleep 2
	else 
		rm -v "${LOGF}" "${PIDF}"
	fi
elif  [ -f "${PIDF}" ] && pid_nn=$(cat "${PIDF}") && ps -p ${pid_nn}; then
	count-files.sh ~/.local/share/nixnote2 | tee -a "${LOGF}"
else
	launch_nn
fi

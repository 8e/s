#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

"${MYREALDIR}"/apt-ensure-cmd.bash ufw ufw
sudo ufw enable
for num in ${*}; do
	sudo ufw delete ${num}
done
"${MYREALDIR}"/fw-status.bash

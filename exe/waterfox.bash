#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

sudo renice -n -10 $$
sudo ionice -c 0 -n 0 -p $$

#/usr/bin/env nice -n -10 ionice -c0 "${MYREALDIR}"/c.bash /usr/lib/waterfox-g/waterfox-g --ProfileManager &
#/usr/bin/env nice -n -10 ionice -c0 "${MYREALDIR}"/c.bash /usr/bin/waterfox-g --ProfileManager &
/usr/bin/env nice -n -10 ionice -c0 "${MYREALDIR}"/c.bash /usr/bin/waterfox-g &
p=$!
sudo renice -n -10 ${p}
sudo ionice -c 0 -n 0 -p ${p}

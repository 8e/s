#!/usr/bin/env bash

set -xe

set -o pipefail

readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly REMOTEME="/tmp/caches/root/$(basename ${ME})"
readonly LOGF="/tmp/caches/root/$(basename ${ME}).log"
readonly OPTSSET='eLwp'
readonly PORT=999

function ontarget() {
	if [[ "${1}" == "${REMOTEME}" ]]; then
		rsync --open-noatime -svauchHP "${ME}" "${MYREALDIR}"/apt-ensure.bash "${MYREALDIR}"/apt-is-installed.bash "${MYREALDIR}"/apt.bash "${MYREALDIR}"/ensure-path.bash "${MYREALDIR}"/combine-chars.py3 "${MYREALDIR}"/bywordlength.awk "${HOST}":"$(dirname ${REMOTEME})" >> "${LOGF}"
	fi
	[[ "${2}" == 'screen ' ]] && kill $$
	echo "ssh \"${HOST}\" ${*}" >> "${LOGF}"
	ssh "${HOST}" ${*}
}

function spawn_receiver() {
	local path="${1}"
	if [[ "${path}" == '' ]]; then
	       kill $$
	elif [[ "${path}" == '/' ]]; then
		ontarget "${REMOTEME}" screen "${TARGETPATH}" 'receive'
	else
		
		ontarget "${REMOTEME}" screen "${TARGETPATH}/${path}" 'receive'
	fi
}

function send() {
	local dataset="${1}"
	local what="${2}"
	local prevsb="${3}"
	[[ "${dataset}" == '' ]] && kill $$
	[[ "${what}" == '' ]] && kill $$

	local dstype=$(zfs list "${what}" -H -o type)
	case "${dstype}" in
		'bookmark'|'snapshot')
			ontarget zfs list "${TARGETPATH}"/"${what}" && return 0
		;;
		'filesystem'|'volume')
			sendopts "${dataset}" -vD --saved "${what}" && return 0
		;;
		*)
			echo "Unknown dadaset type: ${dstype}"
			return 1
		;;
	esac

	if [[ "${prevsb}" == '' ]]; then
		sendopts "${dataset}" -DveLwpR "${what}" && return 0
		sendopts "${dataset}" -DveLwp "${what}" && return 0
	else
		sendopts "${dataset}" -DveLwpR "${what}" -I "${prevsb}" && return 0
		sendopts "${dataset}" -DveLwpR "${what}" -i "${prevsb}" && return 0
		sendopts "${dataset}" -DveLwp "${what}" -I "${prevsb}" && return 0
		sendopts "${dataset}" -DveLwp "${what}" -i "${prevsb}" && return 0
	fi
	try_all_opts "${dataset}" "${what}" "${prevsb}" || true
}

function sendopts() {
	local dataset=${1}
	[[ "${dataset}" == '' ]] && kill $$
	shift
	spawn_receiver "${dataset}"
	echo "zfs send ${*} | ifne pv -petIraT | ifne nc ${HOST} ${PORT} | ifne tee -a \"${LOGF}\"" >> "${LOGF}"
	zfs send ${*} | ifne pv -petIraT | ifne nc ${HOST} ${PORT} | ifne tee -a "${LOGF}"
}

function snapsandbooks() {
	local dataset=${1}
	[[ "${dataset}" == '' ]] && kill $$
	if [[ "${2}" == '' ]]; then
		zfs list -t bookmark "${dataset}" -s creation -o name -H 2>/dev/null
		zfs list -t snapshot "${dataset}" -s creation -o name -H 2>/dev/null
	else
		readonly TARGETPATH="${2}"
		zfs list -t bookmark "${TARGETPATH}/${dataset}" -s creation -o name -H 2>/dev/null | ifne sed "s|^${TARGETPATH}/||g"
		zfs list -t snapshot "${TARGETPATH}/${dataset}" -s creation -o name -H 2>/dev/null | ifne sed "s|^${TARGETPATH}/||g"
	fi
}

function try_all_opts() {
	local dataset=${1}
	local what=${2:-${ds}}
	local prevsb=${3}
	local length=$(( ${#OPTSSET} -1 ))
	[[ "${what}" == '' ]] && kill $$

	for opts in $("${MYREALDIR}"/combine-chars.py3 "${length}" "${OPTSSET}"| "${MYREALDIR}"/bywordlength.awk); do
		if [[ ${prevsb} != '' ]]; then
			sendopts "${dataset}" -"${opts}" -DRv -I ${prevsb} ${what} && return 0
			sendopts "${dataset}" -"${opts}" -DRv -i ${prevsb} ${what} && return 0
			sendopts "${dataset}" -"${opts}" -Dv -I ${prevsb} ${what} && return 0
			sendopts "${dataset}" -"${opts}" -Dv -i ${prevsb} ${what} && return 0
		fi
		sendopts "${dataset}" -"${opts}" -DRvP ${what} && return 0
		sendopts "${dataset}" -"${opts}" -DvP ${what} && return 0
	done

}

function send_sb {
	local dataset=${1}
	local sb=${2}
	local prevsb

	if [[ ${dataset} == '' ]] || [[ ${sb} == '' ]]; then
		kill $$
	fi

	ontarget zfs list "${TARGETPATH}"/"${2}" && return 0

	for prevsb in $(ontarget "${REMOTEME}" sb "${dataset}" "${TARGETPATH}"); do
		[[ ${prevsb} == '' ]] && continue
		send "${dataset}" "${sb}" "${prevsb}" && return 0
	done

	send "${dataset}" "${sb}" && return 0
}

function send_ds() {
	local pool=${1}
	local dataset=${2}
	local prevsb
	local sb

	[[ "${pool}" == '' ]] && kill $$
	[[ "${dataset}" == '' ]] && kill $$

	send "${pool}" "${dataset}"

	for prevsb in $(ontarget "${REMOTEME}" sb "${dataset}" "${TARGETPATH}"); do
		send "${pool}" "${dataset}" "${prevsb}" || true
	done

	for sb in $(snapsandbooks "${dataset}"); do
		send_sb "${dataset}" "${sb}"
	done
}

function send_pool() {
	local pool=${1}
	local dataset
	[[ "${pool}" == '' ]] && kill $$

	send "/" "${pool}"
	for dataset in $(zfs list -H -o name | ifne grep "^${pool}"); do
		send_ds "${pool}" "${dataset}"
	done
}

"${MYREALDIR}"/ensure-path.bash "$(dirname ${LOGF})"
"${MYREALDIR}"/apt-ensure.bash moreutils >/dev/null


if [[ ${1} == 'screen' ]]; then
	shift
	screen -d -m -Logfile "${LOGF}" "${ME}" "${@}" &
elif [[ ${1} == 'sb' ]]; then
	shift
	snapsandbooks ${*}
else
	readonly TARGETPATH="${1}"
	if [[ ${2} == 'receive' ]]; then
		(zfs list "${TARGETPATH}" || zfs create -Ppv "${TARGETPATH}") 2>&1 | ifne tee -a "${LOGF}"
		echo "(lsof -i:\"${PORT}\" | ifne tee -a  \"${LOGF}\") || nc -l ${PORT} | ifne pv -petIraT | ifne zfs receive -dusF \"${TARGETPATH}\"" >> "${LOGF}"
		(lsof -i:"${PORT}" > "${LOGF}") || nc -l ${PORT} | ifne pv -petIraT | ifne zfs receive -dusF "${TARGETPATH}"
	else
		readonly HOST=${2}

		for pool in $(zpool list -H -o name); do
			send_pool "${pool}"
		done

	fi
fi

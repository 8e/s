#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-true}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"

function status() {
	grep -R . /sys/module/zswap || true
	(sudo dmesg | grep -i zswap) || true
	"${MYREALDIR}"/modver.bash zswap || true
	"${MYREALDIR}"/modopts.bash zswap || true
	(sudo lsmod | grep -i zswap) || true

	[[ $(cat /sys/module/zswap/parameters/enabled) == 'Y' ]]

}

status

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash
readonly ARCH=$(dpkg --print-architecture)

(echo awk curl | "${MYREALDIR}"/apt-ensure.bash gawk curl) >/dev/null

#readonly FNAME=$(curl --silent $* | awk -F'"' -vRS='[:,><= /]' "/${ARCH}.deb/ {print \$2}" | sort -uVr | head -1)
readonly FNAME=$(curl --silent $* | grep "${ARCH}.deb" | awk -F'"' '-vRS=[,><= ]' "/${ARCH}.deb/ {print \$2}" | sort -uVr | head -1)
echo "${*%/*}${FNAME}"

#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

## -p: Downloads all files necessary to display the HTML page, including images, CSS, and JavaScript.
#-E: Adjusts file extensions to HTML where appropriate (adds .html to files without extensions).
#-k: Converts links so that they are suitable for offline viewing, i.e., converts absolute links to relative ones.
#-np: Prevents wget from going to parent directories. This keeps the download within the specified directory.
#--cut-dirs=1: Removes the specified number of directory components from the path. Adjust this number based on how the website's URLs are structured.
#-D example.com: Limits downloading to the specified domain (example.com). This ensures that wget only downloads files from this domain and doesn't stray into other domains, even if linked.
# -N timestamping

if (( $# > 1 )); then
	wget -c -N -m -np -k -p -E -e robots=off --wait=1 --random-wait -D "${@}"
else
	wget -c -N -m -np -k -p -E -e robots=off --wait=1 --random-wait "${@}"
fi

#!/usr/bin/env bash

[[ "${POOL}" == '' ]] && (( $(zpool list -H | wc -l) == 1 )) && export POOL=$(zpool list -Ho name)
[[ $OSID == '' ]] && (( $("${MYREALDIR}"/z-osid.bash | wc -l) == 1 )) && export OSID=$("${MYREALDIR}"/z-osid.bash)
if [[ $OSID == '' ]]; then
	read -e -p "Enter OS ID (host id command outputs $(hostid) and /etc/hostid is $(cat /etc/hostid || true): " OSID
fi
if [[ $POOL == '' ]]; then
        read -e -p "Enter pool name: " POOL
fi
export POOL
export OSID

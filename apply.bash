#!/usr/bin/env bash

set -e

[ ${DEBUG} ] && set -x
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/defaults.rc.bash

readonly LOGF=/tmp/bootstrap-layers.log

exec &> >(tee "${LOGF}")

if [ ${#} -eq 0 ] ; then
	Layers=$(ls -1d ${MYREALDIR}/layers/??)
else
	Layers=${*}
fi

which tsp && tsp -K

for layer in ${Layers}; do

	if [ -L "${layer}" ] || [ -d "${layer}" ]; then
		layer_path="${layer}"
	else
		layer_basename="$(basename ${layer})"
		layer_path="${MYREALDIR}"/layers/"${layer_basename}"
	fi
	
	pushd "${layer_path}"
		
		if [ -e "${layer_basename}" ]; then
			"${MYREALDIR}"/exe/focopy.bash "${layer_basename}"/* /
			"${MYREALDIR}"/exe/apt-dedup.bash &>/dev/null &
		fi

		ls -1 *shsource && for shsource in *shsource; do
			echo sourcing "${shsource}"
			source "${shsource}"
		done

		if [ -d apt-cfg -o -L apt-cfg ]; then
			"${MYREALDIR}"/exe/apt-cfg.bash
		fi

		if ls -1 *.list; then
			../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

			declare -a OtherLists=($(../../exe/dirls.bash | grep '.*.list$' | grep -v 'fromurl.*.list'))
			(( ${#OtherLists[*]} > 0 )) && ../../exe/apt-lists.bash -y install ${OtherLists[*]}

			if [ -e fromurl.list ]; then
				../../exe/apt-urls.bash fromurl.list
			fi

		fi

		ls -1 *sh && for script in *sh; do
			echo launching ./"${script}"
			./"${script}"
		done

	popd

done

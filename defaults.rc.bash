# must be silent - no output in this file

if ${DEBUG:-false} && [ ${DEBUG} ]; then
        set -x
fi

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	#open files
	export OPENFILESMAX=1048576
	(( $(cat /proc/sys/fs/file-max) >= ${OPENFILESMAX} )) || sudo sysctl -w fs.file-max="${OPENFILESMAX}" &>/dev/null &
	(( $(ulimit -Hn) >= ${OPENFILESMAX} )) || ulimit -Hn "${OPENFILESMAX}" || true
	(( $(ulimit -Sn) >= ${OPENFILESMAX} )) || ulimit -Sn "${OPENFILESMAX}" || true
	# stack
	export SUGG_STACKSPACE=65536
	export I_SS=$(ulimit -s)
	if (( ${SUGG_STACKSPACE} > ${I_SS} )); then
		ulimit -s ${SUGG_STACKSPACE}
	fi
fi

export PATH="/sbin:/usr/sbin:${PATH}"
[[ "${ME}" != '' ]] && (( $(echo "${ME}" | wc -l) <2 )) || if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	export ME=$( (realpath $(which "${0}" 2>/dev/null) 2>/dev/null || realpath "${0}" 2>/dev/null ) )
else
	export ME=$( realpath "${0}" 2>/dev/null | awk '{print $1; exit 0}' )
fi
[[ "${MYREALDIR}" != '' ]] && (( $(echo "${MYREALDIR}" | wc -l) <2 )) || export MYREALDIR=$( dirname "${ME}" || pwd )

export PATH="${HOME}/.local/bin:${PATH}"
echo "${PATH}" | grep exe &>/dev/null || for p in $(find "${MYREALDIR}"/../.. -ipath '*/exe' -type d 2>/dev/null || echo "${MYREALDIR}"); do
	export PATH="${p}:${PATH}"
done
export DEFUID=1000
export EDITOR=vi
export LOCKPRG=/bin/true # for GNU screen lock sabling
# https://unix.stackexchange.com/questions/17574/is-there-a-maximum-size-to-the-bash-history-file
export HISTCONTROL='erasedups'
export HISTFILESIZE='999999999999999999'
export HISTSIZE='999999999999999999'
readonly PROMPT_COMMAND="${PROMPT_COMMAND:-true}; history -a; history -n" 2>/dev/null
export XAUTHORITY="$HOME/.Xauthority"
export ZT_IFACE=ztklh4dkz5
export CPUS=$(grep -c processor /proc/cpuinfo 2>/dev/null || sysctl -n hw.ncpu)
export DEFUID=1000

#set -u # will prohibit reading non-init variables
set -o pipefail

[[ "${LANGUAGE}" =~ en:en_US.* ]] || if [[ "${LANGUAGE}" == '' ]]; then
	export LANGUAGE="en_US.UTF-8"
else
	export LANGUAGE="$LANGUAGE"
fi
[[ "${LC_ALL}" =~ en_.*.UTF-8 ]] || export LC_ALL=en_US.UTF-8

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	export DEBIAN_FRONTEND=noninteractive
	export DEBIAN_PRIORITY=critical
	export DEBCONF_NONINTERACTIVE_SEEN=true

	export DISTRO=$(lsb_release -cs 2>/dev/null) || export DISTRO=$(awk -vFS='"' '{print $2}' /etc/apt/apt.conf.d/release)
	export ENFORCE_PURGE=false
	export KDE_FULL_SESSION=true
	export XDG_CURRENT_DESKTOP=kde
	export XDG_RUNTIME_DIR="/run/user/${UID}"
	[ -d "${XDG_RUNTIME_DIR}" ] || mkdir -p "${XDG_RUNTIME_DIR}"
	sudo chmod 0700 "${XDG_RUNTIME_DIR}" &
	[ -d /tmp/caches ] || mkdir /tmp/caches
	sudo chmod 1777 /tmp/caches &
	[[ "${DISPLAY}" != '' ]] || export DISPLAY=':0.0'
	[[ $DBUS_SESSION_BUS_PID != '' ]] || export $(dbus-launch)
	#export DBUS_SESSION_BUS_ADDRESS="unix:path=$XDG_RUNTIME_DIR/bus"

	#dpkg-reconfigure -f noninteractive &

	# https://wiki.gentoo.org/wiki/Zswap
	# https://wiki.archlinux.org/title/zswap
	# https://en.wikipedia.org/wiki/Zswap
	# https://www.ibm.com/support/pages/new-linux-zswap-compression-functionality
	# https://www.kernel.org/doc/html/latest/vm/zswap.html
	# https://medium.com/for-linux-users/linux-tip-higher-performance-with-zswap-2a4654b935de
	export SWAPPINESS=${SWAPPINESS:-"0"}
	export ZSWAP_SYSTEMD_FILE_PATH="/etc/systemd/swap.conf.d/swapfc.conf"
	export ZSWAP_COMPRESSION=${ZSWAP_COMPRESSION:-"lz45"}	#zstd
	export ZSWAP_MAX_POOL_PERCENT=${ZSWAP_MAX_POOL_PERCENT:-"50"}
	export ZSWAP_ACCEPT_THRESHOLD_PERCENT=${ZSWAP_ACCEPT_THRESHOLD_PERCENT:-"50"}
	# zbud or zsmalloc or z3fold
	export ZSWAP_POOL_ALLOCATOR=${ZSWAP_POOL_ALLOCATOR:-"zsmalloc"}


	#export CMDLINE_LINUX_DEFAULT="init_on_alloc=0 debug zswap.enable=Y zswap.compressor=${ZSWAP_COMPRESSION} zswap.zpool=${ZSWAP_POOL_ALLOCATOR} zswap.max_pool_percent=${ZSWAP_MAX_POOL_PERCENT} zswap.accept_threshold_percent=${ZSWAP_ACCEPT_THRESHOLD_PERCENT} rd.shell rd.debug log_buf_len=1M kaslr pti=on slab_nomerge page_poison=1 slub_debug=FPZ"
	export CMDLINE_LINUX_DEFAULT="init_on_alloc=0 debug zswap.enable=N zswap.compressor=${ZSWAP_COMPRESSION} zswap.zpool=${ZSWAP_POOL_ALLOCATOR} zswap.max_pool_percent=${ZSWAP_MAX_POOL_PERCENT} zswap.accept_threshold_percent=${ZSWAP_ACCEPT_THRESHOLD_PERCENT} log_buf_len=1M kaslr pti=on slab_nomerge page_poison=1 slub_debug=FPZ thermal.nocrt=1"
	export EFIDIR=/boot/efi

fi

export UNISON=/tmp/caches/unison

case $(basename "${ME}") in

	'apt-ensure.bash'|'apt-ensure-cmd.bash')
	;;

	*)
		if [[ "${DEFUSER}" == '' ]]; then
			pushd /home &>/dev/null || pushd /Users >/dev/null
				if [[ $UID != '0' ]]; then
					export DEFUSER="${USER}"
				elif getent passwd $DEFUID &>/dev/null; then
					export DEFUSER=$(getent passwd $DEFUID | cut -d':' -f1)
				elif [[ "$(echo * | wc -w)" == '1' ]]; then
					export DEFUSER=$(echo *)
				elif getent passwd ubuntu-studio &>/dev/null; then
					export DEFUSER="ubuntu-studio"
				elif [ -x "${MYREALDIR}/exe/homefulhumans.bash" ] &>/dev/null && [[ $("${MYREALDIR}"/exe/homefulhumans.bash | wc -w) == '1' ]]; then
					export DEFUSER=$("${MYREALDIR}"/exe/homefulhumans.bash)
				elif getent passwd ssm-user &>/dev/null; then
					export DEFUSER="ssm-user"
				elif getent passwd ubuntu &>/dev/null; then
					export DEFUSER="ubuntu"
				elif [ -x "${MYREALDIR}/homefulhumans.bash" ] &>/dev/null && [[ $("${MYREALDIR}"/homefulhumans.bash | wc -w) == '1' ]]; then
					export DEFUSER=$("${MYREALDIR}"/homefulhumans.bash)
				elif mac_getent.bash 501 &>/dev/null; then
					export DEFUSER=$(mac_getent.bash 501 | cut -d':' -f1)
        elif [[ "$OSTYPE" != "linux-gnu"* ]]; then
					export DEFUSER=$(dscl . -list Users | tail -1)
				else
					read -e -p "Enter default user: " DEFUSER
					export DEFUSER
				fi


			popd >/dev/null
		fi
	;;

esac

#xset s 0 0
#xset s off
#xset -dpms
#nice -19 systemd-inhibit sleep infinity &

if [[ ! "${SHELL}" =~ zsh$  ]]; then
	shopt -s histappend 2>/dev/null || true
	shopt -s cmdhist 2>/dev/null || true
	shopt -s expand_aliases || true
fi

if [[ "$OSTYPE" != "linux-gnu"* ]]; then
	function ifne() {
	    read line
	    if [ -n "$line" ]; then
		"$@"
	    fi
	}
	export -f ifne &>/dev/null
	# Get list of gnubin directories
	if [[ $GNUBINS == '' ]]; then
		declare -a GNUBINS=("$(find /usr/local/opt -type d -follow -name gnubin -print 2>/dev/null)")
		export GNUBINS
  fi

	for bindir in ${GNUBINS[@]}; do
	  export PATH=$bindir:$PATH;
	done

	alias compress='afsctool -vc -9'
	alias ffmpeg='ffmpeg.mac'
fi

if [ -f ~/.bash_aliases ]; then
.  ~/.bash_aliases
fi
alias cd='pushd'
#alias rb='sudo systemctl stop bluetooth; sudo modprobe -r btusb; sudo modprobe -r btintel; sudo modprobe btusb; sudo modprobe btintel; sudo systemctl start bluetooth'
alias rb='sudo systemctl stop bluetooth; sudo systemctl start bluetooth'
alias ll='ls -al'
alias aptrec="sudo aptitude search '~RBrecommends:~i'"
alias promptvar='read -e -p'
alias kfg_kb='export $(dbus-launch) && /usr/bin/systemsettings5 kcm_keyboard &>/dev/null &'
alias kfg_mouse='export $(dbus-launch) && /usr/bin/systemsettings5 kcm_mouse &>/dev/null &'
alias wctoggle='xdotool key XF86WebCam'
[[ "$OSTYPE" =~ "linux-gnu".* ]] || if which tsp &>/dev/null || which ts &>/dev/null; then
	alias tsp='ts'
	function tsp() {
		ts "${@}"
	}
	export -f tsp &>/dev/null
fi

tsp &>/dev/null && tsp -S "${CPUS}" &>/dev/null

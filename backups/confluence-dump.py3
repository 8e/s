#!/usr/bin/env python3
import os
import shutil
import time
import logging
from urllib.parse import urlparse

from selenium import webdriver
import geckodriver_autoinstaller
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


geckodriver_autoinstaller.install()

logger = logging.getLogger('webdriver')
logger.propagate = False
fhandler = logging.FileHandler(filename='webdriver.log', mode='a')
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
fhandler.setFormatter(formatter)
logger.addHandler(fhandler)
logger.setLevel(logging.DEBUG)


def create_dir_to_save_pages():
    if not os.path.exists("pages_pdf"):
        os.makedirs("pages_pdf")

def main():
    # Read configuration from environment variables
    firefox_user_profile_path = os.environ.get("FIREFOX_USER_PROFILE_PATH")
    atlassian_account_name = os.environ.get("ATLASSIAN_ACCOUNT_NAME")

    if not firefox_user_profile_path or not atlassian_account_name:
        print("Missing required environment variables")
        return

    # Set up logging
    logger = logging.getLogger("webdriver")
    logger.propagate = False
    fhandler = logging.FileHandler(filename="webdriver.log", mode="a")
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    fhandler.setFormatter(formatter)
    logger.addHandler(fhandler)
    logger.setLevel(logging.DEBUG)

    create_dir_to_save_pages()

    # Set up Firefox options
    options = Options()
    options.add_argument("-profile")
    options.add_argument(firefox_user_profile_path)
    options.set_preference("print.always_print_silent", True)
    options.set_preference("print.printer_Mozilla_Save_to_PDF.print_to_file", True)
    options.set_preference("print_printer", "Mozilla Save to PDF")
    options.set_preference("dom.webdriver.enabled", False)
    options.set_preference("useAutomationExtension", False)

    logger.info("============================ START SESSION ============================\n")

    # Set up Firefox webdriver
    with webdriver.Firefox(options=options) as browser:
        # Navigate to the main Confluence page for the account
        base_url = f"https://{atlassian_account_name}.atlassian.net"
        pages_link = f"{base_url}/wiki/spaces"
        browser.get(pages_link)

        # Wait for the Confluence page to fully load
        wait = WebDriverWait(browser, 30)
        wait.until(EC.presence_of_element_located((By.XPATH, "//a[@data-qa='product-header']")))

        # Find all the space links and save their hrefs
        space_links = browser.find_elements(By.XPATH, "//a[contains(@href, '/wiki/spaces/')]")
        space_hrefs = [link.get_attribute("href") for link in space_links]

        # Iterate through all the spaces
        for href in space_hrefs:
            # Navigate to the space page
            browser.get(href)

            # Wait for the space page to fully load
            wait.until(EC.presence_of_element_located((By.XPATH, "//h1[@data-testid='space-name']")))

            # Save all the page links for the space
            page_links = browser.find_elements(By.XPATH, "//a[contains(@href, '/wiki/spaces/')]")
            page_hrefs = [link.get_attribute("href") for link in page_links]

            # Iterate through all the pages in the space
            for page_href in page_hrefs:
                # Navigate to the page
                browser.get(page_href)

                # Wait for the page to fully load
                wait.until(EC.presence_of_element_located((By.XPATH, '//*[@id="title-text"]')))

                # Get the page title and sanitize it for use as a filename
                title_text = browser.find_element(By.XPATH, '//*[@id="title-text"]')
                file_name = title_text.text.strip().replace(" ", "_").replace("/", "_")

                # Save the page as a PDF
                browser.execute_script("window.print();")
                time.sleep(5)
                shutil.copy("mozilla.pdf", f"pages_pdf/{file_name}.pdf")

    logger.info("============================ END SESSION ============================\n")

if __name__ == "__main__":
    main()

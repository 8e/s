#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

readonly P_SEMAPHORENAME=$(basename "${ME}").${USER}.${1}
shift
readonly TMPDIR=$("${MYREALDIR}"/ensure-path.bash /tmp/caches/"${P_SEMAPHORENAME}")
readonly LOGSD=$("${MYREALDIR}"/ensure-path.bash /tmp/log/"${P_SEMAPHORENAME}")
readonly JOBSD=$("${MYREALDIR}"/ensure-path.bash "${TMPDIR}/jobs")
readonly MGMTPIDSFILE="${TMPDIR}/pids"
readonly MAXJOBSFILE="${TMPDIR}/maxjobs"
readonly NICE="19"
readonly IONI="c3"
readonly LOGF="${LOGSD}/$$.log"
declare -a Executioner=($(which bash) '-xem')
declare -a PaShebang=('#!' ${Executioner[@]})
declare -a MgmtPids

which parallel bash tmux screen inotifywait | "${MYREALDIR}"/apt-ensure.bash parallel bash inotify-tools screen tmux

function maxjobs() {
	if "${MYREALDIR}"/is_posint.bash $1; then
		readonly MAXJOBS=$1
		echo $MAXJOBS > "${MAXJOBSFILE}"
	elif "${MYREALDIR}"/is_posint.bash $MAXJOBS; then
		true;
	elif [ -s "${MAXJOBSFILE}" ]; then
		readonly MAXJOBS=$(cat "${MAXJOBSFILE}")
	else
		readonly MAXJOBS=$(grep -c processor /proc/cpuinfo)
	fi
	(( MAXJOBS > 0 )) || exit 1
	echo $MAXJOBS
}

function get_paopts() {
	echo --no-run-if-empty --quote --jobs $(maxjobs) --joblog "${LOGSD}/parallel.log" --ungroup --max-args=1 --max-lines=1 --tmux "${@}"
}


function get_semcmd() {
	echo /usr/bin/sem --semaphorename "${P_SEMAPHORENAME}" $(get_paopts) "${@}"
}

function get_logf() {
	echo "${LOGSD}"/$(basename "${*}").log
}

function write_jobf() {
	local jobf="${*}"
	echo "${PaShebang[@]}" > "${jobf}"
	echo "exec > >(tee -a '$(get_logf "${jobf}")') 2>&1" >> "${jobf}"
	echo "source \"${MYREALDIR}\"/../defaults.rc.bash" >> "${jobf}"
	cat >> "${jobf}"
	${DEBUG} || (echo "rm '${TMPDIR}'/*\$$.* '\$0' '$(get_logf "${jobf}")' || true &" >> "${jobf}")
	chmod +x "${jobf}"
}

function countj {
	find "${TMPDIR}/" -path "${TMPDIR}/job.$$.*.bash" | wc -l
}

function create() {
	[ -s "${MGMTPIDSFILE}" ] && exit 1
	local jobf
	local j=$(countj)
	if [[ "${1}" != '' ]] && (( ${1} > 0 )); then
		maxjobs ${1}
		shift
	fi
	jobf="${JOBSD}/$$.$j.bash"
	echo "inotifywait -qmre attrib --format %w%f '${JOBSD}' | tee >(xargs --max-procs 13 --max-lines=1 $(get_semcmd) ${*} "${Executioner[@]}") &
	source '${MYREALDIR}'/bg-pids.bash > '${MGMTPIDSFILE}'
	fg" | tee -a "${LOGF}" | write_jobf "${jobf}"
	nice -"${NICE}" ionice -"${IONI}" screen -d -m -L -Logfile $(get_logf "${jobf}") "${jobf}"
	echo $! >> "${MGMTPIDSFILE}"
	source "${MYREALDIR}/bg-pids.bash" >> "${MGMTPIDSFILE}"
}

function addjobs() {
	local jobf
	local j=$(countj)
	[ -s "${MGMTPIDSFILE}" ] || create "${@}"
	while read line; do
		j=$(( $j+1 ))
		sleep $j 
		jobf="${JOBSD}/$$.$j.bash"
		echo "sleep $j; ${line}" | write_jobf "${jobf}"
		#echo "/usr/bin/env nice -"${NICE}" ionice -"${IONI}" screen -d -m -L -Logfile '$(get_logf "${jobf}")' '${jobf}'" | tee -a "${LOGF}"
	done
}


function mgmtpids() {
	MgmtPids=($(_out_mgmtpids))
	echo ${MgmtPids[*]}
}

function _out_mgmtpids() {
	if (( ${#MgmtPids[*]} == 0 )); then
	       cat "${MGMTPIDSFILE}"
	else
	       echo ${MgmtPids[*]}
	fi | tr " " "\n" | while read p; do
		(ps -p $p > /dev/null) && echo $p
	done
}

function stop() {
	set +e
	for p in $(mgmtpids); do
		kill -KILL $p || true
	done
	${DEBUG} || (rm -rvf "${TMPDIR}" || true; rm -rvf "${JOBSD}" || true; rm -rvf "${LOGSD}" || true)
}

function finish() {
	echo "
	$(get_semcmd) --wait
	wait
	${DEBUG} || (rm -r '${TMPDIR}' || true; rm -r '${JOBSD}' || true; rm -r '${LOGSD}' || true) &
	for p in $(mgmtpids); do
		kill \$p || true
	done
	for p in $(mgmtpids); do
		sleep 1.33
		kill -KILL \$p || true
	done
	" | addjobs
}


case ${1} in
	create)
		shift
		create "${@}"
	;;
	finish)
		shift
		finish
	;;
	stop|kill)
		shift
		stop
	;;
	wait)
		shift
		$(get_semcmd) --wait
		wait
	;;
	*)
		[ -s "${MGMTPIDSFILE}" ] || create "${@}"
		addjobs "${@}"
	;;
esac


#!/usr/bin/env bash

set -xe
set -o pipefail

readonly LEVEL=80 # %free
readonly PORTION=50
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly DIR_TMP="/tmp/$(basename "${0}").${USER}.$$.d"
readonly LOGF="${DIR_TMP}"/log
readonly SERCHLOGF="${DIR_TMP}"/search.log
readonly DELLOGF="${DIR_TMP}"/del.log
#readonly PROCS=$(( $(grep -c processor /proc/cpuinfo) / 2 ))
readonly PROCS=1

function du_above_threshold() {
	if df -T | grep zfs >> "${LOGF}"; then
		return  $([ `zpool list -o capacity rpool -H  | cut -d'%' -f1` -ge ${LEVEL} ])
	else
		return $([ `df -H | grep -vE '^Filesystem|udev|tmpfs|cdrom' | awk '/[[:digit:]]%/ {print $5 " " $1; exit}' | awk '{print $1}' | cut -d'%' -f1` -ge ${LEVEL} ])
	fi
}

function removetree() {
	if [ ${#} -eq 0 ]; then
		while read path; do
			du_above_threshold &&  [ -e "${path}" ] && rm -rf "${path}"
		done
	else
		for path in ${@}; do
			du_above_threshold && [ -e "${path}" ] && rm -rf "${path}"
		done
	fi
}

function pathisnotempty() {
	if [ ${#} -eq 0 ]; then
		path="${@}"
	else
		read path
	fi
	exitcode=0
	set +x
	([ -d "${path}" ] || [ -L "${path}" ]) && (ls -1A "${path}"/* 2>&1 | head -1 &>/dev/null) || exitcode=$?
	set -x
	return ${exitcode}
}

function eventuallycleandircontents() {
	if [ ${#} -eq 0 ]; then
		while read path; do
			echo "path: ${path}" | tee -a "${LOGF}"
			[ -e "${path}" ] && find "${path}" -mindepth 1 | eventually_clean_files_list
		done
	else
		for path in ${@}; do
			echo "path: ${path}" | tee -a "${LOGF}"
			[ -e "${path}" ] && find "${path}" -mindepth 1 | eventually_clean_files_list
		done
	fi
}

function process_dirs() {
	sort -u > dirs.usorted
	du_above_threshold && cat dirs.usorted | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -l -I{} find {} -mindepth 1 -depth -true || true) | ifne sort -u | ifne "${ME}" -
	du_above_threshold && cat dirs.usorted | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -l -I{} find {} -empty -delete -true || true)
	rm -v dirs.usorted
}

function delete_portion() {
	local i=0
	du_above_threshold && while read f && [[ "${f}" != '.' ]] && [[ "${f}" != '..' ]]; do
		if [[ "${f}" == '' ]]; then
			echo "${f} is empty" >> "${LOGF}"
		elif [ -d "${f}" ]; then
			echo "${f} is a dir" >> "${LOGF}"
			[ -d "${f}" ] && echo "${f}" >> dirs
		elif [ -e "${f}" ]; then
			if (( i > PORTION )); then
				echo "${f}"
			elif (( i == PORTION )); then
				echo "${f}"
				[ -s dirs ] && du_above_threshold && (cat dirs | ifne "${ME}" dirs >> "${LOGF}") &
			else
				echo "${f} exists and is gonna be deleted" | ifne tee -a "${LOGF}" >> "${DELLOGF}"
				if du_above_threshold && [ -e "${f}" ]; then
					(rm -v "${f}" | ifne tee -a "${LOGF}" >> "${DELLOGF}") &
					i=$((i+1))
				fi
			fi
		else
			echo "${f} maybe not exists?" >> "${LOGF}"
		fi
		#wait
		([ -e "${f}" ] && (find "${f}" -empty -delete || true) | ifne tee -a "${LOGF}" >> "${DELLOGF}") &
	done
	[ -s dirs ] && du_above_threshold && (cat dirs | ifne "${ME}" dirs >> "${LOGF}") &
}

function filt() {
	(ifne grep -v '^.$' || true) | (ifne grep -v '^..$' || true)
}

function the_oldest_from_list() {
	set +x
	ifne cat | filt > oldest.input
	du_above_threshold && ifne cat oldest.input | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1Atdr || true) | filt | head -n "${PORTION}"
	du_above_threshold && ifne cat oldest.input | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1Atdr || true) | tail -n +"${PORTION}" > oldest
	rm oldest.input
	set -x
}

function delete_the_oldest_from_list() {
	set +x
	du_above_threshold && (ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1Atdr || true) | delete_portion
	set -x
}

function the_biggest_from_list() {
	set +x
	ifne cat | filt > biggest.input
	du_above_threshold && ifne cat biggest.input | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1AaSd || true) | filt | head -n "${PORTION}"
	du_above_threshold && ifne cat biggest.input | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1AaSd || true) | filt | tail -n +"${PORTION}" > biggest
	rm biggest.input
	set -x
}

function delete_the_biggest_from_list() {
	set +x
	du_above_threshold && ifne tail -n +1 | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1AaSd || true) | filt | delete_portion
	set -x
}


function the_smallest_from_list() {
	set +x
	ifne cat | filt > smallest.input
	du_above_threshold && ifne cat smallest.input | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1AaSd || true) | filt | head -n "${PORTION}"
	du_above_threshold && ifne cat smallest.input | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1AaSd || true) | filt | tail -n +"${PORTION}" > smallest
	rm smallest.input
	set -x
}

function delete_the_smallest_from_list() {
	set +x
	du_above_threshold && ifne tail -n +1 | (ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1AaSd || true) | filt | delete_portion
	set -x
}

function delete_the_smallest_from_list() {
	set +x
	du_above_threshold && ifne tail -n +1 | ifne xargs --no-run-if-empty --max-procs=${PROCS} -d "\n" ls -1AaSd || true | filt | delete_portion
	set -x
}

function the_deepest_from_list() {
	set +x
	ifne cat | filt > deepest.input
	du_above_threshold && ifne awk '{print gsub("/","/"), $0}' deepest.input | ifne sort -rn | ifne cut -d' ' -f2- | filt | head -n "${PORTION}"
	du_above_threshold && ifne awk '{print gsub("/","/"), $0}' deepest.input | ifne sort -rn | ifne cut -d' ' -f2- | filt | tail -n +"${PORTION}" > deepest
	rm deepest.input
	set -x
}

function delete_1emptyifany_from_list() {
	while read f; do
		[ -e "${f}" ] && find "${f}" -empty
	done | ifne head -1 | delete_portion | ifne tee -a "${LOGF}"
}

function eventually_clean_files_list() {
	pushd "${DIR_TMP}" >> "${LOGF}" || cd "${DIR_TMP}"
		#ifne sort -u > input
		ifne cat > input
		while [ -s input ]; do
		#	sleep "${PORTION}" && cat input | ifne "${ME}" oldest >> "${LOGF}" &
		#	sleep "${PORTION}" && cat input | ifne "${ME}" biggest >> "${LOGF}" &
		#	sleep "${PORTION}" && cat input | ifne "${ME}" smallest >> "${LOGF}" &
		#	sleep "${PORTION}" && cat input | ifne "${ME}" deepest >> "${LOGF}" & 
			cat input | delete_the_oldest_from_list >  oldest
			rm input &
			if [ -s oldest ]; then
				cat oldest | delete_the_biggest_from_list > biggest
				rm oldest &
			else
				(rm oldest || true ) &
				break
			fi
			if [ -s biggest ]; then
				cat biggest | delete_the_smallest_from_list > smallest
				(rm biggest || true ) &
			else
				(rm biggest || true ) &
				break
			fi
			if [ -s smallest ]; then
				cat smallest | delete_the_deepest_from_list > deepest
				(rm smallest || true) &
			else
				(rm smallest || true) &
				break
			fi
			if [ -s deepest ]; then
				mv deepest input
			else
				(rm deepest || true) &
				break
			fi
		done
		(rm -v input oldest biggest smallest deepest 2>&1 >> "${LOGF}" || true) &
	popd >> "${LOGF}" || true
}

function zfs_get_backup_datasets() {
	zfs list -H -o name | grep 'rpool/images\|rpool/zbackups'
	zfs list -t snapshot -H -o name
}

function zfs_delete_the_oldest_backup_dataset() {
	du_above_threshold && for dataset2destroy in $(zfs_get_backup_datasets | xargs --no-run-if-empty --max-procs=${PROCS}  -d "\n" zfs list -H -s creation -o name); do
		[[ "${dataset2destroy}" == '' ]] && break
		zfs destroy "${dataset2destroy}" && break
	done
}

function zfs_delete_the_biggest_backup_dataset() {
	du_above_threshold && for dataset2destroy in $(zfs_get_backup_datasets | xargs --no-run-if-empty --max-procs=${PROCS}  -d "\n" zfs list -H -s used -o name | sort -r); do
		[[ "${dataset2destroy}" == '' ]] && break
		zfs destroy "${dataset2destroy}" && break
	done
}

function zfs_delete_the_smallest_backup_dataset() {
	du_above_threshold && for dataset2destroy in $(zfs_get_backup_datasets | xargs --no-run-if-empty --max-procs=${PROCS}  -d "\n" zfs list -H -s used -o name); do
		[[ "${dataset2destroy}" == '' ]] && break
		zfs destroy "${dataset2destroy}" && break
	done
}

function FindFromDicts() {
	local Options=()
	local isfirst=true
	local errlogf="${DIR_TMP}/find.error.log"
	for find_optvalue in "${!SearchOpts[@]}"; do
		echo "find opt value: ${find_optvalue}" >> "${LOGF}"
		find_optname=${SearchOpts[${find_optvalue}]}
		if ${isfirst}; then
			isfirst=false
		else
			Options+=(-or)
		fi
		Options+=(-${find_optname} "${find_optvalue}")
	done
	unset SearchOpts

	if ! find / "${Options[@]}" 2>"${errlogf}"; then
		errcode=$?
		cat "${errlogf}" >> "${LOGF}"
		if [[ $(grep -v '/proc/' "${errlogf}") == '' ]]; then
			exit ${errcode}
		else
			rm "${errlogf}"
		fi
	fi
}

function LocateFromDicts() {
	local Options=()
	local errlogf="${DIR_TMP}/locate.error.log"
	for locate_optvalue in "${!SearchOpts[@]}"; do
		echo "locate opt value: ${locate_optvalue}" >> "${LOGF}"
		locate_optname=${SearchOpts[${locate_optvalue}]}
		Options+=("${locate_optvalue}")
	done
	unset SearchOpts
	if ! locate --existing "${Options[@]}" 2>"${errlogf}"; then
		errcode=$?
		cat "${errlogf}" >> "${LOGF}"
		if [[ $(grep -v '/proc/' "${errlogf}") == '' ]]; then
			exit ${errcode}
		else
			rm "${errlogf}"
		fi
	fi
}

function DelFromDicts() {
	(du_above_threshold && LocateFromDicts | ifne tee -a "${SERCHLOGF}" | ifne "${ME}" - >> "${LOGF}") &
	du_above_threshold && FindFromDicts | ifne tee -a "${SERCHLOGF}" | eventually_clean_files_list
}

function iterate_backup_datasets() {
	which zfs && if [[ $(zfs_get_backup_datasets | wc -l) != '0' ]] && du_above_threshold; then
		zfs_delete_the_oldest_backup_dataset
		zfs_delete_the_biggest_backup_dataset
		zfs_delete_the_smallest_backup_dataset
	fi | tee -a "${LOGF}"
}

"${MYREALDIR}"/ensure-path.bash "${DIR_TMP}"
which ifne || apt -y install moreutils
date +%F_%T > "${LOGF}"
echo "pwd: $(pwd)" | tee -a "${LOGF}"

if [[ "${1}" == '-' ]]; then
	eventually_clean_files_list
	rm -r "${DIR_TMP}" || true
	exit
elif [[ "${1}" == 'dirs' ]]; then
	process_dirs
	rm -r "${DIR_TMP}" || true
	exit
elif [[ "${1}" == 'oldest' ]]; then
	the_oldest_from_list | delete_portion
	rm -r "${DIR_TMP}" || true
	exit
elif [[ "${1}" == 'biggest' ]]; then
	the_biggest_from_list | delete_portion
	rm -r "${DIR_TMP}" || true
	exit
elif [[ "${1}" == 'deepest' ]]; then
	the_deepest_from_list | delete_portion
	rm -r "${DIR_TMP}" || true
	exit
elif [[ "${1}" == 'smallest' ]]; then
	the_smallest_from_list | delete_portion
	rm -r "${DIR_TMP}" || true
	exit
fi

du_above_threshold && "${MYREALDIR}"/zdestroycongrusnaps.bash

iterate_backup_datasets

declare -A SearchOpts
SearchOpts['efs*.tmp']='iname'
SearchOpts['*.DS_Store']='iname'
SearchOpts['thumbs.db']='iname'
SearchOpts["*RECYCLE.BIN"]='iname'
SearchOpts['ViberDownloads']='iname'
SearchOpts[".Trash-*"]='iname'
SearchOpts['Настраиваемые\ шаблоны\ Office']='iname'
DelFromDicts

iterate_backup_datasets

if du_above_threshold; then
	which apt && (apt autoremove || true) | tee -a "${LOGF}"
fi

if du_above_threshold; then
	which apt && (apt autoclean || true) | tee -a "${LOGF}"
fi

if du_above_threshold; then
	which cleanmgr && cleanmgr /autoclean | tee -a "${LOGF}"
fi

if du_above_threshold; then
        for i in /var/log/mail.* ; do echo "" > $i; done | tee -a "${LOGF}"
fi
if du_above_threshold; then
        which docker && docker container ls -a --filter status=exited --filter status=created && docker container prune --force | tee -a "${LOGF}"
fi
if du_above_threshold; then
        which docker && docker image prune -f -a --filter "until=48h" --force | tee -a "${LOGF}"
fi
if du_above_threshold; then
        which docker && docker system prune --force | tee -a "${LOGF}"
fi
if du_above_threshold; then
        [ -e /var/lib/jenkins/workspace ] &&  find /var/lib/jenkins/workspace/ -type f -mtime +30 -mindepth 1 ! -regex '^/var/lib/jenkins/workspace/core\(/.*\)?' -delete | tee -a "${LOGF}"
fi

iterate_backup_datasets

declare -A SearchOpts
SearchOpts["*/Dropbox/ViberDownloads"]='ipath'
SearchOpts["*/inetpub/temp"]='ipath'
SearchOpts["*/ADtemp"]='ipath'
SearchOpts["*/Windows/Downloaded\ Program\ Files/*"]='ipath'
SearchOpts["*/AppData/Local/Temp/*"]='ipath'
SearchOpts["*/My\ Skype\ Received\ Files/*"]='ipath'
SearchOpts["*/.dropbox.cache/*"]='ipath'
SearchOpts["*/\$RECYCLE.BIN"]='ipath'
SearchOpts["/var/lib/jenkins/.m2/repository/*"]='ipath'
SearchOpts['desktop.ini']='iname'
SearchOpts['.directory']='iname'
SearchOpts["/var/cache/apt/archives/*"]='ipath'
DelFromDicts
iterate_backup_datasets

if du_above_threshold; then
	which cleanmgr && cleanmgr /autoclean /lowdisk | tee -a "${LOGF}"
fi

if du_above_threshold; then
	which cleanmgr && cleanmgr /autoclean /verylowdisk | tee -a "${LOGF}"
fi

declare -A SearchOpts
SearchOpts["*/Dropbox/My*"]='ipath'
SearchOpts["*/Dropbox/Мои*"]='ipath'
SearchOpts['LogFiles']='name'
SearchOpts['Logs']='name'
SearchOpts["*/backup/*"]='ipath'
SearchOpts["*/backups/*"]='ipath'
SearchOpts['*.bak']='iname'
SearchOpts['*.log']='iname'
SearchOpts['*.tmp']='iname'
SearchOpts['*.*~']='iname'
SearchOpts["*/audio/music*/Spotify/*"]='path'
DelFromDicts
iterate_backup_datasets

declare -A SearchOpts
SearchOpts['*.old']='iname'
SearchOpts["*/Dropbox/My*"]='ipath'
SearchOpts["*/*temp/*"]='ipath'
SearchOpts["*/*tmp/*"]='ipath'
SearchOpts["Cache*"]='name'
SearchOpts['*.cue']='iname'
SearchOpts['*/var/log/*']='ipath'
SearchOpts['*/var/mail/*']='ipath'
DelFromDicts
iterate_backup_datasets

declare -A SearchOpts
SearchOpts['*/.stversions*']='ipath'
SearchOpts["*/.cache/*"]='ipath'
SearchOpts["*cache2"]='iname'
DelFromDicts
iterate_backup_datasets

declare -A SearchOpts
SearchOpts['*/soft/*']='ipath'
DelFromDicts
iterate_backup_datasets

which zfs && while [[ $(zfs_get_backup_datasets | wc -l) != '0' ]] && du_above_threshold; do
	iterate_backup_datasets
done | tee -a "${LOGF}"

which zfs && "${MYREALDIR}"/ztrim.bash -w

rm -r "${DIR_TMP}"

updatedb &

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>  
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>  
// time 
#include <time.h>
#include <ncurses.h>
#include "../libc/libc-tui.c"

 

int main()
{ 
    int ch, posy , foo;
    char cwd[PATH_MAX];

    printf( "CAM\n" ); 
    //nruncmd(  " pkill  mplayer  ; mplayer -fs -zoom -tv device=/dev/video1 tv://           " );
    nstartncurses(  ); 
    int rows, cols ;
    getmaxyx( stdscr, rows , cols);

    nc_color_default( );
    ncerase();
    mvprintw( 0, 0, "CAM " );
    for( foo = 0 ; foo <= cols -1  ; foo++)
      mvaddch( 1 , foo , ACS_HLINE );
    posy = 4; 
    mvprintw( posy++, 0, " Press Key \"0\" to View Webcam VIDEO-0 (embedded notebook) " );
    mvprintw( posy++, 0, " Press Key \"1\" to View Webcam VIDEO-1 (external webcam) " );

    ch = getch();
    switch( ch ) {
         case '0':
            runcmd(  " pkill  mplayer  ; mplayer -fs -zoom -tv device=/dev/video0 tv://          " );
	    break;
         case '1':
            runcmd(  " pkill  mplayer  ; mplayer -fs -zoom -tv device=/dev/video1 tv://          " );
	    break;
    }

    curs_set( 1 ) ;
    attroff( A_BOLD );
    attroff( A_REVERSE );
    curs_set( 1 );
    endwin();	 
    return 0; 
}

#!/usr/bin/env bash

set -e
readonly DEBUG=${DEBUG:-false}
${DEBUG} && set -x

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

source "${MYREALDIR}"/../defaults.rc.bash

set -o pipefail

readonly LEVEL=${LEVEL:-86} # %du
readonly PORTION=50
readonly P_SEMAPHORENAME=$(basename "${0}").${USER}
readonly TMPDIR_COMMON="$("${MYREALDIR}"/ensure-path.bash /tmp/${P_SEMAPHORENAME})"
readonly TMPDIR_MINE="${TMPDIR_COMMON}/$$"
readonly LOGF="${TMPDIR_MINE}"/$$.log
readonly FINDINGSF="${TMPDIR_MINE}"/findings.list
readonly DELLOGF="${TMPDIR_COMMON}"/del.log
readonly DIRSCACHE="${TMPDIR_COMMON}/dirs"
readonly CORES=$(grep -c processor /proc/cpuinfo)
readonly JOBS=$(( ${CORES} * 2 - 1 ))
readonly ZFS_LAST_SEARCH='images\|backup\|archive\|delete'
declare -A Excludes
Excludes["${TMPDIR_COMMON}/.*"]='regex'
Excludes["${TMPDIR_MINE}/.*"]='regex'
Excludes["${TMPDIR_COMMON}"]='path'
Excludes["${TMPDIR_MINE}"]='path'
Excludes["/var/tmp"]='path'
Excludes["/tmp"]='path'
Excludes["${MYREALDIR}"]='path'
Excludes["${MYREALDIR}/.*"]='regex'
Excludes["/etc/systemd"]='path'
Excludes["/etc/systemd/.*"]='regex'
Excludes["/var/tmp/.*"]='regex'
Excludes["/tmp/.*"]='regex'
Excludes["/var/cache/debconf/.*"]='regex'
Excludes["/var/cache/debconf"]='path'
Excludes["/var/cache/apt/.*.bin"]='regex'
Excludes["/proc/.*"]='regex'
Excludes["/proc"]='path'
Excludes["/dev/.*"]='regex'
Excludes["/dev"]='path'
Excludes["/sys/.*"]='regex'
Excludes["/sys"]='path'
Excludes["/run/.*"]='regex'
Excludes["/run"]='path'
Excludes["/var/run/.*"]='regex'
Excludes["/var/run"]='path'
Excludes["*/qBittorrent/BT_backup*"]='path'
Excludes[".*/qBittorrent/BT_backup.*"]='regex'
Excludes["."]='name'
Excludes[".."]='name'
Excludes['.*/var/lib/jenkins/workspace/core\(/.*\)?']='regex'
declare -a ExclFindOptions=()
readonly EXCLPATTERNSF="${TMPDIR_COMMON}/exclude.list"
for find_optvalue in "${!Excludes[@]}"; do
	find_optname=${Excludes[${find_optvalue}]}
	ExclFindOptions+=(-not -${find_optname} "${find_optvalue}")
	if [[ "${find_optname}" == 'regex' ]]; then
		echo "${find_optvalue}"
	else
		echo "^${find_optvalue}$"
	fi
done > "${EXCLPATTERNSF}"

readonly RSYNC_EXCLUDE_PATTERNS_FILE="${TMPDIR_COMMON}/rsync.exclude.list"
readonly RSYNCD_CONF_FILE=/etc/rsyncd.conf.d/$(basename "${ME}").inc
readonly RSYNCD_CONF_CONTENTS="[global]
exclude from = ${RSYNC_EXCLUDE_PATTERNS_FILE}
"
declare -a ListColumns=('depth' 'datetime' 'size' 'path')

function filt() {
	ifne grep -vf "${EXCLPATTERNSF}" || true
}

function get_colnum() {
	local val="${*}"
	for i in "${!ListColumns[@]}"; do
		if [[ "${ListColumns[$i]}" = "${val}" ]]; then
			echo "${i}"
			break
		fi
	done
}

function du_above_threshold() {
	local dflevel=$(df --exclude-type=tmpfs -H --output=pcent | tail -n +2 | sort -nr | awk -vRS='[% ]' '/[[:digit:]]/ {print; exit}')
	sync &
	[ ${dflevel} -gt ${LEVEL} ] && return 0
	if df --type=zfs -T | grep zfs > /dev/null; then
		local zpoolcapl=$(zpool list -o capacity rpool -H  | cut -d'%' -f1)
		zpool sync &
		[ ${zpoolcapl} -gt ${LEVEL} ] && return 0
	fi
	finish
}

function isroot() {
	return ${UID}
}

function spawn() {
	#"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" "${JOBS}"
	while read line; do
		tsp "${line}"
	done
}

function xargs_every() {
	ifne xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" -I{} "${MYREALDIR}"/bliss.sh "${@}" || true
}

function xargs_all() {
	ifne xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" "${MYREALDIR}"/bliss.sh "${@}" || true
}

function process_dirscache() {
	du_above_threshold
	if [ -s "${DIRSCACHE}" ]; then
		sort -u "${DIRSCACHE}" > dirs.sorted
		mv dirs.sorted "${DIRSCACHE}"
		cat "${DIRSCACHE}" | xargs_every "${ME}" empty &
		cat "${DIRSCACHE}" | xargs_every find -H {} -mindepth 1 -depth "${ExclFindOptions[@]}" -true | ifne sort -u > dirs.uexisorted
		mv dirs.uexisorted "${DIRSCACHE}"
		(cat "${DIRSCACHE}" | ifne "${MYREALDIR}"/dedup.bash -) &
		bg_wait
		rm -v "${DIRSCACHE}"
	fi
}

function process_one_dir() {
	local d=$(realpath "${*}")
	du_above_threshold
	echo "'${MYREALDIR}'/dedup.bash '${d}'" | spawn
	echo "[ -d '${d}' ] && (/usr/bin/env find -H '${d}' -mindepth 1 -depth -true ${ExclFindOptions[*]} | '${ME}' -); [ -d '${d}' ] && '${ME}' empty '${d}'" | spawn
	possible_git_parent "${d}"
}

function delete_portion() {
	local i=0
	local path
	du_above_threshold
	while read line; do
		path=$(echo "${line}" | remove_nonpath_columns)

		if [[ "${path}" == '.' ]] || [[ "${path}" == '..' ]] || [[ "${path}" == '' ]]; then
			echo "${path} is: ${path}" | "${MYREALDIR}"/entee.bash -na "${LOGF}"
			continue
		elif [ -d "${path}" ]; then
			echo "${path}" | "${MYREALDIR}"/entee.bash -na "${DIRSCACHE}"
			process_one_dir "${path}"
		elif [ -e "${path}" ]; then
			if (( i > PORTION )); then
				echo "${line}"
			elif (( i == PORTION )); then
				echo "${line}"
			else
				echo "${path} exists and is gonna be deleted" | "${MYREALDIR}"/entee.bash -a "${LOGF}" | "${MYREALDIR}"/entee.bash -na "${DELLOGF}"
				if [ -e "${path}" ]; then
					(rm -v "${path}" | "${MYREALDIR}"/entee.bash -a "${LOGF}" | "${MYREALDIR}"/entee.bash -na "${DELLOGF}") &
					i=$((i+1))
				fi
			fi
		else
			echo "${path} maybe not exists?" |"${MYREALDIR}"/entee.bash -na "${LOGF}"
			continue
		fi
	done
}

function clean_portion_sorted_by() {
	local by="${*}"
	set +x
	du_above_threshold

	case "${by}" in
		
		'asis')
			delete_portion
		;;
	
		'reverse')
			ifne tac | delete_portion
		;;
		
		'oldest')
			ifne sort -k$(get_colnum 'datetime') | delete_portion
		;;
		
		'newest')
			ifne sort -rk$(get_colnum 'datetime') | delete_portion
		;;

		'biggest')
			ifne sort -nk$(get_colnum 'size') | delete_portion
		;;

		'smallest')
			ifne sort -rnk$(get_colnum 'size') | delete_portion
		;;

		'deepest')
			ifne sort -rnk$(get_colnum 'depth') | delete_portion
		;;

	esac
	${DEBUG} && set -x
}

function remove_nonpath_columns() {
	ifne "${MYREALDIR}"/nocolsfromleft.bash $(( $(get_colnum 'path') - 1 ))
}

function eventually_clean_files_list() {
	local input_f="${TMPDIR_MINE}"/input.list
	local output_f="${TMPDIR_MINE}"/output.list
	ifne cat > "${input_f}"
	while [  -s "${input_f}" ]; do

		clean_portion_sorted_by biggest < "${input_f}" | clean_portion_sorted_by oldest | clean_portion_sorted_by smallest | clean_portion_sorted_by newest | clean_portion_sorted_by deepest > "${output_f}"

		mv "${output_f}" "${input_f}"

		[ -s "${DIRSCACHE}" ] && process_dirscache

		bg_wait

		iterate_backup_datasets

	done
	(rm -v "${input_f}" 2>&1 |"${MYREALDIR}"/entee.bash -na "${LOGF}" || true) &
}

function zfs_get_backup_datasets() {
	zfs list -t snapshot -H -o name
	(zfs list -H -o name | grep 'images\|backup\|delete' || true)
}

function zfs_delete_the_oldest_backup_dataset() {
	du_above_threshold
	for dataset2destroy in $(zfs_get_backup_datasets | xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" zfs list -H -s creation -o name); do
		[[ "${dataset2destroy}" == '' ]] && break
		zfs_destroy "${dataset2destroy}"
		break
	done
}

function zfs_delete_the_biggest_backup_dataset() {
	du_above_threshold
	for dataset2destroy in $(zfs_get_backup_datasets | xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" zfs list -H -s used -o name | sort -r); do
		[[ "${dataset2destroy}" == '' ]] && break
		zfs_destroy "${dataset2destroy}"
		break
	done
}

function zfs_delete_the_smallest_backup_dataset() {
	du_above_threshold
	for dataset2destroy in $(zfs_get_backup_datasets | xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" zfs list -H -s used -o name); do
		[[ "${dataset2destroy}" == '' ]] && break
		zfs_destroy "${dataset2destroy}"
		break
	done
}

function add_depth_column() {
	ifne awk '{print gsub("/","/"), $0}'
}

function find_and_list() {
	declare -a FindCmd=(find -H "${@}" "${ExclFindOptions[@]}" -printf '%T+ %b %p\n' -true)
	if ! "${FindCmd[@]}" 2> >("${MYREALDIR}"/entee.bash "${errlogf}" | add_depth_column | ifne "${MYREALDIR}"/entee.bash -na "${FINDINGSF}"); then
		errcode=$?
		cat "${errlogf}"
		exit ${errcode}
	fi
	cat "${FINDINGSF}"
	rm "${FINDINGSF}" &
}

function find_empty_delete() {
	find -H "${@}" "${ExclFindOptions[@]}" -empty -delete -true &
}

function DelFromDicts() {
	local Options=()
	local isfirst=true
	local errlogf="${TMPDIR_MINE}/find.error.log"
	local find_optvalue
	local FindCmd=()

	du_above_threshold

	for find_optvalue in "${!SearchOpts[@]}"; do
		echo "find opt value: ${find_optvalue}" | "${MYREALDIR}"/entee.bash -na "${LOGF}"
		find_optname=${SearchOpts[${find_optvalue}]}
		if ${isfirst}; then
			isfirst=false
		else
			Options+=(-or)
		fi
		Options+=(-${find_optname} "${find_optvalue}")
	done
	unset SearchOpts

	bg_wait
	du_above_threshold

	if [ -s "${FINDINGSF}" ]; then
		cat "${FINDINGSF}" | (xargs --no-run-if-empty --max-procs=${JOBS} -d "\n" -I{} find -H {} "${ExclFindOptions[@]}" -delete -true || true) | "${MYREALDIR}"/entee.bash -na "${DELLOGF}"
		iterate_backup_datasets
		bg_wait
	fi

	du_above_threshold
	find_empty_delete "${FindRoots[@]}" "${Options[@]}"
	du_above_threshold
	find_and_list "${FindRoots[@]}" -type f "${Options[@]}" | eventually_clean_files_list
	find_empty_delete "${FindRoots[@]}" "${Options[@]}"
	du_above_threshold
	find_and_list "${FindRoots[@]}" "${Options[@]}" | eventually_clean_files_list

}

function gitprocess() {
	du_above_threshold
	find -H "${@}" -ipath '*/.git' -type d "${ExclFindOptions[@]}" -true -execdir git prune \; &
	#find -H "${@}" -ipath '*/.git' -type d "${ExclFindOptions[@]}" -true -execdir "${MYREALDIR}"/bfg.bash -B 1 \;
	du_above_threshold
	find -H "${@}" -ipath '*/.git' -type d "${ExclFindOptions[@]}" -true -execdir git gc --aggressive --prune=all \;
	find -H "${@}" -ipath '*/.git' -type d "${ExclFindOptions[@]}" -true -execdir git push \; &
	find -H "${@}" -ipath '*/.git' -type d "${ExclFindOptions[@]}" -true -execdir git push --force \; &
}

function mypushd() {
	local newd="${@}"
	if [[ "${parent}" != '.' ]]; then
		pushd "${newd}" | "${MYREALDIR}"/entee.bash -na "${LOGF}"
	fi
}

function mypopd() {
	if [ ${#DIRSTACK[@]} -gt 1 ]; then
		popd | "${MYREALDIR}"/entee.bash -na "${LOGF}"
	fi
}

function possible_git_parent() {
	parentd="${@}"
	mypushd "${@}"
		if [ -d /.git ]; then
			du_above_threshold
			#"${MYREALDIR}"/bfg.bash -B 1 .
			du_above_threshold
			git prune
			du_above_threshold
			git gc --aggressive --prune=all # remove the old files
			git push &
			git push --force &
		fi
	mypopd
}

function process_path {
	local f=$(realpath "${*}")
	du_above_threshold
	if [ -d "${f}" ]; then
		process_one_dir "${f}"
		echo ${f} | "${MYREALDIR}"/entee.bash -na "${DIRSCACHE}"
	elif [ -e "${f}" ]; then
		(rm -v "${*}" | "${MYREALDIR}"/entee.bash -a "${DELLOGF}") &
	elif [[ ${1} == [:digit:]+ ]]; then
		shift
		(( ${1} > ${PORTION} )) && echo "${ME} zfsdatasets" | spawn
		process_path "${*}"
	fi
}

function iterate_backup_datasets() {
	du_above_threshold
	isroot && which zfs && if [[ $(zfs_get_backup_datasets | wc -l) != '0' ]]; then
		zfs_delete_the_oldest_backup_dataset
		zfs_delete_the_biggest_backup_dataset
		zfs_delete_the_smallest_backup_dataset
		echo "Snapshots: $(zfs list -t snapshot | wc -l)"
	fi | "${MYREALDIR}"/entee.bash -a "${LOGF}"
}

function zfs_destroy() {
	${DEBUG} && echo destroy "${@}"
	(zfs destroy "${@}" || true) &
}

function bg_wait() {
	#"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" wait
	tsp -f true
	wait
}

function finish_me() {
	"${DEBUG}" || (rm -r "${TMPDIR_MINE}" || true)
}

function finish() {
	#"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" finish
	bg_wait
	"${DEBUG}" || (rm -r "${TMPDIR_COMMON}" || true)
	exit 0
}

trap finish_me EXIT

(echo "ifne tsp" | "${MYREALDIR}"/apt-ensure.bash moreutils task-spooler) | "${MYREALDIR}"/entee.bash -na "${LOGF}"
"${MYREALDIR}"/ensure-path.bash "${TMPDIR_MINE}" "${TMPDIR_COMMON}"  | "${MYREALDIR}"/entee.bash -na "${LOGF}"

mypushd "${TMPDIR_MINE}"

	date +%F_%T |"${MYREALDIR}"/entee.bash -na "${LOGF}"
	echo "pwd: $(pwd)" | "${MYREALDIR}"/entee.bash -na "${LOGF}"

	case "${1}" in
		
		'-')
			eventually_clean_files_list
		exit
		;;

		create|finish|kill|stop|wait)
			#"${MYREALDIR}"/paraq.bash "${P_SEMAPHORENAME}" "${@}"
			tsp -S "${JOBS}"
		exit
		;;

		'empty')
			shift
			find_empty_delete "${@}"
		exit
		;;

		# don't use with -delete! there's a bug it deletes everything
		# use | /var/git/s/exe/xargs.bash -l rm -rvf  instead
		'find')
			shift
			"${ME}" findopts |  xargs --no-run-if-empty -l --max-procs=${JOBS} find -H "${@}" -true | ifne sort -u | filt
		exit
		;;

		'findopts')
			shift
			awk -vFS='[]|[]' '/SearchOpts\[.*=/ {gsub("['\''=]","",$3); print "-"$3,$2}' "${ME}" | ifne sort -u | filt
		exit
		;;

		'patterns')
			shift
			awk -vFS='[]|[]' "/SearchOpts\[.*=/ {print \"${*}\"\$2}" "${ME}" | grep -v "'" | ifne sort -u | filt
		exit
		;;

		'ipatterns')
			shift
			# https://superuser.com/questions/256751/make-rsync-case-insensitive
			# https://forums.gentoo.org/viewtopic-t-1060394-start-0.html
			"${ME}" patterns |  perl -pe "s/([[:alpha:]])/[\U\$1\E\$1]/g; s/^/${*}/g;"
		exit
		;;

		'unison')
			shift
			"${ME}" patterns |  awk '{ print "ignore = Name", $1 }'
		exit
		;;

		'git-parent')
			shift
			possible_git_parent "${*:-.}"
		exit
		;;

		
		'gits')
			shift
			gitprocess "${*:-'/'}"
		exit
		;;

		'rsync-exclude')
			shift
			which rsync | "${MYREALDIR}"/apt-ensure.bash rsync
			"${ME}" patterns | sort -u | sed 's/\*/\*\*/g' | nice -19 ionice -c3 rsync --open-noatime -prune-empty-dirs --ignore-errors --prune-empty-dirs --itemize-changes --inplace --backup-dir=/var/backups/ --exclude-from=- "${@}"
		exit
		;;

		'rsync-insensitive-exclude')
			shift
			which rsync | "${MYREALDIR}"/apt-ensure.bash rsync
			# https://superuser.com/questions/256751/make-rsync-case-insensitive
			# https://forums.gentoo.org/viewtopic-t-1060394-start-0.html
			("${ME}" patterns && "${ME}" ipatterns) | sort -u | sed 's/\*/\*\*/g' | nice -19 ionice -c3 rsync --open-noatime --fuzzy --prune-empty-dirs --ignore-errors --prune-empty-dirs --itemize-changes --inplace --backup-dir=/var/backups/ --exclude-from=- "${@}"
		exit
		;;

		'zfsdatasets')
			isroot && (zfs list -t snapshot -o name -H | grep rpool/delete | "${MYREALDIR}"/xblissargs.bash -l zfs destroy &)
			isroot && ("${MYREALDIR}"/zdestroycongrusnaps.bash &)
			iterate_backup_datasets
		exit
		;;

		'snapshots-reduce')
			zrepl zfs-abstraction release-stale
			du_above_threshold
			zrepl zfs-abstraction release-all
			if which zfs; then 
				for ds in $(zfs list -H -o name); do
					du_above_threshold
					"${MYREALDIR}"/zc-fs.bash receive -A "${ds}"
				done
				for i in `seq 1 $(( $(zfs list -t snapshot -H | wc -l) / 4 ))`; do
					du_above_threshold
					"${ME}" zfsdatasets
				done
			fi
		exit
		;;

		'')
			"${ME}" ipatterns > "${RSYNC_EXCLUDE_PATTERNS_FILE}" &

			"${MYREALDIR}"/ensure-path.bash $(dirname "${RSYNCD_CONF_FILE}")

			echo -n "${RSYNCD_CONF_CONTENTS}" | sudo "${MYREALDIR}"/ensure-contents.bash "${RSYNCD_CONF_FILE}"
			sudo systemctl daemon-reload &

			true # go on
		;;

		'old-versions')
			"${MYREALDIR}"/list-old-versions.bash "${FindRoots[@]}" "${ExclFindOptions[@]}" # | "${ME}" -

		exit
		;;

		'clean')
			shift
			process_path "${*}"
		exit
		;;

		*)
			declare -a FindRoots=("${@}")
		;;

	esac

	if [ ${#FindRoots[@]} -eq 0 ]; then
		declare -a FindRoots=($("${MYREALDIR}"/dirls.bash / | filt))
	fi

	du_above_threshold

	isroot && ("${MYREALDIR}"/zdestroycongrusnaps.bash &)

	du_above_threshold

	iterate_backup_datasets

	du_above_threshold

	declare -A SearchOpts
	SearchOpts['efs*.tmp']='iname'
	SearchOpts['.wget-hsts']='iname'
	SearchOpts['apt.systemd.daily']='name'
	SearchOpts['*/ChatExport_*/*.html']='ipath'
	SearchOpts['*/ChatExport_*/images']='ipath'
	SearchOpts['*/ChatExport_*/images/*']='ipath'
	SearchOpts['*/ChatExport_*/css']='ipath'
	SearchOpts['*/ChatExport_*/css/*']='ipath'
	SearchOpts['*/ChatExport_*/js']='ipath'
	SearchOpts['*/ChatExport_*/js/*']='ipath'
	SearchOpts['*.DS_Store']='iname'
	SearchOpts['thumbs.db']='iname'
	SearchOpts['.thumbnails']='iname'
	SearchOpts["*RECYCLE.BIN"]='iname'
	SearchOpts['ViberDownloads']='iname'
	SearchOpts['RecentDocuments']='iname'
	SearchOpts['temp']='iname'
	SearchOpts['emoji']='iname'
	SearchOpts[".Trash*"]='iname'
	SearchOpts["trash"]='iname'
	SearchOpts["logfiles"]='iname'
	SearchOpts['Настраиваемые*шаблоны*Office']='iname'
	DelFromDicts
	iterate_backup_datasets
	bg_wait

	du_above_threshold
	(apt autoremove || true) | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	(apt autoclean || true) | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	which cleanmgr && cleanmgr /autoclean | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	for i in /var/log/mail.* ; do
		echo "" > $i
	done | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	which docker && docker container ls -a --filter status=exited --filter status=created && docker container prune --force | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	which docker && docker image prune -f -a --filter "until=48h" --force | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	du_above_threshold
	which docker && docker system prune --force | "${MYREALDIR}"/entee.bash -a "${LOGF}"
	"${MYREALDIR}"/apt-clean.bash &

	iterate_backup_datasets


	declare -A SearchOpts
	SearchOpts[".*dump"]='iname'
	SearchOpts["dead.letter"]='iname'
	SearchOpts["ViberDownloads"]='iname'
	SearchOpts["*/inetpub/temp"]='ipath'
	SearchOpts["*/ADtemp"]='ipath'
	SearchOpts["Downloaded*Program*Files/*"]='iname'
	SearchOpts["*/Local/Temp/*"]='ipath'
	SearchOpts["*/My*Skype*Received*Files/*"]='ipath'
	SearchOpts["*/.dropbox.cache/*"]='ipath'
	SearchOpts["*/files/temp/*"]='ipath'
	SearchOpts["fb_temp"]='iname'
	SearchOpts["*/log/*"]='ipath'
	SearchOpts[".RecycleBin*"]='iname'
	SearchOpts['.$recycle_bin$*']='iname'
	SearchOpts["thumbnail_cache"]='iname'
	SearchOpts["musiccache"]='iname'
	SearchOpts["albumthumbs"]='iname'
	SearchOpts['RECYCLE.BIN']='iname'
	SearchOpts['thumbnail.db*']='iname'
	SearchOpts["*/var/lib/jenkins/.m2/repository/*"]='ipath'
	SearchOpts['*/*.bfg-report*']='ipath'
	SearchOpts['*.bfg-report']='iname'
	SearchOpts['desktop.ini']='iname'
	SearchOpts['.directory']='iname'
	DelFromDicts
	iterate_backup_datasets

	which cleanmgr && cleanmgr /autoclean /lowdisk | "${MYREALDIR}"/entee.bash -a "${LOGF}"

	bg_wait

	find_and_list '/var/lib/jenkins/workspace' -mindepth 1 -type f | eventually_clean_files_list
	iterate_backup_datasets
	du_above_threshold

	which cleanmgr && cleanmgr /autoclean /verylowdisk | "${MYREALDIR}"/entee.bash -a "${LOGF}"

	declare -A SearchOpts
	SearchOpts['*RECYCLE*']='iname'
	SearchOpts['Thumbnails']='iname'
	SearchOpts['Temporary']='iname'
	SearchOpts["*/Dropbox/My*"]='ipath'
	SearchOpts["*/Dropbox/Мои*"]='ipath'
	SearchOpts['LogFiles']='name'
	SearchOpts['logs']='iname'
	SearchOpts['Crash Reports']='iname'
	SearchOpts['Crashpad']='iname'
	SearchOpts['crashes']='iname'
	SearchOpts['crash_count.txt']='iname'
	SearchOpts['Code Cache']='iname'
	SearchOpts["themes_backup"]='iname'
	SearchOpts["crash_count.txt"]='iname'
	SearchOpts["*.backup"]='iname'
	SearchOpts["*/.backup/*"]='ipath'
	SearchOpts["*/backup/*"]='ipath'
	SearchOpts["*/backups/*"]='ipath'
	SearchOpts['*.bak']='iname'
	SearchOpts['*.log']='iname'
	SearchOpts['*.tmp']='iname'
	SearchOpts['*.*~']='iname'
	SearchOpts['nohup.out']='iname'
	SearchOpts["*/audio/music*/Spotify*"]='ipath'
	SearchOpts["*/var/cache/apt/archives/*"]='ipath'
	SearchOpts["*/soft/linux/deb/*"]='ipath'
	SearchOpts['LOG']='name'
	SearchOpts['*.old']='iname'
	SearchOpts['*.out']='iname'
	SearchOpts["*/Dropbox/My*"]='ipath'
	SearchOpts["*nixnote*"]='ipath'
	SearchOpts["*/.local/Trash*"]='ipath'
	SearchOpts[".zfs"]='name'
	DelFromDicts
	iterate_backup_datasets
	bg_wait
	du_above_threshold

	declare -A SearchOpts
	SearchOpts["*/*backup*"]='ipath'
	SearchOpts["*backup*"]='ipath'
	SearchOpts["*/*temp/*"]='ipath'
	SearchOpts["*/*tmp/*"]='ipath'
	SearchOpts['*.cue']='iname'
	SearchOpts['*/var/log/*']='ipath'
	SearchOpts['*/files/Download/*']='ipath'
	SearchOpts['*/var/mail/*']='ipath'
	SearchOpts['.*dump']='iname'
	SearchOpts['*/.stversions*']='ipath'
	SearchOpts['*/.zfs']='name'
	SearchOpts["*cache"]='iname'
	SearchOpts['lost+found']='iname'
	SearchOpts['*changelog*']='iname'
	SearchOpts["*/.cache/*"]='ipath'
	SearchOpts["**thumbnail*"]='ipath'
	SearchOpts["*cache2"]='iname'
	SearchOpts["*.save"]='iname'
	SearchOpts["HuaweiBackup"]='iname'
	SearchOpts["IntsigLog"]='iname'
	SearchOpts["*/IntsigLog/*"]='ipath'
	SearchOpts[".bz2"]='name'
	SearchOpts["temp_data*"]='name'
	SearchOpts["*_cache.*"]='iname'
	SearchOpts["*.tmb.*"]='iname'
	DelFromDicts
	iterate_backup_datasets
	bg_wait
	du_above_threshold

	gitprocess / &
	bg_wait

	declare -A SearchOpts
	SearchOpts["Service.*Worker"]='iname'
	SearchOpts["*Cache*"]='iname'
	SearchOpts[".sudo_as_admin_successful"]='iname'
	SearchOpts["*.parts"]='iname'
	SearchOpts["*_thumb.*"]='iname'
	SearchOpts['*конфликтующая копия*']='iname'
	SearchOpts['*конфликт синхронизации*']='iname'
	SearchOpts['*conflicting copy*']='iname'
	SearchOpts['*sync conflict*']='iname'
	SearchOpts['*/*.sync-conflict-*']='ipath'
	SearchOpts['*.sync-conflict-*']='ipath'
	SearchOpts['*.torrent']='ipath'
	SearchOpts['* (\?)']='iname'
	SearchOpts['"']='iname'
	SearchOpts['*/var/lib/flatpak*']='ipath'
	SearchOpts['*/share/flatpack*']='ipath'
	SearchOpts['*/*backup*/*']='ipath'
	SearchOpts['&']='name'
	SearchOpts['&&']='name'
	DelFromDicts
	iterate_backup_datasets
	bg_wait
	du_above_threshold

	"${MYREALDIR}"/list-old-versions.bash "${FindRoots[@]}" "${ExclFindOptions[@]}" | "${ME}" -

	isroot && which zfs && while [[ $(zfs_get_backup_datasets | wc -l) != '0' ]] && du_above_threshold; do
		iterate_backup_datasets
	done | "${MYREALDIR}"/entee.bash -a "${LOGF}"

	du_above_threshold

	if which zfs; then for ds in $(zfs list -H -o name); do
		zrepl zfs-abstraction release-stale &
		du_above_threshold
		zrepl zfs-abstraction release-all &
		du_above_threshold
		zfs recv -A "${ds}" &
	done; fi

	bg_wait
	du_above_threshold

	tsp "${MYREALDIR}"/rmdir-samename-subdirs.bash "${FindRoots[@]}"
	"${MYREALDIR}"/find-broken-symlinks.sh "${FindRoots[@]}" | eventually_clean_files_list

	declare -A SearchOpts
	SearchOpts["*.lock"]='iname'
	SearchOpts["*.dpkg-old"]='iname'
	SearchOpts["*/cache/*"]='ipath'
	SearchOpts['*/archive/*']='ipath'
	SearchOpts['*/apps/*']='ipath'
	SearchOpts['*/soft/*']='ipath'
	SearchOpts['*/man/*']='ipath'
	SearchOpts['*/hard/*']='ipath'
	SearchOpts['*/Dropbox/*']='path'
	SearchOpts['*/var/lib/jenkins/workspace/*']='path'
	SearchOpts['*/var/cache/*']='path'
	DelFromDicts
	iterate_backup_datasets
	bg_wait
	du_above_threshold

	isroot && which zfs && zfs list -H -o name | grep "${ZFS_LAST_SEARCH}"
	isroot && which zfs && while du_above_threshold; do
		du_above_threshold
		for dataset2destroy in $(zfs list -H -o name | grep "${ZFS_LAST_SEARCH}" | xargs --no-run-if-empty -l --max-procs=${JOBS} -d "\n" zfs list -H -s creation -o name); do
			[[ "${dataset2destroy}" == '' ]] && break
			zfs_destroy "${dataset2destroy}"
			break
		done
		du_above_threshold
		for dataset2destroy in $(zfs list -H -o name | grep "${ZFS_LAST_SEARCH}" | xargs --no-run-if-empty -l --max-procs=${JOBS}  -d "\n" zfs list -H -s used -o name | sort -r); do
			[[ "${dataset2destroy}" == '' ]] && break
			zfs_destroy "${dataset2destroy}"
			break
		done
		du_above_threshold
		for dataset2destroy in $(zfs list -H -o name | grep "${ZFS_LAST_SEARCH}" | xargs --no-run-if-empty -l --max-procs=${JOBS}  -d "\n" zfs list -H -s used -o name); do
			[[ "${dataset2destroy}" == '' ]] && break
			zfs_destroy "${dataset2destroy}"
			break
		done
	done

	if which zfs; then
		"${MYREALDIR}"/ztrim.bash -w
	fi

mypopd

rm -rv "${TMPDIR_MINE}" &

rm -rv "${TMPDIR_COMMON}" &

(updatedb || true) & 

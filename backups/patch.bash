#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

which curl || ${MYDIR}/../../exe/apt.bash install -y curl
which patch || ${MYDIR}/../../exe/apt.bash install -y patch

patch /etc/grub.d/10_linux_zfs grub-common-2.04-1ubuntu12.1.dont_scan_snapshots-brianfinley.diff
[ -e /etc/grub.d/10_linux_zfs.orig ] && mv /etc/grub.d/10_linux_zfs.orig /var/backups/

curl https://launchpadlibrarian.net/478315221/2150-fix-systemd-dependency-loops.patch | \
    sed "s|/etc|/lib|;s|\.in$||" | (cd / ; sudo patch -p1)

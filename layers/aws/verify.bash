#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

pushd ${MYDIR}

	aws --version

	session-manager-plugin
	
	session-manager-plugin --version

popd

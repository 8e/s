#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

pushd ${MYDIR}

	../../exe/apt-urls.bash fromurl.list

	../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

	../../exe/apt-lists.bash install custom.list

	../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

popd

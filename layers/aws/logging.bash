#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

pushd ${MYDIR}

	#https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html#plugin-github

	cp /usr/local/sessionmanagerplugin/seelog.xml.template -v /usr/local/sessionmanagerplugin/seelog.xml

popd

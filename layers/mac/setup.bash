# Hide Dock
defaults write com.apple.dock autohide -bool true && killall Dock
defaults write com.apple.dock autohide-delay -float 1000 && killall Dock
defaults write com.apple.dock no-bouncing -bool TRUE && killall Dock

# Restore Dock
#defaults write com.apple.dock autohide -bool false && killall Dock
#defaults delete com.apple.dock autohide-delay && killall Dock
#defaults write com.apple.dock no-bouncing -bool FALSE && killall Dock

defaults write com.apple.dock autohide-delay -float 1000; killall Dock
defaults write http://com.apple.Finder AppleShowAllFiles true
defaults write com.apple.finder AppleShowAllFiles -boolean true; killall Finder;

# Use list view in all Finder windows by default
# Four-letter codes for the other view modes: `icnv`, `clmv`, `glyv`
defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"

sudo find / -name ".DS_Store" -exec rm {} \;


#!/usr/bin/env bash

set -xe


readonly MYDIR=$(dirname "${0}")
readonly INSTALLDIR=/opt/site24x7
readonly INSTALLERN="site24x7_installer.bash"

"${MYDIR}"/../../exe/ensure-path.bash "${INSTALLDIR}"

pushd "${INSTALLDIR}"
	wget -c "https://staticdownloads.site24x7.com/server/Site24x7InstallScript.sh" -O "${INSTALLERN}"
	chmod +x "${INSTALLERN}"
	bash -xc "./${INSTALLERN} -i -key=us_72d8a97556a7eb18fb692ab1a686c94b"
popd


/etc/init.d/site24x7monagent restart

/etc/init.d/site24x7monagent status

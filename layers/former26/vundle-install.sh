#!/usr/bin/env bash

set -xe

readonly MYDIR="$(dirname ${0})"
source "${MYREALDIR}"/../defaults.rc.bash

vim +PluginInstall +qall

sudo -u "${DEFUSER}" vim +PluginInstall +qall


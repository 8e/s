#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

pushd ${MYDIR}

	../../exe/apt-urls.bash	fromurl.list

	../../exe/dpkg-loosen.bash cryptsetup `cat video.list virtual.list`

	../../exe/apt-lists.bash -y full-upgrade custom.list basic.list minimal.list withgui.list video.list virtual.list

	../../exe/dpkg-loosen.bash cryptsetup `cat video.list virtual.list`

popd

for plugin in `vagrant plugin list | cut -f1 -d' '`; do vagrant plugin install ${plugin} || vagrant plugin update ${plugin}; done

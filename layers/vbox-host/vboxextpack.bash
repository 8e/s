#!/usr/bin/env bash

set -xe

readonly VERSION=$(curl http://download.virtualbox.org/virtualbox/LATEST.TXT)
readonly URLEXT=http://download.virtualbox.org/virtualbox/${VERSION}/Oracle_VM_VirtualBox_Extension_Pack-${VERSION}.vbox-extpack
#readonly URLISO=http://download.virtualbox.org/virtualbox/${VERSION}/VBoxGuestAdditions_${VERSION}.iso
readonly FNAME=Oracle_VM_VirtualBox_Extension_Pack-${VERSION}.vbox-extpack

pushd /opt
	VBoxManage extpack uninstall "Oracle VM VirtualBox Extension Pack" &
	wget -c "${URLEXT}" -O "${FNAME}"
	VBoxManage extpack install "${FNAME}"
popd

#version=$(vboxmanage -v)
#echo $version
#var1=$(echo $version | cut -d 'r' -f 1)
#echo $var1
#var2=$(echo $version | cut -d 'r' -f 2)
#echo $var2
#file="Oracle_VM_VirtualBox_Extension_Pack-$var1-$var2.vbox-extpack"
#echo $file
#wget http://download.virtualbox.org/virtualbox/$var1/$file -O /tmp/$file
#http://download.virtualbox.org/virtualbox/6.1.16/


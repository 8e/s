#!/usr/bin/env bash
set -e
if [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}"
	source ../defaults.rc.bash

	systemctl restart openvpnas.service

	systemctl status openvpnas.service

popd

#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

pushd ${MYDIR}

	../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

	../../exe/apt.bash install -y $(sort -u custom.list basic.list)

	../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

popd

if [ ! -e /var/lib/zerotier-one/zerotier-one.pid ] || ! ps -p $(cat /var/lib/zerotier-one/zerotier-one.pid); then
	systemctl restart zerotier-one
fi
zerotier-cli info

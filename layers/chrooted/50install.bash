#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

${MYDIR}/../../exe/apt-urls.bash ${MYDIR}/fromurl.list

pushd ${MYDIR}/../../exe

	./dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

popd

pushd ${MYDIR}

	../../exe/apt-lists.bash -y install custom.list basic.list withgui.list pulseaudio.list jack.list video.list guiextra.list minimal.list extra.list input.list apt.list w.list tops.list music.list reaper.list

popd

pushd ${MYDIR}/../../exe

	./dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

popd

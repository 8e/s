#!/usr/bin/env bash

set -xe

readonly MYREALDIR=$(dirname $(realpath ${0}))

pushd "${MYREALDIR}"/../../exe

	../../s/exe/pwd2path.bash

popd

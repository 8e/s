#!/usr/bin/env bash

set -xe

systemctl daemon-reload
systemctl enable zautosnap.bash.service
systemctl enable clamav-daemon.service
systemctl enable clamav-freshclam.service
systemctl enable proxy.service
systemctl enable cudo-miner.service
systemctl disable syncthing

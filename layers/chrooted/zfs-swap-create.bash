#!/usr/bin/env bash

set -e

readonly DEBUG=${DEBUG:-true}
${DEBUG} && set -x
readonly ME="$(realpath ${0})"
readonly MYREALDIR="$(dirname ${ME})"
readonly SWAPSIZE="$(( $(${MYREALDIR}/ramsizekb.bash) * 3 / 2 ))K"
readonly PAGESZ=$(getconf PAGESIZE)
readonly LBL=swap
if [[ $POOLNAME == '' ]]; then
	        read -e -p "Enter pool name: " POOLNAME
fi
readonly DATASETVOL="$POOLNAME/volumes/swap"
readonly PARENT_DS=$(dirname "${DATASETVOL}")
readonly BLOCKDEV="/dev/zvol/${DATASETVOL}"
readonly COMPRESS='off'
readonly FSTAB=$(sudo "${MYREALDIR}"/ensure-path.bash /etc/fstab.d)/swaps

# beware https://bugs.launchpad.net/ubuntu/+source/zfs-linux/+bug/1847628
# 	 https://github.com/openzfs/zfs/issues/7734

#Peter Passchier (peter-passchier) wrote on 2020-11-16:
#15Is there any hope that a swap on zvol with sync=standard at least doesn't hang/deadlock?
# Like:zfs create rpool/swap -V 2G -b $(getconf PAGESIZE) -o logbias=throughput -o sync=always -o primarycache=metadata -o secondarycache=none
# In my testing this didn't hang when swap got hit.

zfs create -pV "${SWAPSIZE}" -b ${PAGESZ} -o checksum=fletcher4 -o dedup=off -o snapshot_limit=0 -o logbias=throughput -o sync=always -o compression="${COMPRESS}" -o primarycache=metadata -o secondarycache=none -o com.sun:auto-snapshot=false "${@}" "${DATASETVOL}"

zfs set canmount=off "${PARENT_DS}"

mkswap -p ${PAGESZ} -L "${LBL}" "${BLOCKDEV}"

swapon -v -d "${BLOCKDEV}"

swapon -v -s

swapoff "${BLOCKDEV}"

#echo RESUME=none > /etc/initramfs-tools/conf.d/resume
if [ -s /etc/initramfs-tools/conf.d/resume ]; then
	[[ $(cat /etc/initramfs-tools/conf.d/resume) != "${BLOCKDEV}" ]] && mv /etc/initramfs-tools/conf.d/resume /var/backups/ && echo "${BLOCKDEV}" > /etc/initramfs-tools/conf.d/resume
elif [ -e /etc/initramfs-tools/conf.d ]; then
	echo "${BLOCKDEV}" > /etc/initramfs-tools/conf.d/resume
fi


grep "${LBL}" "${FSTAB}" | grep -v '^#' && sed -i.bak "/^.*LABEL=${LBL}.*$/d" "${FSTAB}"
#<file system> <mount point> <type> <options> <dump> <pass>
#echo -e "LABEL=${LBL}\tnone\tswap\tsw,pri=32767,discard\t0\t0" | tee -a /etc/fstab
echo -e ""${BLOCKDEV}"\tnone\tswap\tsw,swap,pri=32767,discard\t0\t0" | tee -a "${FSTAB}"

mount -av --fstab "${FSTAB}"
swapon -av -d

if [ -e /usr/share/initramfs-tools/scripts/local-premount/resume ]; then
	mv /usr/share/initramfs-tools/scripts/local-premount/resume /usr/share/initramfs-tools/scripts/local-bottom
fi

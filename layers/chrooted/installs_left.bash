#!/usr/bin/env bash
set -e
if $DEBUG && [ ${DEBUG} ]; then
        set -x
fi
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
pushd "${MYREALDIR}"/../../lib >/dev/null
        source ../defaults.rc.bash

	readonly APTLISTS=(basic.list apt.list guiextra.list intel.list jack.list minimal.list pulseaudio.list video.list withgui.list ubuntu-studio.list input.list nvidia.list extra.list custom.list w.list tops.list grml.list)
	readonly SORTUP=$(cat ${APTLISTS[*]} | sort -u)
	readonly AMNT=$(echo ${SORTUP} | wc -w)
	declare -a News=()
	readonly LOGF=/var/log/$(basename ${0}).log

	../exe/apt-ensure.bash ${SORTUP}
	i=1
	for p in ${SORTUP}; do
		echo "${i}/${AMNT} ($(( i * 100 / ${AMNT} ))%): ${p}"
		../exe/apt-is-installed.bash ${p} || {
			News+=(${p})
			echo ${p} >> "${LOGF}"
			set +e
			aptitude -o DPkg::Lock::Timeout=-1 install -y ${p}
			apt -o DPkg::Lock::Timeout=-1 install -y ${p}
			set -e
		}
		(( i++ ))
	done
	aptitude -o DPkg::Lock::Timeout=-1 install ${SORTUP}
	apt -o DPkg::Lock::Timeout=-1 install $(echo ${SORTUP} | grep -v ssh-server |grep -v vdpau-driver |grep -v nvidia-persistenced)

popd >/dev/null

echo ${News[*]}

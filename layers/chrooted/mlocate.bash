#!/usr/bin/env bash

set -e

if [ -e /etc/cron.daily/mlocate ]; then
	mv /etc/cron.daily/mlocate /etc/cron.weekly/
fi

#!/usr/bin/env bash
set -e
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")

#sudo adduser xrdp ssl-cert

sudo systemctl enable --now xrdp

#!/usr/bin/env bash

set -xe

[ -d /opt/storj ] || mkdir /opt/storj

pushd /opt/storj

	# https://documentation.storj.io/dependencies/identity
	wget https://github.com/storj/storj/releases/latest/download/identity_linux_$(dpkg --print-architecture).zip -O identity_linux_$(dpkg --print-architecture).zip
	unzip -o identity_linux_$(dpkg --print-architecture).zip
	chmod +x identity
	rm identity_linux_$(dpkg --print-architecture).zip &
	./identity create storagenode &>./identity.log &

popd



#!/usr/bin/env bash
readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash
readonly DAEMON_PATH=/etc/systemd/system/syncthing.service

pushd ${MYREALDIR}

	../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

	../../exe/apt-ensure.bash $(sort -u custom.list)

	../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

popd
[Unit]
Description=Monero Full Node
After=network.target

[Service]
User="${DEFUSER}"
Group=satoshi

Type=forking
PIDFile=/home/satoshi/.bitmonero/monerod.pid

ExecStart=/usr/local/bin/monerod --restricted-rpc --block-sync-size 3 --confirm-external-bind --config-file /home/satoshi/.bitmonero/monerod.conf --detach




echo -n "[Unit]
Requires=network.target
Requires=network-online.target
After=network.target
After=network-online.target
ConditionACPower=true

[Service]
Type=idle
ExecStart=/usr/bin/syncthing -no-browser -logfile=/tmp/log/v/syncthing.log -logflags=3
Restart=on-failure
RestartSec=5
SuccessExitStatus=3 4
RestartForceExitStatus=3 4
User="${DEFUSER}"
Group="${DEFUSER}"
Nice=17
IOSchedulingClass=idle
IOSchedulingPriority=5
# https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html
CPUQuota=20%
# A higher weight means more CPU time, a lower weight means less. The allowed range is 1 to 10000
CPUWeight=3
StartupCPUWeight=3

# Specify the throttling limit on memory usage of the executed processes in this unit. Memory usage may go above the limit if unavoidable, but the processes are heavily slowed down and memory is taken away aggressively in such cases. This is the main mechanism to control memory usage of a unit.
# Takes a memory size in bytes. If the value is suffixed with K, M, G or T, the specified memory size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively. Alternatively, a percentage value may be specified, which is taken relative to the installed physical memory on the system. This setting is supported only if the unified control group hierarchy is used and disables MemoryLimit=.
MemoryHigh=10%

# Specify the absolute limit on swap usage of the executed processes in this unit.
# Takes a swap size in bytes. If the value is suffixed with K, M, G or T, the specified swap size is parsed as Kilobytes, Megabytes, Gigabytes, or Terabytes (with the base 1024), respectively.
MemorySwapMax=500M

Environment=GOMAXPROCS=2
# Hardening                                                                      
SystemCallArchitectures=native                                                   
MemoryDenyWriteExecute=true                                                      
NoNewPrivileges=true
ProtectSystem=full
PrivateTmp=true
PrivateDevices=true
#PrivateNetwork=true
ProtectSystem=true
SystemCallFilter=~@resources:EPERM

[Install]
WantedBy=multi-user.target
WantedBy=default.target
WantedBy=graphical.target" | "${MYREALDIR}"/ensure-contents.bash "${DAEMON_PATH}"

systemctl enable syncthing
systemctl restart syncthing
systemctl status syncthing

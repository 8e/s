#!/usr/bin/env bash

# https://www.one-tab.com/page/hq_n5oC4QKy0vEaw5U1mPQ

set -xe

readonly MYDIR=$(dirname ${0})

pushd ${MYDIR}

	../../exe/apt-is-installed.bash apcupsd 2>dev/null || ../../exe/apt.bash -y install apcupsd

	cp -r --backup apcupsd/* /

	systemctl restart apcupsd

	apcaccess status &

popd

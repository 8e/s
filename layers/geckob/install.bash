#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

pushd ${MYDIR}
	
	../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

	../../exe/apt-lists.bash -y install custom.list

popd

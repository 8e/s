#!/usr/bin/env bash

readonly MYDIR=$(dirname $(realpath ${0}))

pushd /opt
	wget -c https://download.cudo.org/tenants/135790374f46b0107c516a5f5e13069b/5e5f800fdf87209fdf8f9b61441e53a1/linux/x64/stable/install.sh -O cudominer_install.sh
	chmod +x cudominer_install.sh
	"${MYDIR}"/../../exe/dpkg-loosen.bash cudo-miner-service cudo-miner-headless
	./cudominer_install.sh --install-mode=headless --install-cli --update-channel=stable --quiet
	"${MYDIR}"/../../exe/dpkg-loosen.bash cudo-miner-service cudo-miner-headless
popd

"${MYDIR}"/../../exe/apt.bash -y install cudo-miner-cli cudo-miner-core cudo-miner-headless cudo-miner-desktop cudo-miner-service
"${MYDIR}"/../../exe/dpkg-loosen.bash cudo-miner-service cudo-miner-desktop cudo-miner-headless

cudominercli enable
cudominercli login fccc
cudominercli info

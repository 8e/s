#!/usr/bin/env bash

readonly MYDIR=$(dirname $(realpath ${0}))
readonly MODE=headless # headless or desktop

pushd /opt
	wget -c https://download.cudo.org/tenants/135790374f46b0107c516a5f5e13069b/5e5f800fdf87209fdf8f9b61441e53a1/linux/x64/stable/install.sh -O cudominer_install.sh
	chmod +x cudominer_install.sh
	"${MYDIR}"/../../exe/dpkg-loosen.bash cudo-miner-service cudo-miner-${MODE}
	./cudominer_install.sh --install-mode=${MODE} --install-cli --update-channel=stable --quiet
	"${MYDIR}"/../../exe/dpkg-loosen.bash cudo-miner-service cudo-miner-${MODE}
popd

"${MYDIR}"/../../exe/apt.bash -y install cudo-miner cudo-miner-cli cudo-miner-core cudo-miner-${MODE} cudo-miner-service
"${MYDIR}"/../../exe/dpkg-loosen.bash cudo-miner-service cudo-miner-${MODE}

cudominercli enable
cudominercli login fccc
cudominercli info

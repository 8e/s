#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname ${0})

pushd ${MYDIR}

	../../exe/apt-lists.bash full-upgrade basic.list withgui.list minimal.list custom.list

popd

#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))

pushd ${MYDIR}

	../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

	../../exe/apt.bash install -y $(sort -u custom.list basic.list)

	../../exe/dpkg-loosen.bash plymouth-theme-ubuntustudio firmware-sof-signed linux-firmware

popd

zrepl version

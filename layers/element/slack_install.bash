#!/usr/bin/env bash

set -ex

readonly MYREALDIR=$(dirname $(realpath ${0}))

"${MYREALDIR}"/apt-ensure.bash slack-desktop

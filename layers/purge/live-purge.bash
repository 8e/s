#!/usr/bin/env bash

[ ${IGNORE} ] || set -e

readonly ME=$(realpath $(which "${0}"))
readonly MYREALDIR=$(dirname "${ME}")
source "${MYREALDIR}"/../defaults.rc.bash

pushd "${MYREALDIR}"
	./apt-ensure-cmd.bash aptitude aptitude
	killall fluidsynth &
	find /etc/apt -iname '*~' -delete &
	aptitude -o DPkg::Lock::Timeout=-1 purge -y fluid~s carla~s gimp~s anacron krita office~s synth~s guitar~s ardour csound-plugins gstreamer1.0-plugins-bad kdenlive lmms calamares~s ubuquity~s logrotate~s syslog~s ubiquity~s music~s audacity vlc player~s obs-studio thunderbird hydrogen-drumkits openclipart-svg inkscape blender digikam~s snapd hydrogen~s clipart~s icons~s
	./apt-clean.bash
	find /etc/apt -iname '*~' -delete &
	killall -KILL fluidsynth &
	wait || true
popd

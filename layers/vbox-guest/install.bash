#!/usr/bin/env bash

set -xe

readonly MYDIR=$(dirname $(realpath ${0}))
readonly BE=${MYDIR}"/../../exe/apt.bash

"${BE}" install -y apt-transport-https
"${BE}" update
"${BE}" install -y $(cat vbox-guest.list)
